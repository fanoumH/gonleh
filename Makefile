.PHONY: help migrate empty-database reset-database app-setup-default-data install refresh dependencies vendor fixture fixture-user-admin
.DEFAULT_GOAL= help
PHP = php
ENV?=dev
# show help message
help:
	@echo "Liste des cibles disponibles :"
	@awk '/^##/{c=$$0; getline; printf "%-30s %s\n", c, $$0 }' $(MAKEFILE_LIST) | sort -d
##execute migration
migrate:
	$(PHP) bin/console doctrine:schema:update --force --env=$(ENV)
##empty database
empty-database:
	$(PHP) bin/console doctrine:database:drop --force --if-exists --env=$(ENV)
	$(PHP) bin/console doctrine:database:create --env=$(ENV)
##reset database
reset-database: empty-database migrate
##add default data for app
app-setup-default-data:
	$(PHP) bin/console app:setup-default-data 'App\Entity\StaffPost' '/DataFixtures/Fixtures/StaffPost.yml' --env=$(ENV)
##fixture-user-admin
fixture-user-admin:
	$(PHP) bin/console app:add-user admin@transport-leong.com 123456
##fixture
fixture:
	$(PHP) bin/console hautelook:fixtures:load --no-interaction --env=$(ENV)
##install new empty app
install: dependencies reset-database app-setup-default-data
##install with fake data
install-with-fixture: dependencies reset-database fixture fixture-user-admin app-setup-default-data
##dependencies
dependencies: vendor
##dependencies vendor
vendor:
	composer install
##dependencies yarn
node_modules:
	yarn install
##refresh app
refresh: dependencies migrate app-setup-default-data