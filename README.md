##[Symfony](https://symfony.com/doc/current/index.html)

## Prérequis

- [Composer](https://getcomposer.org/)
- [Makefile](https://gnuwin32.sourceforge.net/packages/make.htm)
- [Yarn](https://yarnpkg.com/)

## Installation

1. Cloner le dépôt
2. Accéder au dossier du projet
3. Copier le fichier `.env` en `.env.local` et configurer les variables d'environnement en conséquence
4. Installer l'application `make install`
5. Ajouter un admin dans votre base `php bin/console app:add-user email@example.com password --admin`
6. Lancer le serveur de développement avec `symfony server:start` ou `php  -S localhost:8000 -t public`
7. *Optionnal* Lancer messenger , pour les actions async `php bin/console messenger:consume async -vv`

## Commandes disponibles

Les commandes suivantes sont disponibles :

- `make migrate` : exécute les migrations
- `make empty-database` : vide la base de données
- `make reset-database` : vide et réinitialise la base de données (migration)
- `make app-setup-default-data` : ajoute les données de base pour l'application
- `make install` : installe l'application
- `make dependencies` : installe les dépendances
- `make refresh` : met à jour les dépendances, exécute les migrations et ajoute les données de base pour l'application
- `make test` : exécute les tests dans une base indépendante si env.test est configuré

Pour plus de détails sur les commandes, exécuter `make help`.

## Configuration

Le fichier `.env` contient les variables d'environnement nécessaires à l'exécution du projet.

## License

Ce projet est sous licence MIT.