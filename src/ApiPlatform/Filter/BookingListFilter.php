<?php

namespace App\ApiPlatform\Filter;

use ApiPlatform\Doctrine\Orm\Extension\PaginationExtension;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;
use ApiPlatform\State\Pagination\Pagination;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class BookingListFilter  extends AbstractFilter
{
    public function __construct(
        ManagerRegistry $managerRegistry, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null,
        private PaginationExtension $paginationExtension, private ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, private RequestStack $requestStack,
        private Pagination $pagination,
    )
    {
        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);
    }

    /**
     * @throws ResourceClassNotFoundException
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if($this->requestStack->getCurrentRequest()->query->get("search")) {
            $shippingCompanyAlias = 'shippingCompany_alias'.uniqid();
            $containerAlias = 'containers_alias'.uniqid();
            $customerAlias = 'customer_alias'.uniqid();
            $customerIdentificationAlias = 'customerIdentification_alias'.uniqid();
            $existingAlias = $this->getExistingAlias($queryBuilder, 'o');
            $existingContainerAlias = $this->getExistingAlias($queryBuilder, $shippingCompanyAlias);

            $queryBuilder->leftJoin('o.shippingCompany' ,$shippingCompanyAlias)
                ->leftJoin('o.containers' ,$containerAlias)
                ->leftJoin('o.client' ,$customerAlias)
                ->leftJoin($customerAlias.'.customerIdentification' ,$customerIdentificationAlias)
            ;

            $request = $this->requestStack->getCurrentRequest();
            $pageParameterName = "page";
            $itemsPerPageParameterName = "itemsPerPage";
            $page = $request->query->getInt($pageParameterName, 1);
            $itemsPerPage = $request->query->getInt($itemsPerPageParameterName) === 0 ? 30 : $request->query->getInt($itemsPerPageParameterName);
            // Apply the filter condition based on the value
            $parameterName = $queryNameGenerator->generateParameterName($property);
            $queryBuilder
                ->where($queryBuilder->expr()->orX(
                    $queryBuilder->expr()->like('o.refBooking', ':value'),
                    $queryBuilder->expr()->like($shippingCompanyAlias.'.name', ':value'),
                    $queryBuilder->expr()->like($containerAlias.'.containerNumber', ':value'),
                    $queryBuilder->expr()->like($containerAlias.'.containerStatus', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.customerTitle', ':value'),
                ))->setParameter('value', '%'.$value.'%')
            ;
            // Set the pagination parameters
            $firstResult = ($page - 1) * $itemsPerPage;
            $queryBuilder->setFirstResult($firstResult)
                ->setMaxResults($itemsPerPage);
        }
    }

    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }
        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["search"] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_STRING,
                'required' => false,
                'description' => 'Filtrer tous les utilisateurs à partir de ces attributes (soit nom, prénoms, ou email , etc .)',
                'openapi' => [
                    'example' => 'NIAVO RANDRIANARISON',
                    'allowReserved' => false,// if true, query parameters will be not percent-encoded
                    'allowEmptyValue' => true,
                    'explode' => false, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
                ],
            ];
        }
        return $description;
    }

    // Méthode pour vérifier si un alias est déjà défini dans la requête
    private function getExistingAlias(QueryBuilder $queryBuilder, string $alias): ?string
    {
        $existingAliases = $queryBuilder->getAllAliases();
        return in_array($alias, $existingAliases) ? $alias : null;
    }
}