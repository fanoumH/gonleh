<?php

namespace App\ApiPlatform\Filter;

use ApiPlatform\Doctrine\Orm\Extension\PaginationExtension;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Exception\ResourceClassNotFoundException;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;
use ApiPlatform\State\Pagination\Pagination;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class SearchCustomerFilter  extends AbstractFilter
{
    public function __construct(
        ManagerRegistry $managerRegistry, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null,
        private PaginationExtension $paginationExtension, private ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, private RequestStack $requestStack,
        private Pagination $pagination,
    )
    {
        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);
    }

    /**
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if($this->requestStack->getCurrentRequest()->query->get("search")){
            $customerIdentificationAlias = 'customer_identification_alias_'.uniqid();
            $customerBankAlias = 'customer_bank_alias_'.uniqid();
            $customerFreeFieldAlias = 'customer_freefield_alias_'.uniqid();
            $solvencyAlias = 'solvency_alias_'.uniqid();


            $existingAliasCustomer = $this->getExistingAlias($queryBuilder, 'o');
            $existingAliasCustomerIdentification = $this->getExistingAlias($queryBuilder, $customerIdentificationAlias);
            $existingAliasCustomerBank = $this->getExistingAlias($queryBuilder, $customerBankAlias);
            $existingAliasFreeField = $this->getExistingAlias($queryBuilder, $customerFreeFieldAlias);
            $queryBuilder->leftJoin('o.customerIdentification' ,$customerIdentificationAlias)
                ->leftJoin('o.customerBank' ,$customerBankAlias)
                ->leftJoin('o.customerFreeField' ,$customerFreeFieldAlias)
                ->leftJoin('o.solvency' ,$solvencyAlias)

                /*->leftJoin($customerIdentificationAlias.'.prixVenteType' ,$solvencyAlias)
                ->leftJoin($customerIdentificationAlias.'.suiviStock' ,$suiviStockTypeAlias)
                ->leftJoin($customerIdentificationAlias.'.conditioning' ,$conditioningTypeAlias)
                ->leftJoin($customerIdentificationAlias.'.uniteVente' ,$uniteVenteTypeAlias)
                ->leftJoin($customerBankAlias.'.paysOrigine' ,$paysOrigineTypeAlias)
                ->leftJoin($customerBankAlias.'.catalog' ,$catalogTypeAlias)
                ->leftJoin($customerBankAlias.'.family' ,$familyTypeAlias)
                ->leftJoin($customerBankAlias.'.subFamily' ,$subFamilyTypeAlias)*/;

            $request = $this->requestStack->getCurrentRequest();
            $pageParameterName = "page";
            $itemsPerPageParameterName = "itemsPerPage";
            $page = $request->query->getInt($pageParameterName, 1);
            $itemsPerPage = $request->query->getInt($itemsPerPageParameterName) === 0 ? 30 : $request->query->getInt($itemsPerPageParameterName);
            // Apply the filter condition based on the value
            $alias = $queryBuilder->getRootAliases()[0];
            $parameterName = $queryNameGenerator->generateParameterName($property);
            $queryBuilder
                ->where($queryBuilder->expr()->orX(
                    $queryBuilder->expr()->like($existingAliasCustomer.'.refCustomer', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.thirdParties', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.customerGroupName', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.district', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.rcs', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.cif', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.stat', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.customerTitle', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.abbreviated', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.comments', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.address', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.complement', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.city', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.zipCode', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.nif', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.phone', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.linkedin', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.email', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.siteInternet', ':value'),
                    $queryBuilder->expr()->like($customerIdentificationAlias.'.siteInternet', ':value'),
                    $queryBuilder->expr()->like($customerBankAlias.'.bank', ':value'),
                    $queryBuilder->expr()->like($customerBankAlias.'.accountNumber', ':value'),
                    $queryBuilder->expr()->like($customerBankAlias.'.keyAccount', ':value'),
                    $queryBuilder->expr()->like($customerBankAlias.'.ticketOffice', ':value'),
                    $queryBuilder->expr()->like($customerFreeFieldAlias.'.legalSatus', ':value'),
                    $queryBuilder->expr()->like($customerFreeFieldAlias.'.shareFolderPale', ':value'),
                    $queryBuilder->expr()->like($customerFreeFieldAlias.'.blocNote', ':value'),
                    $queryBuilder->expr()->like($customerFreeFieldAlias.'.title', ':value'),
                ))->setParameter('value', '%'.$value.'%')
            ;
//            dd($queryBuilder->getQuery());
            // Set the pagination parameters
            $firstResult = ($page - 1 ) * $itemsPerPage;
            $queryBuilder->setFirstResult($firstResult)
                ->setMaxResults($itemsPerPage);
        }
    }

    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }
        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["search"] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_STRING,
                'required' => false,
                'description' => 'Filter using a regex. This will appear in the OpenApi documentation!',
                'openapi' => [
                    'example' => 'CLIENT_TO_SEARCH',
                    'allowReserved' => false,// if true, query parameters will be not percent-encoded
                    'allowEmptyValue' => true,
                    'explode' => false, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
                ],
            ];
        }
        return $description;
    }

    // Méthode pour vérifier si un alias est déjà défini dans la requête
    private function getExistingAlias(QueryBuilder $queryBuilder, string $alias): ?string
    {
        $existingAliases = $queryBuilder->getAllAliases();
        return in_array($alias, $existingAliases) ? $alias : null;
    }
}