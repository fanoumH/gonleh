<?php

namespace App\ApiPlatform\Filter;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Metadata\Operation;
use Symfony\Component\PropertyInfo\Type;
use Doctrine\Persistence\ManagerRegistry;
use ApiPlatform\State\Pagination\Pagination;
use Symfony\Component\HttpFoundation\RequestStack;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Extension\PaginationExtension;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;

class StaffFilter extends AbstractFilter
{
    public function __construct(
        ManagerRegistry $managerRegistry, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null,
        private PaginationExtension $paginationExtension, private ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, private RequestStack $requestStack,
        private Pagination $pagination,
    )
    {
        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);
    }
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if(!$this->isPropertyEnabled($property, $resourceClass)){
            return;
        }
        
        if($property !== "search"){
            return;
        }

        $searchParam = $queryNameGenerator->generateParameterName($property);

        $rootAliases = $queryBuilder->getRootAliases();
        $staffEntityAlias = $rootAliases[0];

        $queryBuilder->leftJoin($staffEntityAlias . ".staffPost", "post");

        $queryBuilder
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->like($staffEntityAlias . ".registrationNumber", ':value'),
                    $queryBuilder->expr()->like($staffEntityAlias . ".lastName", ':value'),
                    $queryBuilder->expr()->like($staffEntityAlias . ".firstName", ':value'),
                    $queryBuilder->expr()->like($staffEntityAlias . ".phoneNumber", ':value'),
                    $queryBuilder->expr()->like($staffEntityAlias . ".address", ':value'),
                    $queryBuilder->expr()->like("post.name", ':value'),
                )
            )
            ->setParameter('value', '%' . $value . '%')
            ;
        
            // Set the pagination parameters
            
            $request = $this->requestStack->getCurrentRequest();
            $pageParameterName = "page";
            $itemsPerPageParameterName = "itemsPerPage";
            $page = $request->query->getInt($pageParameterName, 1);
            $itemsPerPage = $request->query->getInt($itemsPerPageParameterName) === 0 ? 30 : $request->query->getInt($itemsPerPageParameterName);

            $firstResult = ($page - 1) * $itemsPerPage;
            $queryBuilder->setFirstResult($firstResult)
                ->setMaxResults($itemsPerPage);
        
    }

    public function getDescription(string $resourceClass): array
    {
        if(!$this->properties)
        {
            return [] ;
        }
        $description = [];

        foreach ($this->properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_STRING,
                'required' => false,
                'description' => 'Rechercher un personnel',
                'openapi' => [
                    'example' => 'jean dupont',
                    'allowReserved' => false,// if true, query parameters will be not percent-encoded
                    'allowEmptyValue' => false,
                    'explode' => false, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
                ]
            ];
        }

        return $description;
    }
}