<?php

namespace App\ApiPlatform\Filter;

use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Metadata\Operation;
use Symfony\Component\PropertyInfo\Type;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;

class BookingFilter extends AbstractFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        $property = $property . '[containers]' ;

        if(!$this->isPropertyEnabled($property, $resourceClass)){
            return;
        }
        if('order[containers]' === $property){

            // $name = $queryNameGenerator->generateParameterName($property);

            $value = $value['containers'] === 'desc' ? 'desc' : 'asc' ;
            
            $rootAliases = $queryBuilder->getRootAliases();
            $entityAlias = $rootAliases[0];
            
            $queryBuilder->leftJoin($entityAlias . '.containers', 'containers' );
            $queryBuilder->orderBy('containers.dateToGetOut', $value);
        }

    }

    public function getDescription($resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }
        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["order[containers]"] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_ARRAY,
                'required' => false,
                'description' => 'Filtrer toutes les reservations selon ordre chrono de get out au plus tard de container',
                'openapi' => [
                    'example' => 'order[containers]=asc',
                    'allowReserved' => false,// if true, query parameters will be not percent-encoded
                    'allowEmptyValue' => false,
                    'explode' => false, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
                ],
            ];
        }
        return $description;
    }

}