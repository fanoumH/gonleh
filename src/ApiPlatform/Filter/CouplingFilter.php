<?php

namespace App\ApiPlatform\Filter;

use ApiPlatform\Doctrine\Orm\Extension\PaginationExtension;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;
use ApiPlatform\State\Pagination\Pagination;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class CouplingFilter  extends AbstractFilter
{
    public function __construct(
        ManagerRegistry $managerRegistry, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null,
        private PaginationExtension $paginationExtension, private ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory, private RequestStack $requestStack,
        private Pagination $pagination,
    )
    {
        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);
    }

    /**
     * @throws ResourceClassNotFoundException
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if($this->requestStack->getCurrentRequest()->query->get("search")) {
            $semiAlias = 'semi_alias'.uniqid();
            $trailerAlias = 'trailer_alias_'.uniqid();
            $semiBrandAlias = 'semi_brand_alias_'.uniqid();
            $TrailerBrandAlias = 'trailer_brand_alias_'.uniqid();
            $semiModelAlias = 'semi_model_alias_'.uniqid();
            $TrailerModelAlias = 'trailer_model_alias_'.uniqid();

            $existingAlias = $this->getExistingAlias($queryBuilder, 'o');
            $existingAliasDepartment = $this->getExistingAlias($queryBuilder, $semiAlias);

            $queryBuilder->leftJoin('o.Semi' ,$semiAlias)
                ->leftJoin('o.Trailer' ,$trailerAlias)
                ->leftJoin($semiAlias.'.vehicleBrand' ,$semiBrandAlias)
                ->leftJoin($trailerAlias.'.vehicleBrand' ,$TrailerBrandAlias)
                ->leftJoin($semiAlias.'.vehicleModel' ,$semiModelAlias)
                ->leftJoin($trailerAlias.'.vehicleModel' ,$TrailerModelAlias)
            ;

            $request = $this->requestStack->getCurrentRequest();
            $pageParameterName = "page";
            $itemsPerPageParameterName = "itemsPerPage";
            $page = $request->query->getInt($pageParameterName, 1);
            $itemsPerPage = $request->query->getInt($itemsPerPageParameterName) === 0 ? 30 : $request->query->getInt($itemsPerPageParameterName);
            // Apply the filter condition based on the value
            $parameterName = $queryNameGenerator->generateParameterName($property);
            $queryBuilder
                ->where($queryBuilder->expr()->orX(
                    $queryBuilder->expr()->like('o.couplingNumber', ':value'),
                    $queryBuilder->expr()->like($semiBrandAlias.'.name', ':value'),
                    $queryBuilder->expr()->like($TrailerBrandAlias.'.name', ':value'),
                    $queryBuilder->expr()->like($semiModelAlias.'.name', ':value'),
                    $queryBuilder->expr()->like($TrailerModelAlias.'.name', ':value'),
                ))->setParameter('value', '%'.$value.'%')
            ;
            // Set the pagination parameters
            $firstResult = ($page - 1) * $itemsPerPage;
            $queryBuilder->setFirstResult($firstResult)
                ->setMaxResults($itemsPerPage);
        }
    }

    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }
        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["search"] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_STRING,
                'required' => false,
                'description' => 'Filtrer tous les utilisateurs à partir de ces attributes (soit nom, prénoms, ou email , etc .)',
                'openapi' => [
                    'example' => 'NIAVO RANDRIANARISON',
                    'allowReserved' => false,// if true, query parameters will be not percent-encoded
                    'allowEmptyValue' => true,
                    'explode' => false, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
                ],
            ];
        }
        return $description;
    }

    // Méthode pour vérifier si un alias est déjà défini dans la requête
    private function getExistingAlias(QueryBuilder $queryBuilder, string $alias): ?string
    {
        $existingAliases = $queryBuilder->getAllAliases();
        return in_array($alias, $existingAliases) ? $alias : null;
    }
}