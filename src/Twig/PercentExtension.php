<?php

namespace App\Twig;

use App\Entity\TerminalGetin;
use App\Service\ExportExcelService;
use App\Utils\MercureTopic;
use App\Utils\Notification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Asset\Packages;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mercure\HubInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Vich\UploaderBundle\Storage\FileSystemStorage;

class PercentExtension extends AbstractExtension
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface  $parameterBag,
        private Environment $twig,
        private FileSystemStorage $fileSystemStorage,
        private ExportExcelService $exportExcelService,
        private HubInterface $hub,
        private Packages $package,
        private Notification $notification
    )
    {
    }

    public function getFilters()
    {
        return [
            new TwigFilter('progression',[$this,'calculateProgression'],['is_safe' => ['html']])
        ];
    }

    public function calculateProgression(TerminalGetin $getin , $argument1= 0 , $argument2 = 1){
        $this->notification->push([
            MercureTopic::DOWNLOAD_FILE_PROGRESSION
        ],'progression','waiting',floor($argument1 * 100 / ($argument2 === 0 ? 1 :  $argument2)));
        return '';
    }
}