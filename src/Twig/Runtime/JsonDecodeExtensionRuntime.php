<?php

namespace App\Twig\Runtime;

use Twig\Extension\RuntimeExtensionInterface;

class JsonDecodeExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct()
    {
        // Inject dependencies if needed
    }

    public function custom_json_decode($value)
    {
        $value = str_replace('\"', '"', $value);
        return json_decode($value, true);
    }
}
