<?php

namespace App\Document;

use ApiPlatform\Doctrine\Odm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\HistoryActionRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des historiques des actions",
            ],
//            normalizationContext: ['groups' => 'user:read'],
        ),
    ]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"date","entity"
],arguments:  ['orderParameterName' => 'order'])
]
#[Document/*(repositoryClass: HistoryActionRepository::class)*/]
class HistoryAction
{
    public function __construct($action)
    {
        $this->action = $action;
    }

    #[Id]
    private $id;

    #[Field(type: "string")]
    private $action;

    #[Field(type:"raw")]
    private $data;

    #[Field(type:"date")]
    private $date;

    #[Field(type:"raw")]
    private $responsable;

    #[Field(type:"string")]
    private $entity;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action): void
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function toArray(){
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * @param mixed $responsable
     */
    public function setResponsable($responsable): void
    {
        $this->responsable = $responsable;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity): void
    {
        $this->entity = $entity;
    }
}
