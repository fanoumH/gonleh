<?php

namespace App\Validator\Constraints;

use App\Service\TimeRangeLogicService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class AmbiguousValidator extends ConstraintValidator
{
    public function __construct(
        private readonly TimeRangeLogicService $timeRangeLogicService,
        private RequestStack $requestStack,
    )
    {
    }

    public function validate(mixed $value, Constraint $constraint)
    {
        $data =json_decode($this->requestStack->getCurrentRequest()->getContent(), true);
        dd($this->timeRangeLogicService->validateNewRangeTime($data));
    }
}