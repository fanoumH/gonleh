<?php

namespace App\MessageHandler;

use App\Entity\TerminalGetin;
use App\Entity\TerminalGetOut;
use App\Message\GetIn;
use App\Message\GetOut;
use App\Service\ExportExcelService;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Vich\UploaderBundle\Storage\FileSystemStorage;

#[AsMessageHandler]
final class GetOutHandler
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface  $parameterBag,
        private Environment $twig,
        private FileSystemStorage $fileSystemStorage,
        private ExportExcelService $exportExcelService,
    )
    {
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function __invoke(
        GetOut $getOut
    )
    {
        $allGetIn = $this->em->getRepository(TerminalGetOut::class)->findAll();
        $data = [
            "allGetOut" => $allGetIn,
        ];
        $html = $this->twig->render('terminalGetOut/terminalGetOut.html.twig' , $data);
        $domPdf = new Dompdf();
        $domPdf->loadHtml($html);
        $domPdf->setPaper('A4', 'landscape');
        $domPdf->render();
        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT,'all-getouts.pdf');
        //récupérez le contenu du PDF généré
        $output = $domPdf->output();

        if(file_exists($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all')."all-getouts.pdf"))
            unlink($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all')."all-getouts.pdf");

        file_put_contents($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all')."all-getouts.pdf" , $output);
        //Generate Excel
        $title      = "Liste de tous les Getouts";
        $lastColumn = 'L';
        $data       = ['count' => 6];

        $spreadsheet = $this->exportExcelService->formatSpreadSheetXls($title, $data, $lastColumn, $html);
        $writer      = new Xlsx($spreadsheet);
        //Generate the file xlsx
        $filename = "all-getouts".'.xlsx';
        $path = './';
        if(file_exists($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all').$filename))
            unlink($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all').$filename);
        $writer->save($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all').$filename);
    }
}