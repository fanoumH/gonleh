<?php

namespace App\MessageHandler;

use App\Entity\TerminalGetin;
use App\Message\GetIn;
use App\Service\ExportExcelService;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Vich\UploaderBundle\Storage\FileSystemStorage;

#[AsMessageHandler]
final class GetInHandler
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface  $parameterBag,
        private Environment $twig,
        private FileSystemStorage $fileSystemStorage,
        private ExportExcelService $exportExcelService,
    )
    {
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function __invoke(
        GetIn $getIn
    )
    {
        //Generate PDF
        $allGetIn = $this->em->getRepository(TerminalGetin::class)->findAll();
        $data = [
            "allGetIn" => $allGetIn,
        ];
        $html = $this->twig->render('terminalGetIn/terminalGetIn.html.twig' , $data);
        $domPdf = new Dompdf();
        $domPdf->loadHtml($html);
        $domPdf->setPaper('A4', 'landscape');
        $domPdf->render();
        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT,'all-getins.pdf');
        //récupérez le contenu du PDF généré
        $output = $domPdf->output();

        if(file_exists($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all')."all-getins.pdf"))
            unlink($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all')."all-getins.pdf");

        file_put_contents($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all')."all-getins.pdf" , $output);
        //Generate Excel
        $title      = "Liste de tous les Getins";
        $lastColumn = 'L';
        $data       = ['count' => 6];

        $spreadsheet = $this->exportExcelService->formatSpreadSheetXls($title, $data, $lastColumn, $html);
        $writer      = new Xlsx($spreadsheet);
        //Generate the file xlsx
        $filename = "all-getins".'.xlsx';
        $path = './';
        if(file_exists($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all').$filename))
            unlink($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all').$filename);
        $writer->save($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all').$filename);
    }
}