<?php

namespace App\Service;

use App\Entity\Staff;
use App\Entity\StaffPost;
use App\Entity\Vehicle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ValidationService
{
    /**
     * @param $staff
     * @param ExecutionContextInterface $context
     * @return void
     */
    public function validateVehicleAllocateTrailers($vehicles, ExecutionContextInterface $context)
    {
        if ($vehicles) {
            $trailers = $this->getTrailersInVehicleAC($vehicles);
            if ($trailers->count() != $vehicles->count()) {
                $context->buildViolation("validate.vehicle.allocate.trailers.all.trailer")->addViolation();
                return;
            }
        }
    }

    /**
     * @param $staff
     * @param ExecutionContextInterface $context
     * @return void
     */
    public function validateVehicleAllocateDrivers($staff, ExecutionContextInterface $context)
    {
        if ($staff) {
            $drivers = $this->getDriversInStaffAC($staff);
            if ($drivers->count() != $staff->count()) {
                $context->buildViolation("validate.vehicle.allocate.drivers.all.driver")->addViolation();
                return;
            }
        }
    }

    /**
     * @param $vehicle
     * @param ExecutionContextInterface $context
     * @return void
     */
    public function validateVehicleToAllocate($vehicle, ExecutionContextInterface $context)
    {
        /**
         * validate if vehicle is tractor
         */
        $this->validateVehicleIsTractor($vehicle, $context);
        /**
         * validate vehicle have the state to be allocated
         */
        $this->validateVehicleStateForAllocation($vehicle, $context);
    }

    /**
     * @param $vehicle
     * @param ExecutionContextInterface $context
     * @return void
     */
    public function validateVehicleIsTractor($vehicle, ExecutionContextInterface $context)
    {
        if ($vehicle) {
            /**
             * @var Vehicle $vehicle
             */
            if ($vehicle->getType() !== Vehicle::TYPE_TRACTOR)
            {
                $context->buildViolation("validate.vehicle.to.allocate.type.tractor")->addViolation();
                return;
            }
        }

    }

    /**
     * @param $vehicle
     * @param ExecutionContextInterface $context
     * @return void
     */
    public function validateVehicleStateForAllocation($vehicle, ExecutionContextInterface $context)
    {
        if($vehicle) {
            /**
             * @var Vehicle $vehicle
             */
            if (!in_array($vehicle->getState(),Vehicle::STATES_ALLOW_TO_DO_ALLOCATION))
            {
                $context->buildViolation("validate.vehicle.to.allocate.state")->addViolation();
                return;
            }
        }
    }

    /**
     * @param ArrayCollection | PersistentCollection $ofs
     * @return ArrayCollection | PersistentCollection
     */
    public function getDriversInStaffAC(ArrayCollection | PersistentCollection $staff): ArrayCollection | PersistentCollection
    {
        return $staff->filter(function($item) {
            /**
             * @var StaffPost $staffPost
             * @var Staff $item
             */
            $staffPost = $item->getStaffPost();
            return $staffPost && $staffPost->getId() === StaffPost::DRIVER_ID;
        });
    }

    /**
     * @param ArrayCollection | PersistentCollection $ofs
     * @return ArrayCollection | PersistentCollection
     */
    public function getTrailersInVehicleAC(ArrayCollection | PersistentCollection $vehicles): ArrayCollection | PersistentCollection
    {
        return $vehicles->filter(function($item) {
            /**
             * @var Vehicle $item
             */
            return $item->getType() === Vehicle::TYPE_TRAILER;
        });
    }
    /**
     * @param FormInterface $form
     * @return array
     */
    public function getArrayOfFormError(FormInterface $form): array
    {
        $errors = $form->getErrors(true, true);
        $errorMessages = [];
        foreach ($errors as $error) {
            /**
             * @var FormError $error
             */
            $errorMessages[$error->getOrigin()->getName()] = $error->getMessage();
        }

        return $errorMessages;
    }

    /**
     * @param FormInterface $form
     * @return string
     */
    public function getStringOfFormError(FormInterface $form): string
    {
        $errors = $this->getArrayOfFormError($form);
        $formattedErrors = [];

        foreach ($errors as $fieldName => $errorMessage) {
            $formattedErrors[] = sprintf('%s: %s', $fieldName, $errorMessage);
        }

        return implode("\n", $formattedErrors);
    }
}