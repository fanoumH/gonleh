<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TimeRangeLogicService
{
    public function __construct(
        private RequestStack $requestStack,
        private EntityManagerInterface  $em,
        private PeriodCalculator $periodCalculator
    )
    {
    }

    public function getNewRangeTime($data){
        if(($this->requestStack->getCurrentRequest()->attributes->get("_route"))  === "_api_/users/{id}{._format}_patch" && isset($data["times_range"])){
            $userId = $this->requestStack->getCurrentRequest()->attributes->get("id");
            $user = $this->em->getRepository(User::class)->find($userId);
            $currentTimeRange = $user->getTimesRange();
            $toRemove = array_filter($data["times_range"] , function($item){
                return $item["remove"] ?? false === true;
            });

            $toRemove = array_map(function($item){
                unset($item["remove"]);
                return $item;
            },$toRemove);

            $data["times_range"] = array_filter($data["times_range"] , function($item){
                return !isset($item["remove"]) && ($item["remove"] ?? false) === false;/* ?? false === false*/;
            });
            $newCurrentTime = [];

            foreach($toRemove as $currentToRemove){
                $remove = true;
                foreach($currentTimeRange as $keyscurries => $curries){
                    if(($curries["start"] === $currentToRemove["start"] && $curries["end"] === $currentToRemove["end"])){
                        unset($currentTimeRange[$keyscurries]);
                    }
                }
            }

            foreach($data["times_range"] as $keyTimeRangeInput=>$timeRangeInput){
                $arrayTrue = [];
                foreach($currentTimeRange as $timeRangeOuput){
                    $matchTimeRangeInput = $this->periodCalculator->isTimeRangeIncluded($timeRangeInput , $timeRangeOuput);
                    $arrayTrue [] = $matchTimeRangeInput;
                }
                $data["times_range"][$keyTimeRangeInput]["match"] = count(array_filter($arrayTrue , function($item){
                        return $item === false;
                    })) === count($currentTimeRange);
            }
            $data["times_range"] = array_filter($data["times_range"] , function($item){
                return $item["match"]  === true;
            });
            $data["times_range"] = array_map(function($item){
                unset($item["match"]);
                return $item;
            },$data["times_range"]);
            $data["times_range"] = array_merge($data["times_range"] , $currentTimeRange);
        }
        return $data;
    }

    public function getToRemoveNotIncluded($data){
        $currentRemoveFalse = [];
        if(($this->requestStack->getCurrentRequest()->attributes->get("_route"))  === "_api_/users/{id}{._format}_patch" && isset($data["times_range"])){
            $userId = $this->requestStack->getCurrentRequest()->attributes->get("id");
            $user = $this->em->getRepository(User::class)->find($userId);
            $currentTimeRange = $user->getTimesRange();
            $toRemove = array_filter($data["times_range"] , function($item){
                return $item["remove"] ?? false === true;
            });

            $toRemove = array_map(function($item){
                unset($item["remove"]);
                return $item;
            },$toRemove);

            $data["times_range"] = array_filter($data["times_range"] , function($item){
                return !isset($item["remove"]) && ($item["remove"] ?? false) === false;/* ?? false === false*/;
            });
            $newCurrentTime = [];

            foreach($toRemove as $currentToRemove){
                $remove = true;
                foreach($currentTimeRange as $keyscurries => $curries){
                    if(($curries["start"] === $currentToRemove["start"] && $curries["end"] === $currentToRemove["end"])){
                        $remove = false;

                        unset($currentTimeRange[$keyscurries]);
                    }
                }
                if($remove === true){
                    $currentRemoveFalse[] = $currentToRemove;
                }
            }
        }

        $currentRemoveFalse = $this->removeDuplicateValues($currentRemoveFalse);

        if(!empty($currentRemoveFalse)){
            $rangeString = "[";
            $inc = 0;
            foreach($currentRemoveFalse as $range){
                $rangeString .= ' {'.$range["start"].' à '.$range["end"].'}' ;
            }
            $rangeString .= "]";
            throw new \Exception("Ce(s) horaire(s) de plage(s) que vous voulez supprimer n'existe pas : " . $rangeString.". Veuillez les modifier !");
        }
        return $data;
    }

    public function removeDuplicateValues($inputArray){
        $uniqueArray = [];
        foreach ($inputArray as $item) {
            // Utilisez une représentation chaîne des éléments pour vérifier les doublons
            $itemString = json_encode($item);

            // Si l'élément n'est pas déjà présent dans $uniqueArray, ajoutez-le
            if (!in_array($itemString, $uniqueArray)) {
                $uniqueArray[] = $itemString;
            }
        }

        // Convertissez les éléments uniques de chaînes en tableaux
        $uniqueArray = array_map(function($item){
            return json_decode($item,true);
        }, $uniqueArray);
        return $uniqueArray;
    }

    public function getFalseRangeTime($data){
        if(($this->requestStack->getCurrentRequest()->attributes->get("_route"))  === "_api_/users/{id}{._format}_patch" && isset($data["times_range"])){
            $userId = $this->requestStack->getCurrentRequest()->attributes->get("id");
            $user = $this->em->getRepository(User::class)->find($userId);
            $currentTimeRange = $user->getTimesRange();
            $toRemove = array_filter($data["times_range"] , function($item){
                return $item["remove"] ?? false === true;
            });

            $toRemove = array_map(function($item){
                unset($item["remove"]);
                return $item;
            },$toRemove);

            $data["times_range"] = array_filter($data["times_range"] , function($item){
                return !isset($item["remove"]) && ($item["remove"] ?? false) === false;/* ?? false === false*/;
            });
            $newCurrentTime = [];

            foreach($toRemove as $currentToRemove){
                $remove = true;
                foreach($currentTimeRange as $keyscurries => $curries){
                    if(($curries["start"] === $currentToRemove["start"] && $curries["end"] === $currentToRemove["end"])){
                        unset($currentTimeRange[$keyscurries]);
                    }
                }
            }

            foreach($data["times_range"] as $keyTimeRangeInput=>$timeRangeInput){
                $arrayTrue = [];
                foreach($currentTimeRange as $timeRangeOuput){
                    $matchTimeRangeInput = $this->periodCalculator->isTimeRangeIncluded($timeRangeInput , $timeRangeOuput);
                    $arrayTrue [] = $matchTimeRangeInput;
                }
                $data["times_range"][$keyTimeRangeInput]["match"] = count(array_filter($arrayTrue , function($item){
                        return $item === true;
                    })) > 0;
            }

            $data["times_range"] = array_filter($data["times_range"] , function($item){
                return $item["match"]  === true;
            });

            if(!empty($data["times_range"])){
                $rangeString = "[";
                $inc = 0;
                foreach($data["times_range"] as $range){
                    $rangeString .= ' {'.$range["start"].' à '.$range["end"].'}' ;
                }
                $rangeString .= "]";
                throw new \Exception("Ces horaires de plages sont déja incluses : " . $rangeString.". Veuillez les modifier !");
            }
        }
    }

    public function getFalseRangeTimeInclude($data){
        $matchTimeRange = false;
        $timeRange = $data["times_range" ] ?? [];
//        $rangeString = "[";
        for($i = 0;$i<count($timeRange);$i++){
            $toCompare = $timeRange[$i];
            for($j = 0;$j<count($timeRange);$j++){
                if($j != $i){
                    $matchTimeRangeInput = $this->periodCalculator->isTimeRangeIncluded($toCompare , $timeRange[$j]);
                    $matchTimeRange = $matchTimeRange || $matchTimeRangeInput;
                    /*if($matchTimeRangeInput){
                        foreach($data["times_range"] as $range){
                            $rangeString .= ' {'.$range["start"].' à '.$range["end"].'}' ;
                        }
                    }*/
                }
            }
        }
//        $rangeString .= "]";
        if($matchTimeRange){
            throw new \Exception("Ces horaires de plages en entrées s'intersectionnent . Veuillez les modifier SVP!");
        }
    }
}