<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Html;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Twig\Environment;

/**
 * Class ExportExcelService
 * @package App\Service
 */
class ExportExcelService
{
    /**
     * @param Environment $twig
     */
    public function __construct(private Environment $twig)
    {
    }

    /**
     * Create XLSX Format
     * @param $title
     * @param $data
     * @param $lastColumn
     * @param $template
     * @param $heads
     * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
     * @throws Exception
     */
    public function formatSpreadSheetXls($title, $data, $lastColumn, $template = null, $heads = null)
    {
        $length = $data['count'] + 1;

        if (!$template)
            $template = $this->twig->render('excel/model.html.twig', [
                'limit' => $lastColumn,
                'heads' => $heads,
                'data'  => $data['result']
            ]);

        $reader = new Html();
        libxml_use_internal_errors(true);
        $spreadsheet = $reader->loadFromString($template);
        $spreadsheet->getActiveSheet()->setTitle($title);
        $styleArray = ['borderStyle' => Border::BORDER_THIN];
        $spreadsheet->getActiveSheet()->getStyle('A1:' . $lastColumn . $length)
            ->getBorders()->getAllBorders()->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('A1:' . $lastColumn . $length)
            ->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:' . $lastColumn . $length)
            ->getAlignment()->setWrapText(true);

        foreach (range('A', $lastColumn) as $selected) {
            $spreadsheet->getActiveSheet()->getColumnDimension($selected)->setAutoSize(true);
            for ($i = 1; $i <= $length; $i++) {
                $date      = \DateTime::createFromFormat('d-m-Y', $spreadsheet->getActiveSheet()->getCell($selected . $i));
                $dateOther = \DateTime::createFromFormat('d/m/Y', $spreadsheet->getActiveSheet()->getCell($selected . $i));
                $numeric   = is_numeric($spreadsheet->getActiveSheet()->getCell($selected . $i)->getValue());
                if ($date || $dateOther) {
                    $spreadsheet->getActiveSheet()->getStyle($selected . $i)->getNumberFormat()
                        ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                } elseif ($numeric) {
                    $spreadsheet->getActiveSheet()->getStyle($selected . $i)->getNumberFormat()
                        ->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
        }

        return $spreadsheet;
    }
}