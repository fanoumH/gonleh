<?php

namespace App\Service;

use DateTime;

class PeriodCalculator
{
    public function __construct()
    {
    }

    function isDateBetween($startDate , $endDate , $dateCurrent){
        $dateC=date('Y-m-d', strtotime($dateCurrent));
        //echo $paymentDate; // echos today!
        $DateBegin = date('Y-m-d', strtotime($startDate));
        $DateEnd = date('Y-m-d', strtotime($endDate));

        return (($dateC >= $DateBegin) && ($dateC <= $DateEnd));
    }

    function isTimeBetween($startDate , $endDate , $dateCurrent){
        $dateC=date('Y-m-d H:i', strtotime($dateCurrent));
        //echo $paymentDate; // echos today!
        $DateBegin = date('Y-m-d H:i', strtotime($startDate));
        $DateEnd = date('Y-m-d H:i', strtotime($endDate));
        return (($dateC >= $DateBegin) && ($dateC <= $DateEnd));
    }

    function isTimeRangeIncluded($range1, $range2) {
        $startTime1 = DateTime::createFromFormat('H:i', $range1['start']);
        $endTime1 = DateTime::createFromFormat('H:i', $range1['end']);
        $startTime2 = DateTime::createFromFormat('H:i', $range2['start']);
        $endTime2 = DateTime::createFromFormat('H:i', $range2['end']);

        $startTime = strtotime($range1['start']);
        $endTime = strtotime($range1['end']);

        $currentTime = $startTime;
        $step = 60; // Une heure en secondes (3600 secondes)
        $interValTimeTrue = false;
        while ($currentTime <= $endTime) {
//            dump(date("H:i", $currentTime).'//'.$startTime2->format("H:i").'//'.$endTime2->format("H:i")); // Affiche l'heure au format "H:i"
            $intervalTime = DateTime::createFromFormat('H:i',date("H:i", $currentTime));
            $currentTime += $step;
            if(($intervalTime >= $startTime2) && ($intervalTime <= $endTime2)){
                $interValTimeTrue = true;
                break;
            }
        }
        return $interValTimeTrue;
    }

    /*function isTimeRangeIncluded($range1, $range2) {
        $startTime1 = strtotime($range1['start']);
        $endTime1 = strtotime($range1['end']);
        $startTime2 = strtotime($range2['start']);
        $endTime2 = strtotime($range2['end']);
        return ($startTime1 >= $startTime2) && ($endTime1 <= $endTime2);
    }*/
}