<?php

namespace App\Service;

use App\Entity\TerminalGetin;
use Doctrine\ORM\EntityManagerInterface;

class TerminalGetinService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateRefGetin()
    {
        $lastInsertedId = 1;
        $lastGetin    = $this->em->getRepository(TerminalGetin::class)->findBy([], ['id' => 'DESC']);
        if ($lastGetin && $lastGetin[0]->getRefGetin()) {
            $i = -5;
            while (!is_numeric(substr($lastGetin[0]->getRefGetin(), $i)))
                $i++;
            $lastInsertedId = (int) substr($lastGetin[0]->getRefGetin(), $i) + 1;
        }
        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = TerminalGetin::GETIN_PREFIX;
        return $prefix . $num;
    }
}