<?php

namespace App\Service;

use App\Entity\TerminalGetOut;
use Doctrine\ORM\EntityManagerInterface;

class TerminalGetOutService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateRefGetOut()
    {
        $lastInsertedId = 1;
        $lastGetOut    = $this->em->getRepository(TerminalGetOut::class)->findBy([], ['id' => 'DESC']);
        if ($lastGetOut && $lastGetOut[0]->getRefGetOut()) {
            $i = -5;
            while (!is_numeric(substr($lastGetOut[0]->getRefGetOut(), $i)))
                $i++;
            $lastInsertedId = (int) substr($lastGetOut[0]->getRefGetOut(), $i) + 1;
        }
        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = TerminalGetOut::GETOUT_PREFIX;
        $now = (new \DateTime())->format("d-m-y");
        return $prefix . "-" . $now . "-" . $num;
    }
}