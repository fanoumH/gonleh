<?php

namespace App\Service;

use App\Entity\Folder;
use Doctrine\ORM\EntityManagerInterface;

class FolderService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateRefFolder()
    {
        $lastInsertedId = 1;
        $lastFolder    = $this->em->getRepository(Folder::class)->findBy([], ['id' => 'DESC']);
        if ($lastFolder && $lastFolder[0]->getRefFolder()) {
            $i = -5;
            while (!is_numeric(substr($lastFolder[0]->getRefFolder(), $i)))
                $i++;
            $lastInsertedId = (int) substr($lastFolder[0]->getRefFolder(), $i) + 1;
        }
        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = Folder::FOLDER_PREFIX;
        return $prefix . $num;
    }
}