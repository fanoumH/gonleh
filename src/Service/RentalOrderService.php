<?php

namespace App\Service;

use App\Entity\RentalOrder;
use Doctrine\ORM\EntityManagerInterface;

class RentalOrderService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateRentalOrderCode()
    {
        $lastInsertedId = 1;
        $lastOT    = $this->em->getRepository(RentalOrder::class)->findBy([], ['id' => 'DESC']);
        if ($lastOT && $lastOT[0]->getCode()) {
            $i = -5;
            while (!is_numeric(substr($lastOT[0]->getCode(), $i)))
                $i++;
            $lastInsertedId = (int) substr($lastOT[0]->getCode(), $i) + 1;
        }
        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = RentalOrder::RENTAL_ORDER_PREFIX;
        return $prefix . $num;
    }

}