<?php

namespace App\Service;

use App\Entity\Terminal;
use App\Repository\ContainerRepository;
use App\Repository\TerminalGetinRepository;
use Doctrine\ORM\EntityManagerInterface;

class TerminalService
{
    public function __construct(
        private EntityManagerInterface $em,
        private TerminalGetinRepository $terminalGetinRepository,
        private ContainerRepository $containerRepository
    )
    {
    }

    public function genereteOccupiedPlaces(Terminal $terminal)
    {
        $occupingContainers = $this->containerRepository->findOccuping($terminal);
        
        $unitedOccupied = 0; // unity of 20 pieds

        
        foreach ($occupingContainers as $key => $occuping) {
            $unitedOccupied += intval($occuping->getContainerType()?->getUnite());
        }   
        
        $placeOf20piedsOccupied = $unitedOccupied / 20;
        
        return $placeOf20piedsOccupied;
    }

    public function genereteOccupiedPlacesRate(Terminal $terminal)
    {
        $capacity = $terminal->getCapacity();
        $placeOf20piedsOccupied = $this->genereteOccupiedPlaces($terminal);

        $rate = ((float) $placeOf20piedsOccupied * 100 ) / $capacity ;

        return round($rate, 2);
    }

}