<?php

namespace App\Service;

use App\Entity\Container;
use Doctrine\ORM\EntityManagerInterface;

class ContainerService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateRefCustomer()
    {
        $lastInsertedId = 1;
        $lastReceipt    = $this->em->getRepository(Container::class)->findBy(
            [],['id' => 'ASC']
        );

        $lastReceipt = end($lastReceipt);
        $lastInsertedId  = $lastReceipt->getId();
        $lastInsertedId ++;

        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = Container::CONTAINER_PREFIX;
        return $prefix . $num;
    }
}