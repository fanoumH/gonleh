<?php

namespace App\Service;

use App\Entity\TransportOrder;
use Doctrine\ORM\EntityManagerInterface;

class TransportOrderService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateTransportOrderCode()
    {
        $lastInsertedId = 1;
        $lastOT    = $this->em->getRepository(TransportOrder::class)->findBy([], ['id' => 'DESC']);
        if ($lastOT && $lastOT[0]->getCode()) {
            $i = -5;
            while (!is_numeric(substr($lastOT[0]->getCode(), $i)))
                $i++;
            $lastInsertedId = (int) substr($lastOT[0]->getCode(), $i) + 1;
        }
        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = TransportOrder::TRANSPORT_ORDER_PREFIX;
        $now = (new \DateTime())->format("dmy");
        return $prefix . "-" . $now . "-" . $num;
    }

}