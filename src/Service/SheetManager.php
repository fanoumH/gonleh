<?php 

namespace App\Service;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SheetManager
{
    public function generateSheet($fileName, $worksheetName, $sheetData)
    {
        header('Access-Control-Allow-Origin: *'); //to get data from firefox addon
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        
        $spreadsheet = new Spreadsheet();
        // delete the default active sheet
        $spreadsheet->removeSheetByIndex(0);

        // $spreadsheet->set

        $worksheet = new Worksheet($spreadsheet, $worksheetName);

        $spreadsheet->addSheet($worksheet);

        $worksheet->fromArray($sheetData);

        foreach($worksheet->getColumnIterator() as $col)
        {
            $worksheet->getColumnDimension($col->getColumnIndex())->setWidth(20);
        }

        $spreadsheet->getDefaultStyle()->getNumberFormat()->setFormatCode("#");
        
        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        // $writer = IOFactory::createWriter($spreadsheet, "Csv");
        
        $tempFile = tempnam(sys_get_temp_dir(), $fileName); 
        
        $writer->save($tempFile);
        
        readfile($tempFile);
        
        unlink($tempFile);

        exit();
    }
}
