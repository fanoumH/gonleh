<?php

namespace App\Service;

use App\Entity\Booking;
use Doctrine\ORM\EntityManagerInterface;

class BookingService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateRefBooking()
    {
        $lastInsertedId = 1;
        $lastCustomer    = $this->em->getRepository(Booking::class)->findBy([], ['id' => 'DESC']);
        if ($lastCustomer && $lastCustomer[0]->getRefBooking()) {
            $i = -5;
            while (!is_numeric(substr($lastCustomer[0]->getRefBooking(), $i)))
                $i++;
            $lastInsertedId = (int) substr($lastCustomer[0]->getRefBooking(), $i) + 1;
        }
        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = Booking::BOOKING_PREFIX;
        return $prefix . $num;
    }
}