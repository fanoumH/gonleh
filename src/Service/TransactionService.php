<?php

namespace App\Service;

use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;

class TransactionService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateAccountCode($accountCode ,Transaction $currentObject)
    {
        $lastTransaction    = $this->em->getRepository(Transaction::class)->findBy(['department'=>$currentObject->getDepartment()->getId()], ['id' => 'DESC']);
        $numberG = 0;
        if(!empty($lastTransaction)) {
            $numberG = (int)explode("-", $lastTransaction[0]->getAccountCode())[1];
        }
        $numberG++;
        $num    = str_pad($numberG, 5, 0, STR_PAD_LEFT);
        return  $num."-".date("m").substr(date("Y-m-y"),2,2)."-".$currentObject->getDepartment()->getAbbreviation().$currentObject->getAccountCode()/*Transaction::TRANSACTION_SUFFIX*/;
    }
}