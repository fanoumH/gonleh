<?php

namespace App\Service;

use App\Entity\Booking;
use App\Entity\Coupling;
use Doctrine\ORM\EntityManagerInterface;

class CouplingService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateCouplingNumber(Coupling $coupling)
    {
        $immaSemi = $coupling->getSemi()->getRegistrationNumber();
        $immaTrailer = $coupling->getTrailer()->getRegistrationNumber();

        $couplingNumber = Coupling::COUPLING_PREFIX . "-" . $immaSemi . "-" . $immaTrailer;
        return $couplingNumber;
    }
}