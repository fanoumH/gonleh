<?php

namespace App\Service;

use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;
use App\Role\Infrastructure\Symfony\Service\Reflection;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;
use function App\Role\Infrastructure\Symfony\Service\convertObjectAttributesToArray;

class ResourceFileFinder
{

    public function __construct(private KernelInterface $kernel , private ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory,private ContainerBagInterface $params)
    {
    }

    public function findResourceFiles(string $resourceName)
    {
        $finder = new Finder();
        $finder->files()->in($this->kernel->getProjectDir().'/src')->name('*'.$resourceName.'.php');

        $resourceFiles = [];

        foreach ($finder as $file) {
            $resourcePath = explode('/',$file->getRealPath());
            $resourcePath = end($resourcePath);
            $resourcePath = explode(".",$resourcePath);
            $resourcePath = $resourcePath[0];
            $resourceFiles[] = /*$this->getNamespaceFromClass*/($resourcePath);
        }
        return $resourceFiles;
    }

    public function findResourceFromParameters(){
        return $this->params->get("menu_resource");
    }

    function getNamespaceFromClass(string $className): ?string {
        try {
            $reflection = new ReflectionClass($className);
            return $reflection->getNamespaceName();
        } catch (ReflectionException $e) {
            return null;
        }
    }


    public function getAllResource(){
        return array_map(function($item){
            return $this->listApiResources($item);
        } , $this->findResourceFromParameters());
    }

    public function listApiResources(string $ressourceClass): array
    {
        $resourceMetadataCollection = $this->resourceMetadataCollectionFactory->create($ressourceClass);

        $resources = [];

        foreach ($resourceMetadataCollection as $resourceMetadata) {
            $resourceName = $resourceMetadata->getShortName();
            $resources[] = [
                'class' => $resourceMetadata->getClass(),
                'resource' => $resourceMetadata->getShortName(),
                'description' => $resourceMetadata->getDescription(),
                'operations' => (function($operation){
                    $op = [];
                    foreach((new ReflectionClass($operation))->getProperties() as $property){
                        $property->setAccessible(true);
                        $op[$property->getName()] = $property->getValue($operation);
                        foreach($op[$property->getName()] as $keyItemOperation=>$itemOperation){
                            if(is_object($itemOperation[1])){
                                $op[$property->getName()][$keyItemOperation][1] = (function($operation){
                                    $op = [];
                                    foreach((new ReflectionClass($operation))->getProperties() as $property){
                                        $op[$property->getName()] = $property->getValue($operation);
                                    }
                                    return $op;
                                })($op[$property->getName()][$keyItemOperation][1]);
//                                $op[$property->getName()][$keyItemOperation][1]["summary"] = json_decode(json_encode(($op[$property->getName()][$keyItemOperation][1])["openapiContext"]),true);
                                $op[$property->getName()][$keyItemOperation][1]["summary"] = ($op[$property->getName()][$keyItemOperation][1])["openapiContext"]["summary"] ?? "";
                            }
//                            dump(json_decode(json_encode(($op[$property->getName()][$keyItemOperation][1])["openapiContext"]),true));
                        }
                    }
                    return $op;
                })($resourceMetadata->getOperations())["operations"],
                // Ajoutez d'autres informations que vous souhaitez récupérer
            ];
        }
        return $resources;
    }

    function convertObjectAttributesToArray($object)
    {
        $reflectionData = [];
        $reflectionClass = new ReflectionClass($object);

        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);

            $propertyName = $property->getName();
            $propertyValue = $property->getValue($object);
            $propertyType = $property->getType() ? $property->getType()->getName() : null;
            $propertyModifiers = Reflection::getModifierNames($property->getModifiers());

            $propertyData = [
                'name' => $propertyName,
                'value' => $propertyValue,
                'type' => $propertyType,
                'modifiers' => $propertyModifiers,
            ];

            if ($propertyValue instanceof ReflectionClass) {
                // Récupérer les informations de la ReflectionClass de la valeur de l'attribut
                $propertyData['value'] = convertObjectAttributesToArray($propertyValue->newInstance());
            }

            $reflectionData[] = $propertyData;
        }
        return $reflectionData;
    }
}
