<?php 

namespace App\Service;

use Dompdf\Dompdf;
use Dompdf\Options;
class PdfManager
{
    public function generatePdf($template, $fileName, $options = [], $asMailAttachment = false, $orientation = "portrait"): string|null
    {
        header('Access-Control-Allow-Origin: *'); //to get data from firefox addon
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment');
        header('filename: file.pdf');
        $options = array_merge([
            'Attachment' => true,
        ], $options);

        $opt = new Options();
        $opt->setIsRemoteEnabled(true);
        $opt->setIsHtml5ParserEnabled(true);
        // $opt->setDebugPng(true);

        $domPdf = new Dompdf($opt);
        $domPdf->loadHtml($template);
        $domPdf->setPaper('A4', $orientation);
        // Render the HTML as PDF
        $domPdf->render();
        // Output the generated PDF to Browser
        if ($asMailAttachment) {
            return $domPdf->output();
        } else {
            $domPdf->stream($fileName, $options);
            exit();
        }
    }
}
