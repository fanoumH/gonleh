<?php

namespace App\Service;

use App\Document\HistoryAction;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\SecurityBundle\Security;
class HistoryService
{
    public function __construct(
        private DocumentManager $dm,
        private Security $security
    )
    {
    }

    public function mergeHistory($action , $data, $className = ""){
        $action = new HistoryAction($action);
//        $action->setAction($action);
        $action->setDate(date("Y-m-d H:s:i"));
        $action->setData($data);
        $action->setResponsable($this->security?->getUser()?->toArray() ?? []);
        $action->setEntity($className);
        $this->dm->persist($action);
        $this->dm->flush();
    }
}