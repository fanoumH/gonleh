<?php

namespace App\Service;

use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;

class CustomerService
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {
    }

    public function generateRefCustomer()
    {
        $lastInsertedId = 1;
        $lastCustomer    = $this->em->getRepository(Customer::class)->findBy([], ['id' => 'DESC']);
        if ($lastCustomer && $lastCustomer[0]->getRefCustomer()) {
            $i = -5;
            while (!is_numeric(substr($lastCustomer[0]->getRefCustomer(), $i)))
                $i++;
            $lastInsertedId = (int) substr($lastCustomer[0]->getRefCustomer(), $i) + 1;
        }
        $num    = str_pad($lastInsertedId, 5, 0, STR_PAD_LEFT);
        $prefix = Customer::CUSTOMER_PREFIX;
        return $prefix . $num;
    }
}