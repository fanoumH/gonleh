<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Repository\ContainerRepository;
use App\Repository\TerminalRepository;

class TerminalComputedProvider implements ProviderInterface
{
    public function __construct(
        private ContainerRepository $containerRepository,
        private TerminalRepository $terminalRepository
    )
    {}
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $occupiedUnities = (int) $this->containerRepository->findAllOccupingUnityNumber()[0]["occupiedUnities"];

        $totalCacityOfAllTerminals = (int) $this->terminalRepository->findCapacityTotalOfAll()[0]["capacityOfAll"];
        
        // Retrieve the state from somewhere
        return [
            "totalOccupiedUnities" => $occupiedUnities,
            "totalCapacityOfAllTerminals" => $totalCacityOfAllTerminals
        ];
    }
}
