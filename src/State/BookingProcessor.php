<?php

namespace App\State;

use ApiPlatform\Doctrine\Common\State\PersistProcessor;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\State\ProcessorInterface;

class BookingProcessor implements ProcessorInterface
{
    public function __construct(
        private ProcessorInterface $persistProcessor
    ){}
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        // Handle the state
        if($operation instanceof Patch){
            dd($context);
        }

        $result = $this->persistProcessor->process($data, $operation, $uriVariables, $context);
        return $result;
   
    }
}
