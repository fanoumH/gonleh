<?php

namespace App\State;

use ApiPlatform\Doctrine\Orm\State\CollectionProvider;
use App\Entity\Booking;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use ApiPlatform\Metadata\CollectionOperationInterface;

class BookingProvider implements ProviderInterface
{
    public function __construct(private CollectionProvider $collectionProvider)
    {
    }
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        // Retrieve the state from somewhere
        // dd($operation);
        $bookings = $this->collectionProvider->provide($operation, $uriVariables, $context);

        dd($bookings);
        
        if ($operation instanceof CollectionOperationInterface) {
            return $bookings;
        }
    }
}
