<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\TerminalGetin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class AddNewTerminalGetInProcessor implements ProcessorInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private SerializerInterface $serializer
    )
    {
    }
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $extractor = new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]);
        $normalizer = new ObjectNormalizer(null, null, null, $extractor);

        $serializer = new Serializer(
            [
                $normalizer,
                new ArrayDenormalizer(),
            ]

        );
        foreach($data  as $terminalGetIn){
            $obj = $serializer->denormalize(
                $terminalGetIn,
                TerminalGetin::class,
                null,
//                [ObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true]
            );
            dump($terminalGetIn);
            dump($obj);
        }
        dd();
        // Handle the state
    }
}
