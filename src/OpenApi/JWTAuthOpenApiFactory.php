<?php

namespace App\OpenApi;

use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\Model\Info;
use ApiPlatform\OpenApi\Model\MediaType;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\Model\Server;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;
use Symfony\Component\HttpFoundation\Response;

class JWTAuthOpenApiFactory implements OpenApiFactoryInterface
{

    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): \ApiPlatform\OpenApi\OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

//        foreach($openApi->getPaths()->getPaths() as $key=>$path){
////            dump($path->getGet()?->getOperationId());
//            if($path->getGet() && $path->getGet()?->getOperationId() === "api_nomenclatures_get_collection"){
//                $path->withSummary("kitasoniny");
////                    $openApi->getPaths()->addPath($key,$path->withParameters([]));
//            }
//        }
//
//        foreach($openApi->getPaths()->getPaths() as $key=>$path){
//            if($path->getGet() && $path->getGet()?->getOperationId() === "api_nomenclatures_get_collection"){
//                dump($path);
//            }
//        }
//
//        dd("kaiza");

        $schemas = $openApi->getComponents()->getSecuritySchemes();
        $schemas['bearerAuth'] = new \ArrayObject([
            'type' => 'http',
            'scheme' => 'bearer',
            'bearerFormat' => 'JWT'
        ]);
        $schemas = $openApi->getComponents()->getSchemas();
        $schemas['Credentials'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'example' => 'johndoe@example.com',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'password',
                ],
            ],
        ]);


        $path = '/api/login';
        $paths = $openApi->getPaths();
        $pathItem = $paths->getPath($path);
        $paths->addPath($path,  (new PathItem())->withPost(
            (new Operation())
                ->withOperationId('login_check_post')
                ->withTags(['Authentication'])
                ->withResponses([
                    Response::HTTP_OK => [
                        'description' => 'Authentification utilisateurs',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    'type' => 'object',
                                    'properties' => [
                                        'token' => [
                                            'readOnly' => true,
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                    ],
                                    'required' => ['token'],
                                ],
                            ],
                        ],
                    ],
                ])
                ->withSummary('Authentification')
                ->withDescription('Authentification d\'utilisateurs et fournisse un token')
                ->withRequestBody(
                    (new RequestBody())
                        ->withDescription('The login data')
                        ->withContent(new \ArrayObject([
                            'application/json' => new MediaType(new \ArrayObject(new \ArrayObject([
                                'type' => 'object',
                                'properties' => [
                                    'username' => [
                                        "description" => "username",
                                        "type"  => "string",
                                        'nullable' => false
                                    ],
                                    'password' => [
                                        "description" => "username",
                                        "type"  => "string",
                                        'nullable' => false
                                    ]
                                ],
                                'required' => ["username","password"],
                            ]))),
                        ]))
                        ->withRequired(true)
                )
        ));
        return $openApi;
    }
}
