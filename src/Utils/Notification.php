<?php

namespace App\Utils;

use App\Service\ExportExcelService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Asset\Packages;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Twig\Environment;
use Vich\UploaderBundle\Storage\FileSystemStorage;

class Notification
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface  $parameterBag,
        private Environment $twig,
        private FileSystemStorage $fileSystemStorage,
        private ExportExcelService $exportExcelService,
        private HubInterface $hub,
        private Packages $package
    )
    {
    }

    public function push($topics ,$type , $message, $data){
        $update = new Update(
            $topics,
            json_encode([
                'type' => $type,
                'message' => $message,
                'data' => $data
            ])
        );
        $this->hub->publish($update);
    }
}