<?php

namespace App\Security\Voter;

use App\Service\TokenService;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

final class UsersVoter extends Voter
{
    public const HEADER_NAME = 'AUTHORIZATION';

    public function __construct(
        private readonly RequestStack $request,
        private Security $security
    )
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        $supportsAttribute = in_array($attribute, ['PERMISSION_ALLOWED']);
        return $supportsAttribute ;
//        return true ;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
//        dd($this?->security?->getUser()?->getCapabilities()?->getPermissions());
//        dd($this->request->getCurrentRequest()->get("menu_id"));
        $menu =$this->findMenuById($this?->security?->getUser()?->getCapabilities()?->getPermissions() , (int)$this->request->getCurrentRequest()->get("menu_id")) ;

        $menuPermissions = $menu["permissions"] ?? [];
        if(
            in_array(
                $this->request->getCurrentRequest()->attributes->get("_route") ,
                $menuPermissions
            )
            ||
            $this?->security?->getUser()?->getCapabilities()?->isIsAdmin()
        ){
            return true;
        }
        return false;
    }

    public function findMenuById($menuList, $menuId) {
        foreach ($menuList ?? [] as $menu) {
            if ($menu['id'] === $menuId) {
                return $menu;
            }

            if (!empty($menu['children'])) {
                $foundMenu = $this->findMenuById($menu['children'], $menuId);
                if ($foundMenu !== null) {
                    return $foundMenu;
                }
            }
        }

        return null;
    }
}
