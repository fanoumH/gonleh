<?php

namespace App\Serializer;

use App\Entity\BasicRate;
use App\Entity\Booking;
use App\Entity\Customer;
use App\Entity\CustomerContact;
use App\Entity\CustomerIdentification;
use App\Entity\Menu;
use App\Entity\Transaction;
use App\Entity\User;
use App\Entity\TerminalGetin;
use App\Entity\TerminalGetOut;
use App\Entity\CustomerFreeField;
use App\Service\PeriodCalculator;
use App\Service\TimeRangeLogicService;
use App\Repository\ContainerRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Serializer\ItemNormalizer;
use App\Entity\Coupling;
use App\Entity\Staff;
use App\Entity\TransportOrder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


//#[AsDecorator(decorates: ItemNormalizer::class)]
final class ApiNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    private $decorated;
    use NormalizerAwareTrait;

    public function __construct(
        NormalizerInterface $decorated,
        private RequestStack $requestStack,
        private EntityManagerInterface  $em,
        private PeriodCalculator $periodCalculator,
        private TimeRangeLogicService $timeRangeLogicService,
        private StorageInterface $storage,
        private ContainerRepository $containerRepository,
        private EntityManagerInterface $entityManagerInterface
    )
    {
        if (!$decorated instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException(sprintf('The decorated normalizer must implement the %s.', DenormalizerInterface::class));
        }
        $this->decorated = $decorated;
    }

    public function normalize(mixed $object, string $format = null, array $context = [])
    {

        $data = $this->decorated->normalize($object, $format, $context);
        if (is_array($data) && $object instanceof Menu && $data['is_leaf'] === true) {
            $data['permissions'] =  $data['permissions'] ?? [];
        }
        if (is_array($data) && $object instanceof User) {
            $data['imageProfile'] = $this->storage->resolveUri($object, 'file');
        }
        if (is_array($data) && $object instanceof Transaction) {
            $data['attachedFile'] = $this->storage->resolveUri($object, 'file');
        }
        if (    is_array($data) && 
                (
                    $object instanceof TerminalGetin ||
                    $object instanceof TerminalGetOut || 
                    $object instanceof Booking ||
                    $object instanceof Coupling
                )
        ) {
            $data['createdAt'] = $object?->getCreatedAt()?->format("Y-m-d H:s:i");
        }
        
        if (is_array($data) && $object instanceof CustomerFreeField) {
            $data['attachedDocumentFile'] = $this->storage->resolveUri($object, 'attachedDocumentFile');
        }
        if (is_array($data) && $object instanceof CustomerIdentification) {
            $data['nifStatFile'] = $this->storage->resolveUri($object, 'nifFile');
            $data['cinFile'] = $this->storage->resolveUri($object, 'cinFile');
        }

        if (is_array($data) && $object instanceof Coupling) {
            $data["vehicles"]['Semi']=$data['Semi'];
            $data["vehicles"]['Trailer']=$data['Trailer'];
            unset($data['Semi']);
            unset($data['Trailer']);
        }

        /*
        if (is_array($data) && $object instanceof Booking){
            $dateGetOutofOnContainer = $object->getContainers()[0]?->getDateToGetOut()?->format("Y-m-d H:s:i") ;
            
            if($object->getContainers()[0]){
                $object->setDateGetOut(new \DateTime($dateGetOutofOnContainer));
            }

            $data['dateGetOut'] = $object->getDateGetOut()?->format("Y-m-d H:s:i") ;
        }
        */
        if (is_array($data) && $object instanceof Staff) {
            $data['profilePic'] = $this->storage->resolveUri($object, 'profilePicFile');
        }
        
        if (is_array($data) && $object instanceof TransportOrder) {
            $data['purchaseOrder'] = $this->storage->resolveUri($object, 'purchaseOrderFile');
            $data['eirAttachment'] = $this->storage->resolveUri($object, 'eirAttachmentFile');
            $data['blAttachment'] = $this->storage->resolveUri($object, 'blAttachmentFile');
        }

        if (is_array($data) && $object instanceof TerminalGetin) {
            $data['bdrAttachment'] = $this->storage->resolveUri($object, 'bdrAttachmentFile');
        }

        if (is_array($data) && $object instanceof Booking) {
            $data['bookingAttachment'] = $this->storage->resolveUri($object, 'bookingAttachmentFile');
        }

        if (is_array($data) && $object instanceof TerminalGetOut) {
            $data['cinAttachment'] = $this->storage->resolveUri($object, 'cinAttachmentFile');
        }

        if (is_array($data) && $object instanceof BasicRate) {
            $data['priceTTC'] = $data["generalPriceHT"] + (($data["generalPriceHT"] * 20) / 100);
        }

        return $data;
    }

    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $this->decorated->supportsDenormalization($data, $type, $format);
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {

        $instance = new $type();
        /*if($instance instanceof User){
            $this->timeRangeLogicService->getFalseRangeTimeInclude($data);
        }*/

        // dd($context['operation']->getClass() === Booking::class && $context['operation']->getMethod() === "PATCH");
        if(array_key_exists("operation", $context)){

                if($context['operation']->getClass() === Booking::class && $context['operation']->getMethod() === "PATCH"){
                    $object = $context['object_to_populate'];
                    
                    if(array_key_exists("containers",$data)){
                        $containers = $data["containers"];

                        if(count($containers) > 0){
                            
                            foreach ($object->getContainers() as $objCont) {
                                $object->removeContainer($objCont);
                            }
                            
                            foreach($containers as $cnt){
                                $container = $this->containerRepository->find($cnt["id"]);
                                $container->setDateToGetOut(new \Datetime($cnt["dateToGetOut"]));
                                $object->addContainer($container);
                            }
                            
                            unset($data["containers"]);
                        }
                    }
                }
        }

        return $this->decorated->denormalize($data, $type, $format, $context);
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        if($this->decorated instanceof SerializerAwareInterface) {
            $this->decorated->setSerializer($serializer);
        }
    }

    // public function getSupportedTypes($format)
    // {
    //     return [];
    // }
}