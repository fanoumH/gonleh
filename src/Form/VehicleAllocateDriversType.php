<?php

namespace App\Form;

use App\Entity\Staff;
use App\Entity\Vehicle;
use App\Service\ValidationService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class VehicleAllocateDriversType extends AbstractType
{
    public function __construct(
        private ValidationService $validationService
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('vehicle', EntityType::class, [
                'class' => Vehicle::class,
                'constraints' => [
                    new Assert\Callback([$this->validationService, 'validateVehicleToAllocate']
                    )
                ]
            ])
            ->add('drivers', EntityType::class, [
                'class' => Staff::class,
                'choice_value' => function (?Staff $entity): string {
                    return $entity ? '/staff/'.$entity->getId() : '';
                },
                'multiple' => true,
                'constraints' => [
                    new Assert\Callback([$this->validationService, 'validateVehicleAllocateDrivers'])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
            'data_class' => null,
            'vehicle_data' => null
        ]);
    }
}
