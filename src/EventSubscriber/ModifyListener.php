<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Service\HistoryService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ModifyListener implements EventSubscriberInterface
{

    public function __construct(
        private HistoryService $historyService
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['modification', EventPriorities::PRE_WRITE],
        ];
    }

    public function modification(ViewEvent $event){
        $request = $event->getRequest();
        $entity = $event->getControllerResult();

        if (
            $request->isMethod('DELETE')
        ) {
            $this->historyService->mergeHistory("suppression" , $entity->toArray(), get_class($entity));
        }
    }
}