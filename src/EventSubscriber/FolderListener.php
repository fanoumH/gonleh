<?php

namespace App\EventSubscriber;

use App\Entity\Folder;
use Doctrine\ORM\Events;
use App\Service\FolderService;
use App\Service\HistoryService;
use App\Service\CustomerService;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FolderListener implements EventSubscriber
{
    private $folderService;
    public function __construct(
        FolderService $folderService
    )
    {

        $this->folderService  = $folderService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Folder) {
            return;
        }

        $entity->setRefFolder($this->folderService->generateRefFolder());

    }

}