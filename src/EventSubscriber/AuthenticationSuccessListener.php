<?php

namespace App\EventSubscriber;

use App\Document\HistoryAction;
use App\Service\HistoryService;
use App\Service\PeriodCalculator;
use Doctrine\ODM\MongoDB\DocumentManager;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class AuthenticationSuccessListener
{
    public function __construct(private RequestStack $requestStack,private RouterInterface $router,private PeriodCalculator $periodCalculator,private HistoryService $historyService)
    {
    }

    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event){
        $data = $event->getData();
        $user = $event->getUser();
        if(!$user->isIsActive()){
            $event->getResponse()->setStatusCode(Response::HTTP_BAD_REQUEST);
            unset($data["token"]);
            $data['message'] = "Utilisateur désactivé";
            $event->stopPropagation();
        }

        $isPermited = false;

        foreach($user->getTimesRange()?? [] as $time_range){
            $isPermited  = $isPermited || $this->periodCalculator->isTimeBetween($time_range["start"] ,$time_range["end"],date("H:i"));
        }

        if(!$isPermited){
            $event->getResponse()->setStatusCode(Response::HTTP_BAD_REQUEST);
            unset($data["token"]);
            $data['message'] = "Nécessite une plage requise";
            $event->stopPropagation();
        }
        $event->setData($data);
        $this->historyService->mergeHistory("connection" , $user->toArray(), get_class($user));
    }
}