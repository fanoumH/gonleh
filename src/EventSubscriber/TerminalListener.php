<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Terminal;
use App\Service\TerminalService;
use App\Service\HistoryService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class TerminalListener implements EventSubscriber
{
    private $terminalService;
    public function __construct(
        TerminalService $terminalService
    )
    {

        $this->terminalService  = $terminalService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postLoad,
        ];
    }

    public function postLoad(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Terminal) {
            return;
        }

        $entity->setOccupiedPlaces($this->terminalService->genereteOccupiedPlaces($entity));
        $entity->setOccupiedPlacesRate($this->terminalService->genereteOccupiedPlacesRate($entity));
    }
}