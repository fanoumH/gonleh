<?php

namespace App\EventSubscriber;

use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\EIR;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EIRListener implements EventSubscriber
{
    public function __construct(
        private Security $security
    )
    {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof EIR) {
            return;
        }

        $theContainer = $entity->getContainer();
        $theGetin = $entity->getContainer()->getTerminalGetin();

        $theGetinContainers = $theGetin->getContainers();

        $keyy = null;

        foreach ($theGetinContainers as $key => $oneContainer) {
            if($oneContainer->getId() === $theContainer->getId()){
                $keyy = $key + 1;
            }
        }
        
        $refGetin = $theGetin->getRefGetin();
        $refEIR = str_replace("GI", "EIR-", $refGetin);
        $refEIR .= $keyy . "-" . (new \DateTime())->format("d-m-Y") ;
        // dd($refEIR);

        $entity->setRefEIR($refEIR);

        $currentUser = $this->security->getUser();

        $entity->setCreatorUser($currentUser);
        
    }


}