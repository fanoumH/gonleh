<?php

namespace App\EventSubscriber;

use App\Entity\BasicRate;
use App\Entity\SpecificRatetransport;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Ramsey\Uuid\Uuid;

class SpecificRateTransportListener implements EventSubscriber
{
    public function __construct()
    {

    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof SpecificRatetransport) {
            return;
        }

        $entity->setReference("TST_". (new \DateTime('now'))->format("YmdHsi"));
    }
}