<?php

namespace App\EventSubscriber;

use App\Entity\TerminalGetOut;
use App\Service\TerminalGetOutService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class TerminalGetOutListener implements EventSubscriber
{
    public function __construct(
        private TerminalGetOutService $terminalGetOutService
    ){}
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::postPersist,
            Events::preRemove,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof TerminalGetOut) {
            return;
        }

        $entity->setRefGetOut($this->terminalGetOutService->generateRefGetOut());
    }

    public function postPersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof TerminalGetOut) {
            return;
        }

        foreach($entity->getContainers() as $container){
            $container->setIsGetOut(true);
            // $entityManager->persist($container);
        }

        // $entityManager->flush();
    }

    public function preRemove(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof TerminalGetOut) {
            return;
        }
        
        foreach($entity->getContainers() as $container){
            $container->setIsGetOut(null);
            $container->setTerminalGetOut(null);
        }

    }

}