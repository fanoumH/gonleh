<?php

namespace App\EventSubscriber;

use App\Entity\BasicRate;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Ramsey\Uuid\Uuid;

class BasicRateListener implements EventSubscriber
{
    public function __construct()
    {

    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof BasicRate) {
            return;
        }

        $entity->setReference("TB_". (new \DateTime('now'))->format("YmdHsi"));
    }
}