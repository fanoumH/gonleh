<?php

namespace App\EventSubscriber;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class JWTCreatedListener
{
    public function __construct(private RequestStack $requestStack,private RouterInterface $router)
    {
    }

    public function onJWTCreated(JWTCreatedEvent $event){
        $request = $this->requestStack->getCurrentRequest();
//        dd($this->router->generate($request->attributes->get('_route')));
        //Check if route is
        if(
            $this->router->generate($request->attributes->get('_route'))  === "/api/logout"
        ){
            $expiration = new \DateTime('+1 day');
            $expiration->setTime(2, 0, 0);
            $payload = $event->getData();
            $payload['username'] = "deconnexion".random_int(1,80);
            $payload['exp'] = $expiration->getTimestamp();
            $event->setData($payload);
        }

    }
}