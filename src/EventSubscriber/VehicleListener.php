<?php

namespace App\EventSubscriber;

use App\Entity\CouplingHistory;
use App\Entity\Vehicle;
use App\Entity\Coupling;
use Doctrine\ORM\Events;
use App\Service\HistoryService;
use App\Service\CouplingService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Repository\CouplingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class VehicleListener implements EventSubscriber
{
    private $couplingRepository;

    public function __construct(
        CouplingRepository $couplingRepository
    )
    {

        $this->couplingRepository  = $couplingRepository;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::preRemove,
            Events::postLoad,
        ];
    }

    public function preRemove(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Vehicle) {
            return;
        }

        $semis = $this->couplingRepository->findBy(["Semi" => $entity]) ;

        foreach ($semis as $semi) {
            $entityManager->remove($semi);
        }

        $trailers = $this->couplingRepository->findBy(["Trailer" => $entity]) ;
        
        foreach ($trailers as $trailer) {
            $entityManager->remove($trailer);
        }

    }

    public function postLoad(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Vehicle) {
            return;
        }

        $couple = $this->couplingRepository->findOneBy(["Semi" => $entity]);
        $trailer = null;
        $coupleId = null;

        if($couple){
            $trailer = $couple->getTrailer();
            $coupleId = $couple->getId();
        }

        $couple = $this->couplingRepository->findOneBy(["Trailer" => $entity]);
        
        if($couple){
            $coupleId = $couple->getId();
        }
        
        $entity->setTrailerData($trailer);
        $entity->setCouplingId($coupleId);
    }
}