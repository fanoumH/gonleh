<?php

namespace App\EventSubscriber;

use App\Entity\CouplingHistory;
use App\Entity\Vehicle;
use App\Entity\Coupling;
use Doctrine\ORM\Events;
use App\Service\HistoryService;
use App\Service\CouplingService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CouplingListener implements EventSubscriber
{
    private $couplingService;
    private $couplingHistory = null;
    private $coupling = null;
    public function __construct(
        CouplingService $couplingService
    )
    {

        $this->couplingService  = $couplingService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preRemove,
            Events::postFlush,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Coupling && !$entity instanceof CouplingHistory) {
            return;
        }

        
        if($entity instanceof Coupling){
            $entity->setCouplingNumber($this->couplingService->generateCouplingNumber($entity));
            $entity->getSemi()->setHasTrailer(true)->setState(Vehicle::STATE_COUPLED);
            $entity->getTrailer()->setIsPulled(true)->setState(Vehicle::STATE_COUPLED);

            $couplingHistory = new CouplingHistory();
            $couplingHistory->setCouplingId($entity->getId());
            $couplingHistory->setCouplingReference($entity->getCouplingNumber());
            $couplingHistory->setSemiId($entity->getSemi()->getId());
            $couplingHistory->setTrailerId($entity->getTrailer()->getId());
            
            $entityManager->persist($couplingHistory);

            $this->coupling = $entity;
            $this->couplingHistory = $couplingHistory;
        }
        
    }

    public function preRemove(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Coupling) {
            return;
        }
        
        $entity->getSemi()->setHasTrailer(false)->setState(Vehicle::STATE_AVAILABLE);
        $entity->getTrailer()->setIsPulled(false)->setState(Vehicle::STATE_AVAILABLE);

    }

    public function postFlush(PostFlushEventArgs $args): void{
        $entityManager = $args->getObjectManager();

        if($this->couplingHistory && $this->coupling && $this->couplingHistory->getCouplingId() === null){
            $this->couplingHistory->setCouplingId($this->coupling->getId());
            $entityManager->flush();
        }

    }
    // public function preUpdate(PreUpdateEventArgs $args): void
    // {
    //     $entity = $args->getObject();
        
    //     /**
    //      * @var EntityManagerInterface
    //      */
    //     $entityManager = $args->getObjectManager();

    //     if (!$entity instanceof Coupling) {
    //         return;
    //     }

    //     $changeSets = $args->getEntityChangeSet() ;

    //     if(array_key_exists("Semi", $changeSets)){
    //         /**
    //          * @var Vehicle
    //          */
    //         $oldSemiValue = $args->getOldValue("Semi");
    //         $oldSemiValue->setHasTrailer(0);

    //         $newSemiValue = $args->getNewValue("Semi");
    //         $newSemiValue->setHasTrailer(0);
    //     }
        
    //     if(array_key_exists("Trailer", $changeSets)){
    //         /**
    //          * @var Vehicle
    //          */
    //         $oldTrailerValue = $args->getOldValue("Trailer");
    //         $oldTrailerValue->setIsPulled(0);

    //         $newTrailerValue = $args->getNewValue("Trailer");
    //         $newTrailerValue->setIsPulled(1);

    //     }

    // }

}