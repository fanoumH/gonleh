<?php

namespace App\EventSubscriber;

use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\RentalOrder;
use App\Service\RentalOrderService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RentalOrderListener implements EventSubscriber
{
    private $rentalOrderService;
    public function __construct(
        RentalOrderService $rentalOrderService
    )
    {

        $this->rentalOrderService  = $rentalOrderService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof RentalOrder) {
            return;
        }

        $entity->setCode($this->rentalOrderService->generateRentalOrderCode());

    }


}