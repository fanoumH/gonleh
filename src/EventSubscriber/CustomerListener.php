<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Company;
use App\Entity\Customer;
use App\Service\CustomerService;
use App\Service\HistoryService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CustomerListener implements EventSubscriber
{
    private $customerService;
    public function __construct(
        CustomerService $customerService
    )
    {

        $this->customerService  = $customerService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preRemove,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Customer) {
            return;
        }

        $entity->setRefCustomer($this->customerService->generateRefCustomer());
    }

    public function preRemove(PreRemoveEventArgs $args)
    {
        $entity        = $args->getObject();
        if ($entity instanceof Customer) {
            
            if($entity->getCustomerIdentification()){
                $entity->getCustomerIdentification()->setDeletedAt((new \DateTime(date("Y-m-d"))));
            }
            if($entity->getCustomerBank()){
                $entity->getCustomerBank()->setDeletedAt((new \DateTime(date("Y-m-d"))));
            }
            if($entity->getCustomerPrice()){
                $entity->getCustomerPrice()->setDeletedAt((new \DateTime(date("Y-m-d"))));
            }
            if($entity->getCustomerFreeField()){
                $entity->getCustomerFreeField()->setDeletedAt((new \DateTime(date("Y-m-d"))));
            }
            if($entity->getSolvency()){
                $entity->getSolvency()->setDeletedAt((new \DateTime(date("Y-m-d"))));
            }
        }
    }
}