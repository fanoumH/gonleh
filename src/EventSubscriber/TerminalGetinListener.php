<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\TerminalGetin;
use App\Service\TerminalGetinService;
use App\Service\HistoryService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class TerminalGetinListener implements EventSubscriber
{
    private $terminalGetinService;
    public function __construct(
        TerminalGetinService $terminalGetinService
    )
    {

        $this->terminalGetinService  = $terminalGetinService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof TerminalGetin) {
            return;
        }

        $entity->setRefGetin($this->terminalGetinService->generateRefGetin());
    }
}