<?php

namespace App\EventSubscriber;

use App\Entity\Booking;
use App\Entity\Customer;
use Doctrine\ORM\Events;
use App\Service\BookingService;
use App\Service\HistoryService;
use App\Service\CustomerService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BookingListener implements EventSubscriber
{
    private $bookingService;
    public function __construct(
        BookingService $bookingService
    )
    {

        $this->bookingService  = $bookingService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::postUpdate,
            Events::postPersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Booking) {
            return;
        }

        // $entity->setRefBooking($this->bookingService->generateRefBooking());

        foreach($entity->getContainers() as $container){
            $container->setReservedStatus(true);
        }
    }

    public function postUpdate(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Booking) {
            return;
        }
        
        if($entity->isIsCancelled()){
            foreach($entity->getContainers() as $container){
                $container->setReservedStatus(false);
                $entity->removeContainer($container);
            }
        }

        $entityManager->flush();
    }

    public function postPersist(PostPersistEventArgs $args):void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Booking) {
            return;
        }

        $containers = $entity->getContainers();

        if(count($containers) > 0){
            $firstContainer = $containers[0];
            $terminalGetin = $firstContainer->getTerminalGetin();
            $terminal = $terminalGetin->getTerminal();
            
            $entity->setTerminal($terminal);
        }
        $entityManager->flush();
    }


}