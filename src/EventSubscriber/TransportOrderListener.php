<?php

namespace App\EventSubscriber;

use Doctrine\ORM\Events;
use App\Entity\TransportOrder;
use Doctrine\Common\EventSubscriber;
use App\Entity\HistoryTransportOrder;
use App\Service\TransportOrderService;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TransportOrderListener implements EventSubscriber
{
    private $historyTO = null;
    private $transportOrderService;
    public function __construct(
        TransportOrderService $transportOrderService
    )
    {

        $this->transportOrderService  = $transportOrderService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postUpdate,
        ];
    }

    public function prePersist(PrePersistEventArgs  $args): void{
        
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof TransportOrder) {
            return;
        }

        $entity->setCode($this->transportOrderService->generateTransportOrderCode());

        $historyTO = new HistoryTransportOrder();
        $historyTO->setTransportOrder($entity);
        $historyTO->setOtStatus($entity->getOtStatus() ?? null);
        $historyTO->setDateChange((new \DateTime(date("Y-m-d"))));
        $entityManager->persist($historyTO);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof TransportOrder) {
            return;
        }
        if($args->hasChangedField("otStatus")){
            $historyTO = new HistoryTransportOrder();
            $historyTO->setTransportOrder($entity);
            // $historyTO->setOtStatus($entity->getOtStatus() ?? null);
            $historyTO->setOtStatus($args->getNewValue("otStatus"));
            $historyTO->setDateChange((new \DateTime(date("Y-m-d"))));
            $this->historyTO = $historyTO;
        }
    }

    public function postUpdate(PostUpdateEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof TransportOrder) {
            return;
        }

        if($this->historyTO){
            $entityManager->persist($this->historyTO);
            $entityManager->flush();
            $this->historyTO = null;
        }
    }

}