<?php

namespace App\EventSubscriber;

use App\Entity\TerminalGetOut;
use App\Entity\Transaction;
use App\Service\TerminalGetOutService;
use App\Service\TransactionService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class TransactionListener implements EventSubscriber
{
    public function __construct(
        private TransactionService $transactionService
    ){}
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Transaction) {
            return;
        }
        $entity->setAccountCode($this->transactionService->generateAccountCode($entity->getDepartment()->getAbbreviation(), $entity));
    }
}