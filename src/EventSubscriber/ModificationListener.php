<?php

namespace App\EventSubscriber;

use App\Entity\RefreshToken;
use App\Service\HistoryService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ModificationListener implements EventSubscriber
{

    public function __construct(
        private HistoryService $historyService
    )
    {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        if($entity instanceof RefreshToken){
            return;
        }
        $this->historyService->mergeHistory("création" , $entity->toArray(), get_class($entity));
    }

    public function postUpdate(LifecycleEventArgs $args): void{
        $entity = $args->getObject();
        $this->historyService->mergeHistory("modification" , $entity->toArray(), get_class($entity));
    }
}