<?php

namespace App\EventSubscriber;

use App\Entity\RefreshToken;
use App\Entity\Role;
use App\Entity\User;
use App\Repository\RoleRepository;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserPasswordListener implements EventSubscriber
{
    private $passwordHasher;
    private $requestStack;
    private $roleRepository;

    private $entityManagerInterface;

    public function __construct(
        UserPasswordHasherInterface $passwordHasher,
        RequestStack $requestStack,
        RoleRepository $roleRepository
    )
    {
        $this->passwordHasher = $passwordHasher;
        $this->requestStack = $requestStack;
        $this->roleRepository = $roleRepository;
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (!$entity instanceof User) {
            return;
        }

        if($this->requestStack->getCurrentRequest()->attributes->get("_route") !== "_api_/users/{id}{._format}_patch"){
            return;
        }
        $this->hashPassword($entity);
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof User) {
            return;
        }

        $this->hashPassword($entity);
        $entity->setIsActive(true);

        $roleAdmin = $this->roleRepository->findOneBy(['name' => Role::NAME_ADMIN]);

        if($roleAdmin){
            $entity->setCapabilities($roleAdmin);
        }
    }

    public function postUpdate(PostUpdateEventArgs $args){
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof User) {
            return;
        }

        $refreshTokens = $entityManager->getRepository(RefreshToken::class)->findBy(["username" => $entity->getEmail()]);
        foreach($refreshTokens as $refreshToken){
            $entityManager->remove($refreshToken);
        }

        $entityManager->flush();
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::preUpdate,
            Events::prePersist,
            Events::postUpdate,
        ];
    }

    private function hashPassword(User $user): void
    {
        $plainPassword = $user->getPassword();
        $hashedPassword = $this->passwordHasher->hashPassword($user, $plainPassword);
        $user->setPassword($hashedPassword);
    }
}
