<?php

namespace App\DTO;

use App\Entity\Besoin;
use App\Entity\TerminalGetin;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
class TerminalGetinDTO
{
    #[Assert\NotNull]
    #[Groups(groups: ['getin:read','getin:update','getin:create'])]
    public array $container;
}