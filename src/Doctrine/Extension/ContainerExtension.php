<?php

namespace App\Doctrine\Extension;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\Booking;
use App\Entity\Container;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;

final class ContainerExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{

    public function __construct(private readonly Security $security)
    {
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, Operation $operation = null, array $context = []): void
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        $concernedEntities = [
            Container::class,
            Booking::class,
        ];

        if ( !in_array($resourceClass, $concernedEntities) || null === $user = $this->security->getUser()) {
            return;
        }


        $isSuperAdmin = $this->security->isGranted('ROLE_SUPER_ADMIN', $user) ;

        if($isSuperAdmin){
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];

        $terminalId = $user?->getTerminal()?->getId();

        if(!$terminalId){
            return;
        }
        
        switch ($resourceClass) {
            case Container::class:
                
                $queryBuilder->leftJoin($rootAlias.'.terminalGetin', 'getin');
                $queryBuilder->leftJoin('getin.terminal','terminal');
                $queryBuilder->andWhere('terminal = :trm');
                $queryBuilder->setParameter('trm', $terminalId);

                break;

            case Booking::class:

                $queryBuilder->andWhere($rootAlias . '.terminal = :trm');
                $queryBuilder->setParameter('trm', $terminalId);

                break;
               
            default:
                # code...
                break;
        }

        
    }
}