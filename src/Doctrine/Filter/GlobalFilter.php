<?php

namespace App\Doctrine\Filter;

use App\Entity\Terminal;
use App\Entity\Container;
use App\Entity\TerminalGetin;
use ApiPlatform\OpenApi\Model\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class GlobalFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        return "";
    }
}