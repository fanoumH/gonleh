<?php

namespace App\Doctrine\Filter;

use App\Entity\Terminal;
use App\Entity\Container;
use App\Entity\TerminalGetin;
use ApiPlatform\OpenApi\Model\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class TerminalFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        $desiredTerminalId = $this->getParameter('terminal_id_of_user');
        $isSuperAdmin = $this->getParameter('is_super_admin');

        switch ($targetEntity->getReflectionClass()->name) {
            
            case Terminal::class :
                
                if($isSuperAdmin === "'1'" || $desiredTerminalId === "'0'"){
                    return '';
                }
        
                return $targetTableAlias.".id=".$desiredTerminalId;
            
            // case Container::class :
            //     dd($targetEntity->getAssociationMapping('terminalGetin'));
            
            default:
               return '';
        }

        
    }
}