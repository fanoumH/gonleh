<?php

namespace App\Repository;

use App\Entity\SubContractorType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SubContractorType>
 *
 * @method SubContractorType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubContractorType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubContractorType[]    findAll()
 * @method SubContractorType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubContractorTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubContractorType::class);
    }

//    /**
//     * @return SubContractorType[] Returns an array of SubContractorType objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SubContractorType
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
