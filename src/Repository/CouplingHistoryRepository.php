<?php

namespace App\Repository;

use App\Entity\CouplingHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CouplingHistory>
 *
 * @method CouplingHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CouplingHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CouplingHistory[]    findAll()
 * @method CouplingHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouplingHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CouplingHistory::class);
    }

//    /**
//     * @return CouplingHistory[] Returns an array of CouplingHistory objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CouplingHistory
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
