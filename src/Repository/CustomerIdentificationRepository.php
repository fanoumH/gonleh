<?php

namespace App\Repository;

use App\Entity\CustomerIdentification;
use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CustomerIdentification>
 *
 * @method CustomerIdentification|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerIdentification|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerIdentification[]    findAll()
 * @method CustomerIdentification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerIdentificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerIdentification::class);
    }

    public function save(CustomerIdentification $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return CustomerIdentification[] Returns an array of CustomerIdentification objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CustomerIdentification
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
