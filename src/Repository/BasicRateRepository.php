<?php

namespace App\Repository;

use App\Entity\BasicRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BasicRate>
 *
 * @method BasicRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method BasicRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method BasicRate[]    findAll()
 * @method BasicRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasicRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BasicRate::class);
    }

//    /**
//     * @return BasicRate[] Returns an array of BasicRate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BasicRate
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
