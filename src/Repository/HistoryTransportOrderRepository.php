<?php

namespace App\Repository;

use App\Entity\HistoryTransportOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HistoryTransportOrder>
 *
 * @method HistoryTransportOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoryTransportOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoryTransportOrder[]    findAll()
 * @method HistoryTransportOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryTransportOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoryTransportOrder::class);
    }

//    /**
//     * @return HistoryTransportOrder[] Returns an array of HistoryTransportOrder objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?HistoryTransportOrder
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
