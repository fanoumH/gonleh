<?php

namespace App\Repository;

use App\Entity\ServiceRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ServiceRate>
 *
 * @method ServiceRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceRate[]    findAll()
 * @method ServiceRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiceRate::class);
    }

//    /**
//     * @return ServiceRate[] Returns an array of ServiceRate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ServiceRate
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
