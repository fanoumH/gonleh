<?php

namespace App\Repository;

use App\Entity\PlaceFunction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PlaceFunction>
 *
 * @method PlaceFunction|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceFunction|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceFunction[]    findAll()
 * @method PlaceFunction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceFunctionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaceFunction::class);
    }

    /**
     * @param PlaceFunction $placeFunction
     * @return bool
     * @throws \Doctrine\DBAL\Exception
     */
    public function deleteAssociation(PlaceFunction $placeFunction)
    {

        $em = $this->getEntityManager();

        $responsePlace   = false;
        $sqlPlace = 'DELETE FROM place_place_function WHERE place_function_id = :placeFunctionId';
        $stmtPlace = $em->getConnection()->prepare($sqlPlace);
        if($stmtPlace->executeQuery(['placeFunctionId' => $placeFunction->getId()])) {
            $responsePlace = true;
        }
        return $responsePlace;
    }

    /**
     * @param PlaceFunction $placeFunction
     * @return float|int|mixed|string
     */
    public function softDeleteItem(PlaceFunction $placeFunction)
    {
        $now    = new \Datetime();
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            UPDATE App\Entity\PlaceFunction pf 
            SET pf.deletedAt = :now
            WHERE pf.id = :placeFunctionId
        ");
        $query->setParameter('now', $now->format('Y-m-d H:i:s'));
        $query->setParameter('placeFunctionId', $placeFunction->getId());
        return $query->execute();
    }

//    /**
//     * @return PlaceFunction[] Returns an array of PlaceFunction objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PlaceFunction
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
