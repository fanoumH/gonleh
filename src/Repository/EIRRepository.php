<?php

namespace App\Repository;

use App\Entity\EIR;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EIR>
 *
 * @method EIR|null find($id, $lockMode = null, $lockVersion = null)
 * @method EIR|null findOneBy(array $criteria, array $orderBy = null)
 * @method EIR[]    findAll()
 * @method EIR[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EIRRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EIR::class);
    }

//    /**
//     * @return EIR[] Returns an array of EIR objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EIR
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
