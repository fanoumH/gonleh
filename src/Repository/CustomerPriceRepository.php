<?php

namespace App\Repository;

use App\Entity\CustomerPrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CustomerPrice>
 *
 * @method CustomerPrice|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerPrice|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerPrice[]    findAll()
 * @method CustomerPrice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerPrice::class);
    }

//    /**
//     * @return CustomerPrice[] Returns an array of CustomerPrice objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CustomerPrice
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
