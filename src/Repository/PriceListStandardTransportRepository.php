<?php

namespace App\Repository;

use App\Entity\PriceListStandardTransport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PriceListStandardTransport>
 *
 * @method PriceListStandardTransport|null find($id, $lockMode = null, $lockVersion = null)
 * @method PriceListStandardTransport|null findOneBy(array $criteria, array $orderBy = null)
 * @method PriceListStandardTransport[]    findAll()
 * @method PriceListStandardTransport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceListStandardTransportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PriceListStandardTransport::class);
    }

//    /**
//     * @return PriceListStandardTransport[] Returns an array of PriceListStandardTransport objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PriceListStandardTransport
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
