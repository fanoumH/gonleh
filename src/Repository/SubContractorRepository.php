<?php

namespace App\Repository;

use App\Entity\SubContractor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SubContractor>
 *
 * @method SubContractor|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubContractor|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubContractor[]    findAll()
 * @method SubContractor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubContractorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubContractor::class);
    }

//    /**
//     * @return SubContractor[] Returns an array of SubContractor objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SubContractor
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
