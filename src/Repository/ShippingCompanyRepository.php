<?php

namespace App\Repository;

use App\Entity\ShippingCompany;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ShippingCompany>
 *
 * @method ShippingCompany|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShippingCompany|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShippingCompany[]    findAll()
 * @method ShippingCompany[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShippingCompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShippingCompany::class);
    }

//    /**
//     * @return ShippingCompany[] Returns an array of ShippingCompany objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ShippingCompany
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
