<?php

namespace App\Repository;

use App\Entity\Proforma;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Proforma>
 *
 * @method Proforma|null find($id, $lockMode = null, $lockVersion = null)
 * @method Proforma|null findOneBy(array $criteria, array $orderBy = null)
 * @method Proforma[]    findAll()
 * @method Proforma[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProformaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Proforma::class);
    }

//    /**
//     * @return Proforma[] Returns an array of Proforma objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Proforma
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
