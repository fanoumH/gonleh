<?php

namespace App\Repository;

use App\Entity\BankContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BankContact>
 *
 * @method BankContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankContact[]    findAll()
 * @method BankContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankContact::class);
    }

//    /**
//     * @return BankContact[] Returns an array of BankContact objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BankContact
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
