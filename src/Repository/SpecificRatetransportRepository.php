<?php

namespace App\Repository;

use App\Entity\SpecificRatetransport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SpecificRatetransport>
 *
 * @method SpecificRatetransport|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpecificRatetransport|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpecificRatetransport[]    findAll()
 * @method SpecificRatetransport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecificRatetransportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpecificRatetransport::class);
    }

//    /**
//     * @return SpecificRatetransport[] Returns an array of SpecificRatetransport objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SpecificRatetransport
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
