<?php

namespace App\Repository;

use App\Entity\StaffPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StaffPost>
 *
 * @method StaffPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method StaffPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method StaffPost[]    findAll()
 * @method StaffPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StaffPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StaffPost::class);
    }

//    /**
//     * @return StaffPost[] Returns an array of StaffPost objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?StaffPost
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
