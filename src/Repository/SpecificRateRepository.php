<?php

namespace App\Repository;

use App\Entity\SpecificRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SpecificRate>
 *
 * @method SpecificRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpecificRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpecificRate[]    findAll()
 * @method SpecificRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecificRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpecificRate::class);
    }

//    /**
//     * @return SpecificRate[] Returns an array of SpecificRate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SpecificRate
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
