<?php

namespace App\Repository;

use App\Entity\CustomerFreeField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CustomerFreeField>
 *
 * @method CustomerFreeField|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerFreeField|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerFreeField[]    findAll()
 * @method CustomerFreeField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerFreeFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerFreeField::class);
    }

//    /**
//     * @return CustomerFreeField[] Returns an array of CustomerFreeField objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CustomerFreeField
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
