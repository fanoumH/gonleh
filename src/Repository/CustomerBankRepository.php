<?php

namespace App\Repository;

use App\Entity\CustomerBank;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CustomerBank>
 *
 * @method CustomerBank|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerBank|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerBank[]    findAll()
 * @method CustomerBank[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerBankRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerBank::class);
    }

//    /**
//     * @return CustomerBank[] Returns an array of CustomerBank objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CustomerBank
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
