<?php

namespace App\Repository;

use App\Entity\OperationTypeRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OperationTypeRate>
 *
 * @method OperationTypeRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method OperationTypeRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method OperationTypeRate[]    findAll()
 * @method OperationTypeRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationTypeRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OperationTypeRate::class);
    }

//    /**
//     * @return OperationTypeRate[] Returns an array of OperationTypeRate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?OperationTypeRate
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
