<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Terminal;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Terminal>
 *
 * @method Terminal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Terminal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Terminal[]    findAll()
 * @method Terminal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TerminalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Terminal::class);

        /**
         * @var User
         */
        $user = $security->getUser();

        if($user){
            
            $isSuperAdmin = $security->isGranted('ROLE_SUPER_ADMIN', $user) ;
            
            $filter = $this->getEntityManager()->getFilters()->enable('terminal_for_user');
            $filter->setParameter("terminal_id_of_user", $user->getTerminal() ? (string) $user->getTerminal()->getId() : (string) 0);
            $filter->setParameter("is_super_admin", $isSuperAdmin);
        }
    }

//    /**
//     * @return Terminal[] Returns an array of Terminal objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Terminal
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function findCapacityTotalOfAll(): array
   {
       $result =  $this->createQueryBuilder('t')
           ->select(
            'SUM(t.capacity) as capacityOfAll'
           )
           ->getQuery()
           ->getResult()
       ;

       return $result;
   }

}
