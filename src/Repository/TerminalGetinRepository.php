<?php

namespace App\Repository;

use App\Entity\TerminalGetin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TerminalGetin>
 *
 * @method TerminalGetin|null find($id, $lockMode = null, $lockVersion = null)
 * @method TerminalGetin|null findOneBy(array $criteria, array $orderBy = null)
 * @method TerminalGetin[]    findAll()
 * @method TerminalGetin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TerminalGetinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TerminalGetin::class);
    }

//    /**
//     * @return TerminalGetin[] Returns an array of TerminalGetin objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TerminalGetin
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
