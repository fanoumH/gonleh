<?php

namespace App\Repository;

use App\Entity\Place;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Place>
 *
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    /**
     * @param Place $place
     * @return bool
     * @throws \Doctrine\DBAL\Exception
     */
    public function deleteAssociation(Place $place)
    {

        $em = $this->getEntityManager();

        $responsePlaceFunction   = false;
        $sqlPlaceFunction = 'DELETE FROM place_place_function WHERE place_id = :placeId';
        $stmtPlaceFunction = $em->getConnection()->prepare($sqlPlaceFunction);
        if($stmtPlaceFunction->executeQuery(['placeId' => $place->getId()])) {
            $responsePlaceFunction = true;
        }
        return $responsePlaceFunction;
    }

    /**
     * @param Place $place
     * @return float|int|mixed|string
     */
    public function softDeleteItem(Place $place)
    {
        $now    = new \Datetime();
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            UPDATE App\Entity\Place p 
            SET p.deletedAt = :now
            WHERE p.id = :placeId
        ");
        $query->setParameter('now', $now->format('Y-m-d H:i:s'));
        $query->setParameter('placeId', $place->getId());
        return $query->execute();
    }
//    /**
//     * @return Place[] Returns an array of Place objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Place
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
