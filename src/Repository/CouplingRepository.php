<?php

namespace App\Repository;

use App\Entity\Coupling;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Coupling>
 *
 * @method Coupling|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coupling|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coupling[]    findAll()
 * @method Coupling[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouplingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Coupling::class);
    }

//    /**
//     * @return Coupling[] Returns an array of Coupling objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Coupling
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
