<?php

namespace App\Repository;

use App\Document\HistoryAction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HistoryAction>
 *
 * @method HistoryAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoryAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoryAction[]    findAll()
 * @method HistoryAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoryAction::class);
    }

//    /**
//     * @return HistoryAction[] Returns an array of HistoryAction objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?HistoryAction
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
