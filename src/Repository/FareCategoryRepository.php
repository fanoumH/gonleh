<?php

namespace App\Repository;

use App\Entity\FareCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FareCategory>
 *
 * @method FareCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method FareCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method FareCategory[]    findAll()
 * @method FareCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FareCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FareCategory::class);
    }

//    /**
//     * @return FareCategory[] Returns an array of FareCategory objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?FareCategory
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
