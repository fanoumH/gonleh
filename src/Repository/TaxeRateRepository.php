<?php

namespace App\Repository;

use App\Entity\TaxeRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TaxeRate>
 *
 * @method TaxeRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxeRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxeRate[]    findAll()
 * @method TaxeRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxeRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxeRate::class);
    }

//    /**
//     * @return TaxeRate[] Returns an array of TaxeRate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TaxeRate
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
