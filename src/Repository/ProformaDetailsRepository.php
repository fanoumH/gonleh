<?php

namespace App\Repository;

use App\Entity\ProformaDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProformaDetails>
 *
 * @method ProformaDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProformaDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProformaDetails[]    findAll()
 * @method ProformaDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProformaDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProformaDetails::class);
    }

//    /**
//     * @return ProformaDetails[] Returns an array of ProformaDetails objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ProformaDetails
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
