<?php

namespace App\Repository;

use App\Entity\FolderTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FolderTransaction>
 *
 * @method FolderTransaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method FolderTransaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method FolderTransaction[]    findAll()
 * @method FolderTransaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FolderTransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FolderTransaction::class);
    }

//    /**
//     * @return FolderTransaction[] Returns an array of FolderTransaction objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?FolderTransaction
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
