<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Terminal;
use App\Entity\Container;
use App\Entity\Role;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Container>
 *
 * @method Container|null find($id, $lockMode = null, $lockVersion = null)
 * @method Container|null findOneBy(array $criteria, array $orderBy = null)
 * @method Container[]    findAll()
 * @method Container[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContainerRepository extends ServiceEntityRepository
{
    private $security;
    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Container::class);

        $this->security = $security;
    }

//    /**
//     * @return Container[] Returns an array of Container objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Container
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

   public function findOccuping(Terminal $terminal): array
   {
       $result =  $this->createQueryBuilder('cnt')
        //    ->select(
        //      'cnt.id as id',
        //      'cnt.reservedStatus as reservedStatus',
        //      'cnt.isGetOut as isGetOut',
        //      'cnt.typeContainer as typeContainer',
        //    )
           ->leftJoin('cnt.terminalGetin', 'getin')
           ->leftJoin('getin.terminal','terminal')
        //    ->addSelect(
        //       'getin.id as getinID',
        //       'terminal.id as terminalID'
        //    )
           ->andWhere('terminal = :trm')
           ->andWhere('(cnt.isGetOut is null or cnt.isGetOut = false)')
           ->andWhere('(cnt.containerStatusAble is not null and cnt.containerStatusAble != false)')
           ->setParameter('trm', $terminal)
           ->orderBy('cnt.id', 'ASC')
           ->getQuery()
           ->getResult()
       ;

       return $result;
   }

   public function findOccupingCount(Terminal $terminal): array
   {
       $result =  $this->createQueryBuilder('cnt')
           ->select(
             'COUNT(cnt.id) AS countOfOccupied'
           )
           ->leftJoin('cnt.terminalGetin', 'getin')
           ->leftJoin('getin.terminal','terminal')
           ->andWhere('terminal = :trm')
           ->andWhere('(cnt.isGetOut is null or cnt.isGetOut = false)')
           ->andWhere('(cnt.containerStatusAble is not null and cnt.containerStatusAble != false)')
           ->setParameter('trm', $terminal)
           ->orderBy('cnt.id', 'ASC')
           ->getQuery()
           ->getResult()
       ;

       return $result;
   }

   public function findAllOccupingUnityNumber(): array
   {
       $qb =  $this->createQueryBuilder('cnt')
           ->leftJoin('cnt.containerType', 'cntType')
           ->leftJoin('cnt.terminalGetin', 'getin')
           ->leftJoin('getin.terminal','terminal')
           ->select(
            'SUM(cntType.unite)/20 as occupiedUnities'
           )
           ->andWhere('((cnt.isGetOut is null or cnt.isGetOut = false) )')
           ->andWhere('(cnt.containerStatusAble is not null and cnt.containerStatusAble != false)')
        ;

        // $this->getEntityManager()->getRepository();
        /**
         * @var User
         */
        $user = $this->security->getUser() ;

        if($user->getCapabilities()->getName() !== Role::NAME_SUPER_ADMIN && $user->getTerminal() ){
            $qb->andWhere('terminal = :trm');
            $qb->setParameter('trm', $user->getTerminal());
        }

        $qb->orderBy('cnt.id', 'ASC')
        ;

       $result = $qb->getQuery()->getResult();

       return $result;
   }


}
