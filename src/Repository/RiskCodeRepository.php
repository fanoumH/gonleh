<?php

namespace App\Repository;

use App\Entity\RiskCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RiskCode>
 *
 * @method RiskCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method RiskCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method RiskCode[]    findAll()
 * @method RiskCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RiskCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RiskCode::class);
    }

//    /**
//     * @return RiskCode[] Returns an array of RiskCode objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RiskCode
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
