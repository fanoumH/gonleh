<?php

namespace App\Repository;

use App\Entity\OtherCost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OtherCost>
 *
 * @method OtherCost|null find($id, $lockMode = null, $lockVersion = null)
 * @method OtherCost|null findOneBy(array $criteria, array $orderBy = null)
 * @method OtherCost[]    findAll()
 * @method OtherCost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OtherCostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OtherCost::class);
    }

//    /**
//     * @return OtherCost[] Returns an array of OtherCost objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?OtherCost
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
