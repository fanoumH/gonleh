<?php

namespace App\Repository;

use App\Entity\TerminalGetOut;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TerminalGetOut>
 *
 * @method TerminalGetOut|null find($id, $lockMode = null, $lockVersion = null)
 * @method TerminalGetOut|null findOneBy(array $criteria, array $orderBy = null)
 * @method TerminalGetOut[]    findAll()
 * @method TerminalGetOut[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TerminalGetOutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TerminalGetOut::class);
    }

//    /**
//     * @return TerminalGetOut[] Returns an array of TerminalGetOut objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TerminalGetOut
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
