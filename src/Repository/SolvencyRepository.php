<?php

namespace App\Repository;

use App\Entity\Solvency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Solvency>
 *
 * @method Solvency|null find($id, $lockMode = null, $lockVersion = null)
 * @method Solvency|null findOneBy(array $criteria, array $orderBy = null)
 * @method Solvency[]    findAll()
 * @method Solvency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SolvencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Solvency::class);
    }

//    /**
//     * @return Solvency[] Returns an array of Solvency objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Solvency
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
