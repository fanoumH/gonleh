<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\UserRepository;
use App\Repository\VehicleBrandRepository;
use App\Repository\VehicleModelRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ModelsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        VehicleModelRepository $vehicleModelRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $models = $vehicleModelRepository->findAll();
        
        $filename = "modeles.xlsx";
        $worksheet = "modeles";

        $sheetData = [];

        $heads = [
            "Marque",
            "Modèle",
        ];
        $sheetData[] = $heads;

        foreach ($models as $model) {
            $row = [];
            $row[] = $model->getVehicleBrand()?->getName();
            $row[] = $model->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
