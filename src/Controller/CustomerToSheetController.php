<?php

namespace App\Controller;

use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\CustomerRepository;
use App\Repository\StaffRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustomerToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        CustomerRepository $customerRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $customers = $customerRepository->findAll();
        
        $filename = "clients.xlsx";
        $worksheet = "clients";

        $sheetData = [];

        $heads = [
            'Référence client',
            'Intitulé client',
            'Email',
            'Adresse',
            'Banque',
        ];
        $sheetData[] = $heads;

        foreach ($customers as $one) {
            $row = [];
            $row[] = $one->getRefCustomer();
            $row[] = $one->getCustomerIdentification()?->getCustomerTitle();
            $row[] = $one->getCustomerIdentification()?->getEmail();
            $row[] = $one->getCustomerIdentification()?->getAddress();
            $row[] = $one->getCustomerBank()?->getBank();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
