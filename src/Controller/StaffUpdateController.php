<?php

namespace App\Controller;

use App\Entity\CustomerFreeField;
use App\Entity\Staff;
use App\Entity\User;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use App\Repository\StaffRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;

class StaffUpdateController extends AbstractController
{
    public function __invoke(
        Request $request,
        StaffPostRepository $staffPostRepository,
        CenterRepository $centerRepository,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):Staff
    {
        $staff = $request->attributes->get('data');

        if(!($staff instanceof  Staff)){
            throw new \RuntimeException('Staff attendu');
        }
        // parse_str($request->getContent(), $data);

        $data = [];

        $firstexp = explode("Content-Disposition: form-data;", $request->getContent());

        $filteredFirstexp = array_filter($firstexp, function($val){
            return str_contains($val, 'name');
        });

        foreach ($filteredFirstexp as $exp) {
            $secondexp = explode("\r\n", $exp);
            $dataKey = substr(explode('="', trim($secondexp[0]))[1], 0, -1);
            $dataValue = $secondexp[2];

            $data[$dataKey] = $dataValue;
        }

        dd($request->files);
        
        foreach ($data as $key => $value) {
            if( in_array($key, ['birthday', 'hiringDate', 'idNumberDelivery', 'driverLicenceExpiration']) ){
                $staff->{"set" . ucfirst($key)}(new \DateTimeImmutable($value));
            }
            elseif($key === "staffPost"){
                $tabVal = explode("/", $value);
                $idStaffPost = end($tabVal);
                $staffPost = $staffPostRepository->find($idStaffPost);
                $staff->{"set" . ucfirst($key)}($staffPost);
            }
            elseif($key === "center"){
                $tabVal = explode("/", $value);
                $idCenter = end($tabVal);
                $center = $centerRepository->find($idCenter);
                $staff->{"set" . ucfirst($key)}($center);
            }
            else{
                $staff->{"set" . ucfirst($key)}($value);
            }
        }
        $staff->setProfilePicFile($request->files->get("profilePic"));
        $uploadHandler->remove($staff, 'profilePicFile');
        $uploadHandler->upload($staff, 'profilePicFile');
        $entityManagerInterface->persist($staff);
        $entityManagerInterface->flush();
        return $staff;
    }
}
