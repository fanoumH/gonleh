<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\SubContractorTypeRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SubContTypesToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        SubContractorTypeRepository $subContractorTypeRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $subContTypes = $subContractorTypeRepository->findAll();
        
        $filename = "types-sous-traitants.xlsx";
        $worksheet = "types-sous-traitants";

        $sheetData = [];

        $heads = [
            "Libllé",
            "Code",
        ];
        $sheetData[] = $heads;

        foreach ($subContTypes as $subContType) {
            $row = [];
            $row[] = $subContType->getName();
            $row[] = $subContType->getCode();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
