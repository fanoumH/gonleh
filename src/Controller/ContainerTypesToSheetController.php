<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\ContainerTypeRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContainerTypesToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        ContainerTypeRepository $containerTypeRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $contTypes = $containerTypeRepository->findAll();
        
        $filename = "types-conteneurs.xlsx";
        $worksheet = "types-conteneurs";

        $sheetData = [];

        $heads = [
            "Dimension/type",
            "Designation type",
            "Compagnie maritime",
        ];
        $sheetData[] = $heads;

        foreach ($contTypes as $contType) {
            $row = [];
            $row[] = $contType->getUnite();
            $row[] = $contType->getLabel();
            $row[] = $contType->getShippingCompany()?->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
