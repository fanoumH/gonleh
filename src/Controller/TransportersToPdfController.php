<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\TransporterRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TransportersToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        TransporterRepository $transporterRepository,
        PdfManager $pdfManager,
    )
    {
        $transporters = $transporterRepository->findAll();
        
        $template = $this->renderView('pdf/all_transporters.html.twig',[
            "transporters" => $transporters,
        ]);
        $filename = "transporteurs";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
