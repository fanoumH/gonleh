<?php

namespace App\Controller;

use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\CustomerRepository;
use App\Repository\StaffRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustomerToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        CustomerRepository $customerRepository,
        PdfManager $pdfManager,
    )
    {
        $customers = $customerRepository->findAll();
        
        $template = $this->renderView('pdf/all_customer.html.twig',[
            "customers" => $customers,
        ]);
        $filename = "clients";
        
        $x = $pdfManager->generatePdf($template, $filename, orientation: "landscape");
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
