<?php

namespace App\Controller;

use App\Entity\EIR;
use App\Entity\MissionOrder;
use App\Service\PdfManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MOPdfController extends AbstractController
{

    public function __invoke(MissionOrder $missionOrder, Request $request, PdfManager $pdfManager){
        $template = $this->renderView('pdf/order_mission.html.twig',[
            "missionOrder" => $missionOrder,
        ]);

        $vehicleNumber = "" ;
        
        if ($missionOrder->getTransportOrder()->getAffectedTruck())
        {
            $vehicleNumber = $missionOrder->getTransportOrder()->getAffectedTruck()->getRegistrationNumber();
        }
        elseif ($missionOrder->getTransportOrder()->getSubContractor())
        {
            $vehicleNumber = $missionOrder->getTransportOrder()->getSubContractorVehicleNumber();
        }

        $filename = 'Ordre-mission-' . $vehicleNumber . "-" . $missionOrder->getTransportOrder()->getCode();
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
