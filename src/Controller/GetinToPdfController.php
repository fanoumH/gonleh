<?php

namespace App\Controller;

use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetinToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        ContainerRepository $containerRepository,
        PdfManager $pdfManager,
    )
    {
        // $vehicles = $containerRepository->findBy([], [], 1000);
        $vehicles = $containerRepository->findAll();
        
        $template = $this->renderView('pdf/all_getins.html.twig',[
            "containers" => $vehicles,
        ]);
        $filename = "getins";
        
        $x = $pdfManager->generatePdf($template, $filename, orientation: "landscape");
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
