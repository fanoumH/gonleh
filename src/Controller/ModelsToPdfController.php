<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\VehicleBrandRepository;
use App\Repository\VehicleModelRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ModelsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        VehicleModelRepository $vehicleModelRepository,
        PdfManager $pdfManager,
    )
    {
        $models = $vehicleModelRepository->findAll();
        
        $template = $this->renderView('pdf/all_models.html.twig',[
            "models" => $models,
        ]);
        $filename = "modeles";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
