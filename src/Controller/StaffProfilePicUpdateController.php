<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Staff;
use App\Entity\CustomerFreeField;
use App\Repository\UserRepository;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Handler\UploadHandler;
use App\Repository\CustomerFreeFieldRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StaffProfilePicUpdateController extends AbstractController
{
    public function __invoke(
        Request $request,
        StaffPostRepository $staffPostRepository,
        CenterRepository $centerRepository,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):Staff
    {
        $staff = $request->attributes->get('data');
        if(!($staff instanceof  Staff)){
            throw new \RuntimeException('Staff attendu');
        }

        $staff->setProfilePicFile($request->files->get("profilePic"));
        $uploadHandler->remove($staff, 'profilePicFile');
        $uploadHandler->upload($staff, 'profilePicFile');
        $entityManagerInterface->persist($staff);
        $entityManagerInterface->flush();
        return $staff;
    }
}
