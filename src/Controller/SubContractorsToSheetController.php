<?php

namespace App\Controller;

use App\Repository\CouplingRepository;
use App\Repository\SubContractorRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SubContractorsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        SubContractorRepository $subContractorRepository,
        SheetManager $sheetManager,
    )
    {
        $subContractors = $subContractorRepository->findAll();
        
        $filename = "sous-traitants.xlsx";
        $worksheet = "sous-traitants";

        $sheetData = [];

        $heads = ["Nom de la société", "Nom du contact", "Type sous-traitant", "Téléphone", "Email"];
        $sheetData[] = $heads;

        foreach ($subContractors as $subContractor) {
            $row = [];
            $row[] = $subContractor->getCompanyName();
            $row[] = $subContractor->getContactName();
            $row[] = $subContractor->getSubContractorType()?->getName();
            $row[] = (string) $subContractor->getContactNumber();
            $row[] = $subContractor->getEmail();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
