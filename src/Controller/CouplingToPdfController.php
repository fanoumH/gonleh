<?php

namespace App\Controller;

use App\Repository\CouplingRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CouplingToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        CouplingRepository $couplingRepository,
        PdfManager $pdfManager,
    )
    {
        $vehicles = $couplingRepository->findAll();
        
        $template = $this->renderView('pdf/all_couplings.html.twig',[
            "couplings" => $vehicles,
        ]);
        $filename = "attelages";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
