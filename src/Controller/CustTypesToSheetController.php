<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\CustomerTypeRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustTypesToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        CustomerTypeRepository $customerTypeRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $custTypes = $customerTypeRepository->findAll();
        
        $filename = "type-clients.xlsx";
        $worksheet = "type-clients";

        $sheetData = [];

        $heads = [
            "Type de client",
        ];
        $sheetData[] = $heads;

        foreach ($custTypes as $custType) {
            $row = [];
            $row[] = $custType->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
