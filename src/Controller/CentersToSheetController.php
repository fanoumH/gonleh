<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CentersToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        CenterRepository $centerRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $centers = $centerRepository->findAll();
        
        $filename = "centres.xlsx";
        $worksheet = "centres";

        $sheetData = [];

        $heads = [
            "Nom du centre",
        ];
        $sheetData[] = $heads;

        foreach ($centers as $center) {
            $row = [];
            $row[] = $center->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
