<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\TerminalGetOutRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetoutsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        TerminalGetOutRepository $terminalGetOutRepository,
        PdfManager $pdfManager,
    )
    {
        $vehicles = $terminalGetOutRepository->findAll();
        
        $template = $this->renderView('pdf/all_getouts.html.twig',[
            "getouts" => $vehicles,
        ]);
        $filename = "getouts";
        
        $x = $pdfManager->generatePdf($template, $filename, orientation: "landscape");
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
