<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PostsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        StaffPostRepository $staffPostRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $posts = $staffPostRepository->findAll();
        
        $filename = "postes.xlsx";
        $worksheet = "postes";

        $sheetData = [];

        $heads = [
            "Intitulé du poste",
            "Code du poste",
        ];
        $sheetData[] = $heads;

        foreach ($posts as $post) {
            $row = [];
            $row[] = $post->getName();
            $row[] = $post->getCode();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
