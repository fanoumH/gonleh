<?php

namespace App\Controller;

use App\Repository\MenuRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MenuHierarchyController extends AbstractController
{
    public function __construct(
        private MenuRepository $menuRepository,
        private EntityManagerInterface $em,
        private ContainerBagInterface $params
    )
    {
    }
    public function __invoke()
    {
        $menus = $this->menuRepository->findAll();
        $grandParents = [];

        // Créez un tableau pour stocker les ID des grands-parents avec fils "leaf" true
        $grandParentIds = [];

        // Parcourez les éléments du menu
        foreach ($menus as $menuItem) {
            if ($menuItem->getParent() === null) {
                // C'est un grand-parent
                $grandParents[] = $menuItem;
            } elseif ($menuItem->isIsLeaf()) {
                // C'est un fils avec "leaf" true, ajoutez l'ID du parent à la liste
                $grandParentIds[] = $menuItem->getParent()->getId();
            }
        }

        dd($grandParents);
        // Filtrez les grands-parents avec au moins un fils "leaf" true
        $filteredGrandParents = array_filter($grandParents, function ($grandParent) use ($grandParentIds) {
            return in_array($grandParent['id'], $grandParentIds);
        });

        // Réindexez le tableau pour obtenir des clés numériques
        $filteredGrandParents = array_values($filteredGrandParents);
        return $menus;
    }
}
