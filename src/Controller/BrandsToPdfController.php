<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\VehicleBrandRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BrandsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        VehicleBrandRepository $vehicleBrandRepository,
        PdfManager $pdfManager,
    )
    {
        $brands = $vehicleBrandRepository->findAll();
        
        $template = $this->renderView('pdf/all_brands.html.twig',[
            "brands" => $brands,
        ]);
        $filename = "marques";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
