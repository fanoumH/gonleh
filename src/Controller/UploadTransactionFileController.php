<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;

#[AsController]
class UploadTransactionFileController extends AbstractController
{
    public function __invoke(
        Request $request,
        TransactionRepository $transactionRepository,
        UploadHandler $uploadHandler,
    ):Transaction
    {
        $transaction = $request->attributes->get('data');
        if(!($transaction instanceof  Transaction)){
            throw new \RuntimeException('Transaction attendu');
        }
        $transaction->setFile($request->files->get("transaction_file"));
        $uploadHandler->remove($transaction, 'file');
        $uploadHandler->upload($transaction, 'file');
        $transactionRepository->save($transaction , true);
        return $transaction;
    }
}
