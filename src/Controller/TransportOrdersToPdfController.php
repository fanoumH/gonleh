<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\TransportOrderRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TransportOrdersToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        TransportOrderRepository $transportOrderRepository,
        PdfManager $pdfManager,
    )
    {
        $orderTransports = $transportOrderRepository->findAll();
        
        $template = $this->renderView('pdf/all_transport_orders.html.twig',[
            "orderTransports" => $orderTransports,
        ]);
        $filename = "Ordres de transports";
        
        $x = $pdfManager->generatePdf($template, $filename, orientation: "landscape");
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
