<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\CustomerTypeRepository;
use App\Repository\ProviderRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProvTypesToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        ProviderRepository $providerRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $provs = $providerRepository->findAll();
        
        $filename = "types-fournisseurs.xlsx";
        $worksheet = "types-fournisseurs";

        $sheetData = [];

        $heads = [
            "Types de fournisseur",
        ];
        $sheetData[] = $heads;

        foreach ($provs as $prov) {
            $row = [];
            $row[] = $prov->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
