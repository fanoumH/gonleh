<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\PlaceFunctionRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PlaceFuncsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        PlaceFunctionRepository $placeFunctionRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $placeFuncs = $placeFunctionRepository->findAll();
        
        $filename = "fonctions-de-lieux.xlsx";
        $worksheet = "fonctions-de-lieux";

        $sheetData = [];

        $heads = [
            "ID",
            "Intitulé de fonction de lieux",
        ];
        $sheetData[] = $heads;

        foreach ($placeFuncs as $placeFunc) {
            $row = [];
            $row[] = $placeFunc->getId();
            $row[] = $placeFunc->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
