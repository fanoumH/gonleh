<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\TerminalRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TerminalsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        TerminalRepository $terminalRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $terminals = $terminalRepository->findAll();
        
        $filename = "terminaux.xlsx";
        $worksheet = "terminaux";

        $sheetData = [];

        $heads = [
            "Centre",
            "Terminal",
            "Occupation",
            "Occupation(%)",
        ];
        $sheetData[] = $heads;

        foreach ($terminals as $terminal) {
            $row = [];
            $row[] = $terminal->getCenter()->getName();
            $row[] = $terminal->getName();
            $row[] = $terminal->getOccupiedPlaces() . "/" . $terminal->getCapacity();
            $row[] = $terminal->getOccupiedPlacesRate();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
