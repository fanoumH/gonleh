<?php

namespace App\Controller;

use App\Message\GetIn;
use App\Message\GetOut;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class ExportAllGetOutController extends AbstractController
{
    public function __construct(
        private RequestStack $request,
        private MessageBusInterface $bus,
        private ParameterBagInterface  $parameterBag
    )
    {
    }

    public function __invoke(

    )
    {
//        dd($this->parameterBag->get('kernel.project_dir')."/".$this->parameterBag->get('path_getinf_folder_export_all'));
        $this->bus->dispatch(new GetOut());
        return null;
    }
}
