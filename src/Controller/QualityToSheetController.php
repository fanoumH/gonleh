<?php

namespace App\Controller;

use App\Repository\CouplingRepository;
use App\Repository\QualityRepository;
use App\Repository\SubContractorRepository;
use App\Repository\TransporterRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QualityToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        QualityRepository  $qualityRepository,
        SheetManager $sheetManager,
    )
    {
        $qualities = $qualityRepository->findAll();
        
        $filename = "qualites.xlsx";
        $worksheet = "qualites";

        $sheetData = [];

        $heads = ["Libellé"];
        $sheetData[] = $heads;

        foreach ($qualities as $qual) {
            $row = [];
            $row[] = $qual->getLabel();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
