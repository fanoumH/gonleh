<?php

namespace App\Controller;

use App\Repository\EndpointRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class GetAllPermissionController extends AbstractController
{
    public function __construct(
        private EndpointRepository $endpointRepository,
        private EntityManagerInterface $em,
        private ContainerBagInterface $params
    )
    {
    }
    public function __invoke()
    {
        $menus = $this->endpointRepository->findAll();
        $menus = array_map(function($item){
            return [
                "name" => $item->getName(),
                "description" => $item->getSummary(),
                "shortname" => $item->getShortName(),
            ];
        },$menus);

        $newMenus = [];
        foreach($menus as $menu){
            $newMenus[$menu["shortname"]] = array_filter($menus , function($item) use ($menu) {
                return $item["shortname"] === $menu["shortname"];
            });
            $newMenus[$menu["shortname"]] = array_map(function($item){
                unset($item["shortname"]);
                return $item;
            },$newMenus[$menu["shortname"]]);
        }
        return $newMenus;
    }
}
