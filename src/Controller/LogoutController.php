<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\TokenService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;


#[AsController]
class LogoutController extends AbstractController
{
    public function __construct(
        private Security $security,
        private JWTTokenManagerInterface $JWTManager,
    )
    {
    }

    public function __invoke(Request $request) :  mixed
    {
        return new JsonResponse(['token' => $this->JWTManager->create($this->security->getToken()->getUser())]);
    }
}
