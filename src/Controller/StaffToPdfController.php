<?php

namespace App\Controller;

use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\StaffRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StaffToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        StaffRepository $staffRepository,
        PdfManager $pdfManager,
    )
    {
        $staff = $staffRepository->findAll();
        
        $template = $this->renderView('pdf/all_staff.html.twig',[
            "staff" => $staff,
        ]);
        $filename = "personnels";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
