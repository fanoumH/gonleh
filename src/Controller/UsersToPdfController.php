<?php

namespace App\Controller;

use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\CustomerRepository;
use App\Repository\StaffRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UsersToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        UserRepository $userRepository,
        PdfManager $pdfManager,
    )
    {
        $users = $userRepository->findAll();
        
        $template = $this->renderView('pdf/all_users.html.twig',[
            "users" => $users,
        ]);
        $filename = "utilisateurs";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
