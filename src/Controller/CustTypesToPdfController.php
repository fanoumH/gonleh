<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\CustomerTypeRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustTypesToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        CustomerTypeRepository $customerTypeRepository,
        PdfManager $pdfManager,
    )
    {
        $custTypes = $customerTypeRepository->findAll();
        
        $template = $this->renderView('pdf/all_custTypes.html.twig',[
            "custTypes" => $custTypes,
        ]);
        $filename = "type-clients";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
