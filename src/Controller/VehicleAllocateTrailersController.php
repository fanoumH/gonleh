<?php

namespace App\Controller;

use App\Entity\Vehicle;
use App\Form\VehicleAllocateDriversType;
use App\Form\VehicleAllocateTrailersType;
use App\Service\ValidationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VehicleAllocateTrailersController extends AbstractController
{
    public function __construct(
        private ValidationService $validationService
    )
    {
    }

    public function __invoke(Request $request, Vehicle $vehicle): JsonResponse|Vehicle
    {
        $form = $this->createForm(VehicleAllocateTrailersType::class, null, [
            'csrf_protection' => false
        ]);
        $input = json_decode($request->getContent(), true);
        $input['vehicle'] = $vehicle->getId();
        $form->submit($input);
        if ($form->isValid()) {
            $data = $form->getData();
            /**
             * extract variable
             * trailers
             */
            extract($data);
            /**
             * set the drivers
             */
            $vehicle->setTrailers($trailers);
        } else {
            return $this->json([
                'status' => false,
                'message' => $this->validationService->getStringOfFormError($form)
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return $vehicle;
    }
}
