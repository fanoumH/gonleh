<?php

namespace App\Controller;

use App\Entity\Place;
use App\Repository\PlaceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class RemovePlaceController extends AbstractController
{
    /**
     * @param PlaceRepository $placeRepository
     */
    public function __construct(
        private PlaceRepository $placeRepository
    )
    {
    }

    /**
     * @param Place $place
     * @return JsonResponse
     * @throws \Doctrine\DBAL\Exception
     */
    public function __invoke(Place $place): JsonResponse
    {
        $result = $this->placeRepository->deleteAssociation($place);
        if ($result) {
            $this->placeRepository->softDeleteItem($place);
            return new JsonResponse([
                "status" => "success",
                "message" => "Suppression avec succès"
            ]);
        }

        return new JsonResponse([
            "status" => "error",
            "message" => "Suppression avec non efféctué"
        ]);
    }
}
