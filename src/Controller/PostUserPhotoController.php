<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;

#[AsController]
class PostUserPhotoController extends AbstractController
{
    public function __invoke(
        Request $request,
        UserRepository $userRepository,
        UploadHandler $uploadHandler,
    ):User
    {
        $user = $request->attributes->get('data');
        if(!($user instanceof  User)){
            throw new \RuntimeException('User attendu');
        }
        $user->setFile($request->files->get("profil_photo"));
        $uploadHandler->remove($user, 'file');
        $uploadHandler->upload($user, 'file');
        $userRepository->save($user , true);
        return $user;
    }
}
