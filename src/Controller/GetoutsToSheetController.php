<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\TerminalGetOutRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetoutsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        TerminalGetOutRepository $terminalGetOutRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $getouts = $terminalGetOutRepository->findAll();
        
        $filename = "getouts.xlsx";
        $worksheet = "getout";

        $sheetData = [];

        $heads = [
            "Conteneurs",
            "Transporteur",
            "Num camion",
            "Client",
            "Num permis chauffeur",
        ];
        $sheetData[] = $heads;

        foreach ($getouts as $getout) {
            $row = [];
            $conts = "";
            foreach ($getout->getContainers() as $cont) {
                $conts .= $cont->getContainerNumber() . ", " ;
            }
            $row[] = $conts;
            $row[] = $getout->getCarrier()?->getName();
            $row[] = $getout->getTruckNumber();
            $row[] = $getout->getCustomer()?->getCustomerIdentification()?->getCustomerTitle();
            // $row = "" ;
            $row[] = $getout->getDriverLicenseNumber();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
