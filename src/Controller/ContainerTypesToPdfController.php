<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\ContainerTypeRepository;
use App\Repository\CouplingRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContainerTypesToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        ContainerTypeRepository $containerTypeRepository,
        PdfManager $pdfManager,
    )
    {
        $contTypes = $containerTypeRepository->findAll();
        
        $template = $this->renderView('pdf/all_contTypes.html.twig',[
            "contTypes" => $contTypes,
        ]);
        $filename = "types-conteneurs";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
