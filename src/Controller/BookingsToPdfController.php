<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookingsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        BookingRepository $bookingRepository,
        PdfManager $pdfManager,
    )
    {
        $vehicles = $bookingRepository->findAll();
        
        $template = $this->renderView('pdf/all_bookings.html.twig',[
            "bookings" => $vehicles,
        ]);
        $filename = "reservations";
        
        $x = $pdfManager->generatePdf($template, $filename, orientation: "landscape");
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
