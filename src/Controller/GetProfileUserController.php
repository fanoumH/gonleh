<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\TokenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GetProfileUserController extends AbstractController
{
    public function __construct(
        private Security $security,
        private TokenStorageInterface $storage,
    )
    {
    }

    public function __invoke(Request $request) :  User
    {
        return ($this->security->getToken()->getUser());
    }
}
