<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\QualityRepository;
use App\Repository\TransporterRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QualityToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        QualityRepository $qualityRepository,
        PdfManager $pdfManager,
    )
    {
        $qualities = $qualityRepository->findAll();
        
        $template = $this->renderView('pdf/all_qualities.html.twig',[
            "qualities" => $qualities,
        ]);
        $filename = "qualites";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
