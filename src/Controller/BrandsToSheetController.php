<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\UserRepository;
use App\Repository\VehicleBrandRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BrandsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        VehicleBrandRepository $vehicleBrandRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $brands = $vehicleBrandRepository->findAll();
        
        $filename = "marques.xlsx";
        $worksheet = "marques";

        $sheetData = [];

        $heads = [
            "Marque",
        ];
        $sheetData[] = $heads;

        foreach ($brands as $brand) {
            $row = [];
            $row[] = $brand->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
