<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\TerminalRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TerminalsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        TerminalRepository $terminalRepository,
        PdfManager $pdfManager,
    )
    {
        $terminals = $terminalRepository->findAll();
        
        $template = $this->renderView('pdf/all_terminals.html.twig',[
            "terminals" => $terminals,
        ]);
        $filename = "terminaux";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
