<?php

namespace App\Controller;

use App\Entity\CustomerIdentification;
use App\Repository\CustomerIdentificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;

#[AsController]
class UploadCinDocumentController extends AbstractController
{
    public function __invoke(
        Request $request,
        CustomerIdentificationRepository $customerIdentificationRepository,
        UploadHandler $uploadHandler,
    ):CustomerIdentification
    {
        $customerIdentification = $request->attributes->get('data');
        if(!($customerIdentification instanceof  CustomerIdentification)){
            throw new \RuntimeException('CustomerIdentification attendu');
        }
        $customerIdentification->setCinFile($request->files->get("cin_file"));
        $uploadHandler->remove($customerIdentification, 'cinFile');
        $uploadHandler->upload($customerIdentification, 'cinFile');
        $customerIdentificationRepository->save($customerIdentification , true);
        return $customerIdentification;
    }
}
