<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\CustomerTypeRepository;
use App\Repository\ProviderRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProvTypesToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        ProviderRepository $providerRepository,
        PdfManager $pdfManager,
    )
    {
        $provs = $providerRepository->findAll();
        
        $template = $this->renderView('pdf/all_provTypes.html.twig',[
            "provTypes" => $provs,
        ]);
        $filename = "types-fournisseurs";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
