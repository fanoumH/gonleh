<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\RentalOrderRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RentalsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        RentalOrderRepository $rentalOrderRepository,
        PdfManager $pdfManager,
    )
    {
        $rentals = $rentalOrderRepository->findAll();
        
        $template = $this->renderView('pdf/all_rental_orders.html.twig',[
            "rentals" => $rentals,
        ]);
        $filename = "ordres-locations";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
