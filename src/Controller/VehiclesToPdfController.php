<?php

namespace App\Controller;

use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class VehiclesToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        VehicleRepository $vehicleRepository,
        PdfManager $pdfManager,
    )
    {
        $vehicles = $vehicleRepository->findAll();
        
        $template = $this->renderView('pdf/all_vehicles.html.twig',[
            "vehicles" => $vehicles,
        ]);
        $filename = "vehicules";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        // return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
