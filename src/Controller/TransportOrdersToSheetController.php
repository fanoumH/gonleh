<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\TransportOrderRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TransportOrdersToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        TransportOrderRepository $transportOrderRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    ) {
        $orderTransports = $transportOrderRepository->findAll();

        $filename = "ordres-de-transports.xlsx";
        $worksheet = "ordres-de-transports";

        $sheetData = [];

        $heads = [
            "Num OT",
            "Client",
            "Trajet",
            "Statut",
            "Date de chargement",
            "Date d'arrivée effective",
        ];
        $sheetData[] = $heads;

        foreach ($orderTransports as $one) {
            $row = [];
            $row[] = $one->getCode();
            $row[] = $one->getCustomer()?->getCustomerIdentification()->getCustomerTitle();

            $route = $one->getMissionOrder()?->getLoadingPlace()?->getName() ?? "";
            $route .= " / ";
            $route .= $one->getMissionOrder()?->getDeliveryLocation()?->getName() ?? "";

            $row[] = $route;

            switch ($one->getOtStatus()) {
                case 'created':
                    $row[] = "Créé";
                    break;
                case 'assigned':
                    $row[] = "Assigné";
                    break;
                case 'delivered':
                    $row[] = "Livré";
                    break;
                default:
                    $row[] = "";
                    break;
            }
            $row[] = $one->getMissionOrder() ? $one->getMissionOrder()->getLoadingDate()->format('d/m/Y') : "";
            $row[] = $one->getActualArrivalDate() ? $one->getActualArrivalDate()->format('d/m/Y') : "";

            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
