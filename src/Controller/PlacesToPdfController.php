<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\PlaceRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PlacesToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        PlaceRepository $placeRepository,
        PdfManager $pdfManager,
    )
    {
        $places = $placeRepository->findAll();
        
        $template = $this->renderView('pdf/all_places.html.twig',[
            "places" => $places,
        ]);
        $filename = "lieux";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
