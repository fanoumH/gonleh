<?php

namespace App\Controller;

use App\Repository\CouplingRepository;
use App\Repository\SubContractorRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SubContractorsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        SubContractorRepository $subContractorRepository,
        PdfManager $pdfManager,
    )
    {
        $subContractors = $subContractorRepository->findAll();
        
        $template = $this->renderView('pdf/all_subcontractors.html.twig',[
            "subContractors" => $subContractors,
        ]);
        $filename = "sous-traitants";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
