<?php

namespace App\Controller;

use App\Entity\CustomerFreeField;
use App\Entity\User;
use App\Repository\CustomerFreeFieldRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;

class AttachmentDocCustomerController extends AbstractController
{
    public function __invoke(
        Request $request,
        CustomerFreeFieldRepository $customerFreeFieldRepository,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):CustomerFreeField
    {
        $customerFreeField = $request->attributes->get('data');
        if(!($customerFreeField instanceof  CustomerFreeField)){
            throw new \RuntimeException('Champ libre client attendu');
        }
        $customerFreeField->setAttachedDocumentFile($request->files->get("attached_document"));
        $uploadHandler->remove($customerFreeField, 'attachedDocumentFile');
        $uploadHandler->upload($customerFreeField, 'attachedDocumentFile');
        $entityManagerInterface->persist($customerFreeField);
        $entityManagerInterface->flush();
        return $customerFreeField;
    }
}
