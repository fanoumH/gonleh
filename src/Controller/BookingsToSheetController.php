<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookingsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        BookingRepository $bookingRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $bookings = $bookingRepository->findAll();
        
        $filename = "reservations.xlsx";
        $worksheet = "reservations";

        $sheetData = [];

        $heads = [
            "Numéro booking",
            "Client final",
            "Compagnie maritime",
            "Num container",
            "Etat d'annulation",
            "Date de Get out au plus tard",
        ];
        $sheetData[] = $heads;

        foreach ($bookings as $booking) {
            $row = [];
            $row[] = $booking->getRefBooking();
            $row[] = $booking->getClient()?->getCustomerIdentification()?->getCustomerTitle();
            // $row[] = "";
            $row[] = $booking->getShippingCompany()?->getName();
            $conts = "";
            foreach ($booking->getContainers() as $cont) {
                $conts .= $cont->getContainerNumber() . "," ;
            }
            $row[] = $conts;
            $row[] = $booking->isIsCancelled() ? "Annulé" : "-" ;
            $row[] = $booking->getContainers()->count() > 0 ? $booking->getContainers()[0]?->getDateToGetOut()->format("d-M-Y") : "-" ;
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
