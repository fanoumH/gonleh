<?php

namespace App\Controller;

use App\Entity\CustomerIdentification;
use App\Entity\Transaction;
use App\Repository\CustomerIdentificationRepository;
use App\Repository\TransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;

#[AsController]
class UploadNifStatDocumentController extends AbstractController
{
    public function __invoke(
        Request $request,
        CustomerIdentificationRepository $customerIdentificationRepository,
        UploadHandler $uploadHandler,
    ):CustomerIdentification
    {
        $customerIdentification = $request->attributes->get('data');
        if(!($customerIdentification instanceof  CustomerIdentification)){
            throw new \RuntimeException('CustomerIdentification attendu');
        }
        $customerIdentification->setNifFile($request->files->get("nif_stat_file"));
        $uploadHandler->remove($customerIdentification, 'nifFile');
        $uploadHandler->upload($customerIdentification, 'nifFile');
        $customerIdentificationRepository->save($customerIdentification , true);
        return $customerIdentification;
    }
}
