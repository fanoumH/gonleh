<?php

namespace App\Controller;

use App\Message\MercureCreateNeedMessage;
use App\Repository\BesoinRepository;
use App\Repository\UserRepository;
use App\Service\MercureCookieGeneratorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\WebLink\Link;

class MercureController extends AbstractController
{

    #[Route('/mercure/index', name: 'app_mercure')]
    /**
     * Summary of publish
     * @return Response
     */
    public function index(Request $request, Security $security): Response
    {
        $response =$this->render('mercure/index.html.twig', []);
//        $response->headers->setCookie($mercureCookieGeneratorService->generate(4));
        return $response;
    }
}
