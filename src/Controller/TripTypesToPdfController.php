<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\TripTypeRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TripTypesToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        TripTypeRepository $tripTypeRepository,
        PdfManager $pdfManager,
    )
    {
        $tripTypes = $tripTypeRepository->findAll();
        
        $template = $this->renderView('pdf/all_tripTypes.html.twig',[
            "tripTypes" => $tripTypes,
        ]);
        $filename = "Types-de-voyages";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
