<?php

namespace App\Controller;

use App\Repository\CouplingRepository;
use App\Repository\SubContractorRepository;
use App\Repository\TransporterRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TransportersToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        TransporterRepository $transporterRepository,
        SheetManager $sheetManager,
    )
    {
        $transporters = $transporterRepository->findAll();
        
        $filename = "transporteurs.xlsx";
        $worksheet = "transporteurs";

        $sheetData = [];

        $heads = ["Nom"];
        $sheetData[] = $heads;

        foreach ($transporters as $transporter) {
            $row = [];
            $row[] = $transporter->getName();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
