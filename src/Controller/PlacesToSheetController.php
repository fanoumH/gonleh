<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\PlaceRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PlacesToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        PlaceRepository $placeRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $places = $placeRepository->findAll();
        
        $filename = "lieux.xlsx";
        $worksheet = "lieux";

        $sheetData = [];

        $heads = [
            "ID",
            "Intitulé lieu",
            "Fonctions lieu",
        ];
        $sheetData[] = $heads;

        foreach ($places as $place) {
            $row = [];
            $row[] = $place->getId();
            $row[] = $place->getName();
            
            $funcs = "";
            
            foreach ($place->getPlaceFunctions() as $func) {
                $funcs .= $func->getName(). ", " ;
            }

            $row[] = $funcs;
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
