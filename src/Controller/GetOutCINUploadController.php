<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\User;
use App\Entity\Staff;
use App\Entity\CustomerFreeField;
use App\Entity\TerminalGetin;
use App\Entity\TerminalGetOut;
use App\Entity\TransportOrder;
use App\Repository\UserRepository;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Handler\UploadHandler;
use App\Repository\CustomerFreeFieldRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetOutCINUploadController extends AbstractController
{
    public function __invoke(
        Request $request,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):TerminalGetOut
    {
        $getout = $request->attributes->get('data');
        if(!($getout instanceof  TerminalGetOut)){
            throw new \RuntimeException('GetOut attendu');
        }

        $getout->setCinAttachmentFile($request->files->get("cinAttachment"));
        $uploadHandler->remove($getout, 'cinAttachmentFile');
        $uploadHandler->upload($getout, 'cinAttachmentFile');
        $entityManagerInterface->persist($getout);
        $entityManagerInterface->flush();
        return $getout;
    }
}
