<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Staff;
use App\Entity\CustomerFreeField;
use App\Entity\TerminalGetin;
use App\Entity\TransportOrder;
use App\Repository\UserRepository;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Handler\UploadHandler;
use App\Repository\CustomerFreeFieldRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BdrUploadController extends AbstractController
{
    public function __invoke(
        Request $request,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):TerminalGetin
    {
        $terminalGetin = $request->attributes->get('data');
        if(!($terminalGetin instanceof  TerminalGetin)){
            throw new \RuntimeException('Getin attendu');
        }

        $terminalGetin->setBdrAttachmentFile($request->files->get("bdrAttachment"));
        $uploadHandler->remove($terminalGetin, 'bdrAttachmentFile');
        $uploadHandler->upload($terminalGetin, 'bdrAttachmentFile');
        $entityManagerInterface->persist($terminalGetin);
        $entityManagerInterface->flush();
        return $terminalGetin;
    }
}
