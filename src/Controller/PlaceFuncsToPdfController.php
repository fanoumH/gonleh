<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\PlaceFunctionRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PlaceFuncsToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        PlaceFunctionRepository $placeFunctionRepository,
        PdfManager $pdfManager,
    )
    {
        $placeFuncs = $placeFunctionRepository->findAll();
        
        $template = $this->renderView('pdf/all_placeFuncs.html.twig',[
            "placeFuncs" => $placeFuncs,
        ]);
        $filename = "fonctions-de-lieux";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
