<?php

namespace App\Controller;

use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\StaffRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StaffToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        StaffRepository $staffRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $staff = $staffRepository->findAll();
        
        $filename = "personnels.xlsx";
        $worksheet = "personnels";

        $sheetData = [];

        $heads = [
            "Matricule",
            "Nom",
            "Prénoms",
            "Poste",
            "Téléphone",
            "Adresse",
        ];
        $sheetData[] = $heads;

        foreach ($staff as $one) {
            $row = [];
            $row[] = $one->getRegistrationNumber();
            $row[] = $one->getLastName();
            $row[] = $one->getFirstName();
            $row[] = $one->getStaffPost()?->getName();
            $row[] = $one->getPhoneNumber();
            $row[] = $one->getAddress();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
