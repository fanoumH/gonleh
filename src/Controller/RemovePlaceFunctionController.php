<?php

namespace App\Controller;

use App\Entity\PlaceFunction;
use App\Repository\PlaceFunctionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class RemovePlaceFunctionController extends AbstractController
{
    /**
     * @param PlaceFunctionRepository $placeFunctionRepository
     */
    public function __construct(
        private PlaceFunctionRepository $placeFunctionRepository
    )
    {
    }

    /**
     * @param PlaceFunction $placeFunction
     * @return JsonResponse
     * @throws \Doctrine\DBAL\Exception
     */
    public function __invoke(PlaceFunction $placeFunction): JsonResponse
    {
        $result = $this->placeFunctionRepository->deleteAssociation($placeFunction);
        if ($result) {
            $this->placeFunctionRepository->softDeleteItem($placeFunction);
            return new JsonResponse([
                "status" => "success",
                "message" => "Suppression avec succès"
            ]);
        }

        return new JsonResponse([
            "status" => "error",
            "message" => "Suppression avec non efféctué"
        ]);
    }
}
