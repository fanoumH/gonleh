<?php

namespace App\Controller;

use App\Entity\CustomerFreeField;
use App\Entity\Staff;
use App\Entity\User;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use App\Repository\StaffRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\UploadHandler;

class StaffRegisterController extends AbstractController
{
    public function __invoke(
        Request $request,
        StaffPostRepository $staffPostRepository,
        CenterRepository $centerRepository,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):Staff
    {
        $data = $request->request->all();
        
        $newStaff = new Staff();
        
        foreach ($data as $key => $value) {
            if( in_array($key, ['birthday', 'hiringDate', 'lastMedicalVisit', 'idNumberDelivery', 'driverLicenceExpiration']) ){
                $newStaff->{"set" . ucfirst($key)}(new \DateTimeImmutable($value));
            }
            elseif($key === "staffPost"){
                $tabVal = explode("/", $value);
                $idStaffPost = end($tabVal);
                $staffPost = $staffPostRepository->find($idStaffPost);
                $newStaff->{"set" . ucfirst($key)}($staffPost);
            }
            elseif($key === "center"){
                $tabVal = explode("/", $value);
                $idCenter = end($tabVal);
                $center = $centerRepository->find($idCenter);
                $newStaff->{"set" . ucfirst($key)}($center);
            }
            else{
                $newStaff->{"set" . ucfirst($key)}($value);
            }
        }
        $newStaff->setProfilePicFile($request->files->get("profilePic"));
        $uploadHandler->remove($newStaff, 'profilePicFile');
        $uploadHandler->upload($newStaff, 'profilePicFile');
        $entityManagerInterface->persist($newStaff);
        $entityManagerInterface->flush();
        return $newStaff;
    }
}
