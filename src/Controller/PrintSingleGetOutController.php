<?php

namespace App\Controller;

use App\Entity\TerminalGetin;
use App\Entity\TerminalGetOut;
use App\Service\ExportExcelService;
use Dompdf\Dompdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class PrintSingleGetOutController extends AbstractController
{
    public function __construct(
        private RequestStack $request,
        private MessageBusInterface $bus,
        private ParameterBagInterface  $parameterBag,
        private ExportExcelService $exportExcelService,
    )
    {
    }

    public function __invoke(
        TerminalGetOut $getout
    )
    {
        $type = ($this->request->getCurrentRequest()->query->all())["type"] ?? "pdf";
        if($type === "pdf") {
            $html = $this->renderView('terminalGetOut/terminalGetOutSingle.html.twig', [
                "getout" => $getout
            ]);
            $domPdf = new Dompdf();
            $domPdf->loadHtml($html);
            $domPdf->setPaper('A4', 'landscape');
            $domPdf->render();
            $disposition = HeaderUtils::makeDisposition(
                HeaderUtils::DISPOSITION_ATTACHMENT,
                ($getout->getRefGetOut() ?? "refgetout" . $getout->getId()) . '.pdf'
            );
            //Récupérez le contenu du PDF généré
            $output = $domPdf->output();
            $response = new Response($output);
            $response->headers->set('Content-type', 'application/pdf');
            $response->headers->set('Content-Disposition', $disposition);
            return $response;
        }
        else{
            $html = $this->render('terminalGetOut/terminalGetOutSingle.html.twig', [
                "getout" => $getout
            ]);

            $title      = "Liste des Getouts";
            $lastColumn = 'L';
            $data       = ['count' => 6];

            $spreadsheet = $this->exportExcelService->formatSpreadSheetXls($title, $data, $lastColumn, $html);
            $writer      = new Xlsx($spreadsheet);
            //Generate the file xlsx
            $filename = "refgetout" . $getout->getId() .'.xlsx';
            $path = './';
            $writer->save($path . $filename);
            return $this->file($path . $filename, $filename);
        }
    }
}
