<?php

namespace App\Controller;

use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Repository\SubContractorTypeRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SubContTypesToPdfController extends AbstractController
{
    public function __invoke(
        Request $request,
        SubContractorTypeRepository $subContractorTypeRepository,
        PdfManager $pdfManager,
    )
    {
        $subContTypes = $subContractorTypeRepository->findAll();
        
        $template = $this->renderView('pdf/all_subContTypes.html.twig',[
            "subContTypes" => $subContTypes,
        ]);
        $filename = "types-sous-traitants";
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
