<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\User;
use App\Entity\Staff;
use App\Entity\CustomerFreeField;
use App\Entity\TerminalGetin;
use App\Entity\TransportOrder;
use App\Repository\UserRepository;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Handler\UploadHandler;
use App\Repository\CustomerFreeFieldRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookingUploadController extends AbstractController
{
    public function __invoke(
        Request $request,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):Booking
    {
        $booking = $request->attributes->get('data');
        if(!($booking instanceof  Booking)){
            throw new \RuntimeException('Reservation attendue');
        }

        $booking->setBookingAttachmentFile($request->files->get("bookingAttachment"));
        $uploadHandler->remove($booking, 'bookingAttachmentFile');
        $uploadHandler->upload($booking, 'bookingAttachmentFile');
        $entityManagerInterface->persist($booking);
        $entityManagerInterface->flush();
        return $booking;
    }
}
