<?php

namespace App\Controller;

use App\Repository\ContainerRepository;
use App\Repository\CouplingRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetinToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        ContainerRepository $containerRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $conts = $containerRepository->findAll();
        
        $filename = "getins.xlsx";
        $worksheet = "getins";

        $sheetData = [];

        $heads = [
            "Terminal",
            "Containers",
            "BDR",
            "Compagnie maritime",
            "Get in",
            "Etat",
            "Aptitude",
        ];
        $sheetData[] = $heads;

        foreach ($conts as $cont) {
            $row = [];
            $row[] = $cont->getTerminalGetin()?->getTerminal()->getName();
            $row[] = $cont->getContainerNumber();
            $row[] = $cont->getTerminalGetin()?->getBDRnumber();
            $row[] = $cont->getTerminalGetin()?->getShippingCompany()?->getName();
            $row[] = $cont->getTerminalGetin()?->getCreatedAt()->format("d-M-Y");
            $row[] = $cont->getContainerStatus();
            $row[] = $cont->isContainerStatusAble() ? "Apte" : "Inapte";

            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
