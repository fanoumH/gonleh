<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Staff;
use App\Entity\CustomerFreeField;
use App\Entity\TransportOrder;
use App\Repository\UserRepository;
use App\Repository\CenterRepository;
use App\Repository\StaffPostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Handler\UploadHandler;
use App\Repository\CustomerFreeFieldRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BLUploadTransportController extends AbstractController
{
    public function __invoke(
        Request $request,
        UploadHandler $uploadHandler,
        EntityManagerInterface $entityManagerInterface,
    ):TransportOrder
    {
        $transportOrder = $request->attributes->get('data');
        if(!($transportOrder instanceof  TransportOrder)){
            throw new \RuntimeException('Ordre de transport attendu');
        }

        $transportOrder->setBlAttachmentFile($request->files->get("blAttachment"));
        $uploadHandler->remove($transportOrder, 'blAttachmentFile');
        $uploadHandler->upload($transportOrder, 'blAttachmentFile');
        $entityManagerInterface->persist($transportOrder);
        $entityManagerInterface->flush();
        return $transportOrder;
    }
}
