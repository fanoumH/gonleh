<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\TripTypeRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TripTypesToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        TripTypeRepository $tripTypeRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $tripTypes = $tripTypeRepository->findAll();
        
        $filename = "types-voyages.xlsx";
        $worksheet = "types-voyages";

        $sheetData = [];

        $heads = [
            "ID",
            "Libellé",
        ];
        $sheetData[] = $heads;

        foreach ($tripTypes as $tripType) {
            $row = [];
            $row[] = $tripType->getId();
            $row[] = $tripType->getLabel();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
