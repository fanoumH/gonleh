<?php

namespace App\Controller;

use App\Entity\Vehicle;
use App\Form\VehicleAllocateDriversType;
use App\Service\ValidationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VehicleAllocateDriversController extends AbstractController
{
    public function __construct(
        private ValidationService $validationService
    )
    {
    }

    public function __invoke(Request $request, Vehicle $vehicle): JsonResponse|Vehicle
    {
        $form = $this->createForm(VehicleAllocateDriversType::class, null, [
            'csrf_protection' => false
        ]);
        $input = json_decode($request->getContent(), true);
        $input['vehicle'] = $vehicle->getId();
        $form->submit($input);
        if ($form->isValid()) {
            $data = $form->getData();
            /**
             * extract variable
             * drivers
             */
            extract($data);
            /**
             * set the drivers
             */
            $vehicle->setDrivers($drivers);
            /**
             * set the state of vehicle
             */
            if($drivers->count() > 0) {
                $vehicle->setState(Vehicle::STATE_AVAILABLE);
            } else {
                $vehicle->setState(Vehicle::STATE_NO_DRIVER);
            }
        } else {
            return $this->json([
                'status' => false,
                'message' => $this->validationService->getStringOfFormError($form)
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return $vehicle;
    }
}
