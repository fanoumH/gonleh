<?php

namespace App\Controller;

use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class VehiclesToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        VehicleRepository $vehicleRepository,
        SheetManager $sheetManager,
    )
    {
        $vehicles = $vehicleRepository->findAll();
        
        $filename = "vehicules.xlsx";
        $worksheet = "vehicules";

        $sheetData = [];

        $heads = ["Immatriculation", "Marque", "Charge utile", "Disponibilité", "Type"];
        $sheetData[] = $heads;

        foreach ($vehicles as $vehicle) {
            $row = [];
            $row[] = $vehicle->getRegistrationNumber();
            $row[] = $vehicle->getVehicleBrand()?->getName();
            $row[] = $vehicle->getPayload();

            $state = "";
            switch ($vehicle->getState()) {
                case 0:
                    $state = "En cours de route";
                    break;
                case 1:
                    $state = "Disponible";
                    break;
                case 2:
                    $state = "Pas de chauffeur";
                    break;
                case 3:
                    $state = "En maintenance";
                    break;
                case 4:
                    $state = "A vendre";
                    break;
                case 5:
                    $state = "En congé";
                    break;
                case 6:
                    $state = "Accidenté";
                    break;
                case 7:
                    $state = "Assigné à une autre société";
                    break;
                case 8:
                    $state = "En attente de tracteur";
                    break;
                case 9:
                    $state = "En panne";
                    break;
                case 10:
                    $state = "Autre";
                    break;
            }

            $row[] = $state;

            $type = "";

            switch ($vehicle->getType()) {
                case 1:
                    $type = "Remorque";
                    break;
                case 2:
                    $type = "Semi";
                    break;
                case 3:
                    $type = "Élévateur";
                    break;
                case 4:
                    $type = "Plateau";
                    break;
                
                default:
                    # code...
                    break;
            }

            $row[] = $type;

            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
        
    }
}
