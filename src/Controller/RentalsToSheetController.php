<?php

namespace App\Controller;

use App\EventSubscriber\RentalOrderListener;
use App\Repository\BookingRepository;
use App\Repository\CenterRepository;
use App\Repository\RentalOrderRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RentalsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        RentalOrderRepository $rentalOrderRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $rentals = $rentalOrderRepository->findAll();
        
        $filename = "ordres-locations.xlsx";
        $worksheet = "ordres-locations";

        $sheetData = [];

        $heads = [
            "",
        ];
        $sheetData[] = $heads;

        foreach ($rentals as $rental) {
            $row = [];
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
