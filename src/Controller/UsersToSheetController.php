<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Repository\UserRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UsersToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        UserRepository $userRepository,
        PdfManager $pdfManager,
        SheetManager $sheetManager,
    )
    {
        $utilisateurs = $userRepository->findAll();
        
        $filename = "utilisateurs.xlsx";
        $worksheet = "utilisateurs";

        $sheetData = [];

        $heads = [
            "Prénom",
            "Nom",
            "Adresse email",
        ];
        $sheetData[] = $heads;

        foreach ($utilisateurs as $user) {
            $row = [];
            $row[] = $user->getFirstname();
            $row[] = $user->getLastname();
            $row[] = $user->getEmail();
            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
    }
}
