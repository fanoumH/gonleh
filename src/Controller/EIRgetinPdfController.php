<?php

namespace App\Controller;

use App\Entity\EIR;
use App\Service\PdfManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EIRgetinPdfController extends AbstractController
{

    public function __invoke(EIR $eir, Request $request, PdfManager $pdfManager){
        $idEIR = ($request->attributes->get('id'));
        $template = $this->renderView('pdf/eir_getin.html.twig',[
            "eir" => $eir,
        ]);
        $filename = $eir->getRefEIR();
        
        $x = $pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
