<?php

namespace App\Controller;

use Dompdf\Dompdf;
use App\Service\PdfManager;
use App\Entity\TerminalGetin;
use App\Entity\TerminalGetOut;
use App\Service\ExportExcelService;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class BlGetOutController extends AbstractController
{
    public function __construct(
        private RequestStack $request,
        private MessageBusInterface $bus,
        private ParameterBagInterface  $parameterBag,
        private ExportExcelService $exportExcelService,
        private PdfManager $pdfManager,
    )
    {
    }

    public function __invoke(
        TerminalGetOut $getout
    )
    {

        $template = $this->renderView('terminalGetOut/terminalGetOutBL.html.twig',[
            "getout" => $getout,
        ]);
        $filename = "BL-" . $getout->getRefGetOut();
        
        $x = $this->pdfManager->generatePdf($template, $filename);
        
        return $this->file($x, $filename, ResponseHeaderBag::DISPOSITION_INLINE);
        
    }
}
