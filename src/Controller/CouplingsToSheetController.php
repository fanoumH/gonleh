<?php

namespace App\Controller;

use App\Repository\CouplingRepository;
use App\Service\PdfManager;
use App\Repository\VehicleRepository;
use App\Service\SheetManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CouplingsToSheetController extends AbstractController
{
    public function __invoke(
        Request $request,
        CouplingRepository $couplingRepository,
        SheetManager $sheetManager,
    )
    {
        $couplings = $couplingRepository->findAll();
        
        $filename = "attelages.xlsx";
        $worksheet = "attelages";

        $sheetData = [];

        $heads = ["Numéro attelage", "Immatriculation", "Marque", "Modèle"];
        $sheetData[] = $heads;

        foreach ($couplings as $coupling) {
            $row = [];
            $row[] = $coupling->getCouplingNumber();
            $row[] = $coupling->getSemi()?->getRegistrationNumber() ?? "Non assigné";
            $row[] = $coupling->getTrailer()?->getRegistrationNumber() ?? "Non assigné";
            $row[] = $coupling->getSemi()?->getVehicleBrand()?->getName() ?? "Non assigné";
            $row[] = $coupling->getTrailer()?->getVehicleBrand()?->getName() ?? "Non assigné";

            $row[] = $coupling->getSemi()?->getVehicleModel()?->getName() ?? "Non assigné";
            $row[] = $coupling->getTrailer()?->getVehicleModel()?->getName() ?? "Non assigné";

            $sheetData[] = $row;
        }

        $sheetManager->generateSheet($filename, $worksheet, $sheetData);
        
    }
}
