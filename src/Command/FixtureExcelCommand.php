<?php

namespace App\Command;

use App\Entity\Booking;
use App\Entity\Staff;
use App\Entity\Center;
use App\Entity\Terminal;
use App\Entity\Container;
use App\Entity\StaffPost;
use App\Entity\ContainerType;
use App\Entity\TerminalGetin;
use App\Entity\ShippingCompany;
use App\Repository\StaffRepository;
use App\Repository\CenterRepository;
use App\Repository\ContainerRepository;
use App\Repository\TerminalRepository;
use App\Repository\StaffPostRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\TransporterRepository;
use App\Repository\ContainerTypeRepository;
use App\Repository\CustomerIdentificationRepository;
use App\Repository\ShippingCompanyRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

#[AsCommand(
    name: 'app:fixture-excel',
    description: 'Add a short description for your command',
)]
class FixtureExcelCommand extends Command
{
    public function __construct(
        private ContainerBagInterface $containerBag,
        private EntityManagerInterface $em,
        private CenterRepository $centerRepository,
        private TerminalRepository $terminalRepository,
        private ShippingCompanyRepository $shippingCompanyRepository,
        private ContainerTypeRepository $containerTypeRepository,
        private StaffPostRepository $staffPostRepository,
        private StaffRepository $staffRepository,
        private TransporterRepository $transporterRepository,
        private ContainerRepository $containerRepository,
        private CustomerIdentificationRepository $customerIdentificationRepository,
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            // ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $xlFile = $this->containerBag->get('kernel.project_dir') . "/files/stock-container-fixture.xlsx";

        $spreadsheet = IOFactory::load($xlFile);

        // RECORD STOCK TVE
        
        $stockTVEsheet = $spreadsheet->getSheet(0);

        $stockTVEsheetArray = $stockTVEsheet->toArray();

        for ($i=2; $i < count($stockTVEsheetArray) ; $i++) { 
            $row = $stockTVEsheetArray[$i] ;

            $center = $this->getTheCenter($row[1]);
            $terminal = $this->getTheTerminal($row[1], $center->getName());
            $shippingCompany = $this->getTheShippingCompany(str_ireplace("/", " ", $row[2]));
            $containerType = $this->getTheContainerType(str_ireplace(" ", "", $row[4]), $shippingCompany);

            $transporter = $this->transporterRepository->findAll()[0];

            $getin = new TerminalGetin();
            $getin->setBDRnumber("bdr-xl-" . time() . "-" . $i );
            $getin->setShippingCompany($shippingCompany);
            $getin->setCarrier($transporter);
            $getin->setCarrierTruckNumber("camion" . time());
            $getin->setTerminal($terminal);
            $created = new \Datetime($row[5]);
            $getin->setCreatedAt($created);

            $this->em->persist($getin);

            $container = new Container();
            $container->setTerminalGetin($getin);
            $container->setContainerNumber($row[3]);
            $container->setContainerType($containerType);
            $created = new \Datetime($row[5]);
            $container->setCreatedAt($created);

            if($row[8]){
                $container->setContainerStatus("bon"); 
            }
            else{
                $container->setContainerStatus("tres_bon"); 
            }

            $container->setContainerStatusAble(true);

            $this->em->persist($container);
        }
        // dd($worksheet);
        $this->em->flush();

        // END RECORD STOCK TVE

        ///////////

        // RECORD STOCK TNR
        
        $stockTNRsheet = $spreadsheet->getSheet(2);

        $stockTNRsheetArray = $stockTNRsheet->toArray();

        for ($i=2; $i < count($stockTNRsheetArray) ; $i++) { 
            $row = $stockTNRsheetArray[$i] ;

            $center = $this->getTheCenter($row[1]);
            $terminal = $this->getTheTerminal("Anosivavaka", $center->getName());
            $shippingCompany = $this->getTheShippingCompany(str_ireplace("/", " ", $row[2]));
            $containerType = $this->getTheContainerType(str_ireplace(" ", "", $row[4]), $shippingCompany);

            $transporter = $this->transporterRepository->findAll()[0];

            $getin = new TerminalGetin();
            $getin->setBDRnumber("bdr-xl-" . time() . "-" . $i );
            $getin->setShippingCompany($shippingCompany);
            $getin->setCarrier($transporter);
            $getin->setCarrierTruckNumber("camion" . time());
            $getin->setTerminal($terminal);
            $created = new \Datetime($row[5]);
            $getin->setCreatedAt($created);

            $this->em->persist($getin);

            $container = new Container();
            $container->setTerminalGetin($getin);
            $container->setContainerNumber($row[3]);
            $container->setContainerType($containerType);
            $created = new \Datetime($row[5]);
            $container->setCreatedAt($created);

            if($row[8]){
                $container->setContainerStatus("bon"); 
            }
            else{
                $container->setContainerStatus("tres_bon"); 
            }

            $container->setContainerStatusAble(true);

            $this->em->persist($container);
        }
        // dd($worksheet);
        $this->em->flush();
        
        // END RECORD STOCK TNR

        // RECORD BOOKING TVE

        $bookingTVEsheet = $spreadsheet->getSheet(1);

        $bookingTVEsheetArray = $bookingTVEsheet->toArray();

        for ($i=2; $i < count($bookingTVEsheetArray) ; $i++) { 
            $row = $bookingTVEsheetArray[$i] ;

            $container = $this->containerRepository->findOneBy(["containerNumber" => $row[2] ]);

            if(! $container){
                continue;
            }

            $customerTitle = $this->customerIdentificationRepository->findOneBy(["customerTitle" => $row[3] ]);

            if(! $customerTitle){
                continue;
            }

            $customer = $customerTitle->getCustomer();

            $booking = new Booking();
            $booking->setRefBooking($row[1] ?? time() );
            $booking->setClient($customer);
            $created = new \Datetime($row[4]);
            $booking->setCreatedAt($created);
            $booking->addContainer($container);

            $container->setReservedStatus(true);

            $this->em->persist($booking);

        }
        // dd($worksheet);
        $this->em->flush();
        

        // END RECORED BOOKING TVE


        // RECORD BOOKING TNR

        
        $bookingTNRsheet = $spreadsheet->getSheet(3);

        $bookingTNRsheetArray = $bookingTNRsheet->toArray();

        for ($i=2; $i < count($bookingTNRsheetArray) ; $i++) { 
            if ($i == 53){
                continue;
            }
            $row = $bookingTNRsheetArray[$i] ;

            $container = $this->containerRepository->findOneBy(["containerNumber" => $row[4] ]);

            if(! $container){
                continue;
            }

            $customerTitle = $this->customerIdentificationRepository->findOneBy(["customerTitle" => $row[5] ]);

            if(! $customerTitle){
                continue;
            }

            $customer = $customerTitle->getCustomer();

            $booking = new Booking();
            $booking->setRefBooking($row[2] ?? time() );
            $booking->setClient($customer);
            $created = new \Datetime($row[3]);
            $booking->setCreatedAt($created);
            $booking->addContainer($container);

            if($row[1] !== null){
                $shipComp = $this->shippingCompanyRepository->findOneBy(["name" => $row[1]]);
                $booking->setShippingCompany($shipComp);
            }

            $container->setReservedStatus(true);

            $this->em->persist($booking);

        }
        // dd($worksheet);
        $this->em->flush();
        

        // END RECORED BOOKING TNR


        $io->success('Fixture recorded');

        return Command::SUCCESS;
    }

    private function getTheCenter(string $centerName): Center
    {
        $center = $this->centerRepository->findOneBy(["name" => $centerName]);

        if(!$center){
            // dd('tsisy');
            $center = new Center();
            $center->setName($centerName);
            $this->em->persist($center);
            $this->em->flush();
        }
        // dd('misy');
        return $center;
    }

    private function getTheTerminal(string $terminalName, string $centerName): Terminal
    {
        $terminal = $this->terminalRepository->findOneBy(["name" => $terminalName]);

        if(!$terminal){
            $center = $this->getTheCenter($centerName);
            $terminal = new Terminal();
            $terminal->setName($terminalName);
            $terminal->setCenter($center);
            $terminal->setCapacity(3000);
            $this->em->persist($terminal);
            $this->em->flush();
        }

        return $terminal;
    }

    private function getTheShippingCompany(string $scName): ShippingCompany
    {
        $shippingC = $this->shippingCompanyRepository->findOneBy(["name" => $scName]);

        if(! $shippingC)
        {
            $shippingC = new ShippingCompany();
            $shippingC->setName($scName);
            $this->em->persist($shippingC);
            $this->em->flush();
        }

        return $shippingC;
    }

    private function getTheContainerType(string $typeLabel, ShippingCompany $shippingCompany): ContainerType
    {
        $containerType = $this->containerTypeRepository->findOneBy(["label" => $typeLabel, "ShippingCompany" => $shippingCompany]);

        if(! $containerType){
            $containerType = $this->containerTypeRepository->findOneBy(["label" => substr_replace($typeLabel, "'", 2, 0), "ShippingCompany" => $shippingCompany]);
        }

        if(! $containerType){
            $containerType = new ContainerType();
            $containerType->setLabel($typeLabel);
            $containerType->setUnite(substr($typeLabel, 0, 2));
            $containerType->setShippingCompany($shippingCompany);
            $this->em->persist($containerType);
            $this->em->flush();
        }

        return $containerType;
    }

}
