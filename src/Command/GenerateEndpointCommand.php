<?php

namespace App\Command;

use App\Entity\Endpoint;
use App\Repository\EndpointRepository;
use App\Service\ResourceFileFinder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'generate:endpoint',
    description: 'Génération des nouveaux endpoints à partir des ressources',
)]
class GenerateEndpointCommand extends Command
{
    public function __construct(
        private ResourceFileFinder $resourceFileFinder,
        private EndpointRepository $endpointRepository,
        private EntityManagerInterface $em,
    )
    {
        parent::__construct(/*$name*/);
    }

    protected function configure(): void
    {
        /* $this
             ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
             ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
         ;*/
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        //truncate data for menu
        $conn = $this->em->getConnection();
        $sql = '
            TRUNCATE TABLE endpoint;
        ';
        $conn->executeQuery($sql)->fetchAllAssociative();
        $output->writeln([
            'Generate new Menu',
            '=========================',
        ]);
        foreach($this->resourceFileFinder->getAllResource() as $resource){
            foreach($resource as $operations){
                foreach($operations["operations"] as $operation){
                    /*Vérifier si le menu*/
                    if(is_null($this->endpointRepository->findOneBy(["name"=> $operation[0]]))/* && !is_null($operation[1]["security"])*/) {
                        /*Create the menu*/
                        $menu = new Endpoint();
                        $output->writeln([
                            'L\'endpoint '. $operation[0].' est généré .',
                            '=========================',
                        ]);
                        $menu->setName($operation[0]);/*name of operation*/
                        $menu->setUriTemplate($operation[1]["uriTemplate"]);/*name of operation*/
                        $menu->setMethod($operation[1]["method"]);/*name of operation*/
                        $menu->setShortName($operation[1]["shortName"]);/*name of operation*/
                        $menu->setSummary($operation[1]["summary"]);
                        $this->endpointRepository->save($menu , true);
                    }
                    else{
                        $output->writeln([
                            'L\'endpoint '. $operation[0].' existe déja .',
                            '=========================',
                        ]);
                    }
                }
            }
        }
        $io->success('Les endpoints sont générées !');
        return Command::SUCCESS;
    }
}
