<?php

namespace App\Command;

use App\Repository\BookingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:set-booking-terminal',
    description: 'Set terminal for booking with null terminal',
)]
class SetBookingTerminalCommand extends Command
{
    public function __construct(
        private BookingRepository $bookingRepository,
        private EntityManagerInterface $entityManagerInterface   
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:set-booking-terminal')
        //     ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        //     ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $bookings = $this->bookingRepository->findBy(['terminal' => null]);

        foreach ($bookings as $booking) {
            $containers = $booking->getContainers();

            if(count($containers) > 0){
                $firstContainer = $containers[0];
                $terminalGetin = $firstContainer->getTerminalGetin();
                $terminal = $terminalGetin->getTerminal();
                
                $booking->setTerminal($terminal);
            }
        }

        $this->entityManagerInterface->flush();
        
        return Command::SUCCESS;
    }
}
