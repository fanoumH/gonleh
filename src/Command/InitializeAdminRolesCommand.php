<?php

namespace App\Command;

use App\Entity\Role;
use App\Repository\RoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:init-admin-roles',
    description: 'Initialize admin roles',
)]
class InitializeAdminRolesCommand extends Command
{
    public function __construct(
        private RoleRepository $roleRepository,
        private EntityManagerInterface $entityManagerInterface   
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:init-admin-roles')
        //     ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        //     ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $superAdmin = $this->roleRepository->findOrCreateAdminRole(true);
        $admin = $this->roleRepository->findOrCreateAdminRole(false);

        $superAdmin->setLabel(Role::NAME_SUPER_ADMIN_LABEL);
        $admin->setLabel(Role::NAME_ADMIN_LABEL);

        $this->entityManagerInterface->persist($superAdmin);
        $this->entityManagerInterface->persist($admin);

        $this->entityManagerInterface->flush();

        return Command::SUCCESS;
    }
}
