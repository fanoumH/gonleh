<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\OtherCostRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OtherCostRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des autres coûts",
            ],
            normalizationContext: ['groups' => 'otherCost:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un autre coût",
            ],
            normalizationContext: ['groups' => 'otherCost:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un autre coût",
            ],
            normalizationContext: ['groups' => 'otherCost:read'],
            denormalizationContext: ['groups' => 'otherCost:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un autre coût",
            ],
            normalizationContext: ['groups' => 'otherCost:read'],
            denormalizationContext: ['groups' => 'otherCost:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un autre coût",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['otherCost:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "label" => "partial",
    "averageCost.name" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "label","averageCost.name"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class OtherCost
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'otherCost:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'otherCost:read','otherCost:create','otherCost:update'
    ])]
    private ?string $label = null;

    #[ORM\ManyToOne(inversedBy: 'otherCosts')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'otherCost:read','otherCost:create','otherCost:update'
    ])]
    private ?AverageCost $averageCost = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'otherCost:read','otherCost:create','otherCost:update'
    ])]
    private ?float $resalePrice = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function getAverageCost(): ?AverageCost
    {
        return $this->averageCost;
    }

    public function setAverageCost(?AverageCost $averageCost): static
    {
        $this->averageCost = $averageCost;

        return $this;
    }

    public function getResalePrice(): ?float
    {
        return $this->resalePrice;
    }

    public function setResalePrice(?float $resalePrice): static
    {
        $this->resalePrice = $resalePrice;

        return $this;
    }
}
