<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\FolderTransactionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: FolderTransactionRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des dossiers avec ces transactions",
            ],
            normalizationContext: ['groups' => 'folderTransaction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un dossier avec ces transactions",
            ],
            normalizationContext: ['groups' => 'folderTransaction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un dossier avec ces transactions",
            ],
            normalizationContext: ['groups' => 'folderTransaction:read'],
            denormalizationContext: ['groups' => 'folderTransaction:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un dossier avec ces transactions",
            ],
            normalizationContext: ['groups' => 'folderTransaction:read'],
            denormalizationContext: ['groups' => 'folderTransaction:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un dossier avec ces transactions",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['folderTransaction:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "folder.name" => "partial",
    "transactions.name" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "folder.name",
    "transactions.name",
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class FolderTransaction
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'folderTransaction:read',
        'folder:read',
        'transaction:read'
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'folderTransactions')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?Folder $folder = null;

    #[ORM\OneToMany(mappedBy: 'folderTransaction', targetEntity: Transaction::class,cascade: ['persist','detach'])]
    #[Groups(groups: [
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update',
        'folder:read', 'folder:create', 'folder:update',
    ])]
    private Collection $transactions;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFolder(): ?Folder
    {
        return $this->folder;
    }

    public function setFolder(?Folder $folder): static
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setFolderTransaction($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getFolderTransaction() === $this) {
                $transaction->setFolderTransaction(null);
            }
        }

        return $this;
    }
}
