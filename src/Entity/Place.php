<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\PlacesToPdfController;
use App\Controller\PlacesToSheetController;
use App\Controller\RemovePlaceController;
use App\Entity\Trait\GeolocatableTrait;
use App\Entity\Trait\HistoryTrait;
use App\Repository\PlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PlaceRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des lieux",
            ],
            normalizationContext: ['groups' => 'place:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/places/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'place:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: PlacesToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/places/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'place:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: PlacesToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un lieux",
            ],
            normalizationContext: ['groups' => 'place:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un lieu",
            ],
            normalizationContext: ['groups' => 'place:read'],
            denormalizationContext: ['groups' => 'place:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un lieux",
            ],
            normalizationContext: ['groups' => 'place:read'],
            denormalizationContext: ['groups' => 'place:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            controller: RemovePlaceController::class,
            openapiContext: [
                "summary" => "Suppression d'un lieux",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "id" => "exact",
    "placeFunctions" => 'exact',
    "placeFunctions.id" => 'exact',
    "placeFunctions.name" => 'iexact',
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"name"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[UniqueEntity('name')]
class Place
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;
    /**
     * history trait
     */
    use HistoryTrait;

    /**
     * GeolocatableTrait
     */
    use GeolocatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'place:read',
        'transportOrder:read',
        'missionOrder:read',
        'missionOrder:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
        'basicRate:read',
        'specificRateTransport:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'place:read', 'place:create', 'place:update',
        'transportOrder:read',
        'missionOrder:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
        'basicRate:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        
    ])]
    private ?string $name = null;

    #[ORM\ManyToMany(targetEntity: PlaceFunction::class, inversedBy: 'places')]
    #[Groups(groups: [
        'place:read', 'place:create', 'place:update',
        'transportOrder:read',
        'missionOrder:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
    ])]
    private Collection $placeFunctions;

    #[ORM\OneToMany(mappedBy: 'departureCity', targetEntity: TransportOrder::class)]
    private Collection $transportOrders;

    #[ORM\OneToMany(mappedBy: 'emptyTCrecoveryLocation', targetEntity: MissionOrder::class)]
    private Collection $emptyTCrecoveryLocations;

    #[ORM\OneToMany(mappedBy: 'loadingPlace', targetEntity: MissionOrder::class)]
    private Collection $loadingPlaces;

    #[ORM\OneToMany(mappedBy: 'deliveryLocation', targetEntity: MissionOrder::class)]
    private Collection $deliveryLocations;

    #[ORM\OneToMany(mappedBy: 'restitutionPlace', targetEntity: MissionOrder::class)]
    private Collection $emptyTCrestitutionPlaces;

    #[ORM\OneToMany(mappedBy: 'departureCity', targetEntity: PriceListStandardTransport::class)]
    private Collection $priceListStandardTransportDepartureCities;

    #[ORM\OneToMany(mappedBy: 'arrivalCity', targetEntity: PriceListStandardTransport::class)]
    private Collection $priceListStandardTransportArrivalCities;

    #[ORM\OneToMany(mappedBy: 'place', targetEntity: BasicRate::class)]
    private Collection $basicRates;

    public function __construct()
    {
        $this->placeFunctions = new ArrayCollection();
        $this->transportOrders = new ArrayCollection();
        $this->emptyTCrecoveryLocations = new ArrayCollection();
        $this->loadingPlaces = new ArrayCollection();
        $this->deliveryLocations = new ArrayCollection();
        $this->emptyTCrestitutionPlaces = new ArrayCollection();
        $this->priceListStandardTransportDepartureCities = new ArrayCollection();
        $this->priceListStandardTransportArrivalCities = new ArrayCollection();
        $this->basicRates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, PlaceFunction>
     */
    public function getPlaceFunctions(): Collection
    {
        return $this->placeFunctions;
    }

    public function addPlaceFunction(PlaceFunction $placeFunction): static
    {
        if (!$this->placeFunctions->contains($placeFunction)) {
            $this->placeFunctions->add($placeFunction);
        }

        return $this;
    }

    public function removePlaceFunction(PlaceFunction $placeFunction): static
    {
        $this->placeFunctions->removeElement($placeFunction);

        return $this;
    }

    /**
     * @return Collection<int, TransportOrder>
     */
    public function getTransportOrders(): Collection
    {
        return $this->transportOrders;
    }

    public function addTransportOrder(TransportOrder $transportOrder): static
    {
        if (!$this->transportOrders->contains($transportOrder)) {
            $this->transportOrders->add($transportOrder);
            $transportOrder->setDepartureCity($this);
        }

        return $this;
    }

    public function removeTransportOrder(TransportOrder $transportOrder): static
    {
        if ($this->transportOrders->removeElement($transportOrder)) {
            // set the owning side to null (unless already changed)
            if ($transportOrder->getDepartureCity() === $this) {
                $transportOrder->setDepartureCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MissionOrder>
     */
    public function getEmptyTCrecoveryLocations(): Collection
    {
        return $this->emptyTCrecoveryLocations;
    }

    public function addEmptyTCrecoveryLocation(MissionOrder $emptyTCrecoveryLocation): static
    {
        if (!$this->emptyTCrecoveryLocations->contains($emptyTCrecoveryLocation)) {
            $this->emptyTCrecoveryLocations->add($emptyTCrecoveryLocation);
            $emptyTCrecoveryLocation->setEmptyTCrecoveryLocation($this);
        }

        return $this;
    }

    public function removeEmptyTCrecoveryLocation(MissionOrder $emptyTCrecoveryLocation): static
    {
        if ($this->emptyTCrecoveryLocations->removeElement($emptyTCrecoveryLocation)) {
            // set the owning side to null (unless already changed)
            if ($emptyTCrecoveryLocation->getEmptyTCrecoveryLocation() === $this) {
                $emptyTCrecoveryLocation->setEmptyTCrecoveryLocation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MissionOrder>
     */
    public function getLoadingPlaces(): Collection
    {
        return $this->loadingPlaces;
    }

    public function addLoadingPlace(MissionOrder $loadingPlace): static
    {
        if (!$this->loadingPlaces->contains($loadingPlace)) {
            $this->loadingPlaces->add($loadingPlace);
            $loadingPlace->setLoadingPlace($this);
        }

        return $this;
    }

    public function removeLoadingPlace(MissionOrder $loadingPlace): static
    {
        if ($this->loadingPlaces->removeElement($loadingPlace)) {
            // set the owning side to null (unless already changed)
            if ($loadingPlace->getLoadingPlace() === $this) {
                $loadingPlace->setLoadingPlace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MissionOrder>
     */
    public function getDeliveryLocations(): Collection
    {
        return $this->deliveryLocations;
    }

    public function addDeliveryLocation(MissionOrder $deliveryLocation): static
    {
        if (!$this->deliveryLocations->contains($deliveryLocation)) {
            $this->deliveryLocations->add($deliveryLocation);
            $deliveryLocation->setDeliveryLocation($this);
        }

        return $this;
    }

    public function removeDeliveryLocation(MissionOrder $deliveryLocation): static
    {
        if ($this->deliveryLocations->removeElement($deliveryLocation)) {
            // set the owning side to null (unless already changed)
            if ($deliveryLocation->getDeliveryLocation() === $this) {
                $deliveryLocation->setDeliveryLocation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MissionOrder>
     */
    public function getEmptyTCRestitutionPlaces(): Collection
    {
        return $this->emptyTCrestitutionPlaces;
    }

    public function addEmptyTCrestitutionPlace(MissionOrder $emptyTCrestitutionPlace): static
    {
        if (!$this->emptyTCrestitutionPlaces->contains($emptyTCrestitutionPlace)) {
            $this->emptyTCrestitutionPlaces->add($emptyTCrestitutionPlace);
            $emptyTCrestitutionPlace->setEmptyTCrestitutionPlace($this);
        }

        return $this;
    }

    public function removeEmptyTCrestitutionPlace(MissionOrder $emptyTCrestitutionPlace): static
    {
        if ($this->emptyTCrestitutionPlaces->removeElement($emptyTCrestitutionPlace)) {
            // set the owning side to null (unless already changed)
            if ($emptyTCrestitutionPlace->getEmptyTCrestitutionPlace() === $this) {
                $emptyTCrestitutionPlace->setEmptyTCrestitutionPlace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PriceListStandardTransport>
     */
    public function getPriceListStandardTransportDepartureCities(): Collection
    {
        return $this->priceListStandardTransportDepartureCities;
    }

    public function addPriceListStandardTransportDepartureCity(PriceListStandardTransport $priceListStandardTransportDepartureCity): static
    {
        if (!$this->priceListStandardTransportDepartureCities->contains($priceListStandardTransportDepartureCity)) {
            $this->priceListStandardTransportDepartureCities->add($priceListStandardTransportDepartureCity);
            $priceListStandardTransportDepartureCity->setDepartureCity($this);
        }

        return $this;
    }

    public function removePriceListStandardTransportDepartureCity(PriceListStandardTransport $priceListStandardTransportDepartureCity): static
    {
        if ($this->priceListStandardTransportDepartureCities->removeElement($priceListStandardTransportDepartureCity)) {
            // set the owning side to null (unless already changed)
            if ($priceListStandardTransportDepartureCity->getDepartureCity() === $this) {
                $priceListStandardTransportDepartureCity->setDepartureCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PriceListStandardTransport>
     */
    public function getPriceListStandardTransportArrivalCities(): Collection
    {
        return $this->priceListStandardTransportArrivalCities;
    }

    public function addPriceListStandardTransportArrivalCity(PriceListStandardTransport $priceListStandardTransportArrivalCity): static
    {
        if (!$this->priceListStandardTransportArrivalCities->contains($priceListStandardTransportArrivalCity)) {
            $this->priceListStandardTransportArrivalCities->add($priceListStandardTransportArrivalCity);
            $priceListStandardTransportArrivalCity->setArrivalCity($this);
        }

        return $this;
    }

    public function removePriceListStandardTransportArrivalCity(PriceListStandardTransport $priceListStandardTransportArrivalCity): static
    {
        if ($this->priceListStandardTransportArrivalCities->removeElement($priceListStandardTransportArrivalCity)) {
            // set the owning side to null (unless already changed)
            if ($priceListStandardTransportArrivalCity->getArrivalCity() === $this) {
                $priceListStandardTransportArrivalCity->setArrivalCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, BasicRate>
     */
    public function getBasicRates(): Collection
    {
        return $this->basicRates;
    }

    public function addBasicRate(BasicRate $basicRate): static
    {
        if (!$this->basicRates->contains($basicRate)) {
            $this->basicRates->add($basicRate);
            $basicRate->setPlace($this);
        }

        return $this;
    }

    public function removeBasicRate(BasicRate $basicRate): static
    {
        if ($this->basicRates->removeElement($basicRate)) {
            // set the owning side to null (unless already changed)
            if ($basicRate->getPlace() === $this) {
                $basicRate->setPlace(null);
            }
        }

        return $this;
    }
}
