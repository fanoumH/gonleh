<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Trait\HistoryTrait;
use App\Repository\SolvencyRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: SolvencyRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource]
class Solvency
{
    use HistoryTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','folder:read','customerContact:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read'
    ])]
    private ?float $inProgressAuthorized = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read'
    ])]
    private ?float $creditInsurance = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read'
    ])]
    private ?int $dueDate = null;

    #[ORM\ManyToOne(inversedBy: 'solvencies')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read'
    ])]
    private ?RiskCode $riskCode = null;

    #[ORM\OneToOne(mappedBy: 'solvency', cascade: ['persist', 'remove'])]
    private ?Customer $customer = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInProgressAuthorized(): ?float
    {
        return $this->inProgressAuthorized;
    }

    public function setInProgressAuthorized(?float $inProgressAuthorized): static
    {
        $this->inProgressAuthorized = $inProgressAuthorized;

        return $this;
    }

    public function getCreditInsurance(): ?float
    {
        return $this->creditInsurance;
    }

    public function setCreditInsurance(?float $creditInsurance): static
    {
        $this->creditInsurance = $creditInsurance;

        return $this;
    }

    public function getDueDate(): ?int
    {
        return $this->dueDate;
    }

    public function setDueDate(?int $dueDate): static
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getRiskCode(): ?RiskCode
    {
        return $this->riskCode;
    }

    public function setRiskCode(?RiskCode $riskCode): static
    {
        $this->riskCode = $riskCode;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        // unset the owning side of the relation if necessary
        if ($customer === null && $this->customer !== null) {
            $this->customer->setSolvency(null);
        }

        // set the owning side of the relation if necessary
        if ($customer !== null && $customer->getSolvency() !== $this) {
            $customer->setSolvency($this);
        }

        $this->customer = $customer;

        return $this;
    }
}
