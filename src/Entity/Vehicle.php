<?php

namespace App\Entity;

use App\ApiPlatform\Filter\AverageCostFilter;
use App\ApiPlatform\Filter\VehicleFilter;
use App\Entity\RentalOrder;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\VehicleRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\VehiclesToPdfController;
use Doctrine\Common\Collections\Collection;
use App\Controller\VehiclesToSheetController;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\VehicleAllocateDriversController;
use App\Controller\VehicleAllocateTrailersController;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: VehicleRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des vehicules",
            ],
            normalizationContext: ['groups' => 'vehicle:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/vehicles/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'vehicle:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: VehiclesToPdfController::class,
        ),
        new GetCollection(
            uriTemplate: '/vehicles/all/sheet',
            openapiContext: [
                "summary" => "Export to spreadsheet",
            ],
            normalizationContext: ['groups' => 'vehicle:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: VehiclesToSheetController::class,
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle:read'],
            denormalizationContext: ['groups' => 'vehicle:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle:read'],
            denormalizationContext: ['groups' => 'vehicle:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un vehicule",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        // new Patch(
        //     uriTemplate: "/vehicles/allocate/drivers/{id}",
        //     controller: VehicleAllocateDriversController::class,
        //     openapiContext: [
        //         "summary" => "Allocation de plusieurs chauffeurs",
        //     ],
        //     normalizationContext: ['groups' => 'vehicle:read'],
        //     security: "is_granted('PERMISSION_ALLOWED' , object)"
        // ),
        // new Patch(
        //     uriTemplate: "/vehicles/allocate/trailers/{id}",
        //     controller: VehicleAllocateTrailersController::class,
        //     openapiContext: [
        //         "summary" => "Allocation de plusieurs remorques",
        //     ],
        //     normalizationContext: ['groups' => 'vehicle:read'],
        //     security: "is_granted('PERMISSION_ALLOWED' , object)"
        // ),
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "registrationNumber" => "ipartial",
    "id" => "iexact",
    "type" => "iexact",
    "state" => "iexact",
    "reservoirCapacity" => "ipartial",
    "vehicleBrand.id" => "iexact",
    "vehicleBrand.name" => "ipartial",
    "typeOfEngine" => "iexact",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"registrationNumber", "type" , "state", "payload", "reservoirCapacity",
    "vehicleBrand.id", "vehicleBrand.name" , "vehicleModel.id" , "vehicleModel.name"
],arguments:  ['orderParameterName' => 'order'])
]

#[ApiFilter(
    BooleanFilter::class, properties: [
        'hasTrailer',
        'isPulled,'
    ]
)]
#[ApiFilter(
    VehicleFilter::class,properties: ['vehicle']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
#[UniqueEntity(['registrationNumber'])]
// #[UniqueEntity(['chassisNumber'])]
class Vehicle
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    /**
     * state / status
     * 1 disponible STATE_AVAILABLE
     * 2 pas de chauffeur STATE_NO_DRIVER
     * 3 en maintenance STATE_UNDER_MAINTENANCE
     * 4 à vendre STATE_FOR_SALE
     * 5 en congé STATE_OFF_DUTY
     * 6 accidenté STATE_ACCIDENT
     */
    public const STATE_ON_THE_WAY = 0;
    public const STATE_AVAILABLE = 1;
    public const STATE_NO_DRIVER = 2;
    public const STATE_UNDER_MAINTENANCE = 3;
    public const STATE_FOR_SALE = 4;
    public const STATE_OFF_DUTY = 5;
    public const STATE_ACCIDENT = 6;
    public const STATE_COUPLED = 7;
    public const STATE_WAITING_COUPLING = 8;
    public const STATE_BROKEN_DOWN = 9;
    public const STATE_OTHER = 10;


    /**
     * state of vehicle allow to do allocation
     */
    public const STATES_ALLOW_TO_DO_ALLOCATION = [
        self::STATE_NO_DRIVER,self::STATE_AVAILABLE
    ];
    /**
     * Utilité
     * 1 Transport Import / Export
     * 2 Transport en ville
     * 3 Voiture de service
     */
    public const UTILITY_IMPORT_EXPORT_TRANSPORT = 1;
    public const UTILITY_CITY_TRANSPORT = 2;
    public const UTILITY_SERVICE_CAR = 3;

    /**
     * type of engine / type de moteur
     * 1 Diesel x
     * 2 Essence x
     * 1,2,3,4,5,6
     */
    // public const TYPE_OF_ENGINE_DIESEL = 1;
    // public const TYPE_OF_ENGINE_ESSENCE = 2;
    public const TYPE_OF_ENGINE_1 = 1;
    public const TYPE_OF_ENGINE_2 = 2;
    public const TYPE_OF_ENGINE_3 = 3;
    public const TYPE_OF_ENGINE_4 = 4;
    public const TYPE_OF_ENGINE_5 = 5;
    public const TYPE_OF_ENGINE_6 = 6;

    /**
     * type / type de vehicule
     * 1 Tracteur
     * 2 Remorque
     * [
     *  1 => "Plateau", 2 => "Semi", 3 => "Remorque, 4 => "élévateur"
     * ]
     */
    // public const TYPE_TRACTOR = 1;
    // public const TYPE_TRAILER = 2;

    public const TYPES_VEHICLE = [1,2,3,4];

    public const TYPE_TRAILER = 1;
    public const TYPE_SEMI = 2;
    public const TYPE_ELEVATOR = 3;
    public const TYPE_FLAT = 4;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'vehicle:read',
        'staff:read',
        'coupling:read',
        'transportOrder:read',
        'rentalOrder:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
        'staff:read',
        'coupling:read',
        'transportOrder:read',
        'rentalOrder:read',
        'missionOrder:read',
    ])]
    private ?string $registrationNumber = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Assert\NotBlank]
    #[Assert\Choice(choices: [
        self::STATE_ON_THE_WAY,
        self::STATE_AVAILABLE,
        self::STATE_NO_DRIVER,
        self::STATE_UNDER_MAINTENANCE,
        self::STATE_FOR_SALE,
        self::STATE_OFF_DUTY,
        self::STATE_ACCIDENT,
        self::STATE_COUPLED,
        self::STATE_WAITING_COUPLING,
        self::STATE_BROKEN_DOWN,
        self::STATE_OTHER,
    ])]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
        'transportOrder:read',
    ])]
    private ?int $state = self::STATE_NO_DRIVER;

    #[ORM\Column(nullable: true)]
    #[Assert\PositiveOrZero]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?float $averageConsumption = 0;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    #[Assert\LessThan('today')]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?\DateTimeImmutable $firstCirculationAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    // #[Assert\NotBlank]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
        'transportOrder:read',
        'rentalOrder:read',
    ])]
    private ?string $chassisNumber = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
    ])]
    private ?bool $hasTag = false;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?bool $hasBuzzer = false;

    #[ORM\Column(nullable: true)]
    #[Assert\PositiveOrZero]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?float $power = null;

    #[ORM\Column(nullable: true)]
    #[Assert\PositiveOrZero]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?float $unloadedWeight = 0;

    #[ORM\Column(nullable: true)]
    #[Assert\PositiveOrZero]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?float $reservoirCapacity = 0;

    #[ORM\Column(nullable: true)]
    #[Assert\PositiveOrZero]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?float $reservoirOilCapacity = 0;

    #[ORM\Column(nullable: true)]
    #[Assert\PositiveOrZero]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?float $payload = 0;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    #[Assert\Choice(choices: [
        self::UTILITY_IMPORT_EXPORT_TRANSPORT,
        self::UTILITY_CITY_TRANSPORT,
        self::UTILITY_SERVICE_CAR
    ])]
    private ?int $utility = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    #[Assert\Choice(choices: [
        self::TYPE_OF_ENGINE_1,
        self::TYPE_OF_ENGINE_2,
        self::TYPE_OF_ENGINE_3,
        self::TYPE_OF_ENGINE_4,
        self::TYPE_OF_ENGINE_5,
        self::TYPE_OF_ENGINE_6,
    ])]
    private ?int $typeOfEngine = null;

    #[ORM\Column(nullable: true)]
    #[Assert\PositiveOrZero]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?float $mileage = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update'
    ])]
    private ?string $gender = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'vehicles')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
        'coupling:read',
        'transportOrder:read',
        'rentalOrder:read',
    ])]
    private ?VehicleBrand $vehicleBrand = null;

    #[ORM\ManyToOne(cascade: ['persist'],inversedBy: 'vehicles')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
        'coupling:read',
        'transportOrder:read',
        'rentalOrder:read',
    ])]
    private ?VehicleModel $vehicleModel = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
        'transportOrder:read',
    ])]
    #[Assert\NotBlank]
    #[Assert\Choice(choices: [
        self::TYPE_FLAT,
        self::TYPE_SEMI,
        self::TYPE_TRAILER,
        self::TYPE_ELEVATOR,
    ])]
    private ?int $type = null;

    // #[ORM\OneToMany(mappedBy: 'vehicle', targetEntity: Staff::class)]
    // #[Groups(groups: [
    //     'vehicle:read'
    // ])]
    // private Collection $drivers;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'trailers')]
    // #[Groups(groups: [
    //     'vehicle:read',
    //     'staff:read'
    // ])]
    private ?self $tractor = null;

    #[ORM\OneToMany(mappedBy: 'tractor', targetEntity: self::class)]
    // #[Groups(groups: [
    //     'vehicle:read',
    //     'staff:read'
    // ])]
    private Collection $trailers;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    #[Groups(groups: [
        'vehicle:read',
        'rentalOrder:read',
    ])]
    private ?bool $hasTrailer = null;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    #[Groups(groups: [
        'vehicle:read',
        'rentalOrder:read',
    ])]
    private ?bool $isPulled = null;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    #[Groups(groups: [
        'vehicle:read', 'vehicle:create', 'vehicle:update',
        'rentalOrder:read',
        'missionOrder:read',
    ])]
    private ?Staff $driver = null;

    #[ORM\OneToMany(mappedBy: 'vehicle', targetEntity: RentalOrder::class)]
    private Collection $rentalOrders;

    #[ORM\OneToMany(mappedBy: 'trailer', targetEntity: RentalOrder::class)]
    private Collection $trailerRentalOrders;
    #[ORM\OneToMany(mappedBy: 'affectedTruck', targetEntity: TransportOrder::class)]
    private Collection $transportOrders;

    #[Groups(groups: [
        'vehicle:read',
        'rentalOrder:read',
    ])]
    private ?self $trailerData = null;

    #[Groups(groups: [
        'vehicle:read',
        'rentalOrder:read',
    ])]
    private ?int $couplingId = null;

    public function __construct()
    {
        // $this->drivers = new ArrayCollection();
        $this->trailers = new ArrayCollection();
        $this->rentalOrders = new ArrayCollection();
        $this->trailerRentalOrders = new ArrayCollection();
        $this->transportOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegistrationNumber(): ?string
    {
        return $this->registrationNumber;
    }

    public function setRegistrationNumber(?string $registrationNumber): static
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): static
    {
        $this->state = $state;

        return $this;
    }

    public function getAverageConsumption(): ?float
    {
        return $this->averageConsumption;
    }

    public function setAverageConsumption(?float $averageConsumption): static
    {
        $this->averageConsumption = $averageConsumption;

        return $this;
    }

    public function getFirstCirculationAt(): ?\DateTimeImmutable
    {
        return $this->firstCirculationAt;
    }

    public function setFirstCirculationAt(\DateTimeImmutable $firstCirculationAt): static
    {
        $this->firstCirculationAt = $firstCirculationAt;

        return $this;
    }

    public function getChassisNumber(): ?string
    {
        return $this->chassisNumber;
    }

    public function setChassisNumber(?string $chassisNumber): static
    {
        $this->chassisNumber = $chassisNumber;

        return $this;
    }

    public function isHasTag(): ?bool
    {
        return $this->hasTag;
    }

    public function setHasTag(?bool $hasTag): static
    {
        $this->hasTag = $hasTag;

        return $this;
    }

    public function isHasBuzzer(): ?bool
    {
        return $this->hasBuzzer;
    }

    public function setHasBuzzer(?bool $hasBuzzer): static
    {
        $this->hasBuzzer = $hasBuzzer;

        return $this;
    }

    public function getPower(): ?float
    {
        return $this->power;
    }

    public function setPower(?float $power): static
    {
        $this->power = $power;

        return $this;
    }

    public function getUnloadedWeight(): ?float
    {
        return $this->unloadedWeight;
    }

    public function setUnloadedWeight(?float $unloadedWeight): static
    {
        $this->unloadedWeight = $unloadedWeight;

        return $this;
    }

    public function getReservoirCapacity(): ?float
    {
        return $this->reservoirCapacity;
    }

    public function setReservoirCapacity(?float $reservoirCapacity): static
    {
        $this->reservoirCapacity = $reservoirCapacity;

        return $this;
    }

    public function getReservoirOilCapacity(): ?float
    {
        return $this->reservoirOilCapacity;
    }

    public function setReservoirOilCapacity(?float $reservoirOilCapacity): static
    {
        $this->reservoirOilCapacity = $reservoirOilCapacity;

        return $this;
    }

    public function getPayload(): ?float
    {
        return $this->payload;
    }

    public function setPayload(?float $payload): static
    {
        $this->payload = $payload;

        return $this;
    }

    public function getUtility(): ?int
    {
        return $this->utility;
    }

    public function setUtility(?int $utility): static
    {
        $this->utility = $utility;

        return $this;
    }

    public function getTypeOfEngine(): ?int
    {
        return $this->typeOfEngine;
    }

    public function setTypeOfEngine(?int $typeOfEngine): static
    {
        $this->typeOfEngine = $typeOfEngine;

        return $this;
    }

    public function getMileage(): ?float
    {
        return $this->mileage;
    }

    public function setMileage(?float $mileage): static
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): static
    {
        $this->gender = $gender;

        return $this;
    }

    public function getVehicleBrand(): ?VehicleBrand
    {
        return $this->vehicleBrand;
    }

    public function setVehicleBrand(?VehicleBrand $vehicleBrand): static
    {
        $this->vehicleBrand = $vehicleBrand;

        return $this;
    }

    public function getVehicleModel(): ?VehicleModel
    {
        return $this->vehicleModel;
    }

    public function setVehicleModel(?VehicleModel $vehicleModel): static
    {
        $this->vehicleModel = $vehicleModel;

        return $this;
    }

    /*#[Assert\Callback]
    public function validateVehicleModel(ExecutionContextInterface $context, $payload)
    {
        dump($this->getVehicleModel());
        dump($this->getVehicleBrand());
        dump($this->getVehicleBrand());
        dd($this->getVehicleModel()->getVehicleBrand());
        if ($this->getVehicleModel() && $this->getVehicleBrand() && $this->getVehicleModel()->getVehicleBrand() !== $this->getVehicleBrand()) {
            $context->buildViolation('validate.vehicle.brand.model')
                ->atPath('vehicleBrand')
                ->addViolation();
        }
    }*/

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): static
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, Staff>
     */
    public function getDrivers(): Collection
    {
        return $this->drivers;
    }

    public function addDriver(Staff $driver): static
    {
        if (!$this->drivers->contains($driver)) {
            $this->drivers->add($driver);
            $driver->setVehicle($this);
        }

        return $this;
    }

    public function removeDriver(Staff $driver): static
    {
        if ($this->drivers->removeElement($driver)) {
            // set the owning side to null (unless already changed)
            if ($driver->getVehicle() === $this) {
                $driver->setVehicle(null);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearDrivers(): static
    {
        foreach ($this->drivers as $driver) {
            $this->removeDriver($driver);
        }
        return $this;
    }

    public function setDrivers(Collection $drivers): static
    {
        // remove actual driver
        $this->clearDrivers();
        // add driver
        foreach ($drivers as $driver) {
            $this->addDriver($driver);
        }
        return $this;
    }

    public function getTractor(): ?self
    {
        return $this->tractor;
    }

    public function setTractor(?self $tractor): static
    {
        $this->tractor = $tractor;

        return $this;
    }


    public function setTrailers(Collection $trailers): static
    {
        // remove actual driver
        $this->clearTrailers();
        // add driver
        foreach ($trailers as $trailer) {
            $this->addTrailer($trailer);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function clearTrailers(): static
    {
        foreach ($this->trailers as $trailer) {
            $this->removeTrailer($trailer);
        }
        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getTrailers(): Collection
    {
        return $this->trailers;
    }

    public function addTrailer(self $trailer): static
    {
        if (!$this->trailers->contains($trailer)) {
            $this->trailers->add($trailer);
            $trailer->setTractor($this);
        }

        return $this;
    }

    public function removeTrailer(self $trailer): static
    {
        if ($this->trailers->removeElement($trailer)) {
            // set the owning side to null (unless already changed)
            if ($trailer->getTractor() === $this) {
                $trailer->setTractor(null);
            }
        }

        return $this;
    }

    public function isHasTrailer(): ?bool
    {
        return $this->hasTrailer;
    }

    public function setHasTrailer(bool $hasTrailer): static
    {
        $this->hasTrailer = $hasTrailer;

        return $this;
    }

    public function isIsPulled(): ?bool
    {
        return $this->isPulled;
    }

    public function setIsPulled(bool $isPulled): static
    {
        $this->isPulled = $isPulled;

        return $this;
    }

    public function getDriver(): ?Staff
    {
        return $this->driver;
    }

    public function setDriver(?Staff $driver): static
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * @return Collection<int, RentalOrder>
     */
    public function getRentalOrders(): Collection
    {
        return $this->rentalOrders;
    }

    public function addRentalOrder(RentalOrder $rentalOrder): static
    {
        if (!$this->rentalOrders->contains($rentalOrder)) {
            $this->rentalOrders->add($rentalOrder);
            $rentalOrder->setVehicle($this);
        }

        return $this;
    }
    
    /**
     * @return Collection<int, TransportOrder>
     */
    public function getTransportOrders(): Collection
    {
        return $this->transportOrders;
    }

    public function addTransportOrder(TransportOrder $transportOrder): static
    {
        if (!$this->transportOrders->contains($transportOrder)) {
            $this->transportOrders->add($transportOrder);
            $transportOrder->setAffectedTruck($this);
        }

        return $this;
    }

    public function removeRentalOrder(RentalOrder $rentalOrder): static
    {
        if ($this->rentalOrders->removeElement($rentalOrder)) {
            // set the owning side to null (unless already changed)
            if ($rentalOrder->getVehicle() === $this) {
                $rentalOrder->setVehicle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RentalOrder>
     */
    public function getTrailerRentalOrders(): Collection
    {
        return $this->trailerRentalOrders;
    }

    public function addTrailerRentalOrder(RentalOrder $trailerRentalOrder): static
    {
        if (!$this->trailerRentalOrders->contains($trailerRentalOrder)) {
            $this->trailerRentalOrders->add($trailerRentalOrder);
            $trailerRentalOrder->setTrailer($this);
        }

        return $this;
    }

    public function removeTrailerRentalOrder(RentalOrder $trailerRentalOrder): static
    {
        if ($this->trailerRentalOrders->removeElement($trailerRentalOrder)) {
            // set the owning side to null (unless already changed)
            if ($trailerRentalOrder->getTrailer() === $this) {
                $trailerRentalOrder->setTrailer(null);
            }
        }

        return $this;
    }
    
    public function removeTransportOrder(TransportOrder $transportOrder): static
    {
        if ($this->transportOrders->removeElement($transportOrder)) {
            // set the owning side to null (unless already changed)
            if ($transportOrder->getAffectedTruck() === $this) {
                $transportOrder->setAffectedTruck(null);
            }
        }
        return $this;
    }

    public function getTrailerData(): ?self 
    {
        return $this->trailerData;
    }

    public function setTrailerData(?self $trailer): static
    {
        $this->trailerData = $trailer;
        return $this;
    }

    public function getCouplingId(): ?int 
    {
        return $this->couplingId;
    }

    public function setCouplingId(?int $couplingId): static
    {
        $this->couplingId = $couplingId;
        return $this;
    }

}
