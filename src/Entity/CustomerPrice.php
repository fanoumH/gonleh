<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Trait\HistoryTrait;
use App\Repository\CustomerPriceRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: CustomerPriceRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
//#[ApiResource]
class CustomerPrice
{
    use HistoryTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read',
        'folder:read',
        'customerContact:read',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?float $discountRate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?float $escompteRate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?float $countRaised = null;

    #[ORM\OneToOne(mappedBy: 'customerPrice', cascade: ['persist', 'remove'])]
    private ?Customer $customer = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?float $rfaRate = null;

    #[ORM\ManyToOne(inversedBy: 'customerPrices')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?FareCategory $fareCategory = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDiscountRate(): ?float
    {
        return $this->discountRate;
    }

    public function setDiscountRate(?float $discountRate): static
    {
        $this->discountRate = $discountRate;

        return $this;
    }

    public function getEscompteRate(): ?float
    {
        return $this->escompteRate;
    }

    public function setEscompteRate(?float $escompteRate): static
    {
        $this->escompteRate = $escompteRate;

        return $this;
    }

    public function getCountRaised(): ?float
    {
        return $this->countRaised;
    }

    public function setCountRaised(?float $countRaised): static
    {
        $this->countRaised = $countRaised;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        // unset the owning side of the relation if necessary
        if ($customer === null && $this->customer !== null) {
            $this->customer->setCustomerPrice(null);
        }

        // set the owning side of the relation if necessary
        if ($customer !== null && $customer->getCustomerPrice() !== $this) {
            $customer->setCustomerPrice($this);
        }

        $this->customer = $customer;

        return $this;
    }

    public function getRfaRate(): ?float
    {
        return $this->rfaRate;
    }

    public function setRfaRate(?float $rfaRate): static
    {
        $this->rfaRate = $rfaRate;

        return $this;
    }

    public function getFareCategory(): ?FareCategory
    {
        return $this->fareCategory;
    }

    public function setFareCategory(?FareCategory $fareCategory): static
    {
        $this->fareCategory = $fareCategory;

        return $this;
    }
}
