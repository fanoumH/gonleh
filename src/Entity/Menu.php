<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\ApiPlatform\Filter\LeafMenuFilter;
use App\Controller\MenuHierarchyController;
use App\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MenuRepository::class)]
#[ORM\Table(name: '`menus`')]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des menus",
            ],
            normalizationContext: ['groups' => 'menu:read'],
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail utilisateur",
            ],
            normalizationContext: ['groups' => 'menu:read'],
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un menu",
            ],
            normalizationContext: ['groups' => 'menu:read'],
            denormalizationContext: ['groups' => 'menu:create'],
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification menu",
            ],
            normalizationContext: ['groups' => 'menu:read'],
            denormalizationContext: ['groups' => 'menu:update','role:update','quality:write'],
        ),
        new GetCollection(
            uriTemplate: "/menu_list",
            defaults: [
                '_api_persist' => false,
            ],
            controller: MenuHierarchyController::class,
            openapiContext: [
                "summary" => "Liste des hiérarchies des menus",
            ],
            normalizationContext: ['groups' => 'menu:read'],
            name: "hierachy_menus",
        ),
    ],
)]
#[ApiFilter(
    LeafMenuFilter::class,properties: ['parent']
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id"
],
    arguments:[
        'orderParameterName' => 'order'
    ]
)]
class Menu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: ['menu:read'])]
    #[ApiProperty(
        identifier: true,
    )]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true, nullable: true)]
    #[Groups(groups: ['menu:read', 'menu:create', 'menu:update'])]
    private ?string $name = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'children')]
    #[Groups(groups: ['menu:read', 'menu:create', 'menu:update'])]
    private ?self $parent = null;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    #[Groups(groups: ['menu:read', 'menu:create', 'menu:update'])]
    private Collection $children;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true, options: ['default' => false])]
    #[Groups(groups: ['menu:read', 'menu:create', 'menu:update'])]
    #[ApiProperty(
        writable: true,
    )]
    private ?bool $is_leaf = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: ['menu:read', 'menu:create', 'menu:update'])]
    private ?array $permissions = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): static
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): static
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): static
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function isIsLeaf(): ?bool
    {
        return $this->is_leaf;
    }

    public function setIsLeaf(?bool $is_leaf): static
    {
        $this->is_leaf = $is_leaf;

        return $this;
    }

    public function getPermissions(): ?array
    {
        return $this->permissions;
    }

    public function setPermissions(?array $permissions): static
    {
        $this->permissions = $permissions;

        return $this;
    }
}
