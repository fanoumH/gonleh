<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: DepartmentRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des départements",
            ],
            normalizationContext: ['groups' => 'department:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un département",
            ],
            normalizationContext: ['groups' => 'department:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un département",
            ],
            normalizationContext: ['groups' => 'department:read'],
            denormalizationContext: ['groups' => 'department:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un département",
            ],
            normalizationContext: ['groups' => 'department:read'],
            denormalizationContext: ['groups' => 'department:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un département",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['department:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "name"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class Department
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'department:read','folder:read','transaction:read',
        'serviceType:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'department:read','department:create','department:update','folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update',
        'serviceType:read','serviceType:create','serviceType:update'
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: Transaction::class,cascade: ['persist','detach'])]
    private Collection $transactions;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'department:read','department:create','department:update','folder:read','transaction:read',
        'serviceType:read'
    ])]
    private ?string $abbreviation = null;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: ServiceType::class)]
    private Collection $serviceTypes;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
        $this->serviceTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setDepartment($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getDepartment() === $this) {
                $transaction->setDepartment(null);
            }
        }

        return $this;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(?string $abbreviation): static
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * @return Collection<int, ServiceType>
     */
    public function getServiceTypes(): Collection
    {
        return $this->serviceTypes;
    }

    public function addServiceType(ServiceType $serviceType): static
    {
        if (!$this->serviceTypes->contains($serviceType)) {
            $this->serviceTypes->add($serviceType);
            $serviceType->setDepartment($this);
        }

        return $this;
    }

    public function removeServiceType(ServiceType $serviceType): static
    {
        if ($this->serviceTypes->removeElement($serviceType)) {
            // set the owning side to null (unless already changed)
            if ($serviceType->getDepartment() === $this) {
                $serviceType->setDepartment(null);
            }
        }

        return $this;
    }
}
