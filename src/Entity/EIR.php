<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EIRRepository;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\EIRgetinPdfController;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: EIRRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des EIR",
            ],
            normalizationContext: ['groups' => 'eir:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un EIR",
            ],
            normalizationContext: ['groups' => 'eir:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            uriTemplate: '/e_i_rs/{id}/pdf',
            openapiContext: [
                "summary" => "Download one EIR",
            ],
            controller: EIRgetinPdfController::class,
            normalizationContext: [],
            denormalizationContext: [],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un EIR",
            ],
            normalizationContext: ['groups' => 'eir:read'],
            denormalizationContext: ['groups' => 'eir:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un EIR",
            ],
            normalizationContext: ['groups' => 'eir:read'],
            denormalizationContext: ['groups' => 'eir:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un EIR",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
        "sealNumber" => "ipartial",
        "refEIR" => "ipartial",
    ]
)]

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
class EIR
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;
    
    const DAMAGE_POSITION_TOIT_EXT = 1;
    const DAMAGE_POSITION_PORTE_EXT = 2;
    const DAMAGE_POSITION_COTE_DROIT_EXT = 3;
    const DAMAGE_POSITION_COTE_GAUCHE_EXT = 4;
    const DAMAGE_POSITION_SOL_EXT = 5;
    const DAMAGE_POSITION_DOS_EXT = 6;
    const DAMAGE_POSITION_TOIT_INT = 7;
    const DAMAGE_POSITION_PORTE_INT = 8;
    const DAMAGE_POSITION_COTE_DROIT_INT = 9;
    const DAMAGE_POSITION_COTE_GAUCHE_INT = 10;
    const DAMAGE_POSITION_SOL_INT = 11;
    const DAMAGE_POSITION_DOS_INT = 12;

    const DAMAGE_TYPE_BROKEN = "B";
    const DAMAGE_TYPE_CUT = "c";
    const DAMAGE_TYPE_DENTED = "D";
    const DAMAGE_TYPE_HOLED = "H";
    const DAMAGE_TYPE_MISSING = "M";
    const DAMAGE_TYPE_TORN = "T";


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'eir:read',
        'container:read',
        'getin:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    #[Groups(groups: [
        'eir:read',
        'eir:create',
        'eir:update',
        'container:create',
        'getin:create',
        'container:read',
        'getin:read',
    ])]
    private ?bool $isFull = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'eir:read',
        'eir:create',
        'eir:update',
        'container:create',
        'getin:create',
        'container:read',
        'getin:read',
    ])]
    private ?string $sealNumber = null;

    // #[ORM\Column(type: Types::TEXT, nullable: true)]
    // #[Groups(groups: [
    //     'eir:read',
    //     'eir:create',
    //     'eir:update',
    //     'container:create',
    //     'getin:create',
    //     'container:read',
    //     'getin:read',
    // ])]
    // private ?string $damageReport = null;

    // #[ORM\Column(type: Types::TEXT, nullable: true)]
    // #[Groups(groups: [
    //     'eir:read',
    //     'eir:create',
    //     'eir:update',
    //     'container:create',
    //     'getin:create',
    //     'container:read',
    //     'getin:read',
    // ])]
    // private ?string $damageComment = null;

    // #[ORM\Column(type: Types::TEXT, nullable: true)]
    // #[Groups(groups: [
    //     'eir:read',
    //     'eir:create',
    //     'eir:update',
    //     'container:create',
    //     'getin:create',
    //     'container:read',
    //     'getin:read',
    // ])]
    // private ?string $remarks = null;

    #[ORM\OneToOne(inversedBy: 'eir', cascade: ['persist', 'remove'])]
    #[Groups(groups: [
        'eir:read',
        'eir:create',
        'eir:update',
    ])]
    private ?Container $container = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(groups: [
        'eir:read',
        'eir:create',
        'eir:update',
        'container:create',
        'getin:create',
        'container:read',
        'getin:read',
    ])]
    private ?string $damages = null;

    #[ORM\ManyToOne(inversedBy: 'eIRs')]
    #[Groups(groups: [
        'eir:create',
        'eir:update',
        'container:read',
        'getin:read',
    ])]
    private ?User $creatorUser = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'eir:read',
        'eir:create',
        'eir:update',
        'container:create',
        'getin:create',
        'container:read',
        'getin:read',
    ])]
    private ?string $refEIR = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isIsFull(): ?bool
    {
        return $this->isFull;
    }

    public function setIsFull(?bool $isFull): static
    {
        $this->isFull = $isFull;

        return $this;
    }

    public function getSealNumber(): ?string
    {
        return $this->sealNumber;
    }

    public function setSealNumber(?string $sealNumber): static
    {
        $this->sealNumber = $sealNumber;

        return $this;
    }

    // public function getDamageReport(): ?string
    // {
    //     return $this->damageReport;
    // }

    // public function setDamageReport(?string $damageReport): static
    // {
    //     $this->damageReport = $damageReport;

    //     return $this;
    // }

    // public function getDamageComment(): ?string
    // {
    //     return $this->damageComment;
    // }

    // public function setDamageComment(?string $damageComment): static
    // {
    //     $this->damageComment = $damageComment;

    //     return $this;
    // }

    // public function getRemarks(): ?string
    // {
    //     return $this->remarks;
    // }

    // public function setRemarks(?string $remarks): static
    // {
    //     $this->remarks = $remarks;

    //     return $this;
    // }

    public function getContainer(): ?Container
    {
        return $this->container;
    }

    public function setContainer(?Container $container): static
    {
        $this->container = $container;

        return $this;
    }

    public function getDamages(): ?string
    {
        return $this->damages;
    }

    public function setDamages(?string $damages): static
    {
        $this->damages = $damages;

        return $this;
    }

    public function getCreatorUser(): ?User
    {
        return $this->creatorUser;
    }

    public function setCreatorUser(?User $creatorUser): static
    {
        $this->creatorUser = $creatorUser;

        return $this;
    }

    public function getRefEIR(): ?string
    {
        return $this->refEIR;
    }

    public function setRefEIR(?string $refEIR): static
    {
        $this->refEIR = $refEIR;

        return $this;
    }
}
