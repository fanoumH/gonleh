<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CustomerQualityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CustomerQualityRepository::class)]
#[ApiResource]
class CustomerQuality
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'customerQuality', targetEntity: CustomerIdentification::class)]
    private Collection $customerIdentifications;

    public function __construct()
    {
        $this->customerIdentifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, CustomerIdentification>
     */
    public function getCustomerIdentifications(): Collection
    {
        return $this->customerIdentifications;
    }

    public function addCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if (!$this->customerIdentifications->contains($customerIdentification)) {
            $this->customerIdentifications->add($customerIdentification);
            $customerIdentification->setCustomerQuality($this);
        }

        return $this;
    }

    public function removeCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if ($this->customerIdentifications->removeElement($customerIdentification)) {
            // set the owning side to null (unless already changed)
            if ($customerIdentification->getCustomerQuality() === $this) {
                $customerIdentification->setCustomerQuality(null);
            }
        }

        return $this;
    }
}
