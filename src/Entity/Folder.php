<?php

namespace App\Entity;

use App\Entity\Status;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\FolderRepository;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: FolderRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des dossiers",
            ],
            normalizationContext: ['groups' => 'folder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un dossier",
            ],
            normalizationContext: ['groups' => 'folder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un dossier",
            ],
            normalizationContext: ['groups' => 'folder:read'],
            denormalizationContext: ['groups' => 'folder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un dossier",
            ],
            normalizationContext: ['groups' => 'folder:read'],
            denormalizationContext: ['groups' => 'folder:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un dossier",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['folder:update']]
)]

#[ApiFilter(
    OrderFilter::class, properties: [
        "refFolder",
        "name",
        "priority",
        "beginDate",
        "deliverydate",
    ],
    arguments:  ['orderParameterName' => 'order']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class Folder
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    const FOLDER_PREFIX = 'DOS';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'folder:read','transaction:read',
        'transaction:read','transaction:create','transaction:update'
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'folders')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'folder:read',
        'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update'
    ])]
    private ?Customer $customer = null;

    #[ORM\ManyToOne(inversedBy: 'folders')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'folder:read',
        'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update'
    ])]
    private ?User $projectHolder = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'folder:read',
        'folder:create', 'folder:update'
    ])]
    private ?string $priority = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'folder:read', 'folder:create', 'folder:update'
    ])]
    private ?\DateTimeInterface $beginDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'folder:read', 'folder:create', 'folder:update','folderTransaction:read','transaction:read'
    ])]
    private ?\DateTimeInterface $deliverydate = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(groups: [
        'folder:read', 'folder:create', 'folder:update'
    ])]
    private ?string $comment = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'folder:read',
        'transaction:read','transaction:create','transaction:update'
    ])]
    private ?string $refFolder = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update'
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'folder', targetEntity: FolderTransaction::class,cascade: ['persist','detach'])]
    #[Groups(groups: [
        'folder:read',
        'folder:create', 'folder:update',
    ])]
    private Collection $folderTransactions;

    #[ORM\ManyToOne(inversedBy: 'folders')]
    #[Groups(groups: [
        'folder:read', 'folder:create', 'folder:update'
    ])]
    private ?Status $status = null;

    public function __construct()
    {
        $this->folderTransactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    public function getProjectHolder(): ?User
    {
        return $this->projectHolder;
    }

    public function setProjectHolder(?User $projectHolder): static
    {
        $this->projectHolder = $projectHolder;

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setPriority(?string $priority): static
    {
        $this->priority = $priority;

        return $this;
    }

    public function getBeginDate(): ?\DateTimeInterface
    {
        return $this->beginDate;
    }

    public function setBeginDate(\DateTimeInterface $beginDate): static
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getDeliverydate(): ?\DateTimeInterface
    {
        return $this->deliverydate;
    }

    public function setDeliverydate(?\DateTimeInterface $deliverydate): static
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getRefFolder(): ?string
    {
        return $this->refFolder;
    }

    public function setRefFolder(?string $refFolder): static
    {
        $this->refFolder = $refFolder;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, FolderTransaction>
     */
    public function getFolderTransactions(): Collection
    {
        return $this->folderTransactions;
    }

    public function addFolderTransaction(FolderTransaction $folderTransaction): static
    {
        if (!$this->folderTransactions->contains($folderTransaction)) {
            $this->folderTransactions->add($folderTransaction);
            $folderTransaction->setFolder($this);
        }

        return $this;
    }

    public function removeFolderTransaction(FolderTransaction $folderTransaction): static
    {
        if ($this->folderTransactions->removeElement($folderTransaction)) {
            // set the owning side to null (unless already changed)
            if ($folderTransaction->getFolder() === $this) {
                $folderTransaction->setFolder(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): static
    {
        $this->status = $status;

        return $this;
    }
}
