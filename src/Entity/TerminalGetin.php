<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use App\ApiPlatform\Filter\TerminalGetInFilter;
use App\DTO\TerminalGetinDTO;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use App\State\TerminalGetinProvider;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\BdrUploadController;
use App\Repository\TerminalGetinRepository;
use App\State\AddNewTerminalGetInProcessor;
use Doctrine\Common\Collections\Collection;
use App\Controller\ExportAllGetInController;
use App\Controller\PrintSingleGetInController;
use App\Controller\TerminalGetinDataController;
use Symfony\Component\HttpFoundation\File\File;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: TerminalGetinRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        // new GetCollection(
        //     openapiContext: [
        //         "summary" => "Liste des conteneurs en entrée",
        //     ],
        //     normalizationContext: ['groups' => 'getin:read'],
        //     security: "is_granted('PERMISSION_ALLOWED' , object)",
        // ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un conteneur en entrée",
            ],
            normalizationContext: ['groups' => 'getin:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un conteneur en entrée",
            ],
            normalizationContext: ['groups' => 'getin:read'],
            denormalizationContext: ['groups' => 'getin:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            types: ['https://schema.org/terminal_getins'],
            uriTemplate: "/terminal_getins/{id}/bdr-attachment",
            openapiContext: [
                "summary" => "Upload BDR",
            ],
            normalizationContext: ['groups' => 'getin:read'],
            denormalizationContext: ['groups' => 'getin:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            inputFormats: ['multipart' => ['multipart/form-data']],
            deserialize: false,
            controller: BdrUploadController::class,
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un conteneur en entrée",
            ],
            normalizationContext: ['groups' => 'getin:read'],
            denormalizationContext: ['groups' => 'getin:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un conteneur en entrée",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: "/exportAllGetins",
            defaults: [
                '_api_persist' => false,
            ],
            controller: ExportAllGetInController::class,
            openapiContext: [
                "summary" => "Export multiple de GETIN",
            ],
            normalizationContext: ['groups' => 'getin:read'],
            read: false,
        ),
        new Get(
            uriTemplate: "/getin/{id}/print",
            controller: PrintSingleGetInController::class,
            openapiContext: [
                "summary" => "Impression en pdf besoin",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
)]

#[ApiFilter(
        SearchFilter::class, properties: [
            'terminal' => 'exact',
            'refGetin' => 'ip   artial',
            'BDRnumber' => 'ipartial',

        ]
)]

#[ApiFilter(
    OrderFilter::class, properties: [
        "refGetin"
    ],
    arguments:  ['orderParameterName' => 'order']
)]
#[ApiFilter(
    TerminalGetInFilter::class,properties: ['terminal']
)]
#[Vich\Uploadable]
class TerminalGetin
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const GETIN_PREFIX = "GI";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'getin:read', 'container:read',
        'transportOrder:read',
        'eir:read',
        'specificRate:read',
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'terminalGetins')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'getin:read','getin:create','getin:update', 'container:read',
        'eir:read',
        'specificRate:read',
        
    ])]
    private ?Transporter $carrier = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'getin:read','getin:create','getin:update', 'container:read',
        'eir:read',
    ])]
    private ?string $carrierTruckNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'getin:read','getin:create','getin:update',
        'transportOrder:read',
        'eir:read',
        'specificRate:read',
    ])]
    private ?string $noDocumentTransport = null;

    #[ORM\OneToMany(mappedBy: 'terminalGetin', targetEntity: Container::class,cascade: ['persist'])]
    #[Groups(groups: [
        'getin:read','getin:create','getin:update'
    ])]
    private Collection $containers;

    #[ORM\ManyToOne(inversedBy: 'terminalGetins')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(groups: [
        'getin:read','getin:create','getin:update', 'container:read', 'booking:read',
        'transportOrder:read',
        'eir:read',
        'specificRate:read',
    ])]
    private ?Terminal $terminal = null;

    #[ORM\ManyToOne(inversedBy: 'terminalGetins')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'getin:read','getin:create','getin:update', 'container:read',
        'transportOrder:read',
        'eir:read',
        'specificRate:read',
    ])]
    private ?ShippingCompany $shippingCompany = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'getin:read', 'container:read',
        'eir:read',
        'specificRate:read',
    ])]
    private ?string $refGetin = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'getin:read','getin:create','getin:update', 'container:read',
        'eir:read',
        'specificRate:read',
    ])]
    private ?string $BDRnumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bdrAttachment = null;

    #[Vich\UploadableField(mapping: "bdr_attachment_file", fileNameProperty: "bdrAttachment")]
    private ?File $bdrAttachmentFile = null;

    public function __construct()
    {
        $this->containers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCarrier(): ?Transporter
    {
        return $this->carrier;
    }

    public function setCarrier(?Transporter $carrier): static
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getCarrierTruckNumber(): ?string
    {
        return $this->carrierTruckNumber;
    }

    public function setCarrierTruckNumber(?string $carrierTruckNumber): static
    {
        $this->carrierTruckNumber = $carrierTruckNumber;

        return $this;
    }

    public function getNoDocumentTransport(): ?string
    {
        return $this->noDocumentTransport;
    }

    public function setNoDocumentTransport(?string $noDocumentTransport): static
    {
        $this->noDocumentTransport = $noDocumentTransport;

        return $this;
    }

    /**
     * @return Collection<int, Container>
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }

    public function addContainer(Container $container): static
    {
        if (!$this->containers->contains($container)) {
            $this->containers->add($container);
            $container->setTerminalGetin($this);
        }

        return $this;
    }

    public function removeContainer(Container $container): static
    {
        if ($this->containers->removeElement($container)) {
            // set the owning side to null (unless already changed)
            if ($container->getTerminalGetin() === $this) {
                $container->setTerminalGetin(null);
            }
        }

        return $this;
    }

    public function getTerminal(): ?Terminal
    {
        return $this->terminal;
    }

    public function setTerminal(?Terminal $terminal): static
    {
        $this->terminal = $terminal;

        return $this;
    }

    public function getShippingCompany(): ?ShippingCompany
    {
        return $this->shippingCompany;
    }

    public function setShippingCompany(?ShippingCompany $shippingCompany): static
    {
        $this->shippingCompany = $shippingCompany;

        return $this;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public function getRefGetin(): ?string
    {
        return $this->refGetin;
    }

    public function setRefGetin(?string $refGetin): static
    {
        $this->refGetin = $refGetin;

        return $this;
    }

    public function getBDRnumber(): ?string
    {
        return $this->BDRnumber;
    }

    public function setBDRnumber(?string $BDRnumber): static
    {
        $this->BDRnumber = $BDRnumber;

        return $this;
    }

    public function getBdrAttachment(): ?string
    {
        return $this->bdrAttachment;
    }

    public function setBdrAttachment(?string $bdrAttachment): static
    {
        $this->bdrAttachment = $bdrAttachment;

        return $this;
    }

    public function getBdrAttachmentFile(): ?File
    {
        return $this->bdrAttachmentFile;
    }

    public function setBdrAttachmentFile(?File $bdrAttachmentFile): static
    {
        $this->bdrAttachmentFile = $bdrAttachmentFile;

        return $this;
    }
}
