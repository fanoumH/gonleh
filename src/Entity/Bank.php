<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\BankRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: BankRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des banques",
            ],
            normalizationContext: ['groups' => 'bank:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'une banque",
            ],
            normalizationContext: ['groups' => 'bank:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'une banque",
            ],
            normalizationContext: ['groups' => 'bank:read'],
            denormalizationContext: ['groups' => 'bank:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'une banque",
            ],
            normalizationContext: ['groups' => 'bank:read'],
            denormalizationContext: ['groups' => 'bank:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'une banque",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['bank:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "abbreviation" => "partial",
    "entitled" => "partial",
    "interlocutor.email" => "partial",
    "address" => "partial",
    "complement" => "partial",
    "city" => "partial",
    "zipCode" => "partial",
    "phone" => "partial",
    "email" => "partial",
    "website" => "partial",
    "region.label" => "partial",
    "country.code" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "abbreviation",
    "entitled",
    "interlocutor.email",
    "address",
    "complement",
    "city",
    "zipCode",
    "phone",
    "email",
    "website",
    "region.label",
    "country.code",
],arguments:  ['orderParameterName' => 'order'])
]
class Bank
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read',
        'subcontractor:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create',
        'subcontractor:read',
    ])]
    private ?string $abbreviation = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create',
        'subcontractor:read',
    ])]
    private ?string $entitled = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?string $complement = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?string $city = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?string $zipCode = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?string $website = null;

    #[ORM\ManyToOne(inversedBy: 'banks')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?Region $region = null;

    #[ORM\OneToMany(mappedBy: 'titledBank', targetEntity: CustomerBank::class)]
    private Collection $customerBanks;

    #[ORM\ManyToOne(inversedBy: 'banks')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?Country $country = null;

    #[ORM\OneToMany(mappedBy: 'bank', targetEntity: BankContact::class,cascade: ['persist','merge'],orphanRemoval: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private Collection $bankContacts;

    #[ORM\ManyToOne(inversedBy: 'banks')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create'
    ])]
    private ?User $interlocutor = null;

    #[ORM\OneToMany(mappedBy: 'bank', targetEntity: SubContractor::class)]
    private Collection $subContractors;

    public function __construct()
    {
        $this->customerBanks = new ArrayCollection();
        $this->bankContacts = new ArrayCollection();
        $this->subContractors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(?string $abbreviation): static
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getEntitled(): ?string
    {
        return $this->entitled;
    }

    public function setEntitled(?string $entitled): static
    {
        $this->entitled = $entitled;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): static
    {
        $this->address = $address;

        return $this;
    }

        public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): static
    {
        $this->complement = $complement;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): static
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): static
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): static
    {
        $this->website = $website;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): static
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection<int, CustomerBank>
     */
    public function getCustomerBanks(): Collection
    {
        return $this->customerBanks;
    }

    public function addCustomerBank(CustomerBank $customerBank): static
    {
        if (!$this->customerBanks->contains($customerBank)) {
            $this->customerBanks->add($customerBank);
            $customerBank->setTitledBank($this);
        }

        return $this;
    }

    public function removeCustomerBank(CustomerBank $customerBank): static
    {
        if ($this->customerBanks->removeElement($customerBank)) {
            // set the owning side to null (unless already changed)
            if ($customerBank->getTitledBank() === $this) {
                $customerBank->setTitledBank(null);
            }
        }

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): static
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection<int, BankContact>
     */
    public function getBankContacts(): Collection
    {
        return $this->bankContacts;
    }

    public function addBankContact(BankContact $bankContact): static
    {
        if (!$this->bankContacts->contains($bankContact)) {
            $this->bankContacts->add($bankContact);
            $bankContact->setBank($this);
        }

        return $this;
    }

    public function removeBankContact(BankContact $bankContact): static
    {
        if ($this->bankContacts->removeElement($bankContact)) {
            // set the owning side to null (unless already changed)
            if ($bankContact->getBank() === $this) {
                $bankContact->setBank(null);
            }
        }

        return $this;
    }

    public function getInterlocutor(): ?User
    {
        return $this->interlocutor;
    }

    public function setInterlocutor(?User $interlocutor): static
    {
        $this->interlocutor = $interlocutor;

        return $this;
    }

    /**
     * @return Collection<int, SubContractor>
     */
    public function getSubContractors(): Collection
    {
        return $this->subContractors;
    }

    public function addSubContractor(SubContractor $subContractor): static
    {
        if (!$this->subContractors->contains($subContractor)) {
            $this->subContractors->add($subContractor);
            $subContractor->setBank($this);
        }

        return $this;
    }

    public function removeSubContractor(SubContractor $subContractor): static
    {
        if ($this->subContractors->removeElement($subContractor)) {
            // set the owning side to null (unless already changed)
            if ($subContractor->getBank() === $this) {
                $subContractor->setBank(null);
            }
        }

        return $this;
    }
}
