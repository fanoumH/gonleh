<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\TripTypeRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\TripTypesToPdfController;
use App\Controller\TripTypesToSheetController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: TripTypeRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des types de voyage",
            ],
            normalizationContext: ['groups' => 'triptype:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/trip_types/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'triptype:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TripTypesToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/trip_types/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'triptype:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TripTypesToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un type de voyage",
            ],
            normalizationContext: ['groups' => 'triptype:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un type de voyage",
            ],
            normalizationContext: ['groups' => 'triptype:read'],
            denormalizationContext: ['groups' => 'triptype:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un type de voyage",
            ],
            normalizationContext: ['groups' => 'triptype:read'],
            denormalizationContext: ['groups' => 'triptype:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un type de voyage",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "id" => "exact",
    "label" => "ipartial",
]
)]

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
class TripType
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'triptype:read',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'triptype:read',
        'triptype:create',
        'triptype:update',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'tripType', targetEntity: TransportOrder::class)]
    private Collection $transportOrders;

    public function __construct()
    {
        $this->transportOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, TransportOrder>
     */
    public function getTransportOrders(): Collection
    {
        return $this->transportOrders;
    }

    public function addTransportOrder(TransportOrder $transportOrder): static
    {
        if (!$this->transportOrders->contains($transportOrder)) {
            $this->transportOrders->add($transportOrder);
            $transportOrder->setTripType($this);
        }

        return $this;
    }

    public function removeTransportOrder(TransportOrder $transportOrder): static
    {
        if ($this->transportOrders->removeElement($transportOrder)) {
            // set the owning side to null (unless already changed)
            if ($transportOrder->getTripType() === $this) {
                $transportOrder->setTripType(null);
            }
        }

        return $this;
    }
}
