<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use App\Controller\MOPdfController;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\MissionOrderRepository;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: MissionOrderRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des ordres de missions",
            ],
            normalizationContext: ['groups' => 'missionOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un ordre de mission",
            ],
            normalizationContext: ['groups' => 'missionOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            uriTemplate: '/mission_orders/{id}/pdf',
            openapiContext: [
                "summary" => "Download one Mission order",
            ],
            controller: MOPdfController::class,
            normalizationContext: [],
            denormalizationContext: [],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un ordre de mission",
            ],
            normalizationContext: ['groups' => 'missionOrder:read'],
            denormalizationContext: ['groups' => 'missionOrder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un ordre de mission",
            ],
            normalizationContext: ['groups' => 'missionOrder:read'],
            denormalizationContext: ['groups' => 'missionOrder:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un ordre de mission",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "id" => "exact",
]
)]
class MissionOrder
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'missionOrder:read',
        'transportOrder:read',
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'emptyTCrecoveryLocations')]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?Place $emptyTCrecoveryLocation = null;

    #[ORM\ManyToOne(inversedBy: 'loadingPlaces')]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?Place $loadingPlace = null;

    #[ORM\ManyToOne(inversedBy: 'deliveryLocations')]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?Place $deliveryLocation = null;

    #[ORM\ManyToOne(inversedBy: 'emptyTCrestitutionPlaces')]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?Place $emptyTCrestitutionPlace = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?\DateTimeInterface $emptyTCrecoveryDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?\DateTimeInterface $loadingDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?\DateTimeInterface $deliveryDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'missionOrder:read',
        'missionOrder:create',
        'missionOrder:update',
        'transportOrder:read',
    ])]
    private ?\DateTimeInterface $emptyTCrestitutionDate = null;

    #[ORM\OneToOne(inversedBy: 'missionOrder', cascade: ['persist', 'remove'])]
    #[Groups(groups: [
        'missionOrder:read',
    ])]
    private ?TransportOrder $transportOrder = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmptyTCrecoveryLocation(): ?Place
    {
        return $this->emptyTCrecoveryLocation;
    }

    public function setEmptyTCrecoveryLocation(?Place $emptyTCrecoveryLocation): static
    {
        $this->emptyTCrecoveryLocation = $emptyTCrecoveryLocation;

        return $this;
    }

    public function getLoadingPlace(): ?Place
    {
        return $this->loadingPlace;
    }

    public function setLoadingPlace(?Place $loadingPlace): static
    {
        $this->loadingPlace = $loadingPlace;

        return $this;
    }

    public function getDeliveryLocation(): ?Place
    {
        return $this->deliveryLocation;
    }

    public function setDeliveryLocation(?Place $deliveryLocation): static
    {
        $this->deliveryLocation = $deliveryLocation;

        return $this;
    }

    public function getEmptyTCRestitutionPlace(): ?Place
    {
        return $this->emptyTCrestitutionPlace;
    }

    public function setEmptyTCRestitutionPlace(?Place $emptyTCrestitutionPlace): static
    {
        $this->emptyTCrestitutionPlace = $emptyTCrestitutionPlace;

        return $this;
    }

    public function getEmptyTCrecoveryDate(): ?\DateTimeInterface
    {
        return $this->emptyTCrecoveryDate;
    }

    public function setEmptyTCrecoveryDate(?\DateTimeInterface $emptyTCrecoveryDate): static
    {
        $this->emptyTCrecoveryDate = $emptyTCrecoveryDate;

        return $this;
    }

    public function getLoadingDate(): ?\DateTimeInterface
    {
        return $this->loadingDate;
    }

    public function setLoadingDate(?\DateTimeInterface $loadingDate): static
    {
        $this->loadingDate = $loadingDate;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(?\DateTimeInterface $deliveryDate): static
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    public function getEmptyTCrestitutionDate(): ?\DateTimeInterface
    {
        return $this->emptyTCrestitutionDate;
    }

    public function setEmptyTCrestitutionDate(?\DateTimeInterface $emptyTCrestitutionDate): static
    {
        $this->emptyTCrestitutionDate = $emptyTCrestitutionDate;

        return $this;
    }

    public function getTransportOrder(): ?TransportOrder
    {
        return $this->transportOrder;
    }

    public function setTransportOrder(?TransportOrder $transportOrder): static
    {
        $this->transportOrder = $transportOrder;

        return $this;
    }


}
