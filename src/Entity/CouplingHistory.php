<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use App\Repository\CouplingHistoryRepository;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: CouplingHistoryRepository::class)]
class CouplingHistory
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $couplingId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $semiId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $TrailerId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $couplingReference = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCouplingId(): ?string
    {
        return $this->couplingId;
    }

    public function setCouplingId(?string $couplingId): static
    {
        $this->couplingId = $couplingId;

        return $this;
    }

    public function getSemiId(): ?string
    {
        return $this->semiId;
    }

    public function setSemiId(?string $semiId): static
    {
        $this->semiId = $semiId;

        return $this;
    }

    public function getTrailerId(): ?string
    {
        return $this->TrailerId;
    }

    public function setTrailerId(?string $TrailerId): static
    {
        $this->TrailerId = $TrailerId;

        return $this;
    }

    public function getCouplingReference(): ?string
    {
        return $this->couplingReference;
    }

    public function setCouplingReference(?string $couplingReference): static
    {
        $this->couplingReference = $couplingReference;

        return $this;
    }
}
