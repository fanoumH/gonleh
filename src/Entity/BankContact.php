<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\BankContactRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: BankContactRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste contact des banques",
            ],
            normalizationContext: ['groups' => 'bankContact:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail contact des banques",
            ],
            normalizationContext: ['groups' => 'bankContact:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification contact des banques",
            ],
            normalizationContext: ['groups' => 'bankContact:read'],
            denormalizationContext: ['groups' => 'bankContact:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression contact des banques",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout contact des banques",
            ],
            normalizationContext: ['groups' => 'bankContact:read'],
            denormalizationContext: ['groups' => 'bankContact:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        )
    ],
)]
#[ApiResource(
    normalizationContext: ['groups' => 'bankContact:read'],denormalizationContext: ['groups' => 'bankContact:write'],
)]
#[ApiResource(
    uriTemplate: '{bankContactId}/bankContact/{id}',
    operations: [ new Patch(
        openapiContext: [
            "summary" => "Modifier contact client",
        ],
        normalizationContext: ['groups' => 'bankContact:read'],
        denormalizationContext: ['groups' => 'bankContact:write'],
        security: "is_granted('PERMISSION_ALLOWED' , object)",
    ) ],
    uriVariables: [
        'bankContactId' => new Link(toProperty: 'customer', fromClass: Bank::class),
        'id' => new Link(fromClass: BankContact::class)
    ]
)]
#[ApiResource(
    uriTemplate: '{bankContactId}/bankContact',
    operations: [ new Post(
        openapiContext: [
            "summary" => "Ajout contact client",
        ],
        normalizationContext: ['groups' => 'bankContact:read'],
        denormalizationContext: ['groups' => 'bankContact:write'],
        security: "is_granted('PERMISSION_ALLOWED' , object)",
    ) ],
    uriVariables: [
        'bankContactId' => new Link(toProperty: 'bank', fromClass: Bank::class),
    ]
)]
#[ApiResource(
    uriTemplate: '{bankContactId}/bankContact/{id}',
    operations: [ new Delete(
        openapiContext: [
            "summary" => "Supprimer contact client",
        ],
        normalizationContext: ['groups' => 'bankContact:read'],
        denormalizationContext: ['groups' => 'bankContact:write'],
        security: "is_granted('PERMISSION_ALLOWED' , object)",
    ) ],
    uriVariables: [
        'bankContactId' => new Link(toProperty: 'bank', fromClass: Customer::class),
        'id' => new Link(fromClass: BankContact::class)
    ]
)]
class BankContact
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'bankContact:read','bank:read','bank:update','bank:create'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'bankContact:read','bankContact:create','bankContact:update','bankContact:write','bank:read','bank:update','bank:create'
    ])]
    private ?string $firstname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'bankContact:read','bankContact:create','bankContact:update','bankContact:write','bank:read','bank:update','bank:create'
    ])]
    private ?string $lastname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'bankContact:read','bankContact:create','bankContact:update','bankContact:write','bank:read','bank:update','bank:create'
    ])]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'bankContact:read','bankContact:create','bankContact:update','bankContact:write','bank:read','bank:update','bank:create'
    ])]
    private ?string $email = null;

    #[ORM\ManyToOne(inversedBy: 'bankContacts')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'bankContact:read','bankContact:create','bankContact:update','bankContact:write','bank:read','bank:update','bank:create'
    ])]
    private ?Country $country = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'bankContact:read','bankContact:create','bankContact:update','bankContact:write','bank:read','bank:update','bank:create'
    ])]
    private ?string $civility = null;

    #[ORM\ManyToOne(inversedBy: 'bankContacts')]
//    #[ORM\JoinColumn(onDelete: "SET NULL")]
    private ?Bank $bank = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): static
    {
        $this->country = $country;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(?string $civility): static
    {
        $this->civility = $civility;

        return $this;
    }

    public function getBank(): ?Bank
    {
        return $this->bank;
    }

    public function setBank(?Bank $bank): static
    {
        $this->bank = $bank;

        return $this;
    }
}
