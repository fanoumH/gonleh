<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\BasicRateRepository;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BasicRateRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des basics rates",
            ],
            normalizationContext: ['groups' => 'basicRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un basic rate",
            ],
            normalizationContext: ['groups' => 'basicRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un basic rate",
            ],
            normalizationContext: ['groups' => 'basicRate:read'],
            denormalizationContext: ['groups' => 'basicRate:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un basic rate",
            ],
            normalizationContext: ['groups' => 'basicRate:read'],
            denormalizationContext: ['groups' => 'basicRate:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un basic rate",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "serviceRate.id" => "exact",
    "serviceRate.name" => "partial",
    "operationTypeRate.id" => "exact",
    "operationTypeRate.name" => "partial",
]
)]
#[UniqueEntity(['operationTypeRate','serviceRate','containerType','terminal'], message: 'type opération,service,type de conteneur,terminal conteneur est dupliqué', ignoreNull: false)]
class BasicRate
{
    const GETIN_PREFIX = "TB_";

    use TimestampableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'basicRate:read',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'basicRates')]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?ServiceRate $serviceRate = null;

    #[ORM\ManyToOne(inversedBy: 'basicRates')]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?TypeRate $typeRate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?\DateTimeInterface $periodBeginning = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?\DateTimeInterface $periodEnding = null;

    #[ORM\ManyToOne(inversedBy: 'basicRates')]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?OperationTypeRate $operationTypeRate = null;

    #[ORM\ManyToOne(inversedBy: 'basicRates')]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?Place $place = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?int $typeTC = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?float $generalPriceHT = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?float $priceTTC = null;

    #[ORM\ManyToOne(inversedBy: 'basicRates')]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?Currency $currency = null;

    #[ORM\ManyToOne(inversedBy: 'basicRates')]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?Terminal $terminal = null;

    #[ORM\ManyToOne(inversedBy: 'basicRates')]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?ContainerType $containerType = null;

    #[ORM\OneToMany(mappedBy: 'basicRate', targetEntity: SpecificRate::class)]
    private Collection $specificRates;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?string $reference = null;

    public function __construct()
    {
        $this->specificRates = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServiceRate(): ?ServiceRate
    {
        return $this->serviceRate;
    }

    public function setServiceRate(?ServiceRate $serviceRate): static
    {
        $this->serviceRate = $serviceRate;

        return $this;
    }

    public function getTypeRate(): ?TypeRate
    {
        return $this->typeRate;
    }

    public function setTypeRate(?TypeRate $typeRate): static
    {
        $this->typeRate = $typeRate;

        return $this;
    }

    public function getPeriodBeginning(): ?\DateTimeInterface
    {
        return $this->periodBeginning;
    }

    public function setPeriodBeginning(?\DateTimeInterface $periodBeginning): static
    {
        $this->periodBeginning = $periodBeginning;

        return $this;
    }

    public function getPeriodEnding(): ?\DateTimeInterface
    {
        return $this->periodEnding;
    }

    public function setPeriodEnding(?\DateTimeInterface $periodEnding): static
    {
        $this->periodEnding = $periodEnding;

        return $this;
    }

    public function getOperationTypeRate(): ?OperationTypeRate
    {
        return $this->operationTypeRate;
    }

    public function setOperationTypeRate(?OperationTypeRate $operationTypeRate): static
    {
        $this->operationTypeRate = $operationTypeRate;

        return $this;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): static
    {
        $this->place = $place;

        return $this;
    }

    public function getTypeTC(): ?int
    {
        return $this->typeTC;
    }

    public function setTypeTC(?int $typeTC): static
    {
        $this->typeTC = $typeTC;

        return $this;
    }

    public function getGeneralPriceHT(): ?float
    {
        return $this->generalPriceHT;
    }

    public function setGeneralPriceHT(?float $generalPriceHT): static
    {
        $this->generalPriceHT = $generalPriceHT;

        return $this;
    }

    public function getPriceTTC(): ?float
    {
        return $this->priceTTC;
    }

    public function setPriceTTC(?float $priceTTC): static
    {
        $this->priceTTC = $priceTTC;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getTerminal(): ?Terminal
    {
        return $this->terminal;
    }

    public function setTerminal(?Terminal $terminal): static
    {
        $this->terminal = $terminal;

        return $this;
    }

    public function getContainerType(): ?ContainerType
    {
        return $this->containerType;
    }

    public function setContainerType(?ContainerType $containerType): static
    {
        $this->containerType = $containerType;

        return $this;
    }

    /**
     * @return Collection<int, SpecificRate>
     */
    public function getSpecificRates(): Collection
    {
        return $this->specificRates;
    }

    public function addSpecificRate(SpecificRate $specificRate): static
    {
        if (!$this->specificRates->contains($specificRate)) {
            $this->specificRates->add($specificRate);
            $specificRate->setBasicRate($this);
        }

        return $this;
    }

    public function removeSpecificRate(SpecificRate $specificRate): static
    {
        if ($this->specificRates->removeElement($specificRate)) {
            // set the owning side to null (unless already changed)
            if ($specificRate->getBasicRate() === $this) {
                $specificRate->setBasicRate(null);
            }
        }

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }
}
