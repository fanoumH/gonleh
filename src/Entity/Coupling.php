<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use App\ApiPlatform\Filter\CouplingFilter;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CouplingRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\CouplingToPdfController;
use App\Controller\CouplingsToSheetController;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: CouplingRepository::class)]

#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des attelages",
            ],
            normalizationContext: ['groups' => 'coupling:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/couplings/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'coupling:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: CouplingToPdfController::class,
        ),
        new GetCollection(
            uriTemplate: '/couplings/all/sheet',
            openapiContext: [
                "summary" => "Export to spreadsheet",
            ],
            normalizationContext: ['groups' => 'coupling:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: CouplingsToSheetController::class,
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un attelage",
            ],
            normalizationContext: ['groups' => 'coupling:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un attelage",
            ],
            normalizationContext: ['groups' => 'coupling:read'],
            denormalizationContext: ['groups' => 'coupling:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un attelage",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
        "Semi" => "exact",
        "Semi.id" => "exact",
        "Trailer" => "exact",
        "Trailer.id" => "exact",
        "couplingNumber" => "ipartial",
    ]
)]
#[ApiFilter(
    CouplingFilter::class,properties: ['coupling']
)]
// #[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
class Coupling
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    // use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    const COUPLING_PREFIX = 'AT';
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'coupling:read',
    ])]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'merge'])]
    #[Groups(groups: [
        'coupling:read',
        'coupling:create',
        'coupling:update',
    ])]
    private ?Vehicle $Semi = null;

    #[ORM\OneToOne(cascade: ['persist', 'merge'])]
    #[Groups(groups: [
        'coupling:read',
        'coupling:create',
        'coupling:update',
    ])]
    private ?Vehicle $Trailer = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'coupling:read',
    ])]
    private ?string $couplingNumber = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSemi(): ?Vehicle
    {
        return $this->Semi;
    }

    public function setSemi(?Vehicle $Semi): static
    {
        $this->Semi = $Semi;

        return $this;
    }

    public function getTrailer(): ?Vehicle
    {
        return $this->Trailer;
    }

    public function setTrailer(?Vehicle $Trailer): static
    {
        $this->Trailer = $Trailer;

        return $this;
    }

    public function getCouplingNumber(): ?string
    {
        return $this->couplingNumber;
    }

    public function setCouplingNumber(?string $couplingNumber): static
    {
        $this->couplingNumber = $couplingNumber;

        return $this;
    }
}
