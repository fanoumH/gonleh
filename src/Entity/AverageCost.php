<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\AverageCostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: AverageCostRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des coût moyens",
            ],
            normalizationContext: ['groups' => 'averageCost:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un coût moyen",
            ],
            normalizationContext: ['groups' => 'averageCost:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un coût moyen",
            ],
            normalizationContext: ['groups' => 'averageCost:read'],
            denormalizationContext: ['groups' => 'averageCost:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un coût moyen",
            ],
            normalizationContext: ['groups' => 'averageCost:read'],
            denormalizationContext: ['groups' => 'averageCost:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un coût moyen",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['averageCost:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "name"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class AverageCost
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'averageCost:read','otherCost:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'averageCost:read','averageCost:create','averageCost:update','otherCost:read','otherCost:create','otherCost:update'
    ])]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'averageCost:read','averageCost:create','averageCost:update','otherCost:read','otherCost:create','otherCost:update'
    ])]
    private ?float $cost = null;

    #[ORM\OneToMany(mappedBy: 'averageCost', targetEntity: OtherCost::class,cascade: ['persist','detach'])]
    private Collection $otherCosts;

    public function __construct()
    {
        $this->otherCosts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(?float $cost): static
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * @return Collection<int, OtherCost>
     */
    public function getOtherCosts(): Collection
    {
        return $this->otherCosts;
    }

    public function addOtherCost(OtherCost $otherCost): static
    {
        if (!$this->otherCosts->contains($otherCost)) {
            $this->otherCosts->add($otherCost);
            $otherCost->setAverageCost($this);
        }

        return $this;
    }

    public function removeOtherCost(OtherCost $otherCost): static
    {
        if ($this->otherCosts->removeElement($otherCost)) {
            // set the owning side to null (unless already changed)
            if ($otherCost->getAverageCost() === $this) {
                $otherCost->setAverageCost(null);
            }
        }

        return $this;
    }
}
