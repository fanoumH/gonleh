<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\PlaceFuncsToPdfController;
use App\Controller\PlaceFuncsToSheetController;
use App\Controller\RemovePlaceFunctionController;
use App\Entity\Trait\HistoryTrait;
use App\Repository\PlaceFunctionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PlaceFunctionRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des fonctions de lieux",
            ],
            normalizationContext: ['groups' => 'placeFunction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/place_functions/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'placeFunction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: PlaceFuncsToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/place_functions/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'placeFunction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: PlaceFuncsToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'une fonction de lieux",
            ],
            normalizationContext: ['groups' => 'placeFunction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'une fonction de lieux",
            ],
            normalizationContext: ['groups' => 'placeFunction:read'],
            denormalizationContext: ['groups' => 'placeFunction:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'une fonction de lieux",
            ],
            normalizationContext: ['groups' => 'placeFunction:read'],
            denormalizationContext: ['groups' => 'placeFunction:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            controller: RemovePlaceFunctionController::class,
            openapiContext: [
                "summary" => "Suppression d'une fonction de lieux",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "id" => "exact"
]
)]

#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"name"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[UniqueEntity('name')]
class PlaceFunction
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;
    /**
     * history trait
     */
    use HistoryTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'place:read',
        'placeFunction:read',
        'transportOrder:read',
        'missionOrder:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
        'basicRate:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'placeFunction:read', 'placeFunction:create', 'placeFunction:update',
        'place:read',
        'transportOrder:read',
        'missionOrder:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
        'basicRate:read',
    ])]
    private ?string $name = null;

    #[ORM\ManyToMany(targetEntity: Place::class, mappedBy: 'placeFunctions')]
    private Collection $places;

    public function __construct()
    {
        $this->places = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Place>
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): static
    {
        if (!$this->places->contains($place)) {
            $this->places->add($place);
            $place->addPlaceFunction($this);
        }

        return $this;
    }

    public function removePlace(Place $place): static
    {
        if ($this->places->removeElement($place)) {
            $place->removePlaceFunction($this);
        }

        return $this;
    }
}
