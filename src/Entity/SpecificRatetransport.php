<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\SpecificRatetransportRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SpecificRatetransportRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des tarifs spécifiques transports",
            ],
            normalizationContext: ['groups' => 'specificRateTransport:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un tarif spécifique transport",
            ],
            normalizationContext: ['groups' => 'specificRateTransport:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un tarif spécifique transport",
            ],
            normalizationContext: ['groups' => 'specificRateTransport:read'],
            denormalizationContext: ['groups' => 'specificRateTransport:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un tarif spécifique transport",
            ],
            normalizationContext: ['groups' => 'specificRateTransport:read'],
            denormalizationContext: ['groups' => 'specificRateTransport:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un tarif spécifique transport",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "priceStandardTransport.serviceRate.id" => "exact",
    "priceStandardTransport.serviceRate.name" => "partial",
    "priceStandardTransport.operationTypeRate.id" => "exact",
    "priceStandardTransport.operationTypeRate.name" => "partial",
    "client.id" => "exact",
]
)]
#[UniqueEntity(['client','serviceRate','priceStandardTransport'], message: 'client,service, tarif standard transport sont dupliqué', ignoreNull: false)]
class SpecificRatetransport
{
    use TimestampableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'specificRateTransport:read',
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'specificRatetransports')]
    #[Groups(groups: [
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?Customer $client = null;

    #[ORM\ManyToOne(inversedBy: 'specificRatetransports')]
    #[Groups(groups: [
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?ServiceRate $serviceRate = null;

    #[ORM\ManyToOne(inversedBy: 'specificRatetransports')]
    #[Groups(groups: [
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?PriceListStandardTransport $priceStandardTransport = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?float $priceTTc = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?float $priceHt = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $reference = null;

    #[ORM\ManyToOne(inversedBy: 'specificRatetransports')]
    #[Groups(groups: [
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?Currency $currency = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?Customer
    {
        return $this->client;
    }

    public function setClient(?Customer $client): static
    {
        $this->client = $client;

        return $this;
    }

    public function getServiceRate(): ?ServiceRate
    {
        return $this->serviceRate;
    }

    public function setServiceRate(?ServiceRate $serviceRate): static
    {
        $this->serviceRate = $serviceRate;

        return $this;
    }

    public function getPriceStandardTransport(): ?PriceListStandardTransport
    {
        return $this->priceStandardTransport;
    }

    public function setPriceStandardTransport(?PriceListStandardTransport $priceStandardTransport): static
    {
        $this->priceStandardTransport = $priceStandardTransport;

        return $this;
    }

    public function getPriceTTc(): ?float
    {
        return $this->priceTTc;
    }

    public function setPriceTTc(?float $priceTTc): static
    {
        $this->priceTTc = $priceTTc;

        return $this;
    }

    public function getPriceHt(): ?float
    {
        return $this->priceHt;
    }

    public function setPriceHt(?float $priceHt): static
    {
        $this->priceHt = $priceHt;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): static
    {
        $this->currency = $currency;

        return $this;
    }
}
