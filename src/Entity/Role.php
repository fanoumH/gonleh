<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Entity\Trait\HistoryTrait;
use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: RoleRepository::class)]
#[ORM\Table(name: '`roles`')]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste rôles",
            ],
            normalizationContext: ['groups' => 'role:read'],
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail rôle",
            ],
            normalizationContext: ['groups' => 'role:read'],
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout rôle",
            ],
            normalizationContext: ['groups' => 'role:read'],
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification rôle",
            ],
            normalizationContext: ['groups' => 'role:read'],
            denormalizationContext: ['groups' => 'role:update'],
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un rôle",
            ],
        ),
    ]
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class Role
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;
    /**
     * history trait
     */
    use HistoryTrait;

    public const NAME_ADMIN = 'ROLE_ADMIN';
    public const NAME_ADMIN_LABEL = 'Admin';
    public const NAME_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    public const NAME_SUPER_ADMIN_LABEL = 'Super-admin';
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: ['role:read','user:read','bank:read','bank:update','bank:create','transaction:read','customer:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true, nullable: true)]
    #[Groups(groups: [
        'role:read', 'role:create', 'role:update','user:read','bank:read','bank:update','bank:create',
        'transaction:read','transaction:create','transaction:update',
        'customer:read'
    ])]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: ['role:read', 'role:create', 'role:update','user:read','bank:read','bank:update','bank:create','customer:read','customer:create','customer:update'])]
    private ?array $permissions = null;

    #[ORM\OneToMany(mappedBy: 'capabilities', targetEntity: User::class,cascade: ['persist','detach'])]
    private Collection $users;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true, options: ['default' => false])]
    #[Groups(groups: ['role:read', 'role:create', 'role:update','user:read','bank:read','bank:update','bank:create','customer:read','customer:create','customer:update'])]
    private ?bool $is_admin = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'role:read', 'role:create', 'role:update','user:read','bank:read','bank:update','bank:create',
        'transaction:read','transaction:create','transaction:update',
        'customer:read'
    ])]
    private ?string $label = null;

//    #[Gedmo\Timestampable(on: 'create')]
//    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
//    protected $createdAt;
//
//    /**
//     * @var \DateTime|null
//     * @Gedmo\Timestampable(on="update")
//     * @ORM\Column(type="datetime")
//     */
//    #[Gedmo\Timestampable(on: 'update')]
//    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
//    protected $updatedAt;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getPermissions(): ?array
    {
        return $this->permissions;
    }

    public function setPermissions(?array $permissions): static
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setCapabilities($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCapabilities() === $this) {
                $user->setCapabilities(null);
            }
        }

        return $this;
    }

    public function isIsAdmin(): ?bool
    {
        return $this->is_admin;
    }

    public function setIsAdmin(?bool $is_admin): static
    {
        $this->is_admin = $is_admin;

        return $this;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;

        return $this;
    }
}
