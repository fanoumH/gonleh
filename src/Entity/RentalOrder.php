<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\RentalOrderRepository;
use App\Repository\TransportOrderRepository;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\RentalsToPdfController;
use App\Controller\RentalsToSheetController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: RentalOrderRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des ordres de location",
            ],
            normalizationContext: ['groups' => 'rentalOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/rental_orders/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'rentalOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: RentalsToPdfController::class,
        ),
        new GetCollection(
            uriTemplate: '/rental_orders/all/sheet',
            openapiContext: [
                "summary" => "Export to spreadsheet",
            ],
            normalizationContext: ['groups' => 'rentalOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: RentalsToSheetController::class,
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un ordre de location",
            ],
            normalizationContext: ['groups' => 'rentalOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un ordre de location",
            ],
            normalizationContext: ['groups' => 'rentalOrder:read'],
            denormalizationContext: ['groups' => 'rentalOrder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un ordre de location",
            ],
            normalizationContext: ['groups' => 'rentalOrder:read'],
            denormalizationContext: ['groups' => 'rentalOrder:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un ordre de location",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "code" => "ipartial",
    "id" => "exact",
]
)]

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
class RentalOrder
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    const RENTAL_ORDER_PREFIX = "LOCATION";
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'rentalOrder:read',
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'rentalOrders')]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?Vehicle $vehicle = null;

    #[ORM\ManyToOne(inversedBy: 'rentalOrders')]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?Customer $customer = null;

    #[ORM\ManyToOne(inversedBy: 'trailerRentalOrders')]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?Vehicle $trailer = null;

    #[ORM\ManyToOne(inversedBy: 'driveRentalOrders')]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?Staff $driver = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?string $fuelRequirement = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?string $site = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?\DateTimeInterface $beginDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?\DateTimeInterface $previewedEndDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?\DateTimeInterface $effectiveEndDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?\DateTimeInterface $beginHour = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?\DateTimeInterface $endHour = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
    ])]
    private ?string $code = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // #[Groups(groups: [
    //     'rentalOrder:read',
    //     'rentalOrder:create',
    //     'rentalOrder:update',
    // ])]
    // private ?string $client = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?string $fleetType = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'rentalOrder:read',
        'rentalOrder:create',
        'rentalOrder:update',
    ])]
    private ?string $externalVehicle = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): static
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    public function getTrailer(): ?Vehicle
    {
        return $this->trailer;
    }

    public function setTrailer(?Vehicle $trailer): static
    {
        $this->trailer = $trailer;

        return $this;
    }

    public function getDriver(): ?Staff
    {
        return $this->driver;
    }

    public function setDriver(?Staff $driver): static
    {
        $this->driver = $driver;

        return $this;
    }

    public function getFuelRequirement(): ?string
    {
        return $this->fuelRequirement;
    }

    public function setFuelRequirement(?string $fuelRequirement): static
    {
        $this->fuelRequirement = $fuelRequirement;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(string $site): static
    {
        $this->site = $site;

        return $this;
    }

    public function getBeginDate(): ?\DateTimeInterface
    {
        return $this->beginDate;
    }

    public function setBeginDate(?\DateTimeInterface $beginDate): static
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getPreviewedEndDate(): ?\DateTimeInterface
    {
        return $this->previewedEndDate;
    }

    public function setPreviewedEndDate(?\DateTimeInterface $previewedEndDate): static
    {
        $this->previewedEndDate = $previewedEndDate;

        return $this;
    }

    public function getEffectiveEndDate(): ?\DateTimeInterface
    {
        return $this->effectiveEndDate;
    }

    public function setEffectiveEndDate(?\DateTimeInterface $effectiveEndDate): static
    {
        $this->effectiveEndDate = $effectiveEndDate;

        return $this;
    }

    public function getBeginHour(): ?\DateTimeInterface
    {
        return $this->beginHour;
    }

    public function setBeginHour(?\DateTimeInterface $beginHour): static
    {
        $this->beginHour = $beginHour;

        return $this;
    }

    public function getEndHour(): ?\DateTimeInterface
    {
        return $this->endHour;
    }

    public function setEndHour(?\DateTimeInterface $endHour): static
    {
        $this->endHour = $endHour;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): static
    {
        $this->code = $code;

        return $this;
    }

    // public function getClient(): ?string
    // {
    //     return $this->client;
    // }

    // public function setClient(?string $client): static
    // {
    //     $this->client = $client;

    //     return $this;
    // }

    public function getFleetType(): ?string
    {
        return $this->fleetType;
    }

    public function setFleetType(?string $fleetType): static
    {
        $this->fleetType = $fleetType;

        return $this;
    }

    public function getExternalVehicle(): ?string
    {
        return $this->externalVehicle;
    }

    public function setExternalVehicle(?string $externalVehicle): static
    {
        $this->externalVehicle = $externalVehicle;

        return $this;
    }
}
