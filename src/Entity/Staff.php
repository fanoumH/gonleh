<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\StaffRepository;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\StaffToPdfController;
use App\Controller\StaffUpdateController;
use App\Controller\StaffToSheetController;
use App\Controller\StaffRegisterController;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\ApiPlatform\Filter\StaffFilter;
use App\Controller\StaffProfilePicUpdateController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: StaffRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des personnels",
            ],
            normalizationContext: ['groups' => 'staff:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/staff/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'staff:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: StaffToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/staff/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'staff:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: StaffToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un personnel",
            ],
            normalizationContext: ['groups' => 'staff:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un personnel",
            ],
            normalizationContext: ['groups' => 'staff:read'],
            denormalizationContext: ['groups' => 'staff:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            inputFormats: ['multipart' => ['multipart/form-data']],
            deserialize: false,
            controller: StaffRegisterController::class,
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un personnel",
            ],
            normalizationContext: ['groups' => 'staff:read'],
            denormalizationContext: ['groups' => 'staff:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un personnel",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            types: ['https://schema.org/staff'],
            uriTemplate: "/staff/{id}/profilePic",
            inputFormats: ['multipart' => ['multipart/form-data']],
            controller: StaffProfilePicUpdateController::class,
            openapiContext: [
                "summary" => "Modifier photo profile",
            ],
            deserialize: false,
            normalizationContext: ['groups' => 'staff:read'],
            denormalizationContext: ['groups' => 'staff:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "registrationNumber" => "ipartial",
    "lastName" => "ipartial",
    "firstName" => "ipartial",
    "email" => "partial",
    "phoneNumber" => "partial",
    "address" => "partial",
    "id" => "exact",
    "staffPost.id" => "exact",
    "staffPost.code" => "exact",
    "staffPost.name" => "partial",
    "vehicle.id" => "exact",
    "vehicle.name" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"registrationNumber", "lastName" , "firstName" , "email", "phoneNumber" , "address",
    "staffPost.name" , "staffPost.id", "staffPost.code",
    "vehicle.id" , "vehicle.name"
],arguments:  ['orderParameterName' => 'order'])
]

#[ApiFilter(
    StaffFilter::class, properties: [
        "search",
    ]
)]


#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
#[UniqueEntity(['registrationNumber'])]
#[UniqueEntity(['driverLicenceId'])]
#[Vich\Uploadable]
class Staff
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'staff:read',
        'vehicle:read',
        'rentalOrder:read',
        'missionOrder:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'vehicle:read',
        'rentalOrder:read',
        'missionOrder:read',
    ])]
    private ?string $registrationNumber = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'vehicle:read',
        'rentalOrder:read',
        'missionOrder:read',
    ])]
    private ?string $lastName = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'vehicle:read',
        'rentalOrder:read',
        'missionOrder:read',
    ])]
    private ?string $firstName = null;

    #[ORM\Column(nullable: true)]
    #[Assert\LessThan('today')]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?\DateTimeImmutable $birthday = null;

    #[ORM\Column(nullable: true)]
    // #[Assert\LessThan('today')]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?\DateTimeImmutable $hiringDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'vehicle:read',
        'rentalOrder:read',
    ])]
    private ?string $idNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?string $idNumberDeliveryPlace = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?\DateTimeImmutable $idNumberDelivery = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?string $phoneNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Email]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'rentalOrder:read',
    ])]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'rentalOrder:read',
    ])]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'vehicle:read'
    ])]
    private ?string $driverLicenceId = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?string $driverLicenceIdCategories = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'vehicle:read'
    ])]
    private ?\DateTimeImmutable $driverLicenceExpiration = null;

    #[ORM\ManyToOne(inversedBy: 'staffs')]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
        'rentalOrder:read',
    ])]
    private ?StaffPost $staffPost = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staffgit:update',
        'rentalOrder:read',
    ])]
    private ?Center $center = null;

    #[ORM\OneToMany(mappedBy: 'driver', targetEntity: Vehicle::class)]
    #[Groups(groups: [
        'staff:read'
    ])]
    private Collection $vehicles;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $profilePic = null;

    #[Vich\UploadableField(mapping: "staff_profil_pic", fileNameProperty: "profilePic")]
    private ?File $profilePicFile = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update',
    ])]
    private ?\DateTimeInterface $lastMedicalVisit = null;

    #[ORM\OneToMany(mappedBy: 'driver', targetEntity: RentalOrder::class)]
    private Collection $driveRentalOrders;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?string $cnapsNumber = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?float $grossSalary = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'staff:read', 'staff:create', 'staff:update'
    ])]
    private ?float $netSalary = null;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
        $this->driveRentalOrders = new ArrayCollection();
    }

    // #[ORM\ManyToOne(inversedBy: 'drivers')]
    // #[Groups(groups: [
    //     'staff:read',
    // ])]
    // private ?Vehicle $vehicle = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegistrationNumber(): ?string
    {
        return $this->registrationNumber;
    }

    public function setRegistrationNumber(string $registrationNumber): static
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): static
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): static
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getBirthday(): ?\DateTimeImmutable
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeImmutable $birthday): static
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getHiringDate(): ?\DateTimeImmutable
    {
        return $this->hiringDate;
    }

    public function setHiringDate(?\DateTimeImmutable $hiringDate): static
    {
        $this->hiringDate = $hiringDate;

        return $this;
    }

    public function getIdNumber(): ?string
    {
        return $this->idNumber;
    }

    public function setIdNumber(?string $idNumber): static
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    public function getIdNumberDeliveryPlace(): ?string
    {
        return $this->idNumberDeliveryPlace;
    }

    public function setIdNumberDeliveryPlace(?string $idNumberDeliveryPlace): static
    {
        $this->idNumberDeliveryPlace = $idNumberDeliveryPlace;

        return $this;
    }

    public function getIdNumberDelivery(): ?\DateTimeImmutable
    {
        return $this->idNumberDelivery;
    }

    public function setIdNumberDelivery(?\DateTimeImmutable $idNumberDelivery): static
    {
        $this->idNumberDelivery = $idNumberDelivery;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): static
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): static
    {
        $this->address = $address;

        return $this;
    }

    public function getDriverLicenceId(): ?string
    {
        return $this->driverLicenceId;
    }

    public function setDriverLicenceId(?string $driverLicenceId): static
    {
        $this->driverLicenceId = $driverLicenceId;

        return $this;
    }

    public function getDriverLicenceIdCategories(): ?string
    {
        return $this->driverLicenceIdCategories;
    }

    public function setDriverLicenceIdCategories(?string $driverLicenceIdCategories): static
    {
        $this->driverLicenceIdCategories = $driverLicenceIdCategories;

        return $this;
    }

    public function getDriverLicenceExpiration(): ?\DateTimeImmutable
    {
        return $this->driverLicenceExpiration;
    }

    public function setDriverLicenceExpiration(?\DateTimeImmutable $driverLicenceExpiration): static
    {
        $this->driverLicenceExpiration = $driverLicenceExpiration;

        return $this;
    }

    public function getStaffPost(): ?StaffPost
    {
        return $this->staffPost;
    }

    public function setStaffPost(?StaffPost $staffPost): static
    {
        $this->staffPost = $staffPost;

        return $this;
    }

    public function getCenter(): ?Center
    {
        return $this->center;
    }

    public function setCenter(?Center $center): static
    {
        $this->center = $center;

        return $this;
    }

    // public function getVehicle(): ?Vehicle
    // {
    //     return $this->vehicle;
    // }

    // public function setVehicle(?Vehicle $vehicle): static
    // {
    //     $this->vehicle = $vehicle;

    //     return $this;
    // }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setDriver($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getDriver() === $this) {
                $vehicle->setDriver(null);
            }
        }

        return $this;
    }

    public function getProfilePic(): ?string
    {
        return $this->profilePic;
    }

    public function setProfilePic(?string $profilePic): static
    {
        $this->profilePic = $profilePic;

        return $this;
    }

    public function getProfilePicFile(): ?File
    {
        return $this->profilePicFile;
    }

    /**
     * @param File|null $profilePicFile
     * @return Staff
     */
    public function setProfilePicFile(?File $profilePicFile): Staff
    {
        $this->profilePicFile = $profilePicFile;
        return $this;
    }

    public function getLastMedicalVisit(): ?\DateTimeInterface
    {
        return $this->lastMedicalVisit;
    }

    public function setLastMedicalVisit(?\DateTimeInterface $lastMedicalVisit): static
    {
        $this->lastMedicalVisit = $lastMedicalVisit;

        return $this;
    }

    /**
     * @return Collection<int, RentalOrder>
     */
    public function getDriveRentalOrders(): Collection
    {
        return $this->driveRentalOrders;
    }

    public function addDriveRentalOrder(RentalOrder $driveRentalOrder): static
    {
        if (!$this->driveRentalOrders->contains($driveRentalOrder)) {
            $this->driveRentalOrders->add($driveRentalOrder);
            $driveRentalOrder->setDriver($this);
        }

        return $this;
    }

    public function removeDriveRentalOrder(RentalOrder $driveRentalOrder): static
    {
        if ($this->driveRentalOrders->removeElement($driveRentalOrder)) {
            // set the owning side to null (unless already changed)
            if ($driveRentalOrder->getDriver() === $this) {
                $driveRentalOrder->setDriver(null);
            }
        }

        return $this;
    }

    public function getCnapsNumber(): ?string
    {
        return $this->cnapsNumber;
    }

    public function setCnapsNumber(?string $cnapsNumber): static
    {
        $this->cnapsNumber = $cnapsNumber;

        return $this;
    }

    public function getGrossSalary(): ?float
    {
        return $this->grossSalary;
    }

    public function setGrossSalary(?float $grossSalary): static
    {
        $this->grossSalary = $grossSalary;

        return $this;
    }

    public function getNetSalary(): ?float
    {
        return $this->netSalary;
    }

    public function setNetSalary(?float $netSalary): static
    {
        $this->netSalary = $netSalary;

        return $this;
    }

}
