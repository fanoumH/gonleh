<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\CenterRepository;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\ApiPlatform\Filter\CenterFilter;
use App\ApiPlatform\Filter\TerminalFilter;
use App\Controller\CentersToPdfController;
use App\Controller\UsersToSheetController;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: CenterRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des centres",
            ],
            normalizationContext: ['groups' => 'center:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/centers/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'center:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: CentersToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/centers/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'center:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: UsersToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un center",
            ],
            normalizationContext: ['groups' => 'center:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un center",
            ],
            normalizationContext: ['groups' => 'center:read'],
            denormalizationContext: ['groups' => 'center:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un center",
            ],
            normalizationContext: ['groups' => 'center:read'],
            denormalizationContext: ['groups' => 'center:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un center",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "id" => "exact",
    ]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"name"
],arguments:  ['orderParameterName' => 'order'])
]
#[ApiFilter(
    CenterFilter::class,properties: ['center']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class Center
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;
    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'center:read',
        'terminal:read',
        'staff:read',
        'getin:read','getin:create','getin:update',
        'container:read',
        'user:read',
        'specificRate:read',
        'proforma:read','proforma:create','proforma:update'

    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Groups(groups: [
        'center:read', 'center:create', 'center:update',
        'terminal:read',
        'staff:read',
        'getin:read','getin:create','getin:update',
        'container:read',
        'user:read',
        'specificRate:read',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'center', targetEntity: Terminal::class,cascade: ['persist','detach'])]
    private Collection $terminals;

    #[ORM\OneToMany(mappedBy: 'staffpost', targetEntity: Staff::class)]
    private Collection $staffs;

    #[ORM\OneToMany(mappedBy: 'centre', targetEntity: Proforma::class)]
    private Collection $proformas;

    public function __construct()
    {
        $this->terminals = new ArrayCollection();
        $this->staffs = new ArrayCollection();
        $this->proformas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Terminal>
     */
    public function getTerminals(): Collection
    {
        return $this->terminals;
    }

    public function addTerminal(Terminal $terminal): static
    {
        if (!$this->terminals->contains($terminal)) {
            $this->terminals->add($terminal);
            $terminal->setCenter($this);
        }

        return $this;
    }

    public function removeTerminal(Terminal $terminal): static
    {
        if ($this->terminals->removeElement($terminal)) {
            // set the owning side to null (unless already changed)
            if ($terminal->getCenter() === $this) {
                $terminal->setCenter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Staff>
     */
    public function getStaffs(): Collection
    {
        return $this->staffs;
    }

    public function addStaffs(Staff $staff): static
    {
        if (!$this->staffs->contains($staff)) {
            $this->staffs->add($staff);
            $staff->setCenter($this);
        }

        return $this;
    }

    public function removeStaffs(Staff $staff): static
    {
        if ($this->staffs->removeElement($staff)) {
            // set the owning side to null (unless already changed)
            if ($staff->getCenter() === $this) {
                $staff->setStaffPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Proforma>
     */
    public function getProformas(): Collection
    {
        return $this->proformas;
    }

    public function addProforma(Proforma $proforma): static
    {
        if (!$this->proformas->contains($proforma)) {
            $this->proformas->add($proforma);
            $proforma->setCentre($this);
        }

        return $this;
    }

    public function removeProforma(Proforma $proforma): static
    {
        if ($this->proformas->removeElement($proforma)) {
            // set the owning side to null (unless already changed)
            if ($proforma->getCentre() === $this) {
                $proforma->setCentre(null);
            }
        }

        return $this;
    }
}
