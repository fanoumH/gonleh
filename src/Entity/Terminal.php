<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\TerminalRepository;
use ApiPlatform\Metadata\GetCollection;
use App\State\TerminalComputedProvider;
use App\ApiPlatform\Filter\TerminalFilter;
use Doctrine\Common\Collections\Collection;
use App\Controller\TerminalsToPdfController;
use App\Controller\TerminalsToSheetController;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: TerminalRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste terminal",
            ],
            normalizationContext: ['groups' => 'terminal:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/terminals/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'terminal:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TerminalsToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/terminals/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'terminal:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TerminalsToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un terminal",
            ],
            normalizationContext: ['groups' => 'terminal:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un terminal",
            ],
            normalizationContext: ['groups' => 'terminal:read'],
            denormalizationContext: ['groups' => 'terminal:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un terminal",
            ],
            normalizationContext: ['groups' => 'terminal:read'],
            denormalizationContext: ['groups' => 'terminal:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un terminal",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Recupérer capacité totale de tous les terminaux et le nombre total d'unités de 20pieds occupées",
            ],
            uriTemplate: '/terminals-computed',
            provider: TerminalComputedProvider::class
        ),
    ]
)]
#[ApiFilter(
    SearchFilter::class, properties: [
    "center" => "exact",
    "name" => 'ipartial',
    ]
)]
#[ApiFilter(
    OrderFilter::class, properties: [
        "center.name", "name", "capacity", "occupiedPlaces", "occupiedPlacesRate"
    ],
    arguments:  ['orderParameterName' => 'order']
)]
#[ApiFilter(
    TerminalFilter::class,properties: ['terminal']
)]
class Terminal
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;
    /**
     * history trait
     */
    use HistoryTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'terminal:read','getin:read','getin:create','getin:update', 'container:read',
        'user:read', 'booking:read',
        'specificRate:read','basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'terminals')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'terminal:read', 'terminal:create', 'terminal:update','getin:read','getin:create','getin:update', 'container:read',
        'user:read',
        'specificRate:read','basicRate:read',
        'basicRate:create',
        'basicRate:update','PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?Center $center = null;

    #[ORM\Column]
    #[Groups(groups: [
        'terminal:read', 'terminal:create', 'terminal:update','getin:read','getin:create','getin:update', 'container:read',
        'user:read',
        'specificRate:read','basicRate:read',
        'basicRate:create',
        'basicRate:update','PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?int $capacity = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'terminal:read', 'terminal:create', 'terminal:update','getin:read','getin:create','getin:update', 'container:read',
        'user:read',
        'specificRate:read','basicRate:read',
        'basicRate:create',
        'basicRate:update','PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'terminal', targetEntity: TerminalGetin::class)]
    private Collection $terminalGetins;

    #[Groups(groups: [
        'terminal:read', 'container:read','user:read','basicRate:read',
        'basicRate:create',
        'basicRate:update','PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?int $occupiedPlaces = null;

    #[Groups(groups: [
        'terminal:read', 'container:read', 'user:read','basicRate:read',
        'basicRate:create',
        'basicRate:update','PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?float $occupiedPlacesRate = null;

    #[ORM\OneToMany(mappedBy: 'terminal', targetEntity: User::class)]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'terminal', targetEntity: Booking::class)]
    private Collection $bookings;

    #[ORM\OneToMany(mappedBy: 'terminal', targetEntity: SpecificRate::class)]
    private Collection $specificRates;

    #[ORM\OneToMany(mappedBy: 'terminal', targetEntity: BasicRate::class)]
    private Collection $basicRates;

    #[ORM\OneToMany(mappedBy: 'terminal', targetEntity: PriceListStandardTransport::class)]
    private Collection $priceListStandardTransports;

    public function __construct()
    {
        $this->terminalGetins = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->specificRates = new ArrayCollection();
        $this->basicRates = new ArrayCollection();
        $this->priceListStandardTransports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCenter(): ?Center
    {
        return $this->center;
    }

    public function setCenter(?Center $center): static
    {
        $this->center = $center;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): static
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, TerminalGetin>
     */
    public function getTerminalGetins(): Collection
    {
        return $this->terminalGetins;
    }

    public function addTerminalGetin(TerminalGetin $terminalGetin): static
    {
        if (!$this->terminalGetins->contains($terminalGetin)) {
            $this->terminalGetins->add($terminalGetin);
            $terminalGetin->setTerminal($this);
        }

        return $this;
    }

    public function removeTerminalGetin(TerminalGetin $terminalGetin): static
    {
        if ($this->terminalGetins->removeElement($terminalGetin)) {
            // set the owning side to null (unless already changed)
            if ($terminalGetin->getTerminal() === $this) {
                $terminalGetin->setTerminal(null);
            }
        }

        return $this;
    }

    public function getOccupiedPlaces(): ?int
    {
        return $this->occupiedPlaces;
    }

    public function setOccupiedPlaces(int $occupiedPlaces): static
    {
        $this->occupiedPlaces = $occupiedPlaces;

        return $this;
    }

    public function getOccupiedPlacesRate(): ?float
    {
        return $this->occupiedPlacesRate;
    }

    public function setOccupiedPlacesRate(float $occupiedPlacesRate): static
    {
        $this->occupiedPlacesRate = $occupiedPlacesRate;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setTerminal($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getTerminal() === $this) {
                $user->setTerminal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): static
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings->add($booking);
            $booking->setTerminal($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): static
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getTerminal() === $this) {
                $booking->setTerminal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRate>
     */
    public function getSpecificRates(): Collection
    {
        return $this->specificRates;
    }

    public function addSpecificRate(SpecificRate $specificRate): static
    {
        if (!$this->specificRates->contains($specificRate)) {
            $this->specificRates->add($specificRate);
            $specificRate->setTerminal($this);
        }

        return $this;
    }

    public function removeSpecificRate(SpecificRate $specificRate): static
    {
        if ($this->specificRates->removeElement($specificRate)) {
            // set the owning side to null (unless already changed)
            if ($specificRate->getTerminal() === $this) {
                $specificRate->setTerminal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, BasicRate>
     */
    public function getBasicRates(): Collection
    {
        return $this->basicRates;
    }

    public function addBasicRate(BasicRate $basicRate): static
    {
        if (!$this->basicRates->contains($basicRate)) {
            $this->basicRates->add($basicRate);
            $basicRate->setTerminal($this);
        }

        return $this;
    }

    public function removeBasicRate(BasicRate $basicRate): static
    {
        if ($this->basicRates->removeElement($basicRate)) {
            // set the owning side to null (unless already changed)
            if ($basicRate->getTerminal() === $this) {
                $basicRate->setTerminal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PriceListStandardTransport>
     */
    public function getPriceListStandardTransports(): Collection
    {
        return $this->priceListStandardTransports;
    }

    public function addPriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if (!$this->priceListStandardTransports->contains($priceListStandardTransport)) {
            $this->priceListStandardTransports->add($priceListStandardTransport);
            $priceListStandardTransport->setTerminal($this);
        }

        return $this;
    }

    public function removePriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if ($this->priceListStandardTransports->removeElement($priceListStandardTransport)) {
            // set the owning side to null (unless already changed)
            if ($priceListStandardTransport->getTerminal() === $this) {
                $priceListStandardTransport->setTerminal(null);
            }
        }

        return $this;
    }
}
