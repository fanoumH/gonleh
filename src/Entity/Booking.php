<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use App\ApiPlatform\Filter\BookingListFilter;
use App\State\BookingProvider;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\BookingRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\ApiPlatform\Filter\BookingFilter;
use App\Controller\BookingsToPdfController;
use App\Controller\BookingUploadController;
use Doctrine\Common\Collections\Collection;
use App\Controller\BookingsToSheetController;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use Symfony\Component\HttpFoundation\File\File;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: BookingRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des reservations",
            ],
            normalizationContext: ['groups' => 'booking:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            order: ["id" => "DESC"],
            // provider: BookingProvider::class,
        ),
        new GetCollection(
            uriTemplate: '/bookings/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'booking:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: BookingsToPdfController::class,
        ),
        new GetCollection(
            uriTemplate: '/bookings/all/sheet',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'booking:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: BookingsToSheetController::class,
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'une reservation",
            ],
            normalizationContext: ['groups' => 'booking:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'une reservation",
            ],
            normalizationContext: ['groups' => 'booking:read'],
            denormalizationContext: ['groups' => 'booking:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            types: ['https://schema.org/bookings'],
            uriTemplate: "/bookings/{id}/booking-attachment",
            openapiContext: [
                "summary" => "Upload boooking attachment",
            ],
            normalizationContext: ['groups' => 'booking:read'],
            denormalizationContext: ['groups' => 'booking:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            inputFormats: ['multipart' => ['multipart/form-data']],
            deserialize: false,
            controller: BookingUploadController::class,
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'une reservation",
            ],
            normalizationContext: ['groups' => 'booking:read'],
            denormalizationContext: ['groups' => 'booking:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'une reservation",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['booking:update']]
)]
#[ApiFilter(
    OrderFilter::class, properties: [
        "refBooking",
    ],
    arguments:  ['orderParameterName' => 'order']
)]
#[ApiFilter(
    BooleanFilter::class, properties: [
        'isCancelled',
    ]
)]

#[ApiFilter(
    BookingFilter::class, properties: [
        'order[containers]',
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "refBooking" => "ipartial",
    "id" => "exact",
]
)]

#[ApiFilter(
    DateFilter::class,properties: [
    "createdAt",
    "dateRelease",
]
)]
// #[ApiFilter(
//     BookingFilter::class,properties: ['booking']
// )]
#[ApiFilter(
    BookingListFilter::class,properties: ['bookingList']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[Vich\Uploadable]
class Booking
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const BOOKING_PREFIX = "RES" ;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
       'booking:read',
       'terminalGetOut:read',
       'transportOrder:read',
       'container:read',
    ])] 
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'booking:read', 'booking:create', 'booking:update', 'terminalGetOut:read',
        'transportOrder:read',
        'container:read',
    ])]
    private ?string $refBooking = null;

    #[ORM\ManyToOne(inversedBy: 'bookings')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'booking:read','booking:create','booking:update',
        'transportOrder:read',
        'container:read',
    ])]
    private ?Customer $client = null;
    
    /*
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'booking:read','booking:create','booking:update'
    ])]
    private ?\DateTimeInterface $getOutDate = null;
    */
    
    /*
    #[Groups(groups: [
        'booking:read'
    ])]
    private ?\DateTimeInterface $dateGetOut = null;
    */

    #[ORM\OneToMany(mappedBy: 'booking', targetEntity: Container::class, cascade: ['persist','detach'])]
    #[Groups(groups: [
        'booking:read','booking:create','booking:update'
    ])]
    private Collection $containers;

    #[ORM\ManyToOne(inversedBy: 'bookings')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'booking:read','booking:create','booking:update',
        'transportOrder:read',
        'terminalGetOut:read',
        'container:read',
    ])]
    private ?ShippingCompany $shippingCompany = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'booking:read', 'booking:create', 'booking:update',
        'container:read',
     ])]
    private ?\DateTimeInterface $dateRelease = null;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    #[Groups(groups: [
        'booking:read','booking:create','booking:update',
        'transportOrder:read',
        'container:read',
    ])]
    private ?bool $isCancelled = null;

    #[ORM\Column(length: 255, nullable: true, options: ["default" => "-"])]
    #[Groups(groups: [
        'booking:read','booking:create','booking:update',
        'container:read',
    ])]
    private ?string $cancelledReason = null;

    #[ORM\ManyToOne(inversedBy: 'bookings')]
    private ?Terminal $terminal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bookingAttachment = null;

    #[Vich\UploadableField(mapping: "booking_attachment_file", fileNameProperty: "bookingAttachment")]
    private ?File $bookingAttachmentFile = null;

    // #[ORM\OneToMany(mappedBy: 'booking', targetEntity: TransportOrder::class)]
    // private Collection $transportOrders;

    public function __construct()
    {
        $this->containers = new ArrayCollection();
        $this->transportOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRefBooking(): ?string
    {
        return $this->refBooking;
    }

    public function setRefBooking(?string $refBooking): static
    {
        $this->refBooking = $refBooking;

        return $this;
    }

    public function getClient(): ?Customer
    {
        return $this->client;
    }

    public function setClient(?Customer $client): static
    {
        $this->client = $client;

        return $this;
    }

    /*
    public function getGetOutDate(): ?\DateTimeInterface
    {
        return $this->getOutDate;
    }

    public function setGetOutDate(?\DateTimeInterface $getOutDate): static
    {
        $this->getOutDate = $getOutDate;

        return $this;
    }
    */

    /*
    public function getDateGetOut(): ?\DateTimeInterface
    {
        return $this->dateGetOut;
    }

    public function setDateGetOut(?\DateTimeInterface $dateGetOut): static
    {
        $this->dateGetOut = $dateGetOut;

        return $this;
    }
    */

    /**
     * @return Collection<int, Container>
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }

    public function addContainer(Container $container): static
    {
        if (!$this->containers->contains($container)) {
            $this->containers->add($container);
            $container->setBooking($this);
        }

        return $this;
    }

    public function removeContainer(Container $container): static
    {
        if ($this->containers->removeElement($container)) {
            // set the owning side to null (unless already changed)
            if ($container->getBooking() === $this) {
                $container->setBooking(null);
            }
        }

        return $this;
    }

    public function getShippingCompany(): ?ShippingCompany
    {
        return $this->shippingCompany;
    }

    public function setShippingCompany(?ShippingCompany $shippingCompany): static
    {
        $this->shippingCompany = $shippingCompany;

        return $this;
    }

    public function toArray(){
        return get_object_vars($this);
    }

    public function getDateRelease(): ?\DateTimeInterface
    {
        return $this->dateRelease;
    }

    public function setDateRelease(?\DateTimeInterface $dateRelease): static
    {
        $this->dateRelease = $dateRelease;

        return $this;
    }

    public function isIsCancelled(): ?bool
    {
        return $this->isCancelled;
    }

    public function setIsCancelled(bool $isCancelled): static
    {
        $this->isCancelled = $isCancelled;

        return $this;
    }

    public function getCancelledReason(): ?string
    {
        return $this->cancelledReason;
    }

    public function setCancelledReason(?string $cancelledReason): static
    {
        $this->cancelledReason = $cancelledReason;

        return $this;
    }

    public function getTerminal(): ?Terminal
    {
        return $this->terminal;
    }

    public function setTerminal(?Terminal $terminal): static
    {
        $this->terminal = $terminal;

        return $this;
    }

    // /**
    //  * @return Collection<int, TransportOrder>
    //  */
    // public function getTransportOrders(): Collection
    // {
    //     return $this->transportOrders;
    // }

    // public function addTransportOrder(TransportOrder $transportOrder): static
    // {
    //     if (!$this->transportOrders->contains($transportOrder)) {
    //         $this->transportOrders->add($transportOrder);
    //         $transportOrder->setBooking($this);
    //     }

    //     return $this;
    // }

    // public function removeTransportOrder(TransportOrder $transportOrder): static
    // {
    //     if ($this->transportOrders->removeElement($transportOrder)) {
    //         // set the owning side to null (unless already changed)
    //         if ($transportOrder->getBooking() === $this) {
    //             $transportOrder->setBooking(null);
    //         }
    //     }

    //     return $this;
    // }

    public function getBookingAttachment(): ?string
    {
        return $this->bookingAttachment;
    }

    public function setBookingAttachment(?string $bookingAttachment): static
    {
        $this->bookingAttachment = $bookingAttachment;

        return $this;
    }

    public function getBookingAttachmentFile(): ?File
    {
        return $this->bookingAttachmentFile;
    }

    public function setBookingAttachmentFile(?File $bookingAttachmentFile): static
    {
        $this->bookingAttachmentFile = $bookingAttachmentFile;

        return $this;
    }
}
