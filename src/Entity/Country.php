<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CountryRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
#[ORM\Entity(repositoryClass: CountryRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des countries",
            ],
            normalizationContext: ['groups' => 'country:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            order: ["nameFr" => "ASC"],
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un countries",
            ],
            normalizationContext: ['groups' => 'country:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un countries",
            ],
            normalizationContext: ['groups' => 'country:read'],
            denormalizationContext: ['groups' => 'country:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un countries",
            ],
            normalizationContext: ['groups' => 'country:read'],
            denormalizationContext: ['groups' => 'country:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un countries",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "code" => "ipartial",
    "id" => "exact",
    "nameEn" => "ipartial",
    "nameFr" => "ipartial",
]
)]
class Country
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','bank:read','country:read','country:create','country:update','customerContact:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create',
        'bankContact:read','bankContact:create','bankContact:update'
        ,'country:read','country:create','country:update'
    ])]
    private ?string $code = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create',
        'bankContact:read','bankContact:create','bankContact:update'
        ,'country:read','country:create','country:update'
    ])]
    private ?string $nameEn = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','bank:read','bank:update','bank:create',
        'bankContact:read','bankContact:create','bankContact:update'
        ,'country:read','country:create','country:update'
    ])]
    private ?string $nameFr = null;

    #[ORM\OneToMany(mappedBy: 'country', targetEntity: CustomerIdentification::class,cascade: ['persist','detach'])]
    private Collection $customerIdentifications;

    #[ORM\OneToMany(mappedBy: 'country', targetEntity: Bank::class,cascade: ['persist','detach'])]
    private Collection $banks;

    #[ORM\OneToMany(mappedBy: 'country', targetEntity: BankContact::class,cascade: ['persist','detach'])]
    private Collection $bankContacts;

    public function __construct()
    {
        $this->customerIdentifications = new ArrayCollection();
        $this->banks = new ArrayCollection();
        $this->bankContacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getNameEn(): ?string
    {
        return $this->nameEn;
    }

    public function setNameEn(?string $nameEn): static
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    public function getNameFr(): ?string
    {
        return $this->nameFr;
    }

    public function setNameFr(?string $nameFr): static
    {
        $this->nameFr = $nameFr;

        return $this;
    }

    /**
     * @return Collection<int, CustomerIdentification>
     */
    public function getCustomerIdentifications(): Collection
    {
        return $this->customerIdentifications;
    }

    public function addCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if (!$this->customerIdentifications->contains($customerIdentification)) {
            $this->customerIdentifications->add($customerIdentification);
            $customerIdentification->setCountry($this);
        }

        return $this;
    }

    public function removeCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if ($this->customerIdentifications->removeElement($customerIdentification)) {
            // set the owning side to null (unless already changed)
            if ($customerIdentification->getCountry() === $this) {
                $customerIdentification->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Bank>
     */
    public function getBanks(): Collection
    {
        return $this->banks;
    }

    public function addBank(Bank $bank): static
    {
        if (!$this->banks->contains($bank)) {
            $this->banks->add($bank);
            $bank->setCountry($this);
        }

        return $this;
    }

    public function removeBank(Bank $bank): static
    {
        if ($this->banks->removeElement($bank)) {
            // set the owning side to null (unless already changed)
            if ($bank->getCountry() === $this) {
                $bank->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, BankContact>
     */
    public function getBankContacts(): Collection
    {
        return $this->bankContacts;
    }

    public function addBankContact(BankContact $bankContact): static
    {
        if (!$this->bankContacts->contains($bankContact)) {
            $this->bankContacts->add($bankContact);
            $bankContact->setCountry($this);
        }

        return $this;
    }

    public function removeBankContact(BankContact $bankContact): static
    {
        if ($this->bankContacts->removeElement($bankContact)) {
            // set the owning side to null (unless already changed)
            if ($bankContact->getCountry() === $this) {
                $bankContact->setCountry(null);
            }
        }

        return $this;
    }
}
