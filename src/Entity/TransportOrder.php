<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use App\ApiPlatform\Filter\TerminalGetOutFilter;
use App\ApiPlatform\Filter\TransportOrderFilter;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use App\Repository\TransportOrderRepository;
use App\Controller\BLUploadTransportController;
use Symfony\Component\HttpFoundation\File\File;
use App\Controller\EirUploadTransportController;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\OrderPurchaseUploadTransportController;
use App\Controller\TransportOrdersToPdfController;
use App\Controller\TransportOrdersToSheetController;

#[ORM\Entity(repositoryClass: TransportOrderRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des ordres de transport",
            ],
            normalizationContext: ['groups' => 'transportOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/transport_orders/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'center:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TransportOrdersToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/transport_orders/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'center:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TransportOrdersToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un ordre de transport",
            ],
            normalizationContext: ['groups' => 'transportOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un ordre de transport",
            ],
            normalizationContext: ['groups' => 'transportOrder:read'],
            denormalizationContext: ['groups' => 'transportOrder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            types: ['https://schema.org/transport_orders'],
            uriTemplate: "/transport_orders/{id}/purchase-order",
            openapiContext: [
                "summary" => "Upload scan bon de commande ordre de transport",
            ],
            normalizationContext: ['groups' => 'transportOrder:read'],
            denormalizationContext: ['groups' => 'transportOrder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            inputFormats: ['multipart' => ['multipart/form-data']],
            deserialize: false,
            controller: OrderPurchaseUploadTransportController::class,
        ),
        new Post(
            types: ['https://schema.org/transport_orders'],
            uriTemplate: "/transport_orders/{id}/eir-attachment",
            openapiContext: [
                "summary" => "Upload EIR",
            ],
            normalizationContext: ['groups' => 'transportOrder:read'],
            denormalizationContext: ['groups' => 'transportOrder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            inputFormats: ['multipart' => ['multipart/form-data']],
            deserialize: false,
            controller: EirUploadTransportController::class,
        ),
        new Post(
            types: ['https://schema.org/transport_orders'],
            uriTemplate: "/transport_orders/{id}/bl-attachment",
            openapiContext: [
                "summary" => "Upload BL",
            ],
            normalizationContext: ['groups' => 'transportOrder:read'],
            denormalizationContext: ['groups' => 'transportOrder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            inputFormats: ['multipart' => ['multipart/form-data']],
            deserialize: false,
            controller: BLUploadTransportController::class,
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un ordre de transport",
            ],
            normalizationContext: ['groups' => 'transportOrder:read'],
            denormalizationContext: ['groups' => 'transportOrder:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un ordre de transport",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "otStatus" => "exact",
    "code" => "ipartial",
]
)]
#[ApiFilter(
    TransportOrderFilter::class,properties: ['transprotOrder']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
#[Vich\Uploadable]
class TransportOrder
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    const TRANSPORT_ORDER_PREFIX = "OT";

    const TO_STATE_CREATED = "created";
    const TO_STATE_ASSIGNED = "assigned";
    const TO_STATE_PROGRESS = "progress";
    const TO_STATE_DELIVERED = "delivered";
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'transportOrder:read',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?int $id = null;

    // #[ORM\Column(length: 255)]
    // #[Groups(groups: [
    //     'transportOrder:read',
    //     'transportOrder:create',
    //     'transportOrder:update',
    // ])]
    // private ?string $tripType = null;

    // #[ORM\Column(length: 255)]
    // #[Groups(groups: [
    //     'transportOrder:read',
    //     'transportOrder:create',
    //     'transportOrder:update',
    // ])]
    // private ?string $departureCity = null;

    // #[ORM\Column(length: 255)]
    // #[Groups(groups: [
    //     'transportOrder:read',
    //     'transportOrder:create',
    //     'transportOrder:update',
    // ])]
    // private ?string $arrivalCity = null;

    // #[ORM\ManyToOne(inversedBy: 'transportOrders')]
    // #[Groups(groups: [
    //     'transportOrder:read',
    //     'transportOrder:create',
    //     'transportOrder:update',
    // ])]
    // private ?TerminalGetOut $getOut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?\DateTimeInterface $departureDate = null;

    // #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    // private ?\DateTimeInterface $previewedDepartureDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?\DateTimeInterface $actualArrivalDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $code = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?\DateTimeInterface $previewedArrivalDate = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // #[Groups(groups: [
    //     'transportOrder:read',
    //     'transportOrder:create',
    //     'transportOrder:update',
    // ])]
    // private ?string $containerWeight = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // #[Groups(groups: [
    //     'transportOrder:read',
    //     'transportOrder:create',
    //     'transportOrder:update',
    // ])]
    // private ?string $customer = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $bookingNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    // #[Groups(groups: [
    //     'transportOrder:read',
    //     'transportOrder:create',
    //     'transportOrder:update',
    // ])]
    private ?string $purchaseOrder = null;
    
    #[Vich\UploadableField(mapping: "purchase_order_file", fileNameProperty: "purchaseOrder")]
    private ?File $purchaseOrderFile = null;

    #[ORM\ManyToOne(inversedBy: 'transportOrders')]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?Vehicle $affectedTruck = null;

    #[ORM\ManyToOne(inversedBy: 'transportOrders')]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?SubContractor $subContractor = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $subContractorVehicleNumber = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $aboutContainers = null;

    #[ORM\ManyToOne(inversedBy: 'transportOrders')]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?Customer $customer = null;

    #[ORM\ManyToOne(inversedBy: 'transportOrders')]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?TripType $tripType = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $PO = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?\DateTimeInterface $unloadingDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $placeOfReturn = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $eirNumberForExport = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $deliveryNoteForImport = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?string $confirmationEmail = null;

    #[ORM\Column(length: 255, nullable: true, options: ['defaut' => self::TO_STATE_CREATED])]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    #[Assert\Choice(choices: [
        self::TO_STATE_CREATED,
        self::TO_STATE_ASSIGNED,
        self::TO_STATE_PROGRESS,
        self::TO_STATE_DELIVERED,
    ])]
    private ?string $otStatus = null;

    #[ORM\ManyToOne(inversedBy: 'transportOrders')]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?Place $departureCity = null;

    #[ORM\ManyToOne(inversedBy: 'transportOrders')]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?Place $arrivalCity = null;

    #[ORM\OneToOne(mappedBy: 'transportOrder', cascade: ['persist', 'remove'])]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'histoTransportOrder:read',
    ])]
    private ?MissionOrder $missionOrder = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'transportOrder:read',
        'transportOrder:create',
        'transportOrder:update',
        'missionOrder:read',
        'histoTransportOrder:read',
    ])]
    private ?\DateTimeInterface $loadingDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $eirAttachment = null;

    #[Vich\UploadableField(mapping: "eir_attachment_file", fileNameProperty: "eirAttachment")]
    private ?File $eirAttachmentFile = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $blAttachment = null;

    #[Vich\UploadableField(mapping: "bl_attachment_file", fileNameProperty: "blAttachment")]
    private ?File $blAttachmentFile = null;

    #[ORM\OneToMany(mappedBy: 'transportOrder', targetEntity: HistoryTransportOrder::class)]
    #[Groups(groups: [
        'transportOrder:read',
    ])]
    private Collection $historyTransportOrders;

    public function __construct()
    {
        $this->historyTransportOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    // public function getCustomer(): ?Customer
    // {
    //     return $this->customer;
    // }

    // public function setCustomer(?Customer $customer): static
    // {
    //     $this->customer = $customer;

    //     return $this;
    // }

    // public function getTripType(): ?string
    // {
    //     return $this->tripType;
    // }

    // public function setTripType(string $tripType): static
    // {
    //     $this->tripType = $tripType;

    //     return $this;
    // }

    // public function getBooking(): ?Booking
    // {
    //     return $this->booking;
    // }

    // public function setBooking(?Booking $booking): static
    // {
    //     $this->booking = $booking;

    //     return $this;
    // }

    // public function getDepartureCity(): ?string
    // {
    //     return $this->departureCity;
    // }

    // public function setDepartureCity(string $departureCity): static
    // {
    //     $this->departureCity = $departureCity;

    //     return $this;
    // }

    // public function getArrivalCity(): ?string
    // {
    //     return $this->arrivalCity;
    // }

    // public function setArrivalCity(string $arrivalCity): static
    // {
    //     $this->arrivalCity = $arrivalCity;

    //     return $this;
    // }


    // public function getGetOut(): ?TerminalGetOut
    // {
    //     return $this->getOut;
    // }

    // public function setGetOut(?TerminalGetOut $getOut): static
    // {
    //     $this->getOut = $getOut;

    //     return $this;
    // }

    public function getDepartureDate(): ?\DateTimeInterface
    {
        return $this->departureDate;
    }

    public function setDepartureDate(?\DateTimeInterface $departureDate): static
    {
        $this->departureDate = $departureDate;

        return $this;
    }

    // public function getPreviewedDepartureDate(): ?\DateTimeInterface
    // {
    //     return $this->previewedDepartureDate;
    // }

    // public function setPreviewedDepartureDate(?\DateTimeInterface $previewedDepartureDate): static
    // {
    //     $this->previewedDepartureDate = $previewedDepartureDate;

    //     return $this;
    // }

    public function getActualArrivalDate(): ?\DateTimeInterface
    {
        return $this->actualArrivalDate;
    }

    public function setActualArrivalDate(?\DateTimeInterface $actualArrivalDate): static
    {
        $this->actualArrivalDate = $actualArrivalDate;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getPreviewedArrivalDate(): ?\DateTimeInterface
    {
        return $this->previewedArrivalDate;
    }

    public function setPreviewedArrivalDate(?\DateTimeInterface $previewedArrivalDate): static
    {
        $this->previewedArrivalDate = $previewedArrivalDate;

        return $this;
    }

    // public function getContainerWeight(): ?string
    // {
    //     return $this->containerWeight;
    // }

    // public function setContainerWeight(?string $containerWeight): static
    // {
    //     $this->containerWeight = $containerWeight;

    //     return $this;
    // }

    // public function getCustomer(): ?string
    // {
    //     return $this->customer;
    // }

    // public function setCustomer(?string $customer): static
    // {
    //     $this->customer = $customer;

    //     return $this;
    // }

    public function getBookingNumber(): ?string
    {
        return $this->bookingNumber;
    }

    public function setBookingNumber(?string $bookingNumber): static
    {
        $this->bookingNumber = $bookingNumber;

        return $this;
    }

    public function getPurchaseOrder(): ?string
    {
        return $this->purchaseOrder;
    }

    public function setPurchaseOrder(?string $purchaseOrder): static
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    public function getAffectedTruck(): ?Vehicle
    {
        return $this->affectedTruck;
    }

    public function setAffectedTruck(?Vehicle $affectedTruck): static
    {
        $this->affectedTruck = $affectedTruck;

        return $this;
    }

    public function getSubContractor(): ?SubContractor
    {
        return $this->subContractor;
    }

    public function setSubContractor(?SubContractor $subContractor): static
    {
        $this->subContractor = $subContractor;

        return $this;
    }

    public function getSubContractorVehicleNumber(): ?string
    {
        return $this->subContractorVehicleNumber;
    }

    public function setSubContractorVehicleNumber(?string $subContractorVehicleNumber): static
    {
        $this->subContractorVehicleNumber = $subContractorVehicleNumber;

        return $this;
    }

    public function getAboutContainers(): ?string
    {
        return $this->aboutContainers;
    }

    public function setAboutContainers(?string $aboutContainers): static
    {
        $this->aboutContainers = $aboutContainers;

        return $this;
    }

    public function getPurchaseOrderFile(): ?File
    {
        return $this->purchaseOrderFile;
    }

    /**
     * @param File|null $purchaseOrderFile
     * @return TransportOrder
     */
    public function setPurchaseOrderFile(?File $purchaseOrderFile): TransportOrder
    {
        $this->purchaseOrderFile = $purchaseOrderFile;
        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    public function getTripType(): ?TripType
    {
        return $this->tripType;
    }

    public function setTripType(?TripType $tripType): static
    {
        $this->tripType = $tripType;

        return $this;
    }

    public function getPO(): ?string
    {
        return $this->PO;
    }

    public function setPO(?string $PO): static
    {
        $this->PO = $PO;

        return $this;
    }

    public function getUnloadingDate(): ?\DateTimeInterface
    {
        return $this->unloadingDate;
    }

    public function setUnloadingDate(?\DateTimeInterface $unloadingDate): static
    {
        $this->unloadingDate = $unloadingDate;

        return $this;
    }

    public function getPlaceOfReturn(): ?string
    {
        return $this->placeOfReturn;
    }

    public function setPlaceOfReturn(?string $placeOfReturn): static
    {
        $this->placeOfReturn = $placeOfReturn;

        return $this;
    }

    public function getEirNumberForExport(): ?string
    {
        return $this->eirNumberForExport;
    }

    public function setEirNumberForExport(?string $eirNumberForExport): static
    {
        $this->eirNumberForExport = $eirNumberForExport;

        return $this;
    }

    public function getDeliveryNoteForImport(): ?string
    {
        return $this->deliveryNoteForImport;
    }

    public function setDeliveryNoteForImport(?string $deliveryNoteForImport): static
    {
        $this->deliveryNoteForImport = $deliveryNoteForImport;

        return $this;
    }

    public function getConfirmationEmail(): ?string
    {
        return $this->confirmationEmail;
    }

    public function setConfirmationEmail(?string $confirmationEmail): static
    {
        $this->confirmationEmail = $confirmationEmail;

        return $this;
    }

    public function getOtStatus(): ?string
    {
        return $this->otStatus;
    }

    public function setOtStatus(?string $otStatus): static
    {
        $this->otStatus = $otStatus;

        return $this;
    }

    public function getDepartureCity(): ?Place
    {
        return $this->departureCity;
    }

    public function setDepartureCity(?Place $departureCity): static
    {
        $this->departureCity = $departureCity;

        return $this;
    }

    public function getArrivalCity(): ?Place
    {
        return $this->arrivalCity;
    }

    public function setArrivalCity(?Place $arrivalCity): static
    {
        $this->arrivalCity = $arrivalCity;

        return $this;
    }

    public function getMissionOrder(): ?MissionOrder
    {
        return $this->missionOrder;
    }

    public function setMissionOrder(?MissionOrder $missionOrder): static
    {
        // unset the owning side of the relation if necessary
        if ($missionOrder === null && $this->missionOrder !== null) {
            $this->missionOrder->setTransportOrder(null);
        }

        // set the owning side of the relation if necessary
        if ($missionOrder !== null && $missionOrder->getTransportOrder() !== $this) {
            $missionOrder->setTransportOrder($this);
        }

        $this->missionOrder = $missionOrder;

        return $this;
    }

    public function getLoadingDate(): ?\DateTimeInterface
    {
        return $this->loadingDate;
    }

    public function setLoadingDate(?\DateTimeInterface $loadingDate): static
    {
        $this->loadingDate = $loadingDate;

        return $this;
    }

    public function getEirAttachment(): ?string
    {
        return $this->eirAttachment;
    }

    public function setEirAttachment(?string $eirAttachment): static
    {
        $this->eirAttachment = $eirAttachment;

        return $this;
    }

    public function getEirAttachmentFile(): ?File
    {
        return $this->eirAttachmentFile;
    }

    public function setEirAttachmentFile(?File $eirAttachmentFile): static
    {
        $this->eirAttachmentFile = $eirAttachmentFile;

        return $this;
    }

    public function getBlAttachment(): ?string
    {
        return $this->blAttachment;
    }

    public function setBlAttachment(?string $blAttachment): TransportOrder
    {
        $this->blAttachment = $blAttachment;

        return $this;
    }

    public function getBlAttachmentFile(): ?File
    {
        return $this->blAttachmentFile;
    }

    public function setBlAttachmentFile(?File $blAttachmentFile): TransportOrder
    {
        $this->blAttachmentFile = $blAttachmentFile;

        return $this;
    }

    /**
     * @return Collection<int, HistoryTransportOrder>
     */
    public function getHistoryTransportOrders(): Collection
    {
        return $this->historyTransportOrders;
    }

    public function addHistoryTransportOrder(HistoryTransportOrder $historyTransportOrder): static
    {
        if (!$this->historyTransportOrders->contains($historyTransportOrder)) {
            $this->historyTransportOrders->add($historyTransportOrder);
            $historyTransportOrder->setTransportOrder($this);
        }

        return $this;
    }

    public function removeHistoryTransportOrder(HistoryTransportOrder $historyTransportOrder): static
    {
        if ($this->historyTransportOrders->removeElement($historyTransportOrder)) {
            // set the owning side to null (unless already changed)
            if ($historyTransportOrder->getTransportOrder() === $this) {
                $historyTransportOrder->setTransportOrder(null);
            }
        }

        return $this;
    }

}
