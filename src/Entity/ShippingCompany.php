<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\ShipsToSheetController;
use App\Controller\ShippingToPdfController;
use Doctrine\Common\Collections\Collection;
use App\Repository\ShippingCompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: ShippingCompanyRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des compagnies maritimes",
            ],
            normalizationContext: ['groups' => 'shippingcompany:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/shipping_companies/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'shippingcompany:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: ShippingToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/shipping_companies/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'shippingcompany:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: ShipsToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'une compagnie maritime",
            ],
            normalizationContext: ['groups' => 'shippingcompany:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'une compagnie maritime",
            ],
            normalizationContext: ['groups' => 'shippingcompany:read'],
            denormalizationContext: ['groups' => 'shippingcompany:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'une compagnie maritime",
            ],
            normalizationContext: ['groups' => 'shippingcompany:read'],
            denormalizationContext: ['groups' => 'shippingcompany:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'une compagnie maritime",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "id" => "exact"
    ]
)]
class ShippingCompany
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;
    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'shippingcompany:read', 'terminal:read','booking:read','getin:read', 'container:read',
        'containerType:read',
        'eir:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Groups(groups: [
        'shippingcompany:read', 'shippingcompany:create', 'shippingcompany:update', 'terminal:read',
        'booking:read',
        'getin:read','getin:create','getin:update', 'container:read',
        'containerType:read',
        'eir:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
        
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'shippingCompany', targetEntity: TerminalGetin::class,cascade: ['persist','detach'])]
    private Collection $terminalGetins;

    #[ORM\OneToMany(mappedBy: 'shippingCompany', targetEntity: Booking::class,cascade: ['persist','detach'])]
    private Collection $bookings;

    #[ORM\OneToMany(mappedBy: 'ShippingCompany', targetEntity: ContainerType::class)]
    private Collection $containerTypes;

    public function __construct()
    {
        $this->terminalGetins = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->containerTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, TerminalGetin>
     */
    public function getTerminalGetins(): Collection
    {
        return $this->terminalGetins;
    }

    public function addTerminalGetin(TerminalGetin $terminalGetin): static
    {
        if (!$this->terminalGetins->contains($terminalGetin)) {
            $this->terminalGetins->add($terminalGetin);
            $terminalGetin->setShippingCompany($this);
        }

        return $this;
    }

    public function removeTerminalGetin(TerminalGetin $terminalGetin): static
    {
        if ($this->terminalGetins->removeElement($terminalGetin)) {
            // set the owning side to null (unless already changed)
            if ($terminalGetin->getShippingCompany() === $this) {
                $terminalGetin->setShippingCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): static
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings->add($booking);
            $booking->setShippingCompany($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): static
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getShippingCompany() === $this) {
                $booking->setShippingCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ContainerType>
     */
    public function getContainerTypes(): Collection
    {
        return $this->containerTypes;
    }

    public function addContainerType(ContainerType $containerType): static
    {
        if (!$this->containerTypes->contains($containerType)) {
            $this->containerTypes->add($containerType);
            $containerType->setShippingCompany($this);
        }

        return $this;
    }

    public function removeContainerType(ContainerType $containerType): static
    {
        if ($this->containerTypes->removeElement($containerType)) {
            // set the owning side to null (unless already changed)
            if ($containerType->getShippingCompany() === $this) {
                $containerType->setShippingCompany(null);
            }
        }

        return $this;
    }
}
