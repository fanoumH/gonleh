<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Trait\HistoryTrait;
use App\Repository\CustomerBankRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: CustomerBankRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
//#[ApiResource]
class CustomerBank
{
    use HistoryTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read',
        'folder:read',
        'customerContact:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $bank = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $accountNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $keyAccount = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $ticketOffice = null;

    #[ORM\ManyToOne(inversedBy: 'customerBanks')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?Currency $currency = null;

    #[ORM\OneToOne(mappedBy: 'customerBank', cascade: ['persist', 'remove'])]
    private ?Customer $customer = null;

    #[ORM\ManyToOne(inversedBy: 'customerBanks')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
    ])]
    private ?Bank $titledBank = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBank(): ?string
    {
        return $this->bank;
    }

    public function setBank(?string $bank): static
    {
        $this->bank = $bank;

        return $this;
    }

    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    public function setAccountNumber(?string $accountNumber): static
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    public function getKeyAccount(): ?string
    {
        return $this->keyAccount;
    }

    public function setKeyAccount(?string $keyAccount): static
    {
        $this->keyAccount = $keyAccount;

        return $this;
    }

    public function getTicketOffice(): ?string
    {
        return $this->ticketOffice;
    }

    public function setTicketOffice(?string $ticketOffice): static
    {
        $this->ticketOffice = $ticketOffice;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        // unset the owning side of the relation if necessary
        if ($customer === null && $this->customer !== null) {
            $this->customer->setCustomerBank(null);
        }

        // set the owning side of the relation if necessary
        if ($customer !== null && $customer->getCustomerBank() !== $this) {
            $customer->setCustomerBank($this);
        }

        $this->customer = $customer;

        return $this;
    }

    public function getTitledBank(): ?Bank
    {
        return $this->titledBank;
    }

    public function setTitledBank(?Bank $titledBank): static
    {
        $this->titledBank = $titledBank;

        return $this;
    }
}
