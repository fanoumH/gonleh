<?php

namespace App\Entity;

use App\Controller\PostsToSheetController;
use App\Entity\Staff;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\StaffPostRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\PostsToPdfController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: StaffPostRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des postes du personnel",
            ],
            normalizationContext: ['groups' => 'staff-post:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/staff_posts/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'staff-post:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: PostsToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/staff_posts/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'staff-post:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: PostsToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un poste du personnel",
            ],
            normalizationContext: ['groups' => 'staff-post:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un poste du personnel",
            ],
            normalizationContext: ['groups' => 'staff-post:read'],
            denormalizationContext: ['groups' => 'staff-post:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un poste du personnel",
            ],
            normalizationContext: ['groups' => 'staff-post:read'],
            denormalizationContext: ['groups' => 'staff-post:update'],
            // security: "is_granted('PERMISSION_ALLOWED' , object) and is_granted('STAFF_POST_EDIT' , object)"
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un poste du personnel",
            ],
            // security: "is_granted('PERMISSION_ALLOWED' , object) and is_granted('STAFF_POST_DELETE' , object)"
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "id" => "exact",
    "code" => "exact",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"name", "code"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
#[UniqueEntity(['name'])]
class StaffPost
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    public const DRIVER_ID = 1;
    public const DEFAULT_REQUIRED_ROLE = [self::DRIVER_ID];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'staff-post:read',
        'staff:read',
        'rentalOrder:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'staff-post:read', 'staff-post:create', 'staff-post:update',
        'staff:read',
        'rentalOrder:read',
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'staffPost', targetEntity: Staff::class)]
    #[Groups(groups: [
        'staff-post:read'
    ])]
    private Collection $staffs;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'staff-post:read', 'staff-post:create', 'staff-post:update',
        'staff:read',
        'rentalOrder:read',
    ])]
    private ?string $code = null;

    // #[ORM\Column(nullable: true, options: ["default" => 0])]
    // #[Groups(groups: [
    //     'staff-post:read', 'staff-post:create', 'staff-post:update',
    //     'staff:read', 'staff:create', 'staff:update'
    // ])]
    // private ?bool $canDrive = null;

    public function __construct()
    {
        $this->staffs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Staff>
     */
    public function getStaffs(): Collection
    {
        return $this->staffs;
    }

    public function addStaff(Staff $staff): static
    {
        if (!$this->staffs->contains($staff)) {
            $this->staffs->add($staff);
            $staff->setStaffPost($this);
        }

        return $this;
    }

    public function removeStaff(Staff $staff): static
    {
        if ($this->staffs->removeElement($staff)) {
            // set the owning side to null (unless already changed)
            if ($staff->getStaffPost() === $this) {
                $staff->setStaffPost(null);
            }
        }

        return $this;
    }

    // public function isCanDrive(): ?bool
    // {
    //     return $this->canDrive;
    // }

    // public function setCanDrive(?bool $canDrive): static
    // {
    //     $this->canDrive = $canDrive;

    //     return $this;
    // }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }


}
