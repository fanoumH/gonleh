<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\CustomerContactRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: CustomerContactRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => 'customerContact:read'],denormalizationContext: ['groups' => 'customerContact:write'],
)]
#[ApiResource(
    uriTemplate: '{customerContactId}/customerContact/{id}',
    operations: [ new Patch(
        openapiContext: [
            "summary" => "Modifier contact client",
        ],
        normalizationContext: ['groups' => 'customerContact:read'],
        denormalizationContext: ['groups' => 'customerContact:write'],
        security: "is_granted('PERMISSION_ALLOWED' , object)",
    ) ],
    uriVariables: [
        'customerContactId' => new Link(toProperty: 'customer', fromClass: Customer::class),
        'id' => new Link(fromClass: CustomerContact::class)
    ]
)]
#[ApiResource(
    uriTemplate: '{customerContactId}/customerContact',
    operations: [ new Post(
        openapiContext: [
            "summary" => "Ajout contact client",
        ],
        normalizationContext: ['groups' => 'customerContact:read'],
        denormalizationContext: ['groups' => 'customerContact:write'],
        security: "is_granted('PERMISSION_ALLOWED' , object)",
    ) ],
    uriVariables: [
        'customerContactId' => new Link(toProperty: 'customer', fromClass: Customer::class),
    ]
)]
#[ApiResource(
    uriTemplate: '{customerContactId}/customerContact/{id}',
    operations: [ new Delete(
        openapiContext: [
            "summary" => "Supprimer contact client",
        ],
        normalizationContext: ['groups' => 'customerContact:read'],
        denormalizationContext: ['groups' => 'customerContact:write'],
        security: "is_granted('PERMISSION_ALLOWED' , object)",
    ) ],
    uriVariables: [
        'customerContactId' => new Link(toProperty: 'customer', fromClass: Customer::class),
        'id' => new Link(fromClass: CustomerContact::class)
    ]
)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des contacts",
            ],
            normalizationContext: ['groups' => 'customerContact:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un contact",
            ],
            normalizationContext: ['groups' => 'customerContact:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un contact",
            ],
            normalizationContext: ['groups' => 'customerContact:read'],
            denormalizationContext: ['groups' => 'customerContact:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un contact",
            ],
            normalizationContext: ['groups' => 'customerContact:read'],
            denormalizationContext: ['groups' => 'customerContact:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un contact",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['user:update']]
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[UniqueEntity(fields:['email'], message: 'bo.value.already.exist', ignoreNull: false)]
class CustomerContact
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read','customerContact:write','customerContact:create','customerContact:update',
        'folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read','customerContact:write','customerContact:create','customerContact:update',
        'folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'customerContacts')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read','customerContact:write','customerContact:create','customerContact:update',
        'folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?Quality $qualite = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read','customerContact:write','customerContact:create','customerContact:update',
        'folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?string $phone = null;

    #[ORM\ManyToOne(inversedBy: 'customerContact')]
    #[Groups(groups: [
        'customerContact:read','customerContact:write','customerContact:create','customerContact:update',
    ])]
    private ?Customer $customer = null;

    #[ORM\ManyToOne(inversedBy: 'clientContact')]
//    #[ORM\JoinColumn(onDelete: "SET NULL")]
    private ?Transaction $transaction = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read','customerContact:write','customerContact:create','customerContact:update',
        'folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?string $firstname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read','customerContact:write','customerContact:create','customerContact:update',
        'folder:read', 'folder:create', 'folder:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?string $email = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getQualite(): ?Quality
    {
        return $this->qualite;
    }

    public function setQualite(?Quality $qualite): static
    {
        $this->qualite = $qualite;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(?Transaction $transaction): static
    {
        $this->transaction = $transaction;

        return $this;
    }

    public function setId(?int $id){
        $this->id = $id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }
}
