<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use App\Repository\StatusRepository;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: StatusRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des statuts",
            ],
            normalizationContext: ['groups' => 'status:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un statut",
            ],
            normalizationContext: ['groups' => 'status:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un statut",
            ],
            normalizationContext: ['groups' => 'status:read'],
            denormalizationContext: ['groups' => 'status:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un statut",
            ],
            normalizationContext: ['groups' => 'status:read'],
            denormalizationContext: ['groups' => 'status:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un statut",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[UniqueEntity(fields: ['label'])]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
class Status
{
    /**
    * Hook timestampable behavior
    * updates createdAt, updatedAt fields.
    */
   use TimestampableEntity;

   /**
    * Hook SoftDeleteable behavior
    * updates deletedAt field.
    */
   use SoftDeleteableEntity;

   /**
    * history trait
    */
   use HistoryTrait;
   
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'status:read', 'transaction:read', 'folder:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'status:read', 'status:update', 'status:create', 'transaction:read', 'folder:read',
    ])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'status', targetEntity: Transaction::class,cascade: ['persist','detach'])]
    private Collection $transactions;

    #[ORM\OneToMany(mappedBy: 'status', targetEntity: Folder::class)]
    private Collection $folders;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
        $this->folders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setStatus($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getStatus() === $this) {
                $transaction->setStatus(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Folder>
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }

    public function addFolder(Folder $folder): static
    {
        if (!$this->folders->contains($folder)) {
            $this->folders->add($folder);
            $folder->setStatus($this);
        }

        return $this;
    }

    public function removeFolder(Folder $folder): static
    {
        if ($this->folders->removeElement($folder)) {
            // set the owning side to null (unless already changed)
            if ($folder->getStatus() === $this) {
                $folder->setStatus(null);
            }
        }

        return $this;
    }
}
