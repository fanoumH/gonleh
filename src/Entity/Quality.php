<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\QualityRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\QualityToPdfController;
use App\Controller\QualityToSheetController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
#[ORM\Entity(repositoryClass: QualityRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des quality",
            ],
            normalizationContext: ['groups' => 'quality:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/qualities/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'quality:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: QualityToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/qualities/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'quality:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: QualityToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un quality",
            ],
            normalizationContext: ['groups' => 'quality:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un quality",
            ],
            normalizationContext: ['groups' => 'quality:read'],
            denormalizationContext: ['groups' => 'quality:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un quality",
            ],
            normalizationContext: ['groups' => 'quality:read'],
            denormalizationContext: ['groups' => 'quality:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un quality",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
)]

#[ApiFilter(
    SearchFilter::class, properties: [
    "label" => 'ipartial',
    "id" => 'exact',
    ]
)]
class Quality
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customerContact:read','customerContact:write','quality:read','quality:create','quality:update','customerContact:create','customerContact:update',
        'customerContact:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','customerContact:read','customerContact:write','customerContact:create','customerContact:update',
        'quality:read','quality:create','quality:update',
        'customerContact:read'
    ])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'quality', targetEntity: CustomerIdentification::class)]
    private Collection $customerIdentifications;

    #[ORM\OneToMany(mappedBy: 'qualite', targetEntity: CustomerContact::class)]
    private Collection $customerContacts;

    public function __construct()
    {
        $this->customerIdentifications = new ArrayCollection();
        $this->customerContacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, CustomerIdentification>
     */
    public function getCustomerIdentifications(): Collection
    {
        return $this->customerIdentifications;
    }

    public function addCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if (!$this->customerIdentifications->contains($customerIdentification)) {
            $this->customerIdentifications->add($customerIdentification);
            $customerIdentification->setQuality($this);
        }

        return $this;
    }

    public function removeCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if ($this->customerIdentifications->removeElement($customerIdentification)) {
            // set the owning side to null (unless already changed)
            if ($customerIdentification->getQuality() === $this) {
                $customerIdentification->setQuality(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CustomerContact>
     */
    public function getCustomerContacts(): Collection
    {
        return $this->customerContacts;
    }

    public function addCustomerContact(CustomerContact $customerContact): static
    {
        if (!$this->customerContacts->contains($customerContact)) {
            $this->customerContacts->add($customerContact);
            $customerContact->setQualite($this);
        }

        return $this;
    }

    public function removeCustomerContact(CustomerContact $customerContact): static
    {
        if ($this->customerContacts->removeElement($customerContact)) {
            // set the owning side to null (unless already changed)
            if ($customerContact->getQualite() === $this) {
                $customerContact->setQualite(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return '';
        // TODO: Implement __toString() method.
    }
}
