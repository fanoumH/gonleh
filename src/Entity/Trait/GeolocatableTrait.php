<?php

namespace App\Entity\Trait;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use LongitudeOne\Spatial\Exception\InvalidValueException;
use LongitudeOne\Spatial\PHP\Types\Geometry\Point;
use Symfony\Component\Serializer\Annotation\Groups;

trait GeolocatableTrait
{
    #[ORM\Column(type: 'point', nullable: true)]
    #[Groups(groups: [
        'place:create', 'place:update'
    ])]
    private ?Point $coordinates;

    public function getCoordinates(): ?Point
    {
        return $this->coordinates;
    }

    /**
     * @param array|null $coordinates
     * @return $this
     * @throws InvalidValueException
     */
    public function setCoordinates(?array $coordinates): static
    {
        try {
            $this->coordinates = $coordinates ? new Point($coordinates['longitude'], $coordinates['latitude']) : null;
        } catch (\Throwable $e) {
            throw new InvalidValueException('Invalid coordinates: ' . $e->getMessage());
        }
        return $this;
    }


    #[Pure] #[Groups(groups: [
        'place:read'
    ])]
    public function getLongitude(): float|int
    {
        $x=0;
        $point = $this->coordinates;
        if($point instanceof Point) {
            $x = $point->getX();
        }
        return $x;
    }

    #[Pure] #[Groups(groups: [
        'place:read'
    ])]
    public function getLatitude(): float|int
    {
        $y=0;
        $point = $this->coordinates;
        if($point instanceof Point) {
            $y = $point->getY();
        }
        return $y;
    }
}
