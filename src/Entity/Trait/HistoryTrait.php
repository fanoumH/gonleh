<?php

namespace App\Entity\Trait;

trait HistoryTrait
{
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
