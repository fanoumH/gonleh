<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Controller\UploadCinDocumentController;
use App\Controller\UploadNifStatDocumentController;
use App\Entity\Trait\HistoryTrait;
use App\Repository\CustomerIdentificationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: CustomerIdentificationRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
//#[ApiResource]
#[Vich\Uploadable]
#[ApiResource(
    types: ['https://schema.org/users'],
    operations: [
        new Post(
            uriTemplate: "/attachmentCustomer/{id}/nifstat",
            inputFormats: ['multipart' => ['multipart/form-data']],
            controller: UploadNifStatDocumentController::class,
            openapiContext: [
                "summary" => "Uploader une pièce jointe",
            ],
            deserialize: false
        )
    ],
    normalizationContext: ['groups' => ['transaction:read']],
    denormalizationContext: ['groups' => ['transaction:create','transaction:update']],
)]
#[ApiResource(
    types: ['https://schema.org/users'],
    operations: [
        new Post(
            uriTemplate: "/attachmentCustomer/{id}/cin",
            inputFormats: ['multipart' => ['multipart/form-data']],
            controller: UploadCinDocumentController::class,
            openapiContext: [
                "summary" => "Uploader une pièce jointe",
            ],
            deserialize: false
        )
    ],
    normalizationContext: ['groups' => ['transaction:read']],
    denormalizationContext: ['groups' => ['transaction:create','transaction:update']],
)]
class CustomerIdentification
{
    use HistoryTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update', 'booking:read',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update', 'booking:read', 'folder:read',
        'transaction:read','transaction:create','transaction:update',
        'customerContact:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'rentalOrder:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $thirdParties = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $customerGroupName = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $district = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $rcs = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $cif = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $stat = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true, options: ['default' => false])]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?bool $prospect = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'booking:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'rentalOrder:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $customerTitle = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $abbreviated = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
    ])]
    private ?Quality $quality = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $comments = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $complement = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
    ])]
    private ?Country $country = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $city = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $zipCode = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $nif = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $linkedin = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read','customerContact:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $siteInternet = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?Region $region = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?Province $province = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?TaxeRate $taxeRate = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?CustomerQuality $customerQuality = null;

    #[ORM\OneToOne(mappedBy: 'customerIdentification', cascade: ['persist', 'remove'])]
    private ?Customer $customer = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?CustomerType $customerType = null;

    #[ORM\ManyToOne(inversedBy: 'customerIdentifications')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?User $interlocutor = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'customerContact:read'
    ])]
    private ?string $civility = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'customerContact:read'
    ])]
    private ?string $cin = null;

    #[Vich\UploadableField(mapping: "attachment_nif", fileNameProperty: "nif_attachment")]
    public ?File $nifFile = null;

    #[ORM\Column(nullable: true)]
    public ?string $nif_attachment = null;

    #[Vich\UploadableField(mapping: "attachment_cin", fileNameProperty: "cin_attachment")]
    public ?File $cinFile = null;

    #[ORM\Column(nullable: true)]
    public ?string $cin_attachment = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThirdParties(): ?string
    {
        return $this->thirdParties;
    }

    public function setThirdParties(?string $thirdParties): static
    {
        $this->thirdParties = $thirdParties;

        return $this;
    }

    public function getCustomerGroupName(): ?string
    {
        return $this->customerGroupName;
    }

    public function setCustomerGroupName(?string $customerGroupName): static
    {
        $this->customerGroupName = $customerGroupName;

        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(?string $district): static
    {
        $this->district = $district;

        return $this;
    }

    public function getRcs(): ?string
    {
        return $this->rcs;
    }

    public function setRcs(?string $rcs): static
    {
        $this->rcs = $rcs;

        return $this;
    }

    public function getCif(): ?string
    {
        return $this->cif;
    }

    public function setCif(?string $cif): static
    {
        $this->cif = $cif;

        return $this;
    }

    public function getStat(): ?string
    {
        return $this->stat;
    }

    public function setStat(?string $stat): static
    {
        $this->stat = $stat;

        return $this;
    }

    public function isProspect(): ?bool
    {
        return $this->prospect;
    }

    public function setProspect(?bool $prospect): static
    {
        $this->prospect = $prospect;

        return $this;
    }

    public function getCustomerTitle(): ?string
    {
        return $this->customerTitle;
    }

    public function setCustomerTitle(?string $customerTitle): static
    {
        $this->customerTitle = $customerTitle;

        return $this;
    }

    public function getAbbreviated(): ?string
    {
        return $this->abbreviated;
    }

    public function setAbbreviated(?string $abbreviated): static
    {
        $this->abbreviated = $abbreviated;

        return $this;
    }

    public function getQuality(): ?Quality
    {
        return $this->quality;
    }

    public function setQuality(?Quality $quality): static
    {
        $this->quality = $quality;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): static
    {
        $this->comments = $comments;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): static
    {
        $this->address = $address;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): static
    {
        $this->complement = $complement;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): static
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): static
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): static
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getNif(): ?string
    {
        return $this->nif;
    }

    public function setNif(?string $nif): static
    {
        $this->nif = $nif;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): static
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getSiteInternet(): ?string
    {
        return $this->siteInternet;
    }

    public function setSiteInternet(?string $siteInternet): static
    {
        $this->siteInternet = $siteInternet;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): static
    {
        $this->region = $region;

        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): static
    {
        $this->province = $province;

        return $this;
    }

    public function getTaxeRate(): ?TaxeRate
    {
        return $this->taxeRate;
    }

    public function setTaxeRate(?TaxeRate $taxeRate): static
    {
        $this->taxeRate = $taxeRate;

        return $this;
    }

    public function getCustomerQuality(): ?CustomerQuality
    {
        return $this->customerQuality;
    }

    public function setCustomerQuality(?CustomerQuality $customerQuality): static
    {
        $this->customerQuality = $customerQuality;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        // unset the owning side of the relation if necessary
        if ($customer === null && $this->customer !== null) {
            $this->customer->setCustomerIdentification(null);
        }

        // set the owning side of the relation if necessary
        if ($customer !== null && $customer->getCustomerIdentification() !== $this) {
            $customer->setCustomerIdentification($this);
        }

        $this->customer = $customer;

        return $this;
    }

    public function getCustomerType(): ?CustomerType
    {
        return $this->customerType;
    }

    public function setCustomerType(?CustomerType $customerType): static
    {
        $this->customerType = $customerType;

        return $this;
    }

    public function getInterlocutor(): ?User
    {
        return $this->interlocutor;
    }

    public function setInterlocutor(?User $interlocutor): static
    {
        $this->interlocutor = $interlocutor;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(?string $civility): static
    {
        $this->civility = $civility;

        return $this;
    }

    public function getCin(): ?string
    {
        return $this->cin;
    }

    public function setCin(?string $cin): static
    {
        $this->cin = $cin;

        return $this;
    }

    public function getNifAttachment(): ?string
    {
        return $this->nif_attachment;
    }

    public function setNifAttachment(?string $nif_attachment): CustomerIdentification
    {
        $this->nif_attachment = $nif_attachment;
        return $this;
    }

    public function getNifFile(): ?File
    {
        return $this->nifFile;
    }

    public function setNifFile(?File $nifFile): CustomerIdentification
    {
        $this->nifFile = $nifFile;
        return $this;
    }

    public function getCinFile(): ?File
    {
        return $this->cinFile;
    }

    public function setCinFile(?File $cinFile): CustomerIdentification
    {
        $this->cinFile = $cinFile;
        return $this;
    }

    public function getCinAttachment(): ?string
    {
        return $this->cin_attachment;
    }

    public function setCinAttachment(?string $cin_attachment): CustomerIdentification
    {
        $this->cin_attachment = $cin_attachment;
        return $this;
    }
}
