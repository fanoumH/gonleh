<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\FareCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: FareCategoryRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: ['summary' => 'Liste catégories tarifaires'],
            normalizationContext: ['groups' => 'fareCategory:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: ['summary' => "Détail catégorie tarifaire"],
            normalizationContext: ['fareCategory:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout catégorie tarifaire",
            ],
            normalizationContext: ['fareCategory:read'],
            denormalizationContext: ['fareCategory:write'],
        // security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: ['summary' => "Modification catégorie tarifaire"],
            normalizationContext: ['fareCategory:read'],
            denormalizationContext: ['fareCategory:write'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: ['summary' => "Suppression catégorie tarifaire"],
        ),
    ],
)]
class FareCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'fareCategory:read','fareCategory:write','customer:read','customer:create','customer:update',
        'customer:read','customer:create','customer:update',
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'fareCategory:read','fareCategory:write','customer:read','customer:create','customer:update',
        'customer:read','customer:create','customer:update',
        'basicRate:read',
        'basicRate:create',
        'basicRate:update',
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'fareCategory', targetEntity: CustomerPrice::class)]
    private Collection $customerPrices;

    public function __construct()
    {
        $this->customerPrices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, CustomerPrice>
     */
    public function getCustomerPrices(): Collection
    {
        return $this->customerPrices;
    }

    public function addCustomerPrice(CustomerPrice $customerPrice): static
    {
        if (!$this->customerPrices->contains($customerPrice)) {
            $this->customerPrices->add($customerPrice);
            $customerPrice->setFareCategory($this);
        }

        return $this;
    }

    public function removeCustomerPrice(CustomerPrice $customerPrice): static
    {
        if ($this->customerPrices->removeElement($customerPrice)) {
            // set the owning side to null (unless already changed)
            if ($customerPrice->getFareCategory() === $this) {
                $customerPrice->setFareCategory(null);
            }
        }

        return $this;
    }
}
