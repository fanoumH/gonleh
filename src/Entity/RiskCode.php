<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\RiskCodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
#[ORM\Entity(repositoryClass: RiskCodeRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des risckode",
            ],
            normalizationContext: ['groups' => 'risckode:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un risckode",
            ],
            normalizationContext: ['groups' => 'risckode:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un risckode",
            ],
            normalizationContext: ['groups' => 'risckode:read'],
            denormalizationContext: ['groups' => 'risckode:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un risckode",
            ],
            normalizationContext: ['groups' => 'risckode:read'],
            denormalizationContext: ['groups' => 'risckode:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un risckode",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
)]
class RiskCode
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','risckode:read','risckode:create','risckode:update','customerContact:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','risckode:read','risckode:create','risckode:update','customerContact:read'
    ])]
    private ?string $label = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','risckode:read','risckode:create','risckode:update','customerContact:read'
    ])]
    private ?float $riskCodeRate = null;

    #[ORM\OneToMany(mappedBy: 'riskCode', targetEntity: Solvency::class)]
    private Collection $solvencies;

    public function __construct()
    {
        $this->solvencies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function getRiskCodeRate(): ?float
    {
        return $this->riskCodeRate;
    }

    public function setRiskCodeRate(?float $riskCodeRate): static
    {
        $this->riskCodeRate = $riskCodeRate;

        return $this;
    }

    /**
     * @return Collection<int, Solvency>
     */
    public function getSolvencies(): Collection
    {
        return $this->solvencies;
    }

    public function addSolvency(Solvency $solvency): static
    {
        if (!$this->solvencies->contains($solvency)) {
            $this->solvencies->add($solvency);
            $solvency->setRiskCode($this);
        }

        return $this;
    }

    public function removeSolvency(Solvency $solvency): static
    {
        if ($this->solvencies->removeElement($solvency)) {
            // set the owning side to null (unless already changed)
            if ($solvency->getRiskCode() === $this) {
                $solvency->setRiskCode(null);
            }
        }

        return $this;
    }
}
