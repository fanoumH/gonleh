<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\ModelsToPdfController;
use App\Repository\VehicleModelRepository;
use App\Controller\ModelsToSheetController;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: VehicleModelRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des modèles de vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-model:read']
        ),
        new GetCollection(
            uriTemplate: '/vehicle_models/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'vehicle-model:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: ModelsToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/vehicle_models/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'vehicle-model:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: ModelsToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un modèle de vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-model:read'],
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un modèle de vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-model:read'],
            denormalizationContext: ['groups' => 'vehicle-model:create']
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un modèle vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-model:read'],
            denormalizationContext: ['groups' => 'vehicle-model:update']
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un modèle de vehicule",
            ]
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "vehicleBrand.id" => "exact",
    "id" => "exact"
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"name","vehicleBrand.name","vehicleBrand.id"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
#[UniqueEntity(['name','vehicleBrand'])]
class VehicleModel
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'vehicle-model:read',
        'vehicle:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'vehicle-model:read', 'vehicle-model:create', 'vehicle-model:update',
        'vehicle:read',
        'coupling:read',
    ])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'vehicleModels')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'vehicle-model:read', 'vehicle-model:create', 'vehicle-model:update'
    ])]
    private ?VehicleBrand $vehicleBrand = null;

    #[ORM\OneToMany(mappedBy: 'vehicleModel', targetEntity: Vehicle::class,cascade: ['persist','detach'])]
    private Collection $vehicles;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getVehicleBrand(): ?VehicleBrand
    {
        return $this->vehicleBrand;
    }

    public function setVehicleBrand(?VehicleBrand $vehicleBrand): static
    {
        $this->vehicleBrand = $vehicleBrand;

        return $this;
    }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setVehicleModel($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getVehicleModel() === $this) {
                $vehicle->setVehicleModel(null);
            }
        }

        return $this;
    }
}
