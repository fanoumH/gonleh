<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\ZoneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: ZoneRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des zone",
            ],
            normalizationContext: ['groups' => 'zone:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un zone",
            ],
            normalizationContext: ['groups' => 'zone:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un zone",
            ],
            normalizationContext: ['groups' => 'zone:read'],
            denormalizationContext: ['groups' => 'zone:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un zone",
            ],
            normalizationContext: ['groups' => 'zone:read'],
            denormalizationContext: ['groups' => 'zone:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un quality",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
)]
class Zone
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','zone:read','zone:create','zone:update'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','zone:read','zone:create','zone:update'
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'localization', targetEntity: CustomerFreeField::class)]
    private Collection $customerFreeFields;

    public function __construct()
    {
        $this->customerFreeFields = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, CustomerFreeField>
     */
    public function getCustomerFreeFields(): Collection
    {
        return $this->customerFreeFields;
    }

    public function addCustomerFreeField(CustomerFreeField $customerFreeField): static
    {
        if (!$this->customerFreeFields->contains($customerFreeField)) {
            $this->customerFreeFields->add($customerFreeField);
            $customerFreeField->setLocalization($this);
        }

        return $this;
    }

    public function removeCustomerFreeField(CustomerFreeField $customerFreeField): static
    {
        if ($this->customerFreeFields->removeElement($customerFreeField)) {
            // set the owning side to null (unless already changed)
            if ($customerFreeField->getLocalization() === $this) {
                $customerFreeField->setLocalization(null);
            }
        }

        return $this;
    }
}
