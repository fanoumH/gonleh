<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use App\Repository\UserRepository;
use ApiPlatform\Metadata\ApiFilter;
use App\Controller\LogoutController;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\UsersToPdfController;
use App\Validator\Constraints\Ambiguous;
use App\Controller\UsersToSheetController;
use App\Controller\PostUserPhotoController;
use Doctrine\Common\Collections\Collection;
use App\ApiPlatform\Filter\SearchUserFilter;
use App\Controller\GetProfileUserController;
use Symfony\Component\HttpFoundation\File\File;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`users`')]
#[Vich\Uploadable]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des utilisateurs",
            ],
            normalizationContext: ['groups' => 'user:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/users/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'user:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: UsersToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/users/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'user:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: UsersToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un utilisateur",
            ],
            normalizationContext: ['groups' => 'user:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un utilisateur",
            ],
            normalizationContext: ['groups' => 'user:read'],
            denormalizationContext: ['groups' => 'user:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un utilisateur",
            ],
            normalizationContext: ['groups' => 'user:read'],
            denormalizationContext: ['groups' => 'user:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un utilisateur",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: "/profile/users",
            defaults: [
                '_api_persist' => false,
            ],
            controller: GetProfileUserController::class,
            openapiContext: [
                "summary" => "Profil d'utilisateurs",
            ],
            normalizationContext: ['groups' => 'user:read'],
            read: false,
        ),
        new Get(
            uriTemplate: "/api/logout",
            defaults: [
                '_api_persist' => false,
            ],
            controller: LogoutController::class,
            openapiContext: [
                "summary" => "Déconnexion",
                "description" => "Permet la déconnexion de l'utilisateur",
                "responses" =>  [
                    "200" => [
                        "description" => "Génère un token pour la déconnexion",
                        "content" => [
                            "application/json" => [
                                "schema" => [
                                    "type" => "string",
                                    "required" => [
                                        "token"
                                    ],
                                    "properties" => [
                                        "token" => [
                                            "type" => "string"
                                        ]
                                    ],
                                    "example" => [
                                        "token" => "eyc.lkdfjfkdjkldjkddjz",
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            normalizationContext: ['groups' => 'user:read'],
            read: false,
        ),
    ],
    validationContext: ['groups' => ['user:update']]
)]
#[ApiResource(
    types: ['https://schema.org/users'],
    operations: [
        new Post(
            uriTemplate: "/photoUser/{id}/users",
            inputFormats: ['multipart' => ['multipart/form-data']],
            controller: PostUserPhotoController::class,
            openapiContext: [
                "summary" => "Publier photo de profil",
                "requestBody" => [
                    "description" => "Permet de publier un photo de profil",
                    "content" => [
                        "application/octet-stream" => [
                            "schema" => [
                                "properties" => [
                                    "file" => [
                                        "type" => "file",
                                        "properties" => [
                                        ]
                                    ]
                                ],
                            ]
                        ]
                    ],
                    "required" =>  true
                ],
                "responses" =>  [
                    "200" => [
                        "description" => "Retourne un lien vers l'image",
                        "content" => [
                            "application/json" => [
                                "schema" => [
                                    "type" => "string",
                                    "required" => [
                                        "imageProfile"
                                    ],
                                    "properties" => [
                                        "imageProfile" => [
                                            "type" => "string"
                                        ]
                                    ],
                                    "example" => [
                                        "imageProfile" => "/upload/users/screenshot-20240214-140746-65d59e4470298654757532.png",
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "500"=> [
                        "description" => "Génère une erreur",
                        "content" => [
                            "application/json" => [
                                "schema" => [
                                    "type" => "object",
                                    "required" => [
                                        "status"
                                    ],
                                    "properties" => [
                                        "status" => [
                                            "type" => "object"
                                        ]
                                    ],
                                    "example" => [
                                        "status" => [
                                            "code" => 500,
                                            "success" => false,
                                            "message" => "Une erreur s'est produite",
                                            "messageDetail" => "Message Detail",
                                            "errorCode" => "codeError",
                                            "detail" => "Error File Detail"
                                        ],
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            deserialize: false
        )
    ],
    normalizationContext: ['groups' => ['user:read', 'read']],
    denormalizationContext: ['groups' => ['user:create','user:update', 'write']],
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "email" => "ipartial",
    "name" => "ipartial",
    "firstname" => "ipartial",
    "capabilities.name" => "partial",
    "capabilities.id" => "exact",
    "id" => "exact",
]
)]
#[ApiFilter(BooleanFilter::class,properties: ['is_active', 'isInterlocutor'])]
#[ApiFilter(
    SearchUserFilter::class,properties: ['user']
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "email" ,"name","firstname","capabilities.name","capabilities.id","id"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;
    /**
     * history trait
     */
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'user:read',
        'folder:read',
        'transaction:read',
        'customer:read',
        'bank:read',
        'customerContact:read',
        'eir:read',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(groups: [
        'user:read','user:create','user:update',
        'transaction:read','transaction:create','transaction:update',
        'customer:read','customer:create','customer:update',
        'eir:read',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $email = null;

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Groups(groups: [
        'user:create','user:update',
    ])]
    private ?string $password = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'user:read','user:create','user:update', 'folder:read',
        'transaction:read','transaction:create','transaction:update',
        'customer:read','customer:create','customer:update', 'bank:read',
        'eir:read',
        'proforma:read','proforma:create','proforma:update'
    ])]
//    #[Assert\NotBlank(groups: ['user:update'],message: "Cette valeur ne doit pas être null")]
    private ?string $firstname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'user:read','user:create','user:update', 'folder:read',
        'transaction:read','transaction:create','transaction:update',
        'customer:read','customer:create','customer:update', 'bank:read',
        'eir:read',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $lastname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'user:read','user:create','user:update',
        'transaction:read','transaction:create','transaction:update',
        'customer:read','customer:create','customer:update', 'bank:read',
        'eir:read','proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $name = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true, options: ['default' => false])]
    #[Groups(groups: [
        'user:read','user:create','user:update',
        'transaction:read','transaction:create','transaction:update',
        'customer:read','customer:create','customer:update', 'bank:read',
    ])]
    #[ApiProperty(
        writable: true,
    )]
    private ?bool $is_active = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'user:read','user:create','user:update',
        'transaction:read','transaction:create','transaction:update',
        'customer:read','customer:create','customer:update',
    ])]
    #[ApiProperty(
        writable: true,
    )]
//    #[Ambiguous(groups: ['user:update'])]
    private ?array $times_range = [];

    #[ORM\ManyToOne(cascade: ['persist'] ,inversedBy: 'users')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'user:read','user:create','user:update',
        'transaction:read','transaction:create','transaction:update',
        'customer:read','customer:create','customer:update',
    ])]
    private ?Role $capabilities = null;

    #[Vich\UploadableField(mapping: "user_photo_profile", fileNameProperty: "filePath")]
    public ?File $file = null;

    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;

    #[ORM\Column(options: ["default" => false])]
    #[Groups(groups: [
        'user:read','user:create','user:update', 'folder:read',
        'transaction:read','transaction:create','transaction:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?bool $isInterlocutor = null;

    #[ORM\OneToMany(mappedBy: 'responsibleBusiness', targetEntity: Transaction::class,cascade: ['persist','detach'])]
    private Collection $transactions;

    #[ORM\OneToMany(mappedBy: 'interlocutor', targetEntity: Bank::class,cascade: ['persist','persist'])]
    private Collection $banks;

    #[ORM\OneToMany(mappedBy: 'projectHolder', targetEntity: Folder::class,cascade: ['persist','detach'])]
    private Collection $folders;

    #[ORM\OneToMany(mappedBy: 'interlocutor', targetEntity: CustomerIdentification::class,cascade: ['persist','detach'])]
    private Collection $customerIdentifications;

    #[ORM\ManyToOne(inversedBy: 'users')]
    #[Groups(groups: [
        'user:read','user:create','user:update',
    ])]
    private ?Terminal $terminal = null;

    #[ORM\OneToMany(mappedBy: 'creatorUser', targetEntity: EIR::class)]
    private Collection $eIRs;

    #[ORM\OneToMany(mappedBy: 'commercial', targetEntity: Proforma::class)]
    private Collection $proformas;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
        $this->banks = new ArrayCollection();
        $this->folders = new ArrayCollection();
        $this->customerIdentifications = new ArrayCollection();
        $this->eIRs = new ArrayCollection();
        $this->proformas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $this->capabilities->toArray();
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): static
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getTimesRange(): ?array
    {
        return $this->times_range;
    }

    public function setTimesRange(?array $times_range): static
    {
        $this->times_range = $times_range;

        return $this;
    }

    public function getCapabilities(): ?Role
    {
        return $this->capabilities;
    }

    public function setCapabilities(?Role $capabilities): static
    {
        $this->capabilities = $capabilities;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     * @return User
     */
    public function setFile(?File $file): User
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param string|null $filePath
     * @return User
     */
    public function setFilePath(?string $filePath): User
    {
        $this->filePath = $filePath;
        return $this;
    }

    public function isIsInterlocutor(): ?bool
    {
        return $this->isInterlocutor;
    }

    public function setIsInterlocutor(bool $isInterlocutor): static
    {
        $this->isInterlocutor = $isInterlocutor;

        return $this;
    }

    /**
     * @return Collection<int, Folder>
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }

    public function addFolder(Folder $folder): static
    {
        if (!$this->folders->contains($folder)) {
            $this->folders->add($folder);
            $folder->setProjectHolder($this);
        }

        return $this;
    }

    public function removeFolder(Folder $folder): static
    {
        if ($this->folders->removeElement($folder)) {
            // set the owning side to null (unless already changed)
            if ($folder->getProjectHolder() === $this) {
                $folder->setProjectHolder(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setResponsibleBusiness($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getResponsibleBusiness() === $this) {
                $transaction->setResponsibleBusiness(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Bank>
     */
    public function getBanks(): Collection
    {
        return $this->banks;
    }

    public function addBank(Bank $bank): static
    {
        if (!$this->banks->contains($bank)) {
            $this->banks->add($bank);
            $bank->setInterlocutor($this);
        }

        return $this;
    }

    public function removeBank(Bank $bank): static
    {
        if ($this->banks->removeElement($bank)) {
            // set the owning side to null (unless already changed)
            if ($bank->getInterlocutor() === $this) {
                $bank->setInterlocutor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CustomerIdentification>
     */
    public function getCustomerIdentifications(): Collection
    {
        return $this->customerIdentifications;
    }

    public function addCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if (!$this->customerIdentifications->contains($customerIdentification)) {
            $this->customerIdentifications->add($customerIdentification);
            $customerIdentification->setInterlocutor($this);
        }

        return $this;
    }

    public function removeCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if ($this->customerIdentifications->removeElement($customerIdentification)) {
            // set the owning side to null (unless already changed)
            if ($customerIdentification->getInterlocutor() === $this) {
                $customerIdentification->setInterlocutor(null);
            }
        }

        return $this;
    }

    public function getTerminal(): ?Terminal
    {
        return $this->terminal;
    }

    public function setTerminal(?Terminal $terminal): static
    {
        $this->terminal = $terminal;

        return $this;
    }

    /**
     * @return Collection<int, EIR>
     */
    public function getEIRs(): Collection
    {
        return $this->eIRs;
    }

    public function addEIR(EIR $eIR): static
    {
        if (!$this->eIRs->contains($eIR)) {
            $this->eIRs->add($eIR);
            $eIR->setCreatorUser($this);
        }

        return $this;
    }

    public function removeEIR(EIR $eIR): static
    {
        if ($this->eIRs->removeElement($eIR)) {
            // set the owning side to null (unless already changed)
            if ($eIR->getCreatorUser() === $this) {
                $eIR->setCreatorUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Proforma>
     */
    public function getProformas(): Collection
    {
        return $this->proformas;
    }

    public function addProforma(Proforma $proforma): static
    {
        if (!$this->proformas->contains($proforma)) {
            $this->proformas->add($proforma);
            $proforma->setCommercial($this);
        }

        return $this;
    }

    public function removeProforma(Proforma $proforma): static
    {
        if ($this->proformas->removeElement($proforma)) {
            // set the owning side to null (unless already changed)
            if ($proforma->getCommercial() === $this) {
                $proforma->setCommercial(null);
            }
        }

        return $this;
    }

}
