<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\CustTypesToPdfController;
use App\Controller\CustTypesToSheetController;
use App\Entity\Trait\HistoryTrait;
use App\Repository\CustomerTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CustomerTypeRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des types de clients",
            ],
            normalizationContext: ['groups' => 'customerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/customer_types/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'customerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: CustTypesToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/customer_types/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'customerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: CustTypesToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un type de clients",
            ],
            normalizationContext: ['groups' => 'customerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un type de clients",
            ],
            normalizationContext: ['groups' => 'customerType:read'],
            denormalizationContext: ['groups' => 'customerType:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un type de clients",
            ],
            normalizationContext: ['groups' => 'customerType:read'],
            denormalizationContext: ['groups' => 'customerType:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un type de clients",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['customerType:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "name"
],arguments:  ['orderParameterName' => 'order'])
]
#[UniqueEntity(['name'], message: 'le nom est dupliqué', ignoreNull: false)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class CustomerType
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customerType:read','customer:read','booking:read','booking:create','booking:update',
        'terminalGetOut:read',
        'customerContact:read',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customerType:read','customerType:create','customerType:update','customer:read','customer:create','customer:update',
        'booking:read','booking:create','booking:update',
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'customerType', targetEntity: customerType::class,cascade: ['persist','detach'])]
    private Collection $customers;

    #[ORM\OneToMany(mappedBy: 'customerType', targetEntity: CustomerIdentification::class,cascade: ['persist','detach'])]
    private Collection $customerIdentifications;

    public function __construct()
    {
        $this->customers = new ArrayCollection();
        $this->customerIdentifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): static
    {
        if (!$this->customers->contains($customer)) {
            $this->customers->add($customer);
            $customer->setCustomerType($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): static
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getCustomerType() === $this) {
                $customer->setCustomerType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CustomerIdentification>
     */
    public function getCustomerIdentifications(): Collection
    {
        return $this->customerIdentifications;
    }

    public function addCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if (!$this->customerIdentifications->contains($customerIdentification)) {
            $this->customerIdentifications->add($customerIdentification);
            $customerIdentification->setCustomerType($this);
        }

        return $this;
    }

    public function removeCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if ($this->customerIdentifications->removeElement($customerIdentification)) {
            // set the owning side to null (unless already changed)
            if ($customerIdentification->getCustomerType() === $this) {
                $customerIdentification->setCustomerType(null);
            }
        }

        return $this;
    }
}
