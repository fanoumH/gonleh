<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CustomerRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\CustomerToPdfController;
use Doctrine\Common\Collections\Collection;
use App\Controller\CustomerToSheetController;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use App\ApiPlatform\Filter\SearchCustomerFilter;

use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des clients",
            ],
            normalizationContext: ['groups' => 'customer:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/customers/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'customer:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: CustomerToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/customers/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'customer:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: CustomerToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un client",
            ],
            normalizationContext: ['groups' => 'customer:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un client",
            ],
            normalizationContext: ['groups' => 'customer:read'],
            denormalizationContext: ['groups' => 'customer:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un client",
            ],
            normalizationContext: ['groups' => 'customer:read'],
            denormalizationContext: ['groups' => 'customer:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un client",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['customer:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "customerIdentification.thirdParties" => "partial",
    "customerIdentification.customerGroupName" => "ipartial",
    "customerIdentification.district" => "ipartial",
    "customerIdentification.rcs" => "partial",
    "customerIdentification.cif" => "partial",
    "customerIdentification.stat" => "partial",
    "customerIdentification.customerTitle" => "ipartial",
    "customerIdentification.abbreviated" => "ipartial",
    "customerIdentification.interlocutor" => "partial",
    "customerIdentification.quality.label" => "ipartial",
    "customerIdentification.comments" => "partial",
    "customerIdentification.complement" => "partial",
    "customerIdentification.country.code" => "partial",
    "customerIdentification.country.nameEn" => "ipartial",
    "customerIdentification.country.nameFr" => "ipartial",
    "customerIdentification.city" => "partial",
    "customerIdentification.zipCode" => "partial",
    "customerIdentification.nif" => "partial",
    "customerIdentification.phone" => "partial",
    "customerIdentification.linkedin" => "partial",
    "customerIdentification.email" => "partial",
    "customerIdentification.siteInternet" => "partial",
    "customerIdentification.region.label" => "partial",
    "customerIdentification.province.label" => "partial",
    "customerIdentification.taxeRate.label" => "partial",
    "customerIdentification.customerQuality.label" => "partial",
    "customerBank.titledBank.abbreviation" => "partial",
    "customerBank.titledBank.entitled" => "partial",
    "customerBank.titledBank.interlocutor" => "partial",
    "customerBank.titledBank.address" => "partial",
    "customerBank.titledBank.complement" => "partial",
    "customerBank.titledBank.city" => "partial",
    "customerBank.titledBank.zipCode" => "partial",
    "customerBank.titledBank.phone" => "partial",
    "customerBank.titledBank.email" => "partial",
    "customerBank.titledBank.website" => "partial",
    "customerBank.titledBank.region.label" => "partial",
    "customerBank.banque" => "partial",
    "customerBank.accountNumber" => "partial",
    "customerBank.keyAccount" => "partial",
    "customerBank.ticketOffice" => "partial",
    "customerBank.currency.reference" => "partial",
    "customerFreeField.legalSatus" => "partial",
    "customerFreeField.shareFolderPale" => "partial",
    "customerFreeField.blocNote" => "partial",
    "customerFreeField.localization.name" => "partial",
    "customerFreeField.title" => "partial",
    "solvency.riskCode.label" => "partial",
]
)]
#[
    ApiFilter(
        OrderFilter::class,properties: [
        "customerIdentification.thirdParties",
        "customerIdentification.customerGroupName",
        "customerIdentification.district",
        "customerIdentification.rcs",
        "customerIdentification.cif",
        "customerIdentification.stat",
        "customerIdentification.customerTitle",
        "customerIdentification.abbreviated",
        "customerIdentification.interlocutor",
        "customerIdentification.quality.label",
        "customerIdentification.comments",
        "customerIdentification.complement",
        "customerIdentification.country.code",
        "customerIdentification.country.nameEn",
        "customerIdentification.country.nameFr",
        "customerIdentification.city",
        "customerIdentification.zipCode",
        "customerIdentification.nif",
        "customerIdentification.phone",
        "customerIdentification.linkedin",
        "customerIdentification.email",
        "customerIdentification.siteInternet",
        "customerIdentification.region.label",
        "customerIdentification.province.label",
        "customerIdentification.taxeRate.label",
        "customerIdentification.customerQuality.label",
        "customerBank.titledBank.abbreviation",
        "customerBank.titledBank.entitled",
        "customerBank.titledBank.interlocutor",
        "customerBank.titledBank.address",
        "customerBank.titledBank.complement",
        "customerBank.titledBank.city",
        "customerBank.titledBank.zipCode",
        "customerBank.titledBank.phone",
        "customerBank.titledBank.email",
        "customerBank.titledBank.website",
        "customerBank.titledBank.region.label",
        "customerBank.banque",
        "customerBank.accountNumber",
        "customerBank.keyAccount",
        "customerBank.ticketOffice",
        "customerBank.currency.reference",
        "customerFreeField.legalSatus",
        "customerFreeField.shareFolderPale",
        "customerFreeField.blocNote",
        "customerFreeField.localization.name",
        "customerFreeField.title",
        "solvency.riskCode.label",
    ],
        arguments:[
            'orderParameterName' => 'order'
        ]
    )
]
#[ApiFilter(
    BooleanFilter::class,properties: ['customerIdentification.prospect']
)]
#[ApiFilter(
    SearchCustomerFilter::class,properties: ['article']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class Customer
{
    const CUSTOMER_PREFIX = "CLT";

    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read',
        'booking:read',
        'transaction:read',
        'folder:read',
        'folder:create',
        'folder:update',
        'customerContact:read',
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
        'rentalOrder:read',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'customer', cascade: ['persist', 'detach'])]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update', 'booking:read', 'folder:read',
        'transaction:read','transaction:create','transaction:update',
        'customerContact:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'rentalOrder:read',
        'proforma:read','proforma:create','proforma:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    #[ApiProperty(
        writable: true,
    )]
    private ?CustomerIdentification $customerIdentification = null;

    #[ORM\OneToOne(inversedBy: 'customer', cascade: ['persist', 'detach'])]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'customerContact:read',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        // 'folder:read'
    ])]
    #[ApiProperty(
        writable: true,
    )]
    private ?CustomerPrice $customerPrice = null;

    #[ORM\OneToOne(inversedBy: 'customer', cascade: ['persist', 'detach'])]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'customerContact:read'
        // 'folder:read'
    ])]
    #[ApiProperty(
        writable: true,
    )]
    private ?CustomerBank $customerBank = null;

    #[ORM\OneToOne(inversedBy: 'customer', cascade: ['persist', 'detach'])]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'customerContact:read'
        // 'folder:read'
    ])]
    #[ApiProperty(
        writable: true,
    )]
    private ?CustomerFreeField $customerFreeField = null;

    #[ORM\OneToOne(inversedBy: 'customer', cascade: ['persist', 'detach'])]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'customerContact:read'
        // 'folder:read'
    ])]
    #[ApiProperty(
        writable: true,
    )]
    private ?Solvency $solvency = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','booking:read',
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transaction:read',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $refCustomer = null;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: CustomerContact::class,cascade: ['persist','merge'],orphanRemoval: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','booking:read',
        'terminalGetOut:read',
        'folder:read', 'folder:create', 'folder:update',
    ])]
    private Collection $customerContact;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Booking::class,cascade: ['persist','detach'])]
    private Collection $bookings;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: TerminalGetOut::class,cascade: ['persist','detach'])]
    private Collection $terminalGetOuts;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: Folder::class,cascade: ['persist','detach'])]
    private Collection $folders;

    

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: RentalOrder::class)]
    private Collection $rentalOrders;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: TransportOrder::class)]
    private Collection $transportOrders;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: Proforma::class)]
    private Collection $proformas;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: SpecificRate::class)]
    private Collection $specificRates;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: SpecificRatetransport::class)]
    private Collection $specificRatetransports;

    public function __construct()
    {
        $this->customerContact = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->folders = new ArrayCollection();
        $this->transportOrders = new ArrayCollection();
        $this->rentalOrders = new ArrayCollection();
        $this->proformas = new ArrayCollection();
        $this->specificRates = new ArrayCollection();
        $this->specificRatetransports = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomerIdentification(): ?CustomerIdentification
    {
        return $this->customerIdentification;
    }

    public function setCustomerIdentification(?CustomerIdentification $customerIdentification): static
    {
        $this->customerIdentification = $customerIdentification;

        return $this;
    }

    public function getCustomerPrice(): ?CustomerPrice
    {
        return $this->customerPrice;
    }

    public function setCustomerPrice(?CustomerPrice $customerPrice): static
    {
        $this->customerPrice = $customerPrice;

        return $this;
    }

    public function getCustomerBank(): ?CustomerBank
    {
        return $this->customerBank;
    }

    public function setCustomerBank(?CustomerBank $customerBank): static
    {
        $this->customerBank = $customerBank;

        return $this;
    }

    public function getCustomerFreeField(): ?CustomerFreeField
    {
        return $this->customerFreeField;
    }

    public function setCustomerFreeField(?CustomerFreeField $customerFreeField): static
    {
        $this->customerFreeField = $customerFreeField;

        return $this;
    }

    public function getSolvency(): ?Solvency
    {
        return $this->solvency;
    }

    public function setSolvency(?Solvency $solvency): static
    {
        $this->solvency = $solvency;

        return $this;
    }

    public function getRefCustomer(): ?string
    {
        return $this->refCustomer;
    }

    public function setRefCustomer(?string $refCustomer): static
    {
        $this->refCustomer = $refCustomer;

        return $this;
    }

    /**
     * @return Collection<int, CustomerContact>
     */
    public function getCustomerContact(): Collection
    {
        return $this->customerContact;
    }

    public function addCustomerContact(CustomerContact $customerContact): static
    {
        if (!$this->customerContact->contains($customerContact)) {
            $this->customerContact->add($customerContact);
            $customerContact->setCustomer($this);
        }

        return $this;
    }

    public function removeCustomerContact(CustomerContact $customerContact): static
    {
        if ($this->customerContact->removeElement($customerContact)) {
            // set the owning side to null (unless already changed)
            if ($customerContact->getCustomer() === $this) {
                $customerContact->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): static
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings->add($booking);
            $booking->setClient($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): static
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getClient() === $this) {
                $booking->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TerminalGetOut>
     */
    public function getTerminalGetOuts(): Collection
    {
        return $this->terminalGetOuts;
    }

    public function addTerminalGetOut(TerminalGetOut $terminalGetOut): static
    {
        if (!$this->terminalGetOuts->contains($terminalGetOut)) {
            $this->terminalGetOuts->add($terminalGetOut);
            $terminalGetOut->setCustomer($this);
        }

        return $this;
    }

    public function removeTerminalGetOut(TerminalGetOut $terminalGetOut): static
    {
        if ($this->terminalGetOuts->removeElement($terminalGetOut)) {
            // set the owning side to null (unless already changed)
            if ($terminalGetOut->getCustomer() === $this) {
                $terminalGetOut->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Folder>
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }

    public function addFolder(Folder $folder): static
    {
        if (!$this->folders->contains($folder)) {
            $this->folders->add($folder);
            $folder->setCustomer($this);
        }

        return $this;
    }

    public function removeFolder(Folder $folder): static
    {
        if ($this->folders->removeElement($folder)) {
            // set the owning side to null (unless already changed)
            if ($folder->getCustomer() === $this) {
                $folder->setCustomer(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, RentalOrder>
     */
    public function getRentalOrders(): Collection
    {
        return $this->rentalOrders;
    }

    public function addRentalOrder(RentalOrder $rentalOrder): static
    {
        if (!$this->rentalOrders->contains($rentalOrder)) {
            $this->rentalOrders->add($rentalOrder);
            $rentalOrder->setCustomer($this);
        }

        return $this;
    }

    public function removeRentalOrder(RentalOrder $rentalOrder): static
    {
        if ($this->rentalOrders->removeElement($rentalOrder)) {
            // set the owning side to null (unless already changed)
            if ($rentalOrder->getCustomer() === $this) {
                $rentalOrder->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TransportOrder>
     */
    public function getTransportOrders(): Collection
    {
        return $this->transportOrders;
    }

    public function addTransportOrder(TransportOrder $transportOrder): static
    {
        if (!$this->transportOrders->contains($transportOrder)) {
            $this->transportOrders->add($transportOrder);
            $transportOrder->setCustomer($this);
        }

        return $this;
    }

    public function removeTransportOrder(TransportOrder $transportOrder): static
    {
        if ($this->transportOrders->removeElement($transportOrder)) {
            // set the owning side to null (unless already changed)
            if ($transportOrder->getCustomer() === $this) {
                $transportOrder->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Proforma>
     */
    public function getProformas(): Collection
    {
        return $this->proformas;
    }

    public function addProforma(Proforma $proforma): static
    {
        if (!$this->proformas->contains($proforma)) {
            $this->proformas->add($proforma);
            $proforma->setCustomer($this);
        }

        return $this;
    }

    public function removeProforma(Proforma $proforma): static
    {
        if ($this->proformas->removeElement($proforma)) {
            // set the owning side to null (unless already changed)
            if ($proforma->getCustomer() === $this) {
                $proforma->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRate>
     */
    public function getSpecificRates(): Collection
    {
        return $this->specificRates;
    }

    public function addSpecificRate(SpecificRate $specificRate): static
    {
        if (!$this->specificRates->contains($specificRate)) {
            $this->specificRates->add($specificRate);
            $specificRate->setClient($this);
        }

        return $this;
    }

    public function removeSpecificRate(SpecificRate $specificRate): static
    {
        if ($this->specificRates->removeElement($specificRate)) {
            // set the owning side to null (unless already changed)
            if ($specificRate->getClient() === $this) {
                $specificRate->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRatetransport>
     */
    public function getSpecificRatetransports(): Collection
    {
        return $this->specificRatetransports;
    }

    public function addSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if (!$this->specificRatetransports->contains($specificRatetransport)) {
            $this->specificRatetransports->add($specificRatetransport);
            $specificRatetransport->setClient($this);
        }

        return $this;
    }

    public function removeSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if ($this->specificRatetransports->removeElement($specificRatetransport)) {
            // set the owning side to null (unless already changed)
            if ($specificRatetransport->getClient() === $this) {
                $specificRatetransport->setClient(null);
            }
        }

        return $this;
    }

}
