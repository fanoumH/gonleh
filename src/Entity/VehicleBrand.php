<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\BrandsToPdfController;
use App\Repository\VehicleBrandRepository;
use App\Controller\BrandsToSheetController;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: VehicleBrandRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des marques de vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-brand:read']
        ),
        new GetCollection(
            uriTemplate: '/vehicle_brands/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'vehicle-brand:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: BrandsToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/vehicle_brands/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'vehicle-brand:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: BrandsToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'une marque de vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-brand:read'],
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'une marque de vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-brand:read'],
            denormalizationContext: ['groups' => 'vehicle-brand:create']
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'une marques vehicule",
            ],
            normalizationContext: ['groups' => 'vehicle-brand:read'],
            denormalizationContext: ['groups' => 'vehicle-brand:update']
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'une marque de vehicule",
            ]
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "id" => "exact"
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id" ,"name"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
#[UniqueEntity(['name'])]
class VehicleBrand
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'vehicle-brand:read',
        'vehicle-model:read',
        'vehicle:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(groups: [
        'vehicle-brand:read', 'vehicle-brand:create', 'vehicle-brand:update',
        'vehicle-model:read',
        'vehicle:read',
        'coupling:read',
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'vehicleBrand', targetEntity: VehicleModel::class, cascade: ['persist','detach'], orphanRemoval: true)]
    private Collection $vehicleModels;

    #[ORM\OneToMany(mappedBy: 'vehicleBrand', targetEntity: Vehicle::class,cascade: ['persist','detach'])]
    private Collection $vehicles;

    public function __construct()
    {
        $this->vehicleModels = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, VehicleModel>
     */
    public function getVehicleModels(): Collection
    {
        return $this->vehicleModels;
    }

    public function addVehicleModel(VehicleModel $vehicleModel): static
    {
        if (!$this->vehicleModels->contains($vehicleModel)) {
            $this->vehicleModels->add($vehicleModel);
            $vehicleModel->setVehicleBrand($this);
        }

        return $this;
    }

    public function removeVehicleModel(VehicleModel $vehicleModel): static
    {
        if ($this->vehicleModels->removeElement($vehicleModel)) {
            // set the owning side to null (unless already changed)
            if ($vehicleModel->getVehicleBrand() === $this) {
                $vehicleModel->setVehicleBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setVehicleBrand($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getVehicleBrand() === $this) {
                $vehicle->setVehicleBrand(null);
            }
        }

        return $this;
    }
}
