<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\ProformaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProformaRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des proformas",
            ],
            normalizationContext: ['groups' => 'proforma:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un proforma",
            ],
            normalizationContext: ['groups' => 'proforma:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un proforma",
            ],
            normalizationContext: ['groups' => 'proforma:read'],
            denormalizationContext: ['groups' => 'proforma:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un proforma",
            ],
            normalizationContext: ['groups' => 'proforma:read'],
            denormalizationContext: ['groups' => 'proforma:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un proforma",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
class Proforma
{
    use TimestampableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'proformas')]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?Customer $customer = null;

    #[ORM\OneToMany(mappedBy: 'proforma', targetEntity: ProformaDetails::class,cascade: ["persist","remove"])]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private Collection $proformaDetails;

    #[ORM\ManyToOne(inversedBy: 'proformas')]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?Center $centre = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?\DateTimeInterface $releaseDate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?float $discount = null;

    #[ORM\ManyToOne(inversedBy: 'proformas')]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?Currency $currency = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?float $totalDiscount = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?float $totalRising = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?\DateTimeInterface $creationDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $comments = null;

    #[ORM\ManyToOne(inversedBy: 'proformas')]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?User $commercial = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $reference = null;

    public function __construct()
    {
        $this->proformaDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, ProformaDetails>
     */
    public function getProformaDetails(): Collection
    {
        return $this->proformaDetails;
    }

    public function addProformaDetail(ProformaDetails $proformaDetail): static
    {
        if (!$this->proformaDetails->contains($proformaDetail)) {
            $this->proformaDetails->add($proformaDetail);
            $proformaDetail->setProforma($this);
        }

        return $this;
    }

    public function removeProformaDetail(ProformaDetails $proformaDetail): static
    {
        if ($this->proformaDetails->removeElement($proformaDetail)) {
            // set the owning side to null (unless already changed)
            if ($proformaDetail->getProforma() === $this) {
                $proformaDetail->setProforma(null);
            }
        }

        return $this;
    }

    public function getCentre(): ?Center
    {
        return $this->centre;
    }

    public function setCentre(?Center $centre): static
    {
        $this->centre = $centre;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(?\DateTimeInterface $releaseDate): static
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): static
    {
        $this->discount = $discount;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getTotalDiscount(): ?float
    {
        return $this->totalDiscount;
    }

    public function setTotalDiscount(?float $totalDiscount): static
    {
        $this->totalDiscount = $totalDiscount;

        return $this;
    }

    public function getTotalRising(): ?float
    {
        return $this->totalRising;
    }

    public function setTotalRising(?float $totalRising): static
    {
        $this->totalRising = $totalRising;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): static
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): static
    {
        $this->comments = $comments;

        return $this;
    }

    public function getCommercial(): ?User
    {
        return $this->commercial;
    }

    public function setCommercial(?User $commercial): static
    {
        $this->commercial = $commercial;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }
}
