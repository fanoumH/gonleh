<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\OperationTypeRateRepository;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: OperationTypeRateRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des operations Types rates",
            ],
            normalizationContext: ['groups' => 'OperationTypeRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'une operation type rate",
            ],
            normalizationContext: ['groups' => 'OperationTypeRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'une operation type rate",
            ],
            normalizationContext: ['groups' => 'OperationTypeRate:read'],
            denormalizationContext: ['groups' => 'OperationTypeRate:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'une oparation type rate",
            ],
            normalizationContext: ['groups' => 'OperationTypeRate:read'],
            denormalizationContext: ['groups' => 'OperationTypeRate:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'une operation service rate",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "partial",
]
)]
class OperationTypeRate
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'OperationTypeRate:read',
        'basicRate:read',
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update','PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRate:read',
        'specificRateTransport:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'OperationTypeRate:read',
        'OperationTypeRate:create',
        'OperationTypeRate:update',
        'basicRate:read',
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update','PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'operationTypeRate', targetEntity: BasicRate::class)]
    private Collection $basicRates;

    #[ORM\OneToMany(mappedBy: 'operationTypeRate', targetEntity: PriceListStandardTransport::class)]
    private Collection $priceListStandardTransports;

    #[ORM\OneToMany(mappedBy: 'operationTypeRate', targetEntity: SpecificRate::class)]
    private Collection $specificRates;

    #[ORM\OneToMany(mappedBy: 'operationTypeRate', targetEntity: ProformaDetails::class)]
    private Collection $proformaDetails;

    public function __construct()
    {
        $this->basicRates = new ArrayCollection();
        $this->priceListStandardTransports = new ArrayCollection();
        $this->specificRates = new ArrayCollection();
        $this->proformaDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, BasicRate>
     */
    public function getBasicRates(): Collection
    {
        return $this->basicRates;
    }

    public function addBasicRate(BasicRate $basicRate): static
    {
        if (!$this->basicRates->contains($basicRate)) {
            $this->basicRates->add($basicRate);
            $basicRate->setOperationTypeRate($this);
        }

        return $this;
    }

    public function removeBasicRate(BasicRate $basicRate): static
    {
        if ($this->basicRates->removeElement($basicRate)) {
            // set the owning side to null (unless already changed)
            if ($basicRate->getOperationTypeRate() === $this) {
                $basicRate->setOperationTypeRate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PriceListStandardTransport>
     */
    public function getPriceListStandardTransports(): Collection
    {
        return $this->priceListStandardTransports;
    }

    public function addPriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if (!$this->priceListStandardTransports->contains($priceListStandardTransport)) {
            $this->priceListStandardTransports->add($priceListStandardTransport);
            $priceListStandardTransport->setOperationTypeRate($this);
        }

        return $this;
    }

    public function removePriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if ($this->priceListStandardTransports->removeElement($priceListStandardTransport)) {
            // set the owning side to null (unless already changed)
            if ($priceListStandardTransport->getOperationTypeRate() === $this) {
                $priceListStandardTransport->setOperationTypeRate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRate>
     */
    public function getSpecificRates(): Collection
    {
        return $this->specificRates;
    }

    public function addSpecificRate(SpecificRate $specificRate): static
    {
        if (!$this->specificRates->contains($specificRate)) {
            $this->specificRates->add($specificRate);
            $specificRate->setOperationTypeRate($this);
        }

        return $this;
    }

    public function removeSpecificRate(SpecificRate $specificRate): static
    {
        if ($this->specificRates->removeElement($specificRate)) {
            // set the owning side to null (unless already changed)
            if ($specificRate->getOperationTypeRate() === $this) {
                $specificRate->setOperationTypeRate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProformaDetails>
     */
    public function getProformaDetails(): Collection
    {
        return $this->proformaDetails;
    }

    public function addProformaDetail(ProformaDetails $proformaDetail): static
    {
        if (!$this->proformaDetails->contains($proformaDetail)) {
            $this->proformaDetails->add($proformaDetail);
            $proformaDetail->setOperationTypeRate($this);
        }

        return $this;
    }

    public function removeProformaDetail(ProformaDetails $proformaDetail): static
    {
        if ($this->proformaDetails->removeElement($proformaDetail)) {
            // set the owning side to null (unless already changed)
            if ($proformaDetail->getOperationTypeRate() === $this) {
                $proformaDetail->setOperationTypeRate(null);
            }
        }

        return $this;
    }
}
