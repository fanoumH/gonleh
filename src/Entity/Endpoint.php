<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use App\Controller\GetAllPermissionController;
use App\Repository\EndpointRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EndpointRepository::class)]
#[ORM\Table(name: '`endpoints`')]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Lister tous les menus",
            ],
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modifier un menu",
            ],
        ),
        new Delete(
            openapiContext: [
                "summary" => "Supprimer un menu",
            ],
        ),
        new GetCollection(
            uriTemplate: '/allPermissions/',
            controller: GetAllPermissionController::class,
            openapiContext: [
                "summary" => "Liste de tous endpoints de permissions",
            ],
            name: "list_endpointpermissions"
        )
    ]
)]
class Endpoint
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $uriTemplate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $summary = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $shortName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $controller = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $method = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUriTemplate(): ?string
    {
        return $this->uriTemplate;
    }

    public function setUriTemplate(?string $uriTemplate): static
    {
        $this->uriTemplate = $uriTemplate;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): static
    {
        $this->summary = $summary;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(?string $shortName): static
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getController(): ?string
    {
        return $this->controller;
    }

    public function setController(?string $controller): static
    {
        $this->controller = $controller;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(?string $method): static
    {
        $this->method = $method;

        return $this;
    }
}
