<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\ServiceRateRepository;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: ServiceRateRepository::class)]

#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des Services rates",
            ],
            normalizationContext: ['groups' => 'serviceRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un service rate",
            ],
            normalizationContext: ['groups' => 'serviceRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un service rate",
            ],
            normalizationContext: ['groups' => 'serviceRate:read'],
            denormalizationContext: ['groups' => 'serviceRate:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un service rate",
            ],
            normalizationContext: ['groups' => 'serviceRate:read'],
            denormalizationContext: ['groups' => 'serviceRate:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un service rate",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "partial",
]
)]
class ServiceRate
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'serviceRate:read',
        'basicRate:read',
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'serviceRate:read',
        'serviceRate:create',
        'serviceRate:update',
        'basicRate:read',
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'serviceRate', targetEntity: BasicRate::class)]
    private Collection $basicRates;

    #[ORM\OneToMany(mappedBy: 'serviceRate', targetEntity: PriceListStandardTransport::class)]
    private Collection $priceListStandardTransports;

    #[ORM\OneToMany(mappedBy: 'serviceRate', targetEntity: SpecificRate::class)]
    private Collection $specificRates;

    #[ORM\OneToMany(mappedBy: 'serviceRate', targetEntity: SpecificRatetransport::class)]
    private Collection $specificRatetransports;

    #[ORM\OneToMany(mappedBy: 'serviceRate', targetEntity: ProformaDetails::class)]
    private Collection $proformaDetails;

    public function __construct()
    {
        $this->basicRates = new ArrayCollection();
        $this->priceListStandardTransports = new ArrayCollection();
        $this->specificRates = new ArrayCollection();
        $this->specificRatetransports = new ArrayCollection();
        $this->proformaDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, BasicRate>
     */
    public function getBasicRates(): Collection
    {
        return $this->basicRates;
    }

    public function addBasicRate(BasicRate $basicRate): static
    {
        if (!$this->basicRates->contains($basicRate)) {
            $this->basicRates->add($basicRate);
            $basicRate->setServiceRate($this);
        }

        return $this;
    }

    public function removeBasicRate(BasicRate $basicRate): static
    {
        if ($this->basicRates->removeElement($basicRate)) {
            // set the owning side to null (unless already changed)
            if ($basicRate->getServiceRate() === $this) {
                $basicRate->setServiceRate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PriceListStandardTransport>
     */
    public function getPriceListStandardTransports(): Collection
    {
        return $this->priceListStandardTransports;
    }

    public function addPriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if (!$this->priceListStandardTransports->contains($priceListStandardTransport)) {
            $this->priceListStandardTransports->add($priceListStandardTransport);
            $priceListStandardTransport->setServiceRate($this);
        }

        return $this;
    }

    public function removePriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if ($this->priceListStandardTransports->removeElement($priceListStandardTransport)) {
            // set the owning side to null (unless already changed)
            if ($priceListStandardTransport->getServiceRate() === $this) {
                $priceListStandardTransport->setServiceRate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRate>
     */
    public function getSpecificRates(): Collection
    {
        return $this->specificRates;
    }

    public function addSpecificRate(SpecificRate $specificRate): static
    {
        if (!$this->specificRates->contains($specificRate)) {
            $this->specificRates->add($specificRate);
            $specificRate->setServiceRate($this);
        }

        return $this;
    }

    public function removeSpecificRate(SpecificRate $specificRate): static
    {
        if ($this->specificRates->removeElement($specificRate)) {
            // set the owning side to null (unless already changed)
            if ($specificRate->getServiceRate() === $this) {
                $specificRate->setServiceRate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRatetransport>
     */
    public function getSpecificRatetransports(): Collection
    {
        return $this->specificRatetransports;
    }

    public function addSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if (!$this->specificRatetransports->contains($specificRatetransport)) {
            $this->specificRatetransports->add($specificRatetransport);
            $specificRatetransport->setServiceRate($this);
        }

        return $this;
    }

    public function removeSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if ($this->specificRatetransports->removeElement($specificRatetransport)) {
            // set the owning side to null (unless already changed)
            if ($specificRatetransport->getServiceRate() === $this) {
                $specificRatetransport->setServiceRate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProformaDetails>
     */
    public function getProformaDetails(): Collection
    {
        return $this->proformaDetails;
    }

    public function addProformaDetail(ProformaDetails $proformaDetail): static
    {
        if (!$this->proformaDetails->contains($proformaDetail)) {
            $this->proformaDetails->add($proformaDetail);
            $proformaDetail->setServiceRate($this);
        }

        return $this;
    }

    public function removeProformaDetail(ProformaDetails $proformaDetail): static
    {
        if ($this->proformaDetails->removeElement($proformaDetail)) {
            // set the owning side to null (unless already changed)
            if ($proformaDetail->getServiceRate() === $this) {
                $proformaDetail->setServiceRate(null);
            }
        }

        return $this;
    }
}
