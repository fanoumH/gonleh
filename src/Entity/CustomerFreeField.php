<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Controller\AttachmentDocCustomerController;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\CustomerFreeFieldRepository;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: CustomerFreeFieldRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[Vich\Uploadable]
//#[ApiResource]
#[ApiResource(
    types: ['https://schema.org/customers'],
    operations: [
        new Post(
            uriTemplate: "/customerAttachedDocument/{id}/customerFreeField",
            inputFormats: ['multipart' => ['multipart/form-data']],
            controller: AttachmentDocCustomerController::class,
            openapiContext: [
                "summary" => "Ajouter document joint client",
            ],
            deserialize: false
        )
    ],
    normalizationContext: ['groups' => ['customer:read', 'read']],
    denormalizationContext: ['groups' => ['customer:create','customer:update', 'write']],
)]
class CustomerFreeField
{
    use HistoryTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read',
        'folder:read',
        'customerContact:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?float $shareCapital = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $legalSatus = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $shareFolderPale = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?\DateTimeInterface $negotiationRegulation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $blocNote = null;

    #[ORM\ManyToOne(inversedBy: 'customerFreeFields')]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
    ])]
    private ?Zone $localization = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?string $title = null;

    #[ORM\OneToOne(mappedBy: 'customerFreeField', cascade: ['persist', 'remove'])]
    private ?Customer $customer = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update',
        'transaction:read','transaction:create','transaction:update',
        'folder:read',
        'customerContact:read'
    ])]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $attachedDocumentPath = null;

    #[Vich\UploadableField(mapping: "customer_attached_document", fileNameProperty: "attachedDocumentPath")]
    private ?File $attachedDocumentFile = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShareCapital(): ?float
    {
        return $this->shareCapital;
    }

    public function setShareCapital(?float $shareCapital): static
    {
        $this->shareCapital = $shareCapital;

        return $this;
    }

    public function getLegalSatus(): ?string
    {
        return $this->legalSatus;
    }

    public function setLegalSatus(?string $legalSatus): static
    {
        $this->legalSatus = $legalSatus;

        return $this;
    }

    public function getShareFolderPale(): ?string
    {
        return $this->shareFolderPale;
    }

    public function setShareFolderPale(?string $shareFolderPale): static
    {
        $this->shareFolderPale = $shareFolderPale;

        return $this;
    }

    public function getNegotiationRegulation(): ?\DateTimeInterface
    {
        return $this->negotiationRegulation;
    }

    public function setNegotiationRegulation(?\DateTimeInterface $negotiationRegulation): static
    {
        $this->negotiationRegulation = $negotiationRegulation;

        return $this;
    }

    public function getBlocNote(): ?string
    {
        return $this->blocNote;
    }

    public function setBlocNote(?string $blocNote): static
    {
        $this->blocNote = $blocNote;

        return $this;
    }

    public function getLocalization(): ?Zone
    {
        return $this->localization;
    }

    public function setLocalization(?Zone $localization): static
    {
        $this->localization = $localization;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        // unset the owning side of the relation if necessary
        if ($customer === null && $this->customer !== null) {
            $this->customer->setCustomerFreeField(null);
        }

        // set the owning side of the relation if necessary
        if ($customer !== null && $customer->getCustomerFreeField() !== $this) {
            $customer->setCustomerFreeField($this);
        }

        $this->customer = $customer;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): static
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getAttachedDocumentPath(): ?string
    {
        return $this->attachedDocumentPath;
    }

    public function setAttachedDocumentPath(?string $attachedDocumentPath): static
    {
        $this->attachedDocumentPath = $attachedDocumentPath;

        return $this;
    }

    public function getAttachedDocumentFile(): ?File
    {
        return $this->attachedDocumentFile;
    }

    /**
     * @param File|null $attachedDocumentFile
     * @return CustomerFreefield
     */
    public function setAttachedDocumentFile(?File $attachedDocumentFile): CustomerFreefield
    {
        $this->attachedDocumentFile = $attachedDocumentFile;
        return $this;
    }
}
