<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use App\Entity\ContainerType;
use ApiPlatform\Metadata\Post;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\SpecificRateRepository;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: SpecificRateRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des tarifs spécifiques",
            ],
            normalizationContext: ['groups' => 'specificRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un tarif spécifique",
            ],
            normalizationContext: ['groups' => 'specificRate:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un tarif spécifique",
            ],
            normalizationContext: ['groups' => 'specificRate:read'],
            denormalizationContext: ['groups' => 'specificRate:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un tarif spécifique",
            ],
            normalizationContext: ['groups' => 'specificRate:read'],
            denormalizationContext: ['groups' => 'specificRate:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un tarif spécifique",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "basicRate.serviceRate.id" => "exact",
    "basicRate.serviceRate.name" => "partial",
    "basicRate.operationTypeRate.id" => "exact",
    "basicRate.operationTypeRate.name" => "partial",
    "client.id" => "exact",
]
)]
#[UniqueEntity(['client','serviceRate','basicRate'], message: 'client,service,tarif basique est dupliqué', ignoreNull: false)]
class SpecificRate
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'specificRate:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?float $getOutRate = null;

    #[ORM\ManyToOne(inversedBy: 'specificRates')]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?Terminal $terminal = null;

    #[ORM\Column(length: 255,nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?string $estimate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?string $mode = null;

    #[ORM\ManyToOne(inversedBy: 'specificRates')]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?ContainerType $containerType = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?\DateTimeInterface $newPriceDate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?float $priceTTC = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?float $getInRate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?float $priceHT = null;

    #[ORM\ManyToOne(inversedBy: 'specificRates')]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?OperationTypeRate $operationTypeRate = null;

    #[ORM\ManyToOne(inversedBy: 'specificRates')]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?Currency $currency = null;

    #[ORM\ManyToOne(inversedBy: 'specificRates')]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?ServiceRate $serviceRate = null;

    #[ORM\ManyToOne(inversedBy: 'specificRates')]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?Customer $client = null;

    #[ORM\ManyToOne(inversedBy: 'specificRates')]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?BasicRate $basicRate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
    ])]
    private ?string $reference = null;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGetOutRate(): ?float
    {
        return $this->getOutRate;
    }

    public function setGetOutRate(?float $getOutRate): static
    {
        $this->getOutRate = $getOutRate;

        return $this;
    }

    public function getTerminal(): ?Terminal
    {
        return $this->terminal;
    }

    public function setTerminal(?Terminal $terminal): static
    {
        $this->terminal = $terminal;

        return $this;
    }

    public function getEstimate(): ?string
    {
        return $this->estimate;
    }

    public function setEstimate(string $estimate): static
    {
        $this->estimate = $estimate;

        return $this;
    }

    public function getMode(): ?string
    {
        return $this->mode;
    }

    public function setMode(?string $mode): static
    {
        $this->mode = $mode;

        return $this;
    }

    public function getContainerType(): ?ContainerType
    {
        return $this->containerType;
    }

    public function setContainerType(?ContainerType $containerType): static
    {
        $this->containerType = $containerType;

        return $this;
    }

    public function getNewPriceDate(): ?\DateTimeInterface
    {
        return $this->newPriceDate;
    }

    public function setNewPriceDate(?\DateTimeInterface $newPriceDate): static
    {
        $this->newPriceDate = $newPriceDate;

        return $this;
    }

    public function getPriceTTC(): ?float
    {
        return $this->priceTTC;
    }

    public function setPriceTTC(?float $priceTTC): static
    {
        $this->priceTTC = $priceTTC;

        return $this;
    }

    public function getGetInRate(): ?float
    {
        return $this->getInRate;
    }

    public function setGetInRate(?float $getInRate): static
    {
        $this->getInRate = $getInRate;

        return $this;
    }

    public function getPriceHT(): ?float
    {
        return $this->priceHT;
    }

    public function setPriceHT(?float $priceHT): static
    {
        $this->priceHT = $priceHT;

        return $this;
    }

    public function getOperationTypeRate(): ?OperationTypeRate
    {
        return $this->operationTypeRate;
    }

    public function setOperationTypeRate(?OperationTypeRate $operationTypeRate): static
    {
        $this->operationTypeRate = $operationTypeRate;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getServiceRate(): ?ServiceRate
    {
        return $this->serviceRate;
    }

    public function setServiceRate(?ServiceRate $serviceRate): static
    {
        $this->serviceRate = $serviceRate;

        return $this;
    }

    public function getClient(): ?Customer
    {
        return $this->client;
    }

    public function setClient(?Customer $client): static
    {
        $this->client = $client;

        return $this;
    }

    public function getBasicRate(): ?BasicRate
    {
        return $this->basicRate;
    }

    public function setBasicRate(?BasicRate $basicRate): static
    {
        $this->basicRate = $basicRate;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }
}
