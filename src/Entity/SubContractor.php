<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use App\ApiPlatform\Filter\SubContractorFilter;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\SubContractorRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\SubContractorsToPdfController;
use App\Controller\SubContractorsToSheetController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;


#[ORM\Entity(repositoryClass: SubContractorRepository::class)]

#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des sous-traitants",
            ],
            normalizationContext: ['groups' => 'subcontractor:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/sub_contractors/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'subcontractor:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: SubContractorsToPdfController::class,
        ),
        new GetCollection(
            uriTemplate: '/sub_contractors/all/sheet',
            openapiContext: [
                "summary" => "Export to spreadsheet",
            ],
            normalizationContext: ['groups' => 'subcontractor:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: SubContractorsToSheetController::class,
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un sous-traitant",
            ],
            normalizationContext: ['groups' => 'subcontractor:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un sous-traitant",
            ],
            normalizationContext: ['groups' => 'subcontractor:read'],
            denormalizationContext: ['groups' => 'subcontractor:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un sous-traitant",
            ],
            normalizationContext: ['groups' => 'subcontractor:read'],
            denormalizationContext: ['groups' => 'subcontractor:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un sous-traitant",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "contactNumber" => "ipartial",
    "id" => "exact",
    'companyName' => "ipartial",
    'contactName' => "ipartial",
]
)]
#[ApiFilter(
    SubContractorFilter::class,properties: ['subcontractor']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
class SubContractor
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'subcontractor:read',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?int $id = null;

    // #[ORM\Column(length: 255)]
    // #[Groups(groups: [
    //     'subcontractor:read',
    //     'subcontractor:create',
    //     'subcontractor:update',
    //     'transportOrder:read',
    // ])]
    // private ?string $name = null;

    // #[ORM\Column(length: 255)]
    // #[Groups(groups: [
    //     'subcontractor:read',
    //     'subcontractor:create',
    //     'subcontractor:update',
    //     'transportOrder:read',
    // ])]
    // private ?string $firstname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'subcontractor:read',
        'subcontractor:create',
        'subcontractor:update',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?string $contactNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'subcontractor:read',
        'subcontractor:create',
        'subcontractor:update',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?string $email = null;

    #[ORM\ManyToOne(inversedBy: 'subContractors')]
    #[Groups(groups: [
        'subcontractor:read',
        'subcontractor:create',
        'subcontractor:update',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?SubContractorType $subContractorType = null;

    // #[ORM\ManyToOne(inversedBy: 'subContractors')]
    // #[Groups(groups: [
    //     'subcontractor:read',
    //     'subcontractor:create',
    //     'subcontractor:update',
    //     'transportOrder:read',
    // ])]
    // private ?Bank $bank = null;

    #[ORM\OneToMany(mappedBy: 'subContractor', targetEntity: TransportOrder::class)]
    private Collection $transportOrders;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'subcontractor:read',
        'subcontractor:create',
        'subcontractor:update',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?string $companyName = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'subcontractor:read',
        'subcontractor:create',
        'subcontractor:update',
        'transportOrder:read',
        'missionOrder:read',
    ])]
    private ?string $contactName = null;

    public function __construct()
    {
        $this->transportOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    // public function getName(): ?string
    // {
    //     return $this->name;
    // }

    // public function setName(string $name): static
    // {
    //     $this->name = $name;

    //     return $this;
    // }

    // public function getFirstname(): ?string
    // {
    //     return $this->firstname;
    // }

    // public function setFirstname(string $firstname): static
    // {
    //     $this->firstname = $firstname;

    //     return $this;
    // }

    public function getContactNumber(): ?string
    {
        return $this->contactNumber;
    }

    public function setContactNumber(?string $contactNumber): static
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getSubContractorType(): ?SubContractorType
    {
        return $this->subContractorType;
    }

    public function setSubContractorType(?SubContractorType $subContractorType): static
    {
        $this->subContractorType = $subContractorType;

        return $this;
    }

    // public function getBank(): ?Bank
    // {
    //     return $this->bank;
    // }

    // public function setBank(?Bank $bank): static
    // {
    //     $this->bank = $bank;

    //     return $this;
    // }

    /**
     * @return Collection<int, TransportOrder>
     */
    public function getTransportOrders(): Collection
    {
        return $this->transportOrders;
    }

    public function addTransportOrder(TransportOrder $transportOrder): static
    {
        if (!$this->transportOrders->contains($transportOrder)) {
            $this->transportOrders->add($transportOrder);
            $transportOrder->setSubContractor($this);
        }

        return $this;
    }

    public function removeTransportOrder(TransportOrder $transportOrder): static
    {
        if ($this->transportOrders->removeElement($transportOrder)) {
            // set the owning side to null (unless already changed)
            if ($transportOrder->getSubContractor() === $this) {
                $transportOrder->setSubContractor(null);
            }
        }

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): static
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(string $contactName): static
    {
        $this->contactName = $contactName;

        return $this;
    }
}
