<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\Trait\HistoryTrait;
use App\Repository\TaxeRateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: TaxeRateRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource]
class TaxeRate
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?string $label = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update'
    ])]
    private ?float $amount = null;

    #[ORM\OneToMany(mappedBy: 'taxeRate', targetEntity: CustomerIdentification::class)]
    private Collection $customerIdentifications;

    public function __construct()
    {
        $this->customerIdentifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Collection<int, CustomerIdentification>
     */
    public function getCustomerIdentifications(): Collection
    {
        return $this->customerIdentifications;
    }

    public function addCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if (!$this->customerIdentifications->contains($customerIdentification)) {
            $this->customerIdentifications->add($customerIdentification);
            $customerIdentification->setTaxeRate($this);
        }

        return $this;
    }

    public function removeCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if ($this->customerIdentifications->removeElement($customerIdentification)) {
            // set the owning side to null (unless already changed)
            if ($customerIdentification->getTaxeRate() === $this) {
                $customerIdentification->setTaxeRate(null);
            }
        }

        return $this;
    }
}
