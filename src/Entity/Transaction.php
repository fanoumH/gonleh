<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\UploadTransactionFileController;
use App\Entity\Trait\HistoryTrait;
use App\Repository\TransactionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: TransactionRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des transactions",
            ],
            normalizationContext: ['groups' => 'transaction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'une transaction",
            ],
            normalizationContext: ['groups' => 'transaction:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'une transaction",
            ],
            normalizationContext: ['groups' => 'transaction:read'],
            denormalizationContext: ['groups' => 'transaction:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'une transactions",
            ],
            normalizationContext: ['groups' => 'transaction:read'],
            denormalizationContext: ['groups' => 'transaction:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'une transaction",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['transaction:update']]
)]
#[ApiResource(
    types: ['https://schema.org/users'],
    operations: [
        new Post(
            uriTemplate: "/attachment/{id}/transaction",
            inputFormats: ['multipart' => ['multipart/form-data']],
            controller: UploadTransactionFileController::class,
            openapiContext: [
                "summary" => "Uploader une pièce jointe",
            ],
            deserialize: false
        )
    ],
    normalizationContext: ['groups' => ['transaction:read']],
    denormalizationContext: ['groups' => ['transaction:create','transaction:update']],
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "partial",
    "department.name" => "partial",
    "accountCode" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "name",
    "department.name",
    "accountCode",
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[Vich\Uploadable]
class Transaction
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;

    const TRANSACTION_SUFFIX = "01";
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'transaction:read','folder:read','folder:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'transactions')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?Department $department = null;

    #[ORM\ManyToOne(inversedBy: 'transactions')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?User $responsibleBusiness = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?string $accountCode = null;

    #[ORM\OneToMany(mappedBy: 'transaction', targetEntity: CustomerContact::class,cascade: ['persist','merge'],orphanRemoval: true)]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private Collection $clientContact;


    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?string $comments = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?string $priority = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?\DateTimeInterface $quoteDeadline = null;

    #[ORM\ManyToOne(inversedBy: 'transactions')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update'
    ])]
    private ?FolderTransaction $folderTransaction = null;

    #[ORM\ManyToOne(inversedBy: 'transactions')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?Status $status = null;

    #[Vich\UploadableField(mapping: "attachment_file_transaction", fileNameProperty: "filePath")]
    public ?File $file = null;

    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'transaction:read','transaction:create','transaction:update','folder:read', 'folder:create', 'folder:update',
        'folderTransaction:read', 'folderTransaction:create', 'folderTransaction:update'
    ])]
    private ?\DateTimeInterface $desiredDeliveryDate = null;

    public function __construct()
    {
        $this->clientContact = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): static
    {
        $this->department = $department;

        return $this;
    }

    public function getResponsibleBusiness(): ?User
    {
        return $this->responsibleBusiness;
    }

    public function setResponsibleBusiness(?User $responsibleBusiness): static
    {
        $this->responsibleBusiness = $responsibleBusiness;

        return $this;
    }

    public function getAccountCode(): ?string
    {
        return $this->accountCode;
    }

    public function setAccountCode(?string $accountCode): static
    {
        $this->accountCode = $accountCode;

        return $this;
    }

    /**
     * @return Collection<int, CustomerContact>
     */
    public function getClientContact(): Collection
    {
        return $this->clientContact;
    }

    public function addClientContact(CustomerContact $clientContact): static
    {
        if (!$this->clientContact->contains($clientContact)) {
            $this->clientContact->add($clientContact);
            $clientContact->setTransaction($this);
        }

        return $this;
    }

    public function removeClientContact(CustomerContact $clientContact): static
    {
        if ($this->clientContact->removeElement($clientContact)) {
            // set the owning side to null (unless already changed)
            if ($clientContact->getTransaction() === $this) {
                $clientContact->setTransaction(null);
            }
        }

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): static
    {
        $this->comments = $comments;

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setPriority(?string $priority): static
    {
        $this->priority = $priority;

        return $this;
    }

    public function getQuoteDeadline(): ?\DateTimeInterface
    {
        return $this->quoteDeadline;
    }

    public function setQuoteDeadline(?\DateTimeInterface $quoteDeadline): static
    {
        $this->quoteDeadline = $quoteDeadline;

        return $this;
    }

    public function getFolderTransaction(): ?FolderTransaction
    {
        return $this->folderTransaction;
    }

    public function setFolderTransaction(?FolderTransaction $folderTransaction): static
    {
        $this->folderTransaction = $folderTransaction;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): static
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     * @return Transaction
     */
    public function setFile(?File $file): Transaction
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param string|null $filePath
     * @return Transaction
     */
    public function setFilePath(?string $filePath): Transaction
    {
        $this->filePath = $filePath;
        return $this;
    }

    public function getDesiredDeliveryDate(): ?\DateTimeInterface
    {
        return $this->desiredDeliveryDate;
    }

    public function setDesiredDeliveryDate(?\DateTimeInterface $desiredDeliveryDate): static
    {
        $this->desiredDeliveryDate = $desiredDeliveryDate;

        return $this;
    }
}