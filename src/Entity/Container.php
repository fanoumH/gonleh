<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use App\ApiPlatform\Filter\ContainerFilter;
use App\ApiPlatform\Filter\TerminalGetInFilter;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\ContainerRepository;
use App\Controller\GetinToPdfController;
use App\Controller\GetinToSheetController;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Doctrine\Odm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: ContainerRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des conteneurs en entrée",
            ],
            normalizationContext: ['groups' => 'container:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/terminal_getins',
            openapiContext: [
                "summary" => "Liste des conteneurs en entrée",
                "tags" => ["TerminalGetin"],
            ],
            normalizationContext: ['groups' => 'container:read', 'skip_null_values'=>false],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/terminal_getins/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'container:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: GetinToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/terminal_getins/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'container:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: GetinToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un conteneur en entrée",
            ],
            normalizationContext: ['groups' => 'container:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un conteneur en entrée",
            ],
            normalizationContext: ['groups' => 'container:read'],
            denormalizationContext: ['groups' => 'container:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un conteneur en entrée",
            ],
            normalizationContext: ['groups' => 'container:read'],
            denormalizationContext: ['groups' => 'container:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un conteneur en entrée",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['container:update']]
)]

#[ApiFilter(
    BooleanFilter::class, properties: [
        'reservedStatus',
        'isGetOut',
        'containerStatusAble',
    ]
)]

#[ApiFilter(
    SearchFilter::class, properties: [
        'terminalGetin.terminal' => 'exact',
        'terminalGetin.terminal.id' => 'exact',
        'terminalGetin.terminal.name' => 'ipartial',
        'terminalGetin.shippingCompany.name' => 'exact',
        'terminalGetin.shippingCompany.id' => 'exact',
        'terminalGetin.shippingCompany' => 'exact',
        'booking' => 'exact',
        'booking.id' => 'exact',
        'containerNumber' => 'ipartial',
        // 'reservedStatus' => 'exact',
        // 'isGetOut' => 'exact',
        
    ]
)]

#[ApiFilter(
    OrderFilter::class, properties: [
        'containerNumber',
        'terminalGetin.terminal.name',
        'terminalGetin.BDRnumber',
        'terminalGetin.shippingCompany.name',
        'terminalGetin.shippingCompany.createdAt',
        'terminalGetOut.refGetOut',
        'dateToGetOut',
        'containerStatus',
        'containerStatusAble',
    ],
    arguments: [
        'orderParameterName' => 'order'
    ]
)]

#[ApiFilter(
    DateFilter::class,properties: [
    "createdAt",
    "dateRelease",
]
)]
#[ApiFilter(
    ContainerFilter::class,properties: ['container']
)]
class Container
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const CONTAINER_PREFIX = "CNT";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'container:read','booking:read', 'booking:update', 'terminalGetOut:read','getin:read',
        'transportOrder:read',
        'eir:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'container:read','container:create','container:update','getin:create','booking:read',
        'terminalGetOut:read',
        'getin:read','getin:create',
        'transportOrder:read',
        'eir:read',
    ])]
    private ?string $containerNumber = null;

    #[ORM\Column(nullable: true)]
    // #[Groups(groups: [
    //     'container:read','container:create','container:update','getin:read','getin:create','booking:read',
    //     'terminalGetOut:read',
    // ])]
    private ?int $typeContainer = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'container:read','container:create','container:update','getin:read','getin:create','booking:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'eir:read',
    ])]
    private ?string $containerStatus = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    #[Groups(groups: [
        'container:read','container:create','container:update','getin:read','getin:create','booking:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'eir:read',
    ])]
    private ?bool $containerStatusAble = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    #[Groups(groups: [
        'container:read','container:create','container:update','getin:read','getin:create','booking:read',
        'terminalGetOut:read',
        'transportOrder:read',
    ])]
    private ?bool $reservedStatus = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    #[Groups(groups: [
        'container:read','container:create','container:update','getin:read','getin:create','booking:read',
        'terminalGetOut:read',
        'transportOrder:read',
    ])]
    private ?bool $isGetOut = null;

    #[ORM\ManyToOne(inversedBy: 'containers')]
    #[Groups(groups: [
        'container:read', 'booking:read',
        'transportOrder:read',
        'eir:read',
    ])]
    private ?TerminalGetin $terminalGetin = null;

    #[ORM\ManyToOne(inversedBy: 'containers')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'container:read', 'terminalGetOut:read',
        'transportOrder:read',
    ])]
    private ?Booking $booking = null;

    #[ORM\ManyToOne(inversedBy: 'containers')]
    #[Groups(groups: [
        'container:read',
    ])]
    private ?TerminalGetOut $terminalGetOut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'container:read', 'container:create', 'container:update', 'booking:read', 'booking:create', 'booking:update',
        'transportOrder:read',
    ])]
    private ?\DateTimeInterface $dateToGetOut = null;

    #[ORM\ManyToOne(inversedBy: 'containers')]
    #[Groups(groups: [
        'container:read','container:create','container:update','getin:read','getin:create','booking:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'eir:read',
    ])]
    private ?ContainerType $containerType = null;

    #[ORM\OneToOne(mappedBy: 'container', cascade: ['persist', 'remove'])]
    #[Groups(groups: [
        'container:read','container:create','container:update',
        'getin:read','getin:create',
    ])]
    private ?EIR $eir = null;

    public function __construct()
    {
        // $this->terminalGetOuts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContainerNumber(): ?string
    {
        return $this->containerNumber;
    }

    public function setContainerNumber(?string $containerNumber): static
    {
        $this->containerNumber = $containerNumber;

        return $this;
    }

    public function getTypeContainer(): ?int
    {
        return $this->typeContainer;
    }

    public function setTypeContainer(?int $typeContainer): static
    {
        $this->typeContainer = $typeContainer;

        return $this;
    }

    public function getContainerStatus(): ?string
    {
        return $this->containerStatus;
    }

    public function setContainerStatus(?string $containerStatus): static
    {
        $this->containerStatus = $containerStatus;

        return $this;
    }

    public function isContainerStatusAble(): ?bool
    {
        return $this->containerStatusAble;
    }

    public function setContainerStatusAble(?bool $containerStatusAble): static
    {
        $this->containerStatusAble = $containerStatusAble;

        return $this;
    }

    public function isReservedStatus(): ?bool
    {
        return $this->reservedStatus;
    }

    public function setReservedStatus(?bool $reservedStatus): static
    {
        $this->reservedStatus = $reservedStatus;

        return $this;
    }

    public function isIsGetOut(): ?bool
    {
        return $this->isGetOut;
    }

    public function setIsGetOut(?bool $isGetOut): static
    {
        $this->isGetOut = $isGetOut;

        return $this;
    }

    public function getTerminalGetin(): ?TerminalGetin
    {
        return $this->terminalGetin;
    }

    public function setTerminalGetin(?TerminalGetin $terminalGetin): static
    {
        $this->terminalGetin = $terminalGetin;

        return $this;
    }

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function setBooking(?Booking $booking): static
    {
        $this->booking = $booking;

        return $this;
    }

    public function getTerminalGetOut(): ?TerminalGetOut
    {
        return $this->terminalGetOut;
    }

    public function setTerminalGetOut(?TerminalGetOut $terminalGetOut): static
    {
        $this->terminalGetOut = $terminalGetOut;

        return $this;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public function getDateToGetOut(): ?\DateTimeInterface
    {
        return $this->dateToGetOut;
    }

    public function setDateToGetOut(?\DateTimeInterface $dateToGetOut): static
    {
        $this->dateToGetOut = $dateToGetOut;

        return $this;
    }

    public function getContainerType(): ?ContainerType
    {
        return $this->containerType;
    }

    public function setContainerType(?ContainerType $containerType): static
    {
        $this->containerType = $containerType;

        return $this;
    }

    public function getEir(): ?EIR
    {
        return $this->eir;
    }

    public function setEir(?EIR $eir): static
    {
        // unset the owning side of the relation if necessary
        if ($eir === null && $this->eir !== null) {
            $this->eir->setContainer(null);
        }

        // set the owning side of the relation if necessary
        if ($eir !== null && $eir->getContainer() !== $this) {
            $eir->setContainer($this);
        }

        $this->eir = $eir;

        return $this;
    }
}
