<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\TransporterRepository;
use Doctrine\Common\Collections\Collection;
use App\Controller\TransportersToPdfController;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\TransportersToSheetController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: TransporterRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des transporteurs",
            ],
            normalizationContext: ['groups' => 'transporter:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/transporters/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'transporter:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TransportersToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/transporters/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'transporter:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: TransportersToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un transporteur",
            ],
            normalizationContext: ['groups' => 'transporter:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un transporteur",
            ],
            normalizationContext: ['groups' => 'transporter:read'],
            denormalizationContext: ['groups' => 'transporter:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un transporteur",
            ],
            normalizationContext: ['groups' => 'transporter:read'],
            denormalizationContext: ['groups' => 'transporter:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un transporteur",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ]
)]

#[ApiFilter(
    SearchFilter::class, properties: [
        'id' => 'exact',
        'name' => 'ipartial',

    ]
)]

 #[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class Transporter
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'transporter:read','getin:read','terminalGetOut:read', 'container:read',
        'eir:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Groups(groups: [
        'transporter:read', 'transporter:create', 'transporter:update','getin:read','getin:create','getin:update',
        'terminalGetOut:read', 'container:read',
        'eir:read',
    ])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'carrier', targetEntity: TerminalGetin::class,cascade: ['persist','detach'])]
    private Collection $terminalGetins;

    #[ORM\OneToMany(mappedBy: 'carrier', targetEntity: TerminalGetOut::class)]
    private Collection $terminalGetOuts;

    public function __construct()
    {
        $this->terminalGetins = new ArrayCollection();
        $this->terminalGetOuts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return Collection<int, TerminalGetin>
     */
    public function getTerminalGetins(): Collection
    {
        return $this->terminalGetins;
    }

    public function addTerminalGetin(TerminalGetin $terminalGetin): static
    {
        if (!$this->terminalGetins->contains($terminalGetin)) {
            $this->terminalGetins->add($terminalGetin);
            $terminalGetin->setCarrier($this);
        }

        return $this;
    }

    public function removeTerminalGetin(TerminalGetin $terminalGetin): static
    {
        if ($this->terminalGetins->removeElement($terminalGetin)) {
            // set the owning side to null (unless already changed)
            if ($terminalGetin->getCarrier() === $this) {
                $terminalGetin->setCarrier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TerminalGetOut>
     */
    public function getTerminalGetOuts(): Collection
    {
        return $this->terminalGetOuts;
    }

    public function addTerminalGetOut(TerminalGetOut $terminalGetOut): static
    {
        if (!$this->terminalGetOuts->contains($terminalGetOut)) {
            $this->terminalGetOuts->add($terminalGetOut);
            $terminalGetOut->setCarrier($this);
        }

        return $this;
    }

    public function removeTerminalGetOut(TerminalGetOut $terminalGetOut): static
    {
        if ($this->terminalGetOuts->removeElement($terminalGetOut)) {
            // set the owning side to null (unless already changed)
            if ($terminalGetOut->getCarrier() === $this) {
                $terminalGetOut->setCarrier(null);
            }
        }

        return $this;
    }
}
