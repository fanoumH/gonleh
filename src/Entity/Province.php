<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\ProvinceRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
#[ORM\Entity(repositoryClass: ProvinceRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des province",
            ],
            normalizationContext: ['groups' => 'province:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un province",
            ],
            normalizationContext: ['groups' => 'province:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un province",
            ],
            normalizationContext: ['groups' => 'province:read'],
            denormalizationContext: ['groups' => 'province:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un province",
            ],
            normalizationContext: ['groups' => 'province:read'],
            denormalizationContext: ['groups' => 'province:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un province",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
)]

#[ApiFilter(
    SearchFilter::class, properties: [
    "label" => 'ipartial',
    "id" => 'exact',
    ]
)]
class Province
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','province:read','province:create','province:update'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','province:read','province:create','province:update'
    ])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'province', targetEntity: CustomerIdentification::class)]
    private Collection $customerIdentifications;

    public function __construct()
    {
        $this->customerIdentifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, CustomerIdentification>
     */
    public function getCustomerIdentifications(): Collection
    {
        return $this->customerIdentifications;
    }

    public function addCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if (!$this->customerIdentifications->contains($customerIdentification)) {
            $this->customerIdentifications->add($customerIdentification);
            $customerIdentification->setProvince($this);
        }

        return $this;
    }

    public function removeCustomerIdentification(CustomerIdentification $customerIdentification): static
    {
        if ($this->customerIdentifications->removeElement($customerIdentification)) {
            // set the owning side to null (unless already changed)
            if ($customerIdentification->getProvince() === $this) {
                $customerIdentification->setProvince(null);
            }
        }

        return $this;
    }
}
