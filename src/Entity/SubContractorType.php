<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use App\Repository\SubContractorTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\SubContTypesToPdfController;
use App\Controller\SubContTypesToSheetController;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: SubContractorTypeRepository::class)]

#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des types de sous-traitants",
            ],
            normalizationContext: ['groups' => 'subcontractor-type:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new GetCollection(
            uriTemplate: '/sub_contractor_types/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'subcontractor-type:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: SubContTypesToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/sub_contractor_types/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'subcontractor-type:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: SubContTypesToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un type de sous-traitant",
            ],
            normalizationContext: ['groups' => 'subcontractor-type:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un type de sous-traitant",
            ],
            normalizationContext: ['groups' => 'subcontractor-type:read'],
            denormalizationContext: ['groups' => 'subcontractor-type:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un type de sous-traitant",
            ],
            normalizationContext: ['groups' => 'subcontractor-type:read'],
            denormalizationContext: ['groups' => 'subcontractor-type:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un type de sous-traitant",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]


#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "ipartial",
    "code" => "iexact",
    "id" => "exact"
]
)]

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
class SubContractorType
{
        /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'subcontractor-type:read',
        'subcontractor:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'subcontractor-type:read',
        'subcontractor-type:create',
        'subcontractor-type:update',
        'subcontractor:read',
    ])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'subcontractor-type:read',
        'subcontractor-type:create',
        'subcontractor-type:update',
        'subcontractor:read',
    ])]
    private ?string $code = null;

    #[ORM\OneToMany(mappedBy: 'subContractorType', targetEntity: SubContractor::class)]
    private Collection $subContractors;

    public function __construct()
    {
        $this->subContractors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection<int, SubContractor>
     */
    public function getSubContractors(): Collection
    {
        return $this->subContractors;
    }

    public function addSubContractor(SubContractor $subContractor): static
    {
        if (!$this->subContractors->contains($subContractor)) {
            $this->subContractors->add($subContractor);
            $subContractor->setSubContractorType($this);
        }

        return $this;
    }

    public function removeSubContractor(SubContractor $subContractor): static
    {
        if ($this->subContractors->removeElement($subContractor)) {
            // set the owning side to null (unless already changed)
            if ($subContractor->getSubContractorType() === $this) {
                $subContractor->setSubContractorType(null);
            }
        }

        return $this;
    }
}
