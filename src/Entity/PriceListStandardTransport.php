<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use App\Repository\PriceListStandardTransportRepository;

#[ORM\Entity(repositoryClass: PriceListStandardTransportRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des baremes transport standard",
            ],
            normalizationContext: ['groups' => 'PriceListStandardTransport:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un bareme transport standard",
            ],
            normalizationContext: ['groups' => 'PriceListStandardTransport:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un bareme transport standard",
            ],
            normalizationContext: ['groups' => 'PriceListStandardTransport:read'],
            denormalizationContext: ['groups' => 'PriceListStandardTransport:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un bareme transport standard",
            ],
            normalizationContext: ['groups' => 'PriceListStandardTransport:read'],
            denormalizationContext: ['groups' => 'PriceListStandardTransport:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un bareme transport standard",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "serviceRate.id" => "exact",
    "serviceRate.name" => "partial",
    "operationTypeRate.id" => "exact",
    "operationTypeRate.name" => "partial",
]
)]
#[UniqueEntity(['departureCity','arrivalCity','operationTypeRate','serviceRate','containerType','maxWeight'], message: 'ville départ, ville arrivée, type opération,service,type de conteneur,poids maximal sont dupliqué', ignoreNull: false)]
class PriceListStandardTransport
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields.
     */
    use TimestampableEntity;

    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field.
     */
    use SoftDeleteableEntity;

    /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'specificRateTransport:read',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?int $id = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // #[Groups(groups: [
    //     'PriceListStandardTransport:read',
    //     'PriceListStandardTransport:create',
    //     'PriceListStandardTransport:update',
    // ])]
    // private ?string $departureCity = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // #[Groups(groups: [
    //     'PriceListStandardTransport:read',
    //     'PriceListStandardTransport:create',
    //     'PriceListStandardTransport:update',
    // ])]
    // private ?string $arrivalCity = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?\DateTimeInterface $newPriceDate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?float $priceHT = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?float $maxWeight = null;

    // #[ORM\Column(length: 255, nullable: true)]
    // #[Groups(groups: [
    //     'PriceListStandardTransport:read',
    //     'PriceListStandardTransport:create',
    //     'PriceListStandardTransport:update',
    // ])]
    // private ?string $typeTC = null;

    #[ORM\ManyToOne(inversedBy: 'priceListStandardTransportDepartureCities')]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?Place $departureCity = null;

    #[ORM\ManyToOne(inversedBy: 'priceListStandardTransportArrivalCities')]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?Place $arrivalCity = null;

    #[ORM\ManyToOne(inversedBy: 'priceListStandardTransports')]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?ContainerType $typeTC = null;

    #[ORM\ManyToOne(inversedBy: 'priceListStandardTransports')]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?OperationTypeRate $operationTypeRate = null;

    #[ORM\ManyToOne(inversedBy: 'priceListStandardTransports')]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?Terminal $terminal = null;

    #[ORM\ManyToOne(inversedBy: 'priceListStandardTransports')]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?ServiceRate $serviceRate = null;

    #[ORM\ManyToOne(inversedBy: 'priceListStandardTransports')]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?Currency $currency = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?float $generalPriceHT = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
    ])]
    private ?float $priceTTC = null;

    #[ORM\OneToMany(mappedBy: 'priceStandardTransport', targetEntity: SpecificRatetransport::class)]
    private Collection $specificRatetransports;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $reference = null;

    public function __construct()
    {
        $this->specificRatetransports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    // public function getDepartureCity(): ?string
    // {
    //     return $this->departureCity;
    // }

    // public function setDepartureCity(?string $departureCity): static
    // {
    //     $this->departureCity = $departureCity;

    //     return $this;
    // }

    // public function getArrivalCity(): ?string
    // {
    //     return $this->arrivalCity;
    // }

    // public function setArrivalCity(?string $arrivalCity): static
    // {
    //     $this->arrivalCity = $arrivalCity;

    //     return $this;
    // }

    public function getNewPriceDate(): ?\DateTimeInterface
    {
        return $this->newPriceDate;
    }

    public function setNewPriceDate(?\DateTimeInterface $newPriceDate): static
    {
        $this->newPriceDate = $newPriceDate;

        return $this;
    }

    public function getPriceHT(): ?float
    {
        return $this->priceHT;
    }

    public function setPriceHT(?float $priceHT): static
    {
        $this->priceHT = $priceHT;

        return $this;
    }

    public function getMaxWeight(): ?float
    {
        return $this->maxWeight;
    }

    public function setMaxWeight(?float $maxWeight): static
    {
        $this->maxWeight = $maxWeight;

        return $this;
    }

    // public function getTypeTC(): ?string
    // {
    //     return $this->typeTC;
    // }

    // public function setTypeTC(?string $typeTC): static
    // {
    //     $this->typeTC = $typeTC;

    //     return $this;
    // }

    public function getDepartureCity(): ?Place
    {
        return $this->departureCity;
    }

    public function setDepartureCity(?Place $departureCity): static
    {
        $this->departureCity = $departureCity;

        return $this;
    }

    public function getArrivalCity(): ?Place
    {
        return $this->arrivalCity;
    }

    public function setArrivalCity(?Place $arrivalCity): static
    {
        $this->arrivalCity = $arrivalCity;

        return $this;
    }

    public function getTypeTC(): ?ContainerType
    {
        return $this->typeTC;
    }

    public function setTypeTC(?ContainerType $typeTC): static
    {
        $this->typeTC = $typeTC;

        return $this;
    }

    public function getOperationTypeRate(): ?OperationTypeRate
    {
        return $this->operationTypeRate;
    }

    public function setOperationTypeRate(?OperationTypeRate $operationTypeRate): static
    {
        $this->operationTypeRate = $operationTypeRate;

        return $this;
    }

    public function getTerminal(): ?Terminal
    {
        return $this->terminal;
    }

    public function setTerminal(?Terminal $terminal): static
    {
        $this->terminal = $terminal;

        return $this;
    }

    public function getServiceRate(): ?ServiceRate
    {
        return $this->serviceRate;
    }

    public function setServiceRate(?ServiceRate $serviceRate): static
    {
        $this->serviceRate = $serviceRate;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getGeneralPriceHT(): ?float
    {
        return $this->generalPriceHT;
    }

    public function setGeneralPriceHT(?float $generalPriceHT): static
    {
        $this->generalPriceHT = $generalPriceHT;

        return $this;
    }

    public function getPriceTTC(): ?float
    {
        return $this->priceTTC;
    }

    public function setPriceTTC(?float $priceTTC): static
    {
        $this->priceTTC = $priceTTC;

        return $this;
    }

    /**
     * @return Collection<int, SpecificRatetransport>
     */
    public function getSpecificRatetransports(): Collection
    {
        return $this->specificRatetransports;
    }

    public function addSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if (!$this->specificRatetransports->contains($specificRatetransport)) {
            $this->specificRatetransports->add($specificRatetransport);
            $specificRatetransport->setPriceStandardTransport($this);
        }

        return $this;
    }

    public function removeSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if ($this->specificRatetransports->removeElement($specificRatetransport)) {
            // set the owning side to null (unless already changed)
            if ($specificRatetransport->getPriceStandardTransport() === $this) {
                $specificRatetransport->setPriceStandardTransport(null);
            }
        }

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }
}
