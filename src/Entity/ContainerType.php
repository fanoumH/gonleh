<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\ContainerTypesToPdfController;
use App\Controller\ContainerTypesToSheetController;
use App\Controller\GetProfileUserController;
use App\Controller\LogoutController;
use App\Entity\Trait\HistoryTrait;
use App\Repository\ContainerTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: ContainerTypeRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des types de conteneurs",
            ],
            normalizationContext: ['groups' => 'containerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: '/container_types/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'containerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: ContainerTypesToPdfController::class,
            order: ["id" => "DESC"]
        ),
        new GetCollection(
            uriTemplate: '/container_types/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'containerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: ContainerTypesToSheetController::class,
            order: ["id" => "DESC"]
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un type de conteneur",
            ],
            normalizationContext: ['groups' => 'containerType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un type de conteneur",
            ],
            normalizationContext: ['groups' => 'containerType:read'],
            denormalizationContext: ['groups' => 'containerType:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un type de conteneur",
            ],
            normalizationContext: ['groups' => 'containerType:read'],
            denormalizationContext: ['groups' => 'containerType:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un type de conteneur",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['containerType:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "unite" => "ipartial",
    "label" => "ipartial",
    "ShippingCompany" => 'exact',
    "ShippingCompany.id" => 'exact',
    "ShippingCompany.name" => 'exact',
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "unite",
    "ShippingCompany",
    "ShippingCompany.id",
    "ShippingCompany.name",
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class ContainerType
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'containerType:read',
        'container:read',
        'booking:read',
        'getin:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'eir:read',
        'PriceListStandardTransport:read',
        'specificRate:read','basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRateTransport:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'containerType:read','containerType:create','containerType:update',
        'container:read',
        'booking:read',
        'getin:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'eir:read',
        'PriceListStandardTransport:read',
        'specificRate:read','basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?int $unite = null;

    #[ORM\ManyToOne(inversedBy: 'containerTypes')]
    #[Groups(groups: [
        'containerType:read','containerType:create','containerType:update',
        'container:read',
        'booking:read',
        'getin:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'eir:read',
        'PriceListStandardTransport:read',
        'specificRate:read',
    ])]
    private ?ShippingCompany $ShippingCompany = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'containerType:read','containerType:create','containerType:update',
        'container:read',
        'booking:read',
        'getin:read',
        'terminalGetOut:read',
        'transportOrder:read',
        'eir:read',
        'PriceListStandardTransport:read',
        'specificRate:read','basicRate:read',
        'basicRate:create',
        'basicRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
    ])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'containerType', targetEntity: Container::class)]
    private Collection $containers;

    #[ORM\OneToMany(mappedBy: 'typeTC', targetEntity: PriceListStandardTransport::class)]
    private Collection $priceListStandardTransports;

    #[ORM\OneToMany(mappedBy: 'containerType', targetEntity: SpecificRate::class)]
    private Collection $specificRates;

    #[ORM\OneToMany(mappedBy: 'containerType', targetEntity: BasicRate::class)]
    private Collection $basicRates;

    public function __construct()
    {
        $this->containers = new ArrayCollection();
        $this->priceListStandardTransports = new ArrayCollection();
        $this->specificRates = new ArrayCollection();
        $this->basicRates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUnite(): ?int
    {
        return $this->unite;
    }

    public function setUnite(?int $unite): static
    {
        $this->unite = $unite;

        return $this;
    }

    public function getShippingCompany(): ?ShippingCompany
    {
        return $this->ShippingCompany;
    }

    public function setShippingCompany(?ShippingCompany $ShippingCompany): static
    {
        $this->ShippingCompany = $ShippingCompany;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, Container>
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }

    public function addContainer(Container $container): static
    {
        if (!$this->containers->contains($container)) {
            $this->containers->add($container);
            $container->setContainerType($this);
        }

        return $this;
    }

    public function removeContainer(Container $container): static
    {
        if ($this->containers->removeElement($container)) {
            // set the owning side to null (unless already changed)
            if ($container->getContainerType() === $this) {
                $container->setContainerType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PriceListStandardTransport>
     */
    public function getPriceListStandardTransports(): Collection
    {
        return $this->priceListStandardTransports;
    }

    public function addPriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if (!$this->priceListStandardTransports->contains($priceListStandardTransport)) {
            $this->priceListStandardTransports->add($priceListStandardTransport);
            $priceListStandardTransport->setTypeTC($this);
        }

        return $this;
    }

    public function removePriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if ($this->priceListStandardTransports->removeElement($priceListStandardTransport)) {
            // set the owning side to null (unless already changed)
            if ($priceListStandardTransport->getTypeTC() === $this) {
                $priceListStandardTransport->setTypeTC(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRate>
     */
    public function getSpecificRates(): Collection
    {
        return $this->specificRates;
    }

    public function addSpecificRate(SpecificRate $specificRate): static
    {
        if (!$this->specificRates->contains($specificRate)) {
            $this->specificRates->add($specificRate);
            $specificRate->setContainerType($this);
        }

        return $this;
    }

    public function removeSpecificRate(SpecificRate $specificRate): static
    {
        if ($this->specificRates->removeElement($specificRate)) {
            // set the owning side to null (unless already changed)
            if ($specificRate->getContainerType() === $this) {
                $specificRate->setContainerType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, BasicRate>
     */
    public function getBasicRates(): Collection
    {
        return $this->basicRates;
    }

    public function addBasicRate(BasicRate $basicRate): static
    {
        if (!$this->basicRates->contains($basicRate)) {
            $this->basicRates->add($basicRate);
            $basicRate->setContainerType($this);
        }

        return $this;
    }

    public function removeBasicRate(BasicRate $basicRate): static
    {
        if ($this->basicRates->removeElement($basicRate)) {
            // set the owning side to null (unless already changed)
            if ($basicRate->getContainerType() === $this) {
                $basicRate->setContainerType(null);
            }
        }

        return $this;
    }
}
