<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CurrencyRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: CurrencyRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des currency",
            ],
            normalizationContext: ['groups' => 'currency:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un currency",
            ],
            normalizationContext: ['groups' => 'currency:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un currency",
            ],
            normalizationContext: ['groups' => 'currency:read'],
            denormalizationContext: ['groups' => 'currency:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un currency",
            ],
            normalizationContext: ['groups' => 'currency:read'],
            denormalizationContext: ['groups' => 'currency:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un currency",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[ApiFilter(
    SearchFilter::class,properties: [
        "id" => "exact",
        "reference" => "partial",
    ]
)]
class Currency
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'customer:read','currency:read','currency:create','currency:update','basicRate:read','basicRate:create','basicRate:update',
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'customer:read','customer:create','customer:update','currency:read','currency:create','currency:update','basicRate:read','basicRate:create','basicRate:update',
        'PriceListStandardTransport:read',
        'PriceListStandardTransport:create',
        'PriceListStandardTransport:update',
        'specificRate:read',
        'specificRate:create',
        'specificRate:update',
        'specificRateTransport:read',
        'specificRateTransport:create',
        'specificRateTransport:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?string $reference = null;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: CustomerBank::class,cascade: ['persist','detach'])]
    private Collection $customerBanks;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: BasicRate::class)]
    private Collection $basicRates;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: PriceListStandardTransport::class)]
    private Collection $priceListStandardTransports;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: SpecificRate::class)]
    private Collection $specificRates;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: SpecificRatetransport::class)]
    private Collection $specificRatetransports;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: Proforma::class)]
    private Collection $proformas;

    public function __construct()
    {
        $this->customerBanks = new ArrayCollection();
        $this->basicRates = new ArrayCollection();
        $this->priceListStandardTransports = new ArrayCollection();
        $this->specificRates = new ArrayCollection();
        $this->specificRatetransports = new ArrayCollection();
        $this->proformas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Collection<int, CustomerBank>
     */
    public function getCustomerBanks(): Collection
    {
        return $this->customerBanks;
    }

    public function addCustomerBank(CustomerBank $customerBank): static
    {
        if (!$this->customerBanks->contains($customerBank)) {
            $this->customerBanks->add($customerBank);
            $customerBank->setCurrency($this);
        }

        return $this;
    }

    public function removeCustomerBank(CustomerBank $customerBank): static
    {
        if ($this->customerBanks->removeElement($customerBank)) {
            // set the owning side to null (unless already changed)
            if ($customerBank->getCurrency() === $this) {
                $customerBank->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, BasicRate>
     */
    public function getBasicRates(): Collection
    {
        return $this->basicRates;
    }

    public function addBasicRate(BasicRate $basicRate): static
    {
        if (!$this->basicRates->contains($basicRate)) {
            $this->basicRates->add($basicRate);
            $basicRate->setCurrency($this);
        }

        return $this;
    }

    public function removeBasicRate(BasicRate $basicRate): static
    {
        if ($this->basicRates->removeElement($basicRate)) {
            // set the owning side to null (unless already changed)
            if ($basicRate->getCurrency() === $this) {
                $basicRate->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PriceListStandardTransport>
     */
    public function getPriceListStandardTransports(): Collection
    {
        return $this->priceListStandardTransports;
    }

    public function addPriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if (!$this->priceListStandardTransports->contains($priceListStandardTransport)) {
            $this->priceListStandardTransports->add($priceListStandardTransport);
            $priceListStandardTransport->setCurrency($this);
        }

        return $this;
    }

    public function removePriceListStandardTransport(PriceListStandardTransport $priceListStandardTransport): static
    {
        if ($this->priceListStandardTransports->removeElement($priceListStandardTransport)) {
            // set the owning side to null (unless already changed)
            if ($priceListStandardTransport->getCurrency() === $this) {
                $priceListStandardTransport->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRate>
     */
    public function getSpecificRates(): Collection
    {
        return $this->specificRates;
    }

    public function addSpecificRate(SpecificRate $specificRate): static
    {
        if (!$this->specificRates->contains($specificRate)) {
            $this->specificRates->add($specificRate);
            $specificRate->setCurrency($this);
        }

        return $this;
    }

    public function removeSpecificRate(SpecificRate $specificRate): static
    {
        if ($this->specificRates->removeElement($specificRate)) {
            // set the owning side to null (unless already changed)
            if ($specificRate->getCurrency() === $this) {
                $specificRate->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SpecificRatetransport>
     */
    public function getSpecificRatetransports(): Collection
    {
        return $this->specificRatetransports;
    }

    public function addSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if (!$this->specificRatetransports->contains($specificRatetransport)) {
            $this->specificRatetransports->add($specificRatetransport);
            $specificRatetransport->setCurrency($this);
        }

        return $this;
    }

    public function removeSpecificRatetransport(SpecificRatetransport $specificRatetransport): static
    {
        if ($this->specificRatetransports->removeElement($specificRatetransport)) {
            // set the owning side to null (unless already changed)
            if ($specificRatetransport->getCurrency() === $this) {
                $specificRatetransport->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Proforma>
     */
    public function getProformas(): Collection
    {
        return $this->proformas;
    }

    public function addProforma(Proforma $proforma): static
    {
        if (!$this->proformas->contains($proforma)) {
            $this->proformas->add($proforma);
            $proforma->setCurrency($this);
        }

        return $this;
    }

    public function removeProforma(Proforma $proforma): static
    {
        if ($this->proformas->removeElement($proforma)) {
            // set the owning side to null (unless already changed)
            if ($proforma->getCurrency() === $this) {
                $proforma->setCurrency(null);
            }
        }

        return $this;
    }
}
