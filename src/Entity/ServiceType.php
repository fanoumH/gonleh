<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\ServiceTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
#[ORM\Entity(repositoryClass: ServiceTypeRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des types de services",
            ],
            normalizationContext: ['groups' => 'serviceType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un type de service",
            ],
            normalizationContext: ['groups' => 'serviceType:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un type de service",
            ],
            normalizationContext: ['groups' => 'serviceType:read'],
            denormalizationContext: ['groups' => 'serviceType:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un type de service",
            ],
            normalizationContext: ['groups' => 'serviceType:read'],
            denormalizationContext: ['groups' => 'serviceType:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un type de service",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['serviceType:update']]
)]
#[ApiFilter(
    SearchFilter::class,properties: [
    "name" => "partial",
]
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "name"
],arguments:  ['orderParameterName' => 'order'])
]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
class ServiceType
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'serviceType:read'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'serviceType:read','serviceType:create','serviceType:update'
    ])]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'serviceType:read','serviceType:create','serviceType:update'
    ])]
    private ?float $servicePrice = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'serviceType:read','serviceType:create','serviceType:update'
    ])]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'serviceType:read','serviceType:create','serviceType:update'
    ])]
    private ?string $code = null;

    #[ORM\ManyToOne(inversedBy: 'serviceTypes')]
    #[Groups(groups: [
        'serviceType:read','serviceType:create','serviceType:update'
    ])]
    private ?Department $department = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getServicePrice(): ?float
    {
        return $this->servicePrice;
    }

    public function setServicePrice(?float $servicePrice): static
    {
        $this->servicePrice = $servicePrice;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): static
    {
        $this->department = $department;

        return $this;
    }
}
