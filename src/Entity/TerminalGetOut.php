<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use App\ApiPlatform\Filter\TerminalGetInFilter;
use App\ApiPlatform\Filter\TerminalGetOutFilter;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Controller\BlGetOutController;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\GetoutsToPdfController;
use Doctrine\Common\Collections\Collection;
use App\Controller\GetoutsToSheetController;
use App\Repository\TerminalGetOutRepository;
use App\Controller\ExportAllGetOutController;
use App\Controller\GetOutCINUploadController;
use App\Controller\PrintSingleGetOutController;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[ORM\Entity(repositoryClass: TerminalGetOutRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des getouts",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            order: ["id" => "DESC"],
        ),
        new GetCollection(
            uriTemplate: '/terminal_get_outs/all/pdf',
            openapiContext: [
                "summary" => "Export to pdf",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: GetoutsToPdfController::class,
        ),
        new GetCollection(
            uriTemplate: '/terminal_get_outs/all/sheet',
            openapiContext: [
                "summary" => "Export to sheet",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            controller: GetoutsToSheetController::class,
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un getout",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un getout",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            denormalizationContext: ['groups' => 'terminalGetOut:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Post(
            types: ['https://schema.org/terminal_get_outs'],
            uriTemplate: "/terminal_get_outs/{id}/cin-attachment",
            openapiContext: [
                "summary" => "Upload cin driver attachment",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            denormalizationContext: ['groups' => 'terminalGetOut:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
            inputFormats: ['multipart' => ['multipart/form-data']],
            deserialize: false,
            controller: GetOutCINUploadController::class,
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un getout",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            denormalizationContext: ['groups' => 'terminalGetOut:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un getout",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new GetCollection(
            uriTemplate: "/exportAllGetouts",
            defaults: [
                '_api_persist' => false,
            ],
            controller: ExportAllGetOutController::class,
            openapiContext: [
                "summary" => "Export multiple de GETOUT",
            ],
            normalizationContext: ['groups' => 'terminalGetOut:read'],
            read: false,
        ),
        new Get(
            uriTemplate: "/getout/{id}/print",
            controller: PrintSingleGetOutController::class,
            openapiContext: [
                "summary" => "Impression en pdf besoin",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Get(
            uriTemplate: "/terminal_get_outs/{id}/bl",
            controller: BlGetOutController::class,
            openapiContext: [
                "summary" => "Impression en pdf BL",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    validationContext: ['groups' => ['terminalGetOut:update']]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "refGetOut" => "ipartial",
    "id" => "exact",
    ]
)]
#[ApiFilter(
    TerminalGetOutFilter::class,properties: ['terminalGetout']
)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt',timeAware: false)]
#[Vich\Uploadable]
class TerminalGetOut
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const GETOUT_PREFIX = "GETOUT";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'terminalGetOut:read', 'container:read',
        'transportOrder:read',
        'specificRate:read',
    ])]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'terminalGetOut', targetEntity: Container::class)]
    #[Groups(groups: [
        'terminalGetOut:read', 'terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
    ])]
    private Collection $containers;

    #[ORM\ManyToOne(inversedBy: 'terminalGetOuts')]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
    ])]
    private ?Transporter $carrier = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
        'specificRate:read',
    ])]
    private ?string $truckNumber = null;

    #[ORM\ManyToOne(inversedBy: 'terminalGetOuts')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
    ])]
    private ?Customer $customer = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
    ])]
    private ?string $driverLicenseNumber = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
    ])]
    private ?string $nationalIDdriver = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
    ])]
    private ?bool $isDeliveryNoteSend = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update',
        'transportOrder:read',
    ])]
    private ?bool $isInvoiceSend = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'terminalGetOut:read','terminalGetOut:create','terminalGetOut:update', 'container:read',
        'transportOrder:read',
        'specificRate:read',
    ])]
    private ?string $refGetOut = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cinAttachment = null;

    #[Vich\UploadableField(mapping: "cin_attachment_file", fileNameProperty: "cinAttachment")]
    private ?File $cinAttachmentFile = null;

    // #[ORM\OneToMany(mappedBy: 'getOut', targetEntity: TransportOrder::class)]
    // private Collection $transportOrders;

    public function __construct()
    {
        $this->containers = new ArrayCollection();
        // $this->transportOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Container>
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }

    public function addContainer(Container $container): static
    {
        if (!$this->containers->contains($container)) {
            $this->containers->add($container);
            $container->setTerminalGetOut($this);
        }

        return $this;
    }

    public function removeContainer(Container $container): static
    {
        if ($this->containers->removeElement($container)) {
            // set the owning side to null (unless already changed)
            if ($container->getTerminalGetOut() === $this) {
                $container->setTerminalGetOut(null);
            }
        }

        return $this;
    }

    public function getCarrier(): ?Transporter
    {
        return $this->carrier;
    }

    public function setCarrier(?Transporter $carrier): static
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getTruckNumber(): ?string
    {
        return $this->truckNumber;
    }

    public function setTruckNumber(string $truckNumber): static
    {
        $this->truckNumber = $truckNumber;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    public function getDriverLicenseNumber(): ?string
    {
        return $this->driverLicenseNumber;
    }

    public function setDriverLicenseNumber(string $driverLicenseNumber): static
    {
        $this->driverLicenseNumber = $driverLicenseNumber;

        return $this;
    }

    public function getNationalIDdriver(): ?string
    {
        return $this->nationalIDdriver;
    }

    public function setNationalIDdriver(string $nationalIDdriver): static
    {
        $this->nationalIDdriver = $nationalIDdriver;

        return $this;
    }

    public function isIsDeliveryNoteSend(): ?bool
    {
        return $this->isDeliveryNoteSend;
    }

    public function setIsDeliveryNoteSend(?bool $isDeliveryNoteSend): static
    {
        $this->isDeliveryNoteSend = $isDeliveryNoteSend;

        return $this;
    }

    public function isIsInvoiceSend(): ?bool
    {
        return $this->isInvoiceSend;
    }

    public function setIsInvoiceSend(?bool $isInvoiceSend): static
    {
        $this->isInvoiceSend = $isInvoiceSend;

        return $this;
    }

    public function toArray(){
        return get_object_vars($this);
    }

    public function getRefGetOut(): ?string
    {
        return $this->refGetOut;
    }

    public function setRefGetOut(?string $refGetOut): static
    {
        $this->refGetOut = $refGetOut;

        return $this;
    }


    // /**
    //  * @return Collection<int, TransportOrder>
    //  */
    // public function getTransportOrders(): Collection
    // {
    //     return $this->transportOrders;
    // }

    // public function addTransportOrder(TransportOrder $transportOrder): static
    // {
    //     if (!$this->transportOrders->contains($transportOrder)) {
    //         $this->transportOrders->add($transportOrder);
    //         $transportOrder->setGetOut($this);
    //     }

    //     return $this;
    // }

    // public function removeTransportOrder(TransportOrder $transportOrder): static
    // {
    //     if ($this->transportOrders->removeElement($transportOrder)) {
    //         // set the owning side to null (unless already changed)
    //         if ($transportOrder->getGetOut() === $this) {
    //             $transportOrder->setGetOut(null);
    //         }
    //     }

    //     return $this;
    // }

    public function getCinAttachment(): ?string
    {
        return $this->cinAttachment;
    }

    public function setCinAttachment(?string $cinAttachment): static
    {
        $this->cinAttachment = $cinAttachment;

        return $this;
    }

    public function getCinAttachmentFile(): ?File
    {
        return $this->cinAttachmentFile;
    }

    public function setCinAttachmentFile(?File $cinAttachmentFile): static
    {
        $this->cinAttachmentFile = $cinAttachmentFile;

        return $this;
    }
}
