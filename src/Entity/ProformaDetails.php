<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Trait\HistoryTrait;
use App\Repository\ProformaDetailsRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProformaDetailsRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des détails proforma",
            ],
            normalizationContext: ['groups' => 'proformaDetails:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un détail proforma",
            ],
            normalizationContext: ['groups' => 'proformaDetails:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un détail proforma",
            ],
            normalizationContext: ['groups' => 'proformaDetails:read'],
            denormalizationContext: ['groups' => 'proformaDetails:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un détail proforma",
            ],
            normalizationContext: ['groups' => 'proformaDetails:read'],
            denormalizationContext: ['groups' => 'proformaDetails:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un détail proforma",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]
class ProformaDetails
{
    use TimestampableEntity;
    use HistoryTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'proformaDetails')]
    #[Groups(groups: [
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?OperationTypeRate $operationTypeRate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?float $quantity = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?float $rising = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update'
    ])]
    private ?array $tarif = null;

    #[ORM\ManyToOne(cascade: ["persist"], inversedBy: 'proformaDetails')]
    private ?Proforma $proforma = null;

    #[ORM\ManyToOne(inversedBy: 'proformaDetails')]
    #[Groups(groups: [
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update',
    ])]
    private ?ServiceRate $serviceRate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(groups: [
        'proformaDetails:read','proformaDetails:create','proformaDetails:update',
        'proforma:read','proforma:create','proforma:update',
    ])]
    private ?float $discount = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOperationTypeRate(): ?OperationTypeRate
    {
        return $this->operationTypeRate;
    }

    public function setOperationTypeRate(?OperationTypeRate $operationTypeRate): static
    {
        $this->operationTypeRate = $operationTypeRate;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(?float $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getRising(): ?float
    {
        return $this->rising;
    }

    public function setRising(?float $rising): static
    {
        $this->rising = $rising;

        return $this;
    }

    public function getTarif(): ?array
    {
        return $this->tarif;
    }

    public function setTarif(?array $tarif): static
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getProforma(): ?Proforma
    {
        return $this->proforma;
    }

    public function setProforma(?Proforma $proforma): static
    {
        $this->proforma = $proforma;

        return $this;
    }

    public function getServiceRate(): ?ServiceRate
    {
        return $this->serviceRate;
    }

    public function setServiceRate(?ServiceRate $serviceRate): static
    {
        $this->serviceRate = $serviceRate;

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): static
    {
        $this->discount = $discount;

        return $this;
    }
}
