<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Trait\HistoryTrait;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\HistoryTransportOrderRepository;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: HistoryTransportOrderRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des historique ordres de transport",
            ],
            normalizationContext: ['groups' => 'histoTransportOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un historique ordre de transport",
            ],
            normalizationContext: ['groups' => 'histoTransportOrder:read'],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un historique ordre de transport",
            ],
            normalizationContext: ['groups' => 'histoTransportOrder:read'],
            denormalizationContext: ['groups' => 'histoTransportOrder:create'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un historique ordre de transport",
            ],
            normalizationContext: ['groups' => 'histoTransportOrder:read'],
            denormalizationContext: ['groups' => 'histoTransportOrder:update'],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un historique ordre de transport",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)"
        )
    ]
)]

#[ApiFilter(
    SearchFilter::class,properties: [
    "id" => "exact",
    "transportOrder" => "exact",
    "transportOrder.id" => "exact",
]
)]
class HistoryTransportOrder
{
     /**
     * history trait
     */
    use HistoryTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: [
        'histoTransportOrder:read',
        'histoTransportOrder:create',
        'histoTransportOrder:update',
        'transportOrder:read',
    ])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'historyTransportOrders')]
    #[Groups(groups: [
        'histoTransportOrder:read',
        'histoTransportOrder:create',
        'histoTransportOrder:update',
    ])]
    private ?TransportOrder $transportOrder = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'histoTransportOrder:read',
        'histoTransportOrder:create',
        'histoTransportOrder:update',
        'transportOrder:read',
    ])]
    private ?\DateTimeInterface $dateChange = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'histoTransportOrder:read',
        'histoTransportOrder:create',
        'histoTransportOrder:update',
        'transportOrder:read',
    ])]
    private ?string $otStatus = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTransportOrder(): ?TransportOrder
    {
        return $this->transportOrder;
    }

    public function setTransportOrder(?TransportOrder $transportOrder): static
    {
        $this->transportOrder = $transportOrder;

        return $this;
    }

    public function getDateChange(): ?\DateTimeInterface
    {
        return $this->dateChange;
    }

    public function setDateChange(?\DateTimeInterface $dateChange): static
    {
        $this->dateChange = $dateChange;

        return $this;
    }

    public function getOtStatus(): ?string
    {
        return $this->otStatus;
    }

    public function setOtStatus(?string $otStatus): static
    {
        $this->otStatus = $otStatus;

        return $this;
    }
}
